<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('login/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('login/{provider}/callback', 'Auth\LoginController@handleProviderCallback');

Route::middleware(['iflychatmiddleware','web'])->group(function(){
    Route::post('/contact', [
        'uses' => 'ContactController@contact',
        'as' => 'contact'
    ]);
    Route::get('/campaign-tasks-example',[
        'uses' => 'PagesController@campaign_tasks_example',
        'as' => 'campaign.tasks.example'
    ]);

    Route::get('/crowdfunding-resources',[
        'uses' => 'ResourcesController@index',
        'as' => 'crowdfunding.resources'
    ]);

    Route::get('/crowdfunding-project-products/{id}',[
        'uses' => 'ProjectsController@products',
        'as' => 'project.products'
    ]);

    Route::get('/resource-show/{id}/{slug}',[
        'uses' => 'ResourcesController@show',
        'as' => 'resource.show'
    ]);

    Route::get('/',[
        'uses' => 'PagesController@homepage',
        'as' => 'home'
    ]);

    Route::get('/desktop-notifications',[
        'uses' => 'PagesController@desktop_notifications',
        'as' => 'desktop.notifications'
    ]);

    Route::get('/how-it-works',[
        'uses' => 'PagesController@how_it_works',
        'as' => 'howitworks'
    ]);

    Route::get('/test',[
        'uses' => 'PagesController@test',
        'as' => 'test'
    ]);


    Auth::routes();

    Route::post('/need-help',[
        'uses' => 'AdminsDashboardController@need_help',
        'as' => 'need.help'
    ]);

    Route::get('/tom',[
        'uses' => 'PagesController@tom',
        'as' => 'tom'
    ]);

    Route::get('/project/create',[
        'uses' => 'ProjectsController@create',
        'as' => 'project.create'
    ]);

    Route::get('/project/create-start',[
        'uses' => 'ProjectsController@create_start',
        'as' => 'project.create-start'
    ]);

    Route::get('/project/create2',[
        'uses' => 'ProjectsController@create2',
        'as' => 'project.create2'
    ]);

    Route::get('/create-crowdfunding-project',[
        'uses' => 'ProjectsController@create3',
        'as' => 'project.create3'
    ]);

    Route::get('/drafts',[
        'uses' => 'ProjectsController@drafts',
        'as' => 'projects.drafts'
    ]);

    Route::post('/draft/store',[
        'uses' => 'ProjectsContoller@save_draft',
        'as' => 'save.draft'
    ]);

    Route::post('/project/store',[
        'uses' => 'ProjectsController@store',
        'as' => 'project.store'
    ]);


    Route::get('/dashboard', [
        'uses' => 'DashboardController@index',
        'as' => 'dashboard'
    ]);

    Route::get('/projects/{filter?}/{value?}',[
        'uses' => 'ProjectsController@index',
        'as' => 'projects'
    ]);

    Route::get('/load-more-projects',[
        'uses' => 'ProjectsController@load_more_projects',
        'as' => 'load.more.projects'
    ]);

    Route::get('/products',[
        'uses' => 'ProductsController@index',
        'as' => 'products'
    ]);

    Route::get('/product/create',[
        'uses' => 'ProductsController@create',
        'as' => 'product.create'
    ]);

    Route::post('/product/store',[
        'uses' => 'ProductsController@store',
        'as' => 'product.store'
    ]);

    Route::get('/product/edit/{id}',[
        'uses' => 'ProductsController@edit',
        'as' => 'product.edit'
    ]);

    Route::post('/product/update/{id}',[
        'uses' => 'ProductsController@update',
        'as' => 'product.update'
    ]);

    Route::get('/created-products',[
        'uses' => "ProductsController@created",
        'as' => 'products.created'
    ]);

    Route::get('/product/{id}',[
        'uses' => 'ProductsController@show',
        'as' => 'product.show'
    ]);

    Route::get('/product/approve/{id}',[
        'uses' => 'ProductsController@approve',
        'as' => 'product.approve'
    ]);

    Route::get('/product/destroy/{id}',[
        'uses' => 'ProductsController@destroy',
        'as' => 'product.delete'
    ]);

    Route::get('/{slug}/crowdfunding-project-{id}',[
        'uses' => 'ProjectsController@show',
        'as' => 'project.show'
    ]);

    Route::get('/members',[
        'uses' => 'PagesController@members',
        'as' => 'members'
    ]);

    Route::get('/forum',[
        'uses' => 'TopicsController@index',
        'as' => 'topics'
    ]);

    Route::get('/topic/show/{id}',[
        'uses' => 'TopicsController@show',
        'as' => 'topic.show'
    ]);

    Route::get('/discussion/show/{id}',[
        'uses' => 'DiscussionsController@show',
        'as' => 'discussion.show'
    ]);

    Route::post('backer/pay',[
        'uses' => 'BackersController@pay',
        'as' => 'backer.pay'
    ]);

    Route::get('/new-backer',[
        'uses' => 'BackersController@new_backer',
        'as' => 'new.backer'
    ]);

    Route::get('/chatroom',[
        'uses' => 'PagesController@chatroom',
        'as' => 'chatroom'
    ]);

    Route::get('/activation/{activation_link}/{user_id}',[
        'uses' => 'DashboardController@activate',
        'as' => 'dashboard.activate'
    ]);

    Route::get('/show/activity/{id}',[
        'uses' => 'ActivityLogsController@show',
        'as' => 'activity.show'
    ]);


    Route::get('/activity',[
        'uses' => 'ActivityLogsController@index',
        'as' => 'activity'
    ]);

    Route::get('/load-more-activities/{from}',[
        'uses' => 'ActivityLogsController@load_more_activities',
        'as' => 'load.more.activities'
    ]);

    Route::get('/actions',[
        'uses' => 'ActivityLogsController@actions',
        'as' => 'actions'
    ]);

    Route::get('/user-profile/{id}/{slug}',[
        'uses' => 'ProfilesController@show',
        'as' => 'profile.show'
    ]);

    Route::get('/news-article/{id}/{slug}',[
        'uses' => 'PostsController@show',
        'as' => 'post.show'
    ]);

    Route::get('/news',[
        'uses' => 'PostsController@index',
        'as' => 'posts'
    ]);

    Route::get('/evaluate-campaign',[
        'uses' => 'PagesController@evaluate_campaign',
        'as' => 'evaluate.campaign'
    ]);

    Route::post('/take-tour',[
        'uses' => 'AdminsDashboardController@take_tour',
        'as' => 'take.tour'
    ]);

    Route::post('/exit-feedback',[
        'uses' => 'AdminsDashboardController@exit_feedback',
        'as' => 'exit.feedback'
    ]);

    Route::get('/evaluate',[
        'uses' => 'PagesController@evaluate',
        'as' => 'evaluate'
    ]);

    Route::get('/categories',[
        'uses' => "CategoriesController@page",
        'as' => 'categories'
    ]);

    // Admin

    Route::group(['middleware' => 'checkIfAdmin'],function(){

        Route::get('/bots-auto-pledge-form', function(){
            return view('admin.autoBackProjects');
        })->name('bots.auto.pledge.form');

        Route::post('/bots-auto-pledge',[
            'uses' => 'AdminsDashboardController@bots_auto_pledge',
            'as' => 'bots.auto.pledge'
        ]);

        Route::get('/set-bots-avatar',[
            'uses' => 'AdminsDashboardController@set_bots_avatars',
            'as' => 'set.bots.avatars'
        ]);

        Route::get('/admin-new-backer',[
            'uses' => 'BackersController@admin_new_backer',
            'as' => 'admin.new.backer'
        ]);

        Route::get('/export-emails',[
            'uses' => 'AdminsDashboardController@export_emails',
            'as' => 'export.emails'
        ]);

        Route::get('/fetch-all-emails',[
            'uses' => 'AdminsDashboardController@fetch_all_emails',
            'as' => 'fetch.all.emails'
        ]);

        Route::get('/download-exported-emails',[
            'uses' => 'AdminsDashboardController@download_exported_emails',
            'as' => 'download.exported.emails'
        ]);

        Route::get('/start-queue-worker',[
            'uses' => 'AdminsDashboardController@complete_jobs',
            'as' => 'complete.jobs'
        ]);

        Route::get('/make-exclusive-{id}',[
            'uses' => 'ExclusiveCampaigns@make_exclusive',
            'as' => 'make.exclusive'
        ]);

        Route::get('/projects-slider',[
            'uses' => 'AdminsDashboardController@projectsSlider',
            'as' => 'projects.slider'
        ]);

        Route::post('/add-project-slide',[
            'uses' => 'AdminsDashboardController@add_project_slide',
            'as' => 'add.projects.slider'
        ]);

        Route::get('/delete-project-slide/{id}',[
            'uses' => 'AdminsDashboardController@delete_project_slide',
            'as' => 'delete.project.slide'
        ]);

        Route::get('/resource/edit/{id}',[
            'uses' => 'ResourcesController@edit',
            'as' => 'resource.edit'
        ]);

        Route::post('/resource/update/{id}',[
            'uses' => 'ResourcesController@update',
            'as' => 'resource.update'
        ]);

        Route::get('/resource/delete/{id}',[
            'uses' => 'ResourcesController@delete',
            'as' => 'resource.delete'
        ]);

        Route::get('/resource/create',[
            'uses' => 'ResourcesController@create',
            'as' => 'resource.create'
        ]);

        Route::post('/resource/store',[
            'uses' => 'ResourcesController@store',
            'as' => 'resource.store'
        ]);

        Route::post('switch-account',[
            'uses' => 'AdminsDashboardController@switch_account',
            'as' => 'switch.account'
        ]);

        Route::get('/emails-unread-notifications',[
            'uses' => 'AdminsDashboardController@emails_unread_notifications',
            'as' => 'emails.unread.notifications'
        ]);

        Route::get('/run-once',[
            'uses' => 'AdminsDashboardController@run_once',
            'as' => 'run.once'
        ]);

        Route::get('/admin-backers',[
            'uses' => "AdminsDashboardController@admin_backers",
            'as' => 'admin.backers'
        ]);

        Route::get('/ranks',[
            'uses' => "AdminsDashboardController@ranks",
            'as' => 'ranks'
        ]);

        Route::post('/rank-store',[
            'uses' => 'AdminsDashboardController@rank_store',
            'as' => 'rank.store'
        ]);

        Route::post('/rank-update',[
            'uses' => 'AdminsDashboardController@rank_update',
            'as' => 'rank.update'
        ]);

        Route::post('/set-rank',[
            'uses' => 'AdminsDashboardController@set_rank',
            'as' => 'set.rank'
        ]);

        Route::get('/email',[
            'uses' => 'AdminsDashboardController@email',
            'as' => 'email'
        ]);

        Route::post('/send-mails',[
            'uses' => 'AdminsDashboardController@send_mails',
            'as' => 'send.mails'
        ]);

        Route::post('/save-email-template',[
            'uses' => 'AdminsDashboardController@save_email_template',
            'as' => 'save.email.template'
        ]);

        Route::get('/delete-email-template/{id}',[
            'uses' => 'AdminsDashboardController@delete_email_template',
            'as' => 'delete.email.template'
        ]);

        Route::get('/admin/importusers',[
            'uses' => 'AdminsDashboardController@import_users',
            'as' => 'import.user'
        ]);

        Route::get('/admin/dashboard',[
            'uses' => 'AdminsDashboardController@index',
            'as' => 'admin.dashboard'
        ]);

        Route::get('/admin/dashboard/projects',[
            'uses' => 'AdminsDashboardController@projects',
            'as' => 'admin.dashboard.projects'
        ]);

        Route::get('/admin/dashboard/syncs',[
            'uses' => 'AdminsDashboardController@project_syncs',
            'as' => 'admin.syncs'
        ]);

        Route::get('/change-project-author/{id}',[
            'uses' => 'AdminsDashboardController@change_project_author',
            'as' => 'change.project.author'
        ]);

        Route::post('/author-change-save',[
            'uses' => 'AdminsDashboardController@save_author_change',
            'as' => 'author.change.save'
        ]);

        Route::get('/meta-tags',[
            'uses' => 'AdminsDashboardController@meta_tags',
            'as' => 'meta.tags'
        ]);

        Route::post('/save-meta',[
            'uses' => 'AdminsDashboardController@save_meta',
            'as' => 'save.meta'
        ]);

        Route::get('/help-requests',[
            'uses' => 'AdminsDashboardController@help_requests',
            'as' => 'help.requests'
        ]);

        Route::get('/admin/dashboard/project_sync/{id}',[
            'uses' => 'AdminsDashboardController@project_sync',
            'as' => 'admin.project.sync'
        ]);

        Route::post('/add-sync',[
            'uses' => 'AdminsDashboardController@add_sync',
            'as' => 'add.sync'
        ]);

        Route::get('/admin/dashboard/posts',[
            'uses' => 'AdminsDashboardController@posts',
            'as' => 'admin.dashboard.posts'
        ]);

        Route::get('/admin/dashboard/user-search',[
            'uses' => 'AdminsDashboardController@user_search',
            'as' => 'admin.dashboard.user.search'
        ]);

        Route::get('/admin/dashboard/users-table',[
            'uses' => 'AdminsDashboardController@users_table',
            'as' => 'admin.dashboard.users.table'
        ]);

        Route::get('/admin/dashboard/user/{id}',[
            'uses' => 'AdminsDashboardController@user',
            'as' => 'admin.dashboard.user'
        ]);

        Route::get('/admin/create',[
            'uses' => 'ProjectsController@adminCreate',
            'as' => 'admin.create'
        ]);

        Route::get('/project/approve/{id}',[
            'uses' => 'ProjectsController@approve',
            'as' => 'project.approve'
        ]);

        Route::get('/post/create',[
            'uses' => 'PostsController@create',
            'as' => 'post.create'
        ]);

        Route::post('/post/store',[
            'uses' => 'PostsController@store',
            'as' => 'post.store'
        ]);

        Route::get('/post/edit/{id}',[
            'uses' => 'PostsController@edit',
            'as' => 'post.edit'
        ]);

        Route::post('/post/update/{id}',[
            'uses' => 'PostsController@update',
            'as' => 'post.update'
        ]);

        Route::get('/post/delete/{id}',[
            'uses' => 'PostsController@destroy',
            'as' => 'post.delete'
        ]);

        // Categories

        Route::get('/manage-categories',[
            'uses' => 'CategoriesController@index',
            'as' => 'manage.categories'
        ]);

        Route::get('/category/create',[
            'uses' => 'CategoriesController@create',
            'as' => 'category.create'
        ]);

        Route::post('category/store',[
            'uses' => 'CategoriesController@store',
            'as' => 'category.store'
        ]);

        Route::post('category/store-subcategory',[
            'uses' => 'CategoriesController@store_subcategory',
            'as' => 'subcategory.store'
        ]);

        Route::get('category/edit/{id}',[
            'uses' => 'CategoriesController@edit',
            'as' => 'category.edit'
        ]);

        Route::post('category/update/{id}',[
            'uses' => 'CategoriesController@update',
            'as' => 'category.update'
        ]);

        Route::get('subcategory/edit/{id}',[
            'uses' => 'CategoriesController@edit_subcategory',
            'as' => 'subcategory.edit'
        ]);

        Route::post('subcategory/update/{id}',[
            'uses' => 'CategoriesController@update_subcategory',
            'as' => 'subcategory.update'
        ]);

        Route::get('category/delete/{id}',[
            'uses' => 'CategoriesController@destroy',
            'as' => 'category.delete'
        ]);

        Route::get('/admin/announcement',[
            'uses' => 'AdminsDashboardController@announcement',
            'as' => 'announcement'
        ]);

        Route::post('/admin/send-announcement',[
            'uses' => 'AdminsDashboardController@send_announcement',
            'as' => 'send.announcement'
        ]);
    });

    /* Search box */

    Route::get('/results', [
        'uses' => 'PagesController@results',
        'as' => 'results'
    ]);

    Route::post('/got-idea', [
        'uses' => 'AdminsDashboardController@got_idea',
        'as' => 'got.idea'
    ]);

    Route::post('/store-subscribed-user', [
        'uses' => 'AdminsDashboardController@store_subscribed_user',
        'as' => 'store.subscribed.user'
    ]);

    Route::group(['middleware' => 'auth'],function(){

        Route::get('/watch-project/{id}',[
            'uses' => 'ProjectsController@watch',
            'as' => 'watch.project'
        ]);

        Route::get('/unwatch-project/{id}',[
            'uses' => 'ProjectsController@unwatch',
            'as' => 'unwatch.project'
        ]);

        Route::get('/campaign-tasks-{id}',[
            'uses' => 'ExclusiveCampaigns@campaign_tasks',
            'as' => 'campaign.tasks'
        ]);

        Route::post('/store-campaign-team',[
            'uses' => 'ExclusiveCampaigns@store_campaign_team',
            'as' => 'store.campaign.team'
        ]);

        Route::post('/store-campaign-tasks',[
            'uses' => 'ExclusiveCampaigns@store_campaign_tasks',
            'as' => 'store.campaign.tasks'
        ]);

        Route::post('/store-campaign-perks',[
            'uses' => 'ExclusiveCampaigns@store_campaign_perks',
            'as' => 'store.campaign.perks'
        ]);

        Route::post('/store-general-plan',[
            'uses' => 'ExclusiveCampaigns@store_general_plan',
            'as' => 'store.general.plan'
        ]);

        Route::post('/store-week-plan',[
            'uses' => 'ExclusiveCampaigns@store_week_plan',
            'as' => 'store.week.plan'
        ]);

        Route::get('/become-collaborator/{token}',[
            'uses' => 'ProjectsController@become_collaborator',
            'as' => 'become.collaborator'
        ]);

        Route::get('/conversations',[
            'uses' => 'ConversationsController@index',
            'as' => 'conversations'
        ]);

        Route::get('/notifications',[
            'uses' => 'PagesController@notifications',
            'as' => 'notifications'
        ]);

        Route::get('/start-conversation/{id}',[
            'uses' => 'ConversationsController@start_conversation',
            'as' => 'start.conversation'
        ]);

        Route::get('/delete-conversation/{id}',[
            'uses' => 'ConversationsController@delete_conversation',
            'as' => 'delete.conversation'
        ]);

        Route::post('/message-store',[
            'uses' => 'ConversationsController@message_store',
            'as' => 'message.store'
        ]);

        Route::get('/project/project-url',[
            'uses' => 'ProjectsController@project_url',
            'as' => 'project.url'
        ]);

        Route::get('/project/locodor-url',[
            'uses' => 'ProjectsController@locodor_url',
            'as' => 'locodor.url'
        ]);

        Route::post('/project/results',[
            'uses' => 'ProjectsController@results',
            'as' => 'project.results'
        ]);

        Route::post('/project/results/indiegogo',[
            'uses' => 'ProjectsController@results_indiegogo',
            'as' => 'project.results.indiegogo'
        ]);

        Route::post('/project/results/locodor',[
            'uses' => 'ProjectsController@results_locodor',
            'as' => 'project.results.locodor'
        ]);

        Route::get('/project/edit/{id}',[
            'uses' => 'ProjectsController@edit',
            'as' => 'project.edit'
        ]);

        Route::post('/project/update/{id}',[
            'uses' => 'ProjectsController@update',
            'as' => 'project.update'
        ]);

        Route::get('/created-projects',[
            'uses' => 'ProjectsController@created',
            'as' => 'projects.created'
        ]);

        Route::get('/exclusive-projects',[
            'uses' => 'ProjectsController@exclusive_projects',
            'as' => 'exclusive.projects'
        ]);

        Route::get('/delete/project/{id}',[
            'uses' => 'ProjectsController@destroy',
            'as' => 'project.delete'
        ]);

        Route::get('/profile/edit/{id}',[
            'uses' => 'ProfilesController@edit',
            'as' => 'profile.edit'
        ]);

        Route::post('/profile/update/{id}',[
            'uses' => 'ProfilesController@update',
            'as' => 'profile.update'
        ]);

        Route::post('/profile/post-store',[
            'uses' => 'ProfilesController@store_profile_post',
            'as' => 'store.profile.post'
        ]);

        Route::get('/profile/post-delete/{id}',[
            'uses' => 'ProfilesController@delete_profile_post',
            'as' => 'delete.profile.post'
        ]);

        Route::post('/profile/reply-store',[
            'uses' => 'ProfilesController@profile_reply_store',
            'as' => 'profile.reply.store'
        ]);

        Route::get('/profile/reply-delete/{id}',[
            'uses' => 'ProfilesController@reply_delete',
            'as' => 'profile.reply.delete'
        ]);

        Route::post('/comment/store',[
            'uses' => 'CommentsController@store',
            'as' => 'comment.store'
        ]);

        Route::get('comment/edit/{id}',[
            'uses' => 'CommentsController@edit',
            'as' => 'comment.edit'
        ]);

        Route::get('comment/update/{id}',[
            'uses' => 'CommentsController@update',
            'as' => 'comment.update'
        ]);

        Route::get('comment/delete/{id}',[
            'uses' => 'CommentsController@destroy',
            'as' => 'comment.delete'
        ]);

        Route::get('comment/like/{id}',[
            'uses' => 'CommentsController@like',
            'as' => 'comment.like'
        ]);

        Route::get('comment/unlike/{id}',[
            'uses' => 'CommentsController@unlike',
            'as' => 'comment.unlike'
        ]);

        Route::get('/topic/create',[
            'uses' => 'TopicsController@create',
            'as' => 'topic.create'
        ]);

        Route::post('/topic/store',[
            'uses' => 'TopicsController@store',
            'as' => 'topic.store'
        ]);

        Route::get('/topics/delete/{id}',[
            'uses' => 'TopicsController@destroy',
            'as' => 'topic.delete'
        ]);

        Route::get('/topic/edit/{id}',[
            'uses' => 'TopicsController@edit',
            'as' => 'topic.edit'
        ]);

        Route::post('/topic/update/{id}',[
            'uses' => 'TopicsController@update',
            'as' => 'topic.update'
        ]);

        Route::get('/discussion/create/{topic_id}',[
            'uses' => 'DiscussionsController@create',
            'as' => 'discussion.create'
        ]);

        Route::post('/discussion/store/{topic_id}',[
            'uses' => 'DiscussionsController@store',
            'as' => 'discussion.store'
        ]);

        Route::get('discussion/edit/{id}',[
            'uses' => 'DiscussionsController@edit',
            'as' => 'discussion.edit'
        ]);

        Route::post('discussion/update/{id}',[
            'uses' => 'DiscussionsController@update',
            'as' => 'discussion.update'
        ]);

        Route::get('discussion/delete/{id}',[
            'uses' => 'DiscussionsController@destroy',
            'as' => 'discussion.delete'
        ]);

        Route::get('discussion/like/{id}',[
            'uses' => 'DiscussionsController@like',
            'as' => 'discussion.like'
        ]);

        Route::get('discussion/unlike/{id}',[
            'uses' => 'DiscussionsController@unlike',
            'as' => 'discussion.unlike'
        ]);

        Route::get('discussion/watch/{id}',[
            'uses' => 'DiscussionsController@watch',
            'as' => 'discussion.watch'
        ]);

        Route::get('discussion/unwatch/{id}',[
            'uses' => 'DiscussionsController@unwatch',
            'as' => 'discussion.unwatch'
        ]);

        Route::get('reply/like/{id}',[
            'uses' => 'RepliesController@like',
            'as' => 'reply.like'
        ]);

        Route::get('reply/unlike/{id}',[
            'uses' => 'RepliesController@unlike',
            'as' => 'reply.unlike'
        ]);

        Route::post('reply/store',[
            'uses' => 'RepliesController@store',
            'as' => 'reply.store'
        ]);

        Route::get('reply/edit/{id}',[
            'uses' => 'RepliesController@edit',
            'as' => 'reply.edit'
        ]);

        Route::post('reply/update/{id}',[
            'uses' => 'RepliesController@update',
            'as' => 'reply.update'
        ]);

        Route::get('reply/delete/{id}',[
            'uses' => 'RepliesController@destroy',
            'as' => 'reply.delete'
        ]);

        Route::post('/activity/post',[
            'uses' => 'UserPostsController@store',
            'as' => 'userpost.store'
        ]);

        Route::get('/activity/post-delete/{id}',[
            'uses' => 'UserPostsController@user_post_delete',
            'as' => 'userpost.delete'
        ]);

        Route::get('/activity/like/{id}',[
            'uses' => 'ActivityLogsController@activity_like',
            'as' => 'activity.like'
        ]);

        Route::get('/activity/unlike/{id}',[
            'uses' => 'ActivityLogsController@activity_unlike',
            'as' => 'activity.unlike'
        ]);

        Route::post('/activity/comment',[
            'uses' => 'ActivityLogsController@activity_comment',
            'as' => 'activity.comment'
        ]);

        Route::get('/activity/comment/delete/{id}',[
            'uses' => 'ActivityLogsController@activity_comment_delete',
            'as' => 'activity.comment.delete'
        ]);

        Route::get('/add-friend/{id}',[
            'uses' => 'ProfilesController@add_friend',
            'as' => 'add.friend'
        ]);

        Route::get('/accept-friend/{id}',[
            'uses' => 'ProfilesController@accept_friend',
            'as' => 'accept.friend'
        ]);

        Route::get('/remove-friend/{id}',[
            'uses' => 'ProfilesController@remove_friend',
            'as' => 'remove.friend'
        ]);
    });
});
