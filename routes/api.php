<?php
use PayPal\Api\Item;
use PayPal\Api\Payer;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Payment;
use PayPal\Api\ItemList;
use PayPal\Api\WebProfile;
use PayPal\Api\InputFields;
use PayPal\Api\Transaction;
use Illuminate\Http\Request;
use PayPal\Api\RedirectUrls;
use PayPal\Api\PaymentExecution;
use App\Backer;
use App\Project;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('create-payment', function () {
    $apiContext = new \PayPal\Rest\ApiContext(
        new \PayPal\Auth\OAuthTokenCredential(
            'AacGDpsS8aoAfLdJN4DblsZELEQBD05QwkKFa-XBBmuKPC9plpAdNFiwzesaiuZ3acQ2FCKUtbImr3Fo',     // ClientID
            'EKy8KQ4r1CTCZGWAnDEm2b_mfF1H_XfUE-jCviLlafCqOIzAuRZi-vN9wNZzyQavwRW3NrfYv899QJ6b'      // ClientSecret
        )
    );
    $payer = new Payer();
    $payer->setPaymentMethod("paypal");
    $item1 = new Item();
    $item1->setName($_POST['product_title'])
        ->setCurrency('USD')
        ->setQuantity(1)
        ->setSku($_POST['product_id']) // Similar to `item_number` in Classic API
        ->setPrice($_POST['product_price']);
    $itemList = new ItemList();
    $itemList->setItems(array($item1));
    $details = new Details();
    $details->setShipping(0)
        ->setTax(0)
        ->setSubtotal($_POST['product_price']);
    $amount = new Amount();
    $amount->setCurrency("USD")
        ->setTotal($_POST['product_price'])
        ->setDetails($details);
    $transaction = new Transaction();
    $transaction->setAmount($amount)
        ->setItemList($itemList)
        ->setDescription("Payment description")
        ->setInvoiceNumber(uniqid());
    $redirectUrls = new RedirectUrls();
    $redirectUrls->setReturnUrl("www.locodor.com")
        ->setCancelUrl("www.locodor.com");
    // Add NO SHIPPING OPTION
    $inputFields = new InputFields();
    $inputFields->setNoShipping(1);
    $webProfile = new WebProfile();
    $webProfile->setName('test' . uniqid())->setInputFields($inputFields);
    $webProfileId = $webProfile->create($apiContext)->getId();
    $payment = new Payment();
    $payment->setExperienceProfileId($webProfileId); // no shipping
    $payment->setIntent("sale")
        ->setPayer($payer)
        ->setRedirectUrls($redirectUrls)
        ->setTransactions(array($transaction));
    try {
        $payment->create($apiContext);
    } catch (Exception $ex) {
        echo $ex;
        exit(1);
    }
    return $payment;
});

Route::post('execute-payment', function (Request $request) {
    $apiContext = new \PayPal\Rest\ApiContext(
        new \PayPal\Auth\OAuthTokenCredential(
            'AacGDpsS8aoAfLdJN4DblsZELEQBD05QwkKFa-XBBmuKPC9plpAdNFiwzesaiuZ3acQ2FCKUtbImr3Fo',     // ClientID
            'EKy8KQ4r1CTCZGWAnDEm2b_mfF1H_XfUE-jCviLlafCqOIzAuRZi-vN9wNZzyQavwRW3NrfYv899QJ6b'      // ClientSecret
        )
    );
    $paymentId = $request->paymentID;
    $payment = Payment::get($paymentId, $apiContext);
    $execution = new PaymentExecution();
    $execution->setPayerId($request->payerID);
    // $transaction = new Transaction();
    // $amount = new Amount();
    // $details = new Details();
    // $details->setShipping(2.2)
    //     ->setTax(1.3)
    //     ->setSubtotal(17.50);
    // $amount->setCurrency('USD');
    // $amount->setTotal(21);
    // $amount->setDetails($details);
    // $transaction->setAmount($amount);
    // $execution->addTransaction($transaction);
    try {
        $result = $payment->execute($execution, $apiContext);

        $project = Project::find($request->project_id);

        $project->current_amount += $request->product_price;
        $project->funding_percent = round($project->current_amount / $project->project_goal * 100);
        $project->save();

        $backer = new Backer;
        $backer->user_id = $request->auth_id;
        $backer->level_id = $request->product_id;
        $backer->project_id = $request->project_id;
        $backer->amount = $request->product_price;
        $backer->save();

    } catch (Exception $ex) {
        echo $ex;
        exit(1);
    }
    return $result;
});