<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MetaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('metas')->insert([
            'meta' => 'title',
            'page' => 'homepage',
            'value' => '',
        ]);

        DB::table('metas')->insert([
            'meta' => 'description',
            'page' => 'homepage',
            'value' => '',
        ]);

        DB::table('metas')->insert([
            'meta' => 'title',
            'page' => 'projectsIndex',
            'value' => '',
        ]);

        DB::table('metas')->insert([
            'meta' => 'description',
            'page' => 'projectsIndex',
            'value' => '',
        ]);

        DB::table('metas')->insert([
            'meta' => 'title',
            'page' => 'socialCrowd',
            'value' => '',
        ]);

        DB::table('metas')->insert([
            'meta' => 'description',
            'page' => 'socialCrowd',
            'value' => '',
        ]);

        DB::table('metas')->insert([
            'meta' => 'title',
            'page' => 'conversations',
            'value' => '',
        ]);

        DB::table('metas')->insert([
            'meta' => 'description',
            'page' => 'conversations',
            'value' => '',
        ]);

        DB::table('metas')->insert([
            'meta' => 'title',
            'page' => 'news',
            'value' => '',
        ]);

        DB::table('metas')->insert([
            'meta' => 'description',
            'page' => 'news',
            'value' => '',
        ]);

        DB::table('metas')->insert([
            'meta' => 'title',
            'page' => 'profileShow',
            'value' => '',
        ]);

        DB::table('metas')->insert([
            'meta' => 'description',
            'page' => 'profileShow',
            'value' => '',
        ]);

        DB::table('metas')->insert([
            'meta' => 'title',
            'page' => 'members',
            'value' => '',
        ]);

        DB::table('metas')->insert([
            'meta' => 'description',
            'page' => 'members',
            'value' => '',
        ]);
    }
}
