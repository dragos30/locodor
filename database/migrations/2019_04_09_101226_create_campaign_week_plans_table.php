<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignWeekPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaign_week_plans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id');
            $table->string('focus')->nullable();
            $table->string('monday_channel1')->nullable();
            $table->string('monday_channel2')->nullable();
            $table->string('monday_email')->nullable();
            $table->string('tuesday_channel1')->nullable();
            $table->string('tuesday_channel2')->nullable();
            $table->string('tuesday_email')->nullable();
            $table->string('wednesday_channel1')->nullable();
            $table->string('wednesday_channel2')->nullable();
            $table->string('wednesday_email')->nullable();
            $table->string('thursday_channel1')->nullable();
            $table->string('thursday_channel2')->nullable();
            $table->string('thursday_email')->nullable();
            $table->string('friday_channel1')->nullable();
            $table->string('friday_channel2')->nullable();
            $table->string('friday_email')->nullable();
            $table->string('saturday_channel1')->nullable();
            $table->string('saturday_channel2')->nullable();
            $table->string('saturday_email')->nullable();
            $table->string('sunday_channel1')->nullable();
            $table->string('sunday_channel2')->nullable();
            $table->string('sunday_email')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaign_week_plans');
    }
}
