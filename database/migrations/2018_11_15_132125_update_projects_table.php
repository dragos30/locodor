<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */// first project, previous url, p_website, shipping_location
    public function up()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->string('first_project')->nullable()->change();
            $table->string('previous_url')->nullable()->change();
            $table->string('website')->nullable()->change();
            $table->string('shipping_location')->nullable()->change();
            $table->string('upload_other')->nullable()->change();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->string('first_project')->change();
            $table->string('previous_url')->change();
            $table->string('website')->change();
            $table->string('shipping_location')->change();
            $table->string('upload_other')->change();
        });
    }
}
