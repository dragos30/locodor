<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->boolean('first_project');
            $table->integer('category_id');
            $table->integer('project_type');
            $table->string('previous_url');
            $table->boolean('upload_other');
            $table->integer('ready');
            $table->string('title',255);
            $table->string('subtitle',255);
            $table->text('image');
            $table->integer('start_date');
            $table->integer('end_date');
            $table->integer('current_amount')->default(0);
            $table->integer("project_goal");
            $table->integer('funding_percent')->default(0);
            $table->string('shipping_location');
            $table->integer('comments_number')->default(0);
            $table->integer('views')->default(0);
            $table->text('content');
            $table->string('website');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
