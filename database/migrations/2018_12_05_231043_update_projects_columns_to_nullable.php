<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProjectsColumnsToNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->integer('category_id')->nullable()->change();
            $table->integer('project_type')->nullable()->change();
            $table->integer('ready')->nullable()->change();
            $table->string('subtitle',255)->nullable()->change();
            $table->text('image')->nullable()->change();
            $table->integer('start_date')->nullable()->change();
            $table->integer('end_date')->nullable()->change();
            $table->integer("project_goal")->nullable()->change();
            $table->text('content')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->integer('category_id')->change();
            $table->integer('project_type')->change();
            $table->integer('ready')->change();
            $table->string('subtitle',255)->change();
            $table->text('image')->change();
            $table->integer('start_date')->change();
            $table->integer('end_date')->change();
            $table->integer("project_goal")->change();
            $table->text('content')->change();
        });
    }
}
