<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignPerksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaign_perks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id');
            $table->integer('price')->nullable();
            $table->integer('cost')->nullable();
            $table->integer('goal')->nullable();
            $table->string('name')->nullable();
            $table->string('description')->nullable();
            $table->boolean('tested')->nullable();
            $table->text('notes')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaign_perks');
    }
}
