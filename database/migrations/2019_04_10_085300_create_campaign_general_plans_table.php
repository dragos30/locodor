<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignGeneralPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaign_general_plans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id');
            $table->string('status')->nullable();
            $table->string('roles')->nullable();
            $table->string('availability')->nullable();
            $table->string('pricing')->nullable();
            $table->string('commission')->nullable();
            $table->boolean('general')->nullable();
            $table->string('focus')->nullable();
            $table->string('social_media')->nullable();
            $table->string('email')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaign_general_plans');
    }
}
