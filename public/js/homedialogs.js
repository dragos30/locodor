jQuery( document ).ready( function() {
	var currentdialog=1;
	
	window.setInterval(function(){
		currentdialog=currentdialog+1;
		if(currentdialog==6){
			currentdialog=1;
		}
		displaynextdialog(currentdialog);
	}, 4000);
	
});

function displaynextdialog(dialog) {
	jQuery('.landing_block--cta-visual-wrapper p').fadeOut();
	if(dialog==1){
		jQuery('.landing_block--cta-visual-wrapper p.first').delay( 1000 ).fadeIn();
	}else if(dialog==2){
		jQuery('.landing_block--cta-visual-wrapper p.second').delay( 1000 ).fadeIn();
	}else if(dialog==3){
		jQuery('.landing_block--cta-visual-wrapper p.third').delay( 1000 ).fadeIn();
	}else if(dialog==4){
		jQuery('.landing_block--cta-visual-wrapper p.four').delay( 1000 ).fadeIn();
	}else if(dialog==5){
		jQuery('.landing_block--cta-visual-wrapper p.five').delay( 1000 ).fadeIn();
	}

} //display-next-dialog()