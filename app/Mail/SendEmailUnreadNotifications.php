<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Notification;
use App\User;

class SendEmailUnreadNotifications extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $id;
    public $user;
    public $unread_notifications;


    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->user = User::find($this->id);
        $this->unread_notifications = Notification::where('user_id',$this->id)->where('status','new')->get();

        return $this->view('emails.unreadNotifications');
    }
}
