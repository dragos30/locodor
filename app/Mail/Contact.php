<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Contact extends Mailable
{
    use Queueable, SerializesModels;

    public $email;
    public $name;
    public $body;
    public function __construct($request)
    {
        $this->email = $request->email;
        $this->name = $request->name;
        $this->body = $request->message;
    }

    public function build()
    {
        return $this->view('emails.contact');
    }
}
