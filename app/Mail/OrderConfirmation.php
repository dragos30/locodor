<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderConfirmation extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $backer;
    public $orderID;
    public function __construct($backer)
    {
        $this->backer = $backer;
        if($backer->data = 'admin'){
            $this->orderID = 'admin';
        }else{
            $order_data = unserialize($backer->data);
            $this->orderID = $order_data['orderID'];
        }

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.orderConfirmation');
    }
}
