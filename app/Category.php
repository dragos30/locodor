<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function project(){
        return $this->hasMany('App\Project');
    }

    public function subcategories(){
        return $this->hasMany('App\Subcategory','main_category_id');
    }
}
