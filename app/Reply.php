<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reply extends Model
{
    //

    protected $fillable = ['user_id','discussion_id','content'];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function discussion(){
        return $this->belongsTo('App\Discussion');
    }

    public function reply_likes(){
        return $this->hasMany('App\ReplyLike');
    }

    public function replyLikes(){
        return $this->hasMany('App\ReplyLike');
    }
}
