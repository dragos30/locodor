<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivityLog extends Model
{
    protected $fillable = ['user_id','action_id','action_name'];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function project(){
        return $this->belongsTo('App\Project','action_id');
    }

    public function discussion(){
        return $this->belongsTo('App\Discussion','action_id');
    }

    public function reply(){
        return $this->belongsTo('App\Reply','action_id');
    }

    public function comment(){
        return $this->belongsTo('App\Comment','action_id');
    }

    public function topic(){
        return $this->belongsTo('App\Topic','action_id');
    }

    public function commentLike(){
        return $this->belongsTo('App\CommentLike','action_id');
    }

    public function discussionLike(){
        return $this->belongsTo('App\DiscussionLike','action_id');
    }

    public function replyLike(){
        return $this->belongsTo('App\ReplyLike','action_id');
    }

    public function watcher(){
        return $this->belongsTo('App\Watcher','action_id');
    }

    public function post(){
        return $this->belongsTo('App\Post','action_id');
    }

    public function userpost(){
        return $this->belongsTo('App\UserPost','action_id');
    }

    public function activityLikes(){
        return $this->hasMany('App\ActivityLike','activity_id');
    }

    public function activityComments(){
        return $this->hasMany('App\ActivityComment','activity_id');
    }

    public function backer(){
        return $this->belongsTo('App\Backer','action_id');
    }

    public function profilePost(){
        return $this->belongsTo('App\ProfilePost','action_id');
    }
}
