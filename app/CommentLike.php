<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class CommentLike extends Model
{
    protected $fillable = ['user_id','comment_id'];

    public function users(){
        return $this->belongsToMany('App\User');
    }

    public function comment(){
        return $this->belongsTo('App\Comment');
    }

    public function is_liked_by_auth(){
        $id = Auth::id();

        $liked = array();

        dd($this);
    }
}
