<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Discussion extends Model
{
    protected $fillable = ['topic_id','user_id','title','description','content'];

    public function topic(){
        return $this->belongsTo('App\Topic');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function replies(){
        return $this->hasMany('App\Reply');
    }

    public function discussionLike(){
        return $this->hasMany('App\DiscussionLike');
    }

    public function watchers(){
        return $this->hasMany('App\Watcher');
    }
}
