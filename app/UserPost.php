<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPost extends Model
{
    protected $fillable = ['user_id','content','image'];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function activity(){
        return $this->belongsTo('App\ActivityLog','action_id');
    }
}
