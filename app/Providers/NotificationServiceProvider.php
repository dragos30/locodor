<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Notification;
use Illuminate\Support\Facades\View;
use Auth;
use Illuminate\Contracts\Auth\Guard;

class NotificationServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(Guard $auth)
    {

        view()->composer('*', function($view) use ($auth) {
            // get the current user
            if(Auth::check()){
                $currentUser = $auth->user();

                $notifications = Notification::where('user_id',$currentUser->id)->where('status','new')->get();
                View::share('notifications', $notifications);
            }
        });


    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
