<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReplyLike extends Model
{
    protected $fillable = ['reply_id','user_id'];

    public function reply(){
        return $this->belongsTo('App\Reply');
    }

    public function users(){
        return $this->belongsToMany('App\User');
    }
}
