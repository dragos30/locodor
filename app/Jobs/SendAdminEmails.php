<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendAdminEmails implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    public $record;
    public function __construct($record)
    {
        $this->record = $record;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        if (filter_var($this->record[2], FILTER_VALIDATE_EMAIL)) {
            $email = $this->record[2];


            $mail_content = $this->record[1];
            $mail_subject = $this->record[0];

            if(isset($this->record[3])){
                $mail_content = str_replace("[[var1]]",$this->record[3],$mail_content);
                $mail_subject = str_replace("[[var1]]",$this->record[3],$mail_subject);
            }
            if(isset($this->record[4])){
                $mail_content = str_replace("[[var2]]",$this->record[4],$mail_content);
                $mail_subject = str_replace("[[var2]]",$this->record[4],$mail_subject);
            }
            if(isset($this->record[5])){
                $mail_content = str_replace("[[var3]]",$this->record[5],$mail_content);
                $mail_subject = str_replace("[[var3]]",$this->record[5],$mail_subject);
            }
            if(isset($this->record[6])){
                $mail_content = str_replace("[[var4]]",$this->record[6],$mail_content);
                $mail_subject = str_replace("[[var4]]",$this->record[6],$mail_subject);
            }
            if(isset($this->record[7])){
                $mail_content = str_replace("[[var5]]",$this->record[7],$mail_content);
                $mail_subject = str_replace("[[var5]]",$this->record[7],$mail_subject);
            }

//            if(array_key_exists($this->record[3],$this->record)){
//                $mail_content = str_replace("[[var1]]",$this->record[3],$mail_content);
//                $mail_subject = str_replace("[[var1]]",$this->record[3],$mail_subject);
//            }
//            if(array_key_exists($this->record[4],$this->record)){
//                $mail_content = str_replace("[[var2]]",$this->record[4],$mail_content);
//                $mail_subject = str_replace("[[var2]]",$this->record[4],$mail_subject);
//            }
//            if(array_key_exists($this->record[5],$this->record)){
//                $mail_content = str_replace("[[var3]]",$this->record[5],$mail_content);
//                $mail_subject = str_replace("[[var3]]",$this->record[5],$mail_subject);
//            }
//            if(array_key_exists($this->record[6],$this->record)){
//                $mail_content = str_replace("[[var4]]",$this->record[6],$mail_content);
//                $mail_subject = str_replace("[[var4]]",$this->record[6],$mail_subject);
//            }
//            if(array_key_exists($this->record[7],$this->record)){
//                $mail_content = str_replace("[[var5]]",$this->record[7],$mail_content);
//                $mail_subject = str_replace("[[var5]]",$this->record[7],$mail_subject);
//            }









            Mail::send([], [],
                function ($message) use ($email,$mail_content,$mail_subject) {
                    $message->subject($mail_subject);
                    $message->setBody($mail_content, 'text/html');
                    $message->to($email);
                });
        }
    }
}
