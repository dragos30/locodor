<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    public function user(){
        return $this->belongsTo('App\User');
    }

    public function message(){
        return $this->belongsTo('App\Message','action_id');
    }

    public function conversation(){
        return $this->belongsTo('App\Conversation','action_id');
    }

    public function profile_reply(){
        return $this->belongsTo('App\ProfileReply','action_id');
    }

    public function watcher(){
        return $this->belongsTo('App\Watcher','action_id');
    }

    public function project(){
        return $this->belongsTo('App\Project','action_id');
    }

    public function comment(){
        return $this->belongsTo('App\Comment','action_id');
    }
}
