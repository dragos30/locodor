<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivityLike extends Model
{
    public function activityLog(){
        return $this->belongsTo('App\ActivityLog');
    }
}
