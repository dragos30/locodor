<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'admin' ,'activated','activation'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function projects(){
        return $this->hasMany('App\Project');
    }

    public function profile(){
        return $this->hasOne('App\Profile');
    }

    public function comments(){
        return $this->hasMany('App\Comment');
    }

    public function replies(){
        return $this->hasMany('App\Reply');
    }

    public function discussionLikes(){
        return $this->hasMany('App\DiscussionLike');
    }

    public function replyLikes(){
        return $this->hasMany('App\ReplyLike');
    }

    public function commentLikes(){
        return $this->hasMany('App\CommentLike');
    }

    public function watchers(){
        return $this->hasMany('App\Watcher');
    }

    public function activityLogs(){
        return $this->hasMany('App\ActivityLog');
    }

    public function user_posts(){
        return $this->hasMany('App\UserPost');
    }

    public function friends(){
        return $this->hasMany('App\Friend');
    }

    public function requests(){
        return $this->hasMany('App\Friend');
    }

    public function projectViews(){
        return $this->hasMany('App\ProjectView');
    }

    public function backers(){
        return $this->hasMany('App\Backer');
    }

    public function activityComments(){
        return $this->hasMany('App\ActivityComment');
    }

    public function rank(){
        return $this->belongsTo('App\Rank');
    }

    public function profilePosts(){
        return $this->hasMany('App\ProfilePost');
    }

    public function collaborators(){
        return $this->hasMany('App\Collaborator');
    }
}
