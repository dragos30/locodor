<?php

namespace App\Handlers;

class LfmConfigHandler extends \UniSharp\LaravelFilemanager\Handlers\ConfigHandler
{
    public function userField()
    {
        if(isset(auth()->user()->id)){
            return auth()->user()->id;
        } else {
            return 999999;
        }
    }
}
