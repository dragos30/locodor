<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class ProfilePost extends Model
{
    public function user_1(){
        return $this->belongsTo('App\User','user1');
    }

    public function user_2(){
        return $this->belongsTo('App\User','user2');
    }

    public function replies(){
        return $this->hasMany('App\ProfileReply');
    }
}
