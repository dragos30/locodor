<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;

class Subcategory extends Model
{
    public function category(){
        return $this->belongsTo('App\Category','main_category_id');
    }
}
