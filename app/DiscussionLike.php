<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiscussionLike extends Model
{
    protected $fillable = ['discussion_id','user_id'];

    public function discussion(){
        return $this->belongsTo('App\Discussion');
    }

    public function users(){
        return $this->belongsToMany('App\User');
    }
}
