<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Backer extends Model
{
    public function level(){
        return $this->belongsTo('App\Level');
    }

    public function level_including_trashed(){
        return $this->level()->withTrashed()->get()->first();
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function project(){
        return $this->belongsTo('App\Project');
    }
}
