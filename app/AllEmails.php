<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AllEmails extends Model
{
    protected $fillable = ['email'];
}
