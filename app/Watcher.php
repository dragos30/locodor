<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Watcher extends Model
{
    protected $fillable = ['user_id','discussion_id'];

    public function users(){
        return $this->belongsToMany('App\User');
    }

    public function discussion(){
        return $this->belongsTo('App\Discussion');
    }

    public function project(){
        return $this->belongsTo('App\Project');
    }

    public static function watching($id){
        $watcher = Watcher::where('user_id',Auth::id())->where('project_id',$id)->get()->first();

        if($watcher != null){
            return true;
        }else{
            return false;
        }
    }
}
