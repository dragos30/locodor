<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = ['user_id','title','subtitle','image','start_date','end_date','project_type','website','content'];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function levels(){
        return $this->hasMany('App\Level');
    }

    public function comments(){
        return $this->hasMany('App\Comment');
    }

    public function category(){
        return $this->belongsTo('App\Category');
    }

    public function projectViews(){
        return $this->hasMany('App\ProjectView');
    }

    public function backers(){
        return $this->hasMany('App\Backer');
    }

    public function projectSync(){
        return $this->hasOne('App\ProjectSync');
    }

    public function products(){
        return $this->hasMany('App\Product');
    }

     public function collaborators(){
        return $this->hasMany('App\Collaborator');
     }

     public function tasks(){
        return $this->hasMany('App\CampaignTask');
     }

    public function completed_tasks() {
        return $this->tasks()->where('status','=', 'completed')->get();
    }

    public function watchers(){
        return $this->hasMany('App\Watchers');
    }
}
