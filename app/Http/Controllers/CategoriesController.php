<?php

namespace App\Http\Controllers;

use App\Category;
use App\Subcategory;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subcategories = Subcategory::all();
        return view('categories.index')
            ->with('subcategories',$subcategories)
            ->with('categories',Category::all());
    }

    public function page(){
        $categories = Category::all();
        return view('categories.page')->with('categories',$categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('categories.create')
            ->with('categories',$categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required'
        ]);

        $category = new Category;
        $category->name = $request->name;
        $category->icon = $request->icon;
        $category->image = $request->image;
        $category->save();

        return redirect()->route('categories');
    }

    public function store_subcategory(Request $request){
        $this->validate($request,[
            'main_category_id' => 'required',
            'name' => 'required',
            'icon' => 'required'
        ]);

        $subcategory = new Subcategory;
        $subcategory->name = $request->name;
        $subcategory->icon = $request->icon;
        $subcategory->main_category_id = $request->main_category_id;
        $subcategory->save();

        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('categories.edit')->with('category',Category::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required'
        ]);

        $category = Category::find($id);

        $category->name = $request->name;
        if(isset($request->icon)){
            $category->icon = $request->icon;
        }
        $category->image = $request->image;

        $category->save();

        return redirect()->route('manage.categories');
    }

    public function edit_subcategory($id){
        $categories = Category::all();
        return view('categories.subcategoryEdit')
            ->with('categories',$categories)
            ->with('subcategory',Subcategory::find($id));
    }

    public function update_subcategory(Request $request,$id){
        $this->validate($request,[
            'name' => 'required'
        ]);

        $category = Subcategory::find($id);

        $category->name = request()->name;
        $category->icon = request()->icon;
        $category->main_category_id = $request->main_category_id;

        $category->save();

        return redirect()->route('categories');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);

        $category->delete();

        return redirect()->back();
    }
}
