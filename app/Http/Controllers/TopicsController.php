<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Topic;
use App\Discussion;
use App\User;
use App\ActivityLog;
use Auth;
use Session;

class TopicsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $topics = Topic::all();
        $user = User::find(Auth::id());

        return view('forum.topics.index')
            ->with('topics',$topics)
            ->with('user', $user);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('forum.topics.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required'
        ]);

        $topic = Topic::create([
            'name' => $request->name
        ]);

        $activity = new ActivityLog;

        $activity->user_id = Auth::id();
        $activity->action_id = $topic->id;
        $activity->action_name = "topic_create";

        $activity->save();

        Session::flash('success','Topic created!');

        return redirect()->route('topics');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $topic = Topic::find($id);
        $discussions = Discussion::where('topic_id',$id)->get();

        return view('forum.topics.show')
            ->with('topic',$topic)
            ->with('discussions', $discussions)
            ->with('user',User::find(Auth::id()));;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $topic = Topic::find($id);
        return view('forum.topics.edit')->with('topic',$topic);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $topic = Topic::find($id);

        $topic->name = $request->name;

        $topic->save();

        return redirect()->route('topics');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $topic = Topic::find($id);
        $user = User::find(Auth::id());

        if($user->admin == 1){

            // Delete Discussions and Discussions activity
            foreach($topic->discussions as $discussion){
                // Delete Discussions likes

                foreach($discussion->discussionLike as $like){
                    $activity = ActivityLog::where('action_name','discussion_like')->where('action_id',$like->id)->first();
                    $activity->delete();

                    $like->delete();
                }

                // Delete Replies and activity
                foreach($discussion->replies as $reply){

                    // Delete ReplyLikes and activity
                    foreach($reply->replyLikes as $like){
                        $activity = ActivityLog::where('action_name','reply_like')->where('action_id',$like->id)->first();
                        $activity->delete();

                        $like->delete();
                    }


                    $activity = ActivityLog::where('action_name','discussion_reply')->where('action_id',$reply->id)->first();
                    $activity->delete();

                    $reply->delete();
                }

                // Delete Watchers and activity
                foreach($discussion->watchers as $watcher){
                    $activity = ActivityLog::where('action_name','discussion_watch')->where('action_id',$watcher->id)->first();
                    $activity->delete();

                    $watcher->delete();
                }

                // Delete Discussions and Discussions activity
                $activity = ActivityLog::where('action_name','discussion_create')->where('action_id',$discussion->id)->first();
                $activity->delete();

                $discussion->delete();
            }

            // Delete Topic from activity logs
            $activity = ActivityLog::where('action_name','topic_create')->where('action_id',$id)->first();
            $activity->delete();

            // Delete topic
            $topic->delete();
            Session::flash('success','Topic deleted');
        } else {
            Session::flash('failed','You dont have permission to do that!');
            return redirect()->back();
        }

        return redirect()->back();
    }
}
