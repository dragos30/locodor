<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\ActivityLog;
use Auth;
use Session;
use Illuminate\Support\Facades\DB;
use App\Meta;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = DB::table('posts')->orderBy('created_at', 'DESC')->paginate(21);
        $latest_posts = DB::table('posts')->orderBy('created_at', 'DESC')->take(5)->get();

        $meta_title = Meta::where('page','projectsIndex')->where('meta','title')->get()->first();
        $meta_description = Meta::where('page','projectsIndex')->where('meta','description')->get()->first();

        return view('posts.index')
            ->with('meta_title',$meta_title->value)
            ->with('meta_description',$meta_description->value)
            ->with('posts',$posts)
            ->with('latest_posts',$latest_posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'=>'required|max:255',
            'image' => 'required|image',
            'content' => 'required',
        ]);

        $image = $request->image;
        $image_new_name = time().$image->getClientOriginalName();
        $image_new_name = preg_replace('/\s+/', '', $image_new_name);
        $image->move('uploads/posts',$image_new_name);

        $post = new Post;

        $post->user_id = Auth::id();
        $post->title = $request->title;
        $post->slug = str_slug($post->title, '-');
        $post->content = $request->content;
        $post->image = 'uploads/posts/'.$image_new_name;

        $post->save();

        $activity = new ActivityLog;

        $activity->user_id = Auth::id();
        $activity->action_id = $post->id;
        $activity->action_name = "post_create";

        $activity->save();

        Session::flash('succes','Post created succesfully');

        return redirect()->route('posts');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,$slug)
    {
        $post = Post::find($id);

        $meta_title = $post->title;
        $meta_description = htmlspecialchars_decode(preg_replace( "/\r|\n/", " ", substr(strip_tags($post->content), 0, 155) ));
        $meta_image = $post->image;

        return view('posts.show')
            ->with('meta_title',$meta_title)
            ->with('meta_description',$meta_description)
            ->with('meta_image',$meta_image)
            ->with('post',$post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);

        return view('posts.edit')->with('post',$post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::find($id);

        $this->validate($request, [
            'title'=>'max:255|required',
            'content' => 'required',
            'image' => 'image',
        ]);

        if ($request->hasFile('image')) {
            $image = $request->image;
            $image_new_name = time().$image->getClientOriginalName();
            $image->move('uploads/posts',$image_new_name);
        }

        $post->title = $request->title;
        $post->content = $request->content;
        $post->image = 'uploads/posts/'.$image_new_name;

        $post->save();

        Session::flash('succes','Post updated succesfully');

        return redirect()->route('posts');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);

        $activity = ActivityLog::where('action_name','post_create')->where('action_id',$post->id)->first();
        $activity->delete();

        $post->delete();

        Session::flash('success','Post deleted');

        return redirect()->route('posts');
    }
}
