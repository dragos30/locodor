<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Discussion;
use App\Reply;
use App\User;
use App\DiscussionLike;
use App\ReplyLike;
use App\ActivityLog;
use Auth;
use Session;
use App\Watcher;

class DiscussionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($topic_id)
    {
        return view('discussions.create')->with('topic_id',$topic_id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$topic_id)
    {
        $this->validate($request, [
            'title'=>'required|max:255',
            'description' => 'required',
            'content' => 'required',
        ]);

        $discussion = Discussion::create([
            'user_id' => Auth::id(),
            'topic_id' => $topic_id,
            'title' => $request->title,
            'description' => $request->description,
            'content' => $request->content
        ]);

        $activity = new ActivityLog;

        $activity->user_id = Auth::id();
        $activity->action_id = $discussion->id;
        $activity->action_name = "discussion_create";

        $activity->save();

        Session::flash('success','Discussion created');

        return redirect()->route('topic.show',['id' => $topic_id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $discussion = Discussion::find($id);
        $replies = Reply::where('discussion_id',$id)->get();

        $likes = DiscussionLike::where('discussion_id',$id)->get();

        $liked = false;
        foreach($likes as $like){
            if($like->user_id == Auth::id()){
                $liked = true;
            }
        }

        $liked_replies = array();

        foreach($replies as $reply){
            foreach($reply->reply_likes as $reply_like){
                if($reply_like->user_id == Auth::id()){
                    array_push($liked_replies,$reply_like->reply_id);
                }
            }
        }

        $watching = false;

        $watchers = Watcher::where('discussion_id',$id)->get();

        foreach($watchers as $watcher){
            if($watcher->user_id == Auth::id()){
                $watching = true;
            }
        }

        return view('discussions.show')
            ->with('replies',$replies)
            ->with('discussion',$discussion)
            ->with('user',User::find(Auth::id()))
            ->with('liked',$liked)
            ->with('likes',$likes)
            ->with('liked_replies',$liked_replies)
            ->with('watching',$watching);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $discussion = Discussion::find($id);
        return view('discussions.edit')->with('discussion',$discussion);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title'=>'required|max:255',
            'description' => 'required',
            'content' => 'required',
        ]);

        $discussion = Discussion::find($id);

        $discussion->title = $request->title;
        $discussion->description = $request->description;
        $discussion->content = $request->content;

        $discussion->save();

        Session::flash('success','Discussion updated');

        return redirect()->route('discussion.show',['id' => $id]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $discussion = Discussion::find($id);

        // Delete Discussions likes

        foreach($discussion->discussionLike as $like){
            $activity = ActivityLog::where('action_name','discussion_like')->where('action_id',$like->id)->first();
            $activity->delete();

            $like->delete();
        }

        // Delete Replies and activity
        foreach($discussion->replies as $reply){

            // Delete ReplyLikes and activity
            foreach($reply->replyLikes as $like){
                $activity = ActivityLog::where('action_name','reply_like')->where('action_id',$like->id)->first();
                $activity->delete();

                $like->delete();
            }


            $activity = ActivityLog::where('action_name','discussion_reply')->where('action_id',$reply->id)->first();
            $activity->delete();

            $reply->delete();
        }

        // Delete Watchers and activity
        foreach($discussion->watchers as $watcher){
            $activity = ActivityLog::where('action_name','discussion_watch')->where('action_id',$watcher->id)->first();
            $activity->delete();

            $watcher->delete();
        }

        // Delete Discussions and Discussions activity
        $activity = ActivityLog::where('action_name','discussion_create')->where('action_id',$discussion->id)->first();
        $activity->delete();

        $discussion->delete();

        Session::flash('success','Discussion deleted');

        return redirect()->route('topic.show',['id' => $discussion->topic_id]);
    }

    public function like($id){
        $like = new DiscussionLike;
        $like->user_id = Auth::id();
        $like->discussion_id = $id;
        $like->save();

        $activity = new ActivityLog;

        $activity->user_id = Auth::id();
        $activity->action_id = $like->id;
        $activity->action_name = "discussion_like";

        $activity->save();

        Session::flash('success','Discussion liked');

        return redirect()->back();

    }

    public function unlike($id){
        $like = DiscussionLike::where('discussion_id',$id);
        $like->delete();

        Session::flash('success','Discussion unliked');

        return redirect()->back();

    }

    public function watch($id){

        $watch = new Watcher;

        $watch->discussion_id = $id;
        $watch->user_id = Auth::id();

        $watch->save();

        $activity = new ActivityLog;

        $activity->user_id = Auth::id();
        $activity->action_id = $watch->id;
        $activity->action_name = "discussion_watch";

        $activity->save();

        Session::flash('success','You are now watching this discussion');

        return redirect()->back();
    }

    public function unwatch($id){
        $watch = Watcher::where('discussion_id',$id)->where('user_id',Auth::id());

        $watch->delete();

        Session::flash('success','You are no longer watching this discussion');

        return redirect()->back();
    }
}
