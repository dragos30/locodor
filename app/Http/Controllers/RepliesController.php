<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use Notification;
use App\Reply;
use App\ReplyLike;
use App\User;
use App\Discussion;
use App\ActivityLog;

class RepliesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $discussion = Discussion::find($request->discussion_id);

        $this->validate($request,[
            'content' => 'required'
        ]);

        $reply = Reply::create([
            'user_id' => Auth::id(),
            'discussion_id' => $request->discussion_id,
            'content' => $request->content,
        ]);

        $watchers = array();

        foreach($discussion->watchers as $watcher){
            array_push($watchers, User::find($watcher->user_id));
        }

        Notification::send($watchers, new \App\Notifications\NewReplyAdded($discussion));

        $activity = new ActivityLog;

        $activity->user_id = Auth::id();
        $activity->action_id = $reply->id;
        $activity->action_name = "discussion_reply";

        $activity->save();

        Session::flash('success','Reply added');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $reply = Reply::find($id);

        return view('replies.edit')
            ->with('reply',$reply)
            ->with('user',User::find(Auth::id()));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'content' => 'required'
        ]);

        $reply = Reply::find($id);
        $reply->content = $request->content;
        $reply->save();

        Session::flash('success','Reply updated');

        return redirect()->route('discussion.show',['id' => $reply->discussion_id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $reply = Reply::find($id);
        if($reply->user_id == Auth::id()){
            // Delete ReplyLikes and activity
            foreach($reply->replyLikes as $like){
                $activity = ActivityLog::where('action_name','reply_like')->where('action_id',$like->id)->first();
                $activity->delete();

                $like->delete();
            }


            $activity = ActivityLog::where('action_name','discussion_reply')->where('action_id',$reply->id)->first();
            $activity->delete();

            $reply->delete();
        } else {
            Session::flash('failed','You dont have permission to do that!');
            return redirect()->back();
        }

        return redirect()->back();
    }

    public function like($id){
        $like = new ReplyLike;

        $like->user_id = Auth::id();
        $like->reply_id = $id;

        $like->save();

        $activity = new ActivityLog;

        $activity->user_id = Auth::id();
        $activity->action_id = $like->id;
        $activity->action_name = "reply_like";

        $activity->save();

        Session::flash('success','Reply liked');

        return redirect()->back();
    }

    public function unlike($id){
        $like = ReplyLike::where('reply_id',$id)->where('user_id',Auth::id());

        $like->delete();

        Session::flash('success','Reply unliked');

        return redirect()->back();
    }
}
