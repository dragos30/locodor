<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Hash;
use \Illuminate\Http\Request;
use Auth;
use App\User;
use App\Profile;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    public function redirectPath()
    {
        return '/user-profile/'. Auth::user()->profile->id.'/'. Auth::user()->profile->slug;
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function validateLogin(Request $request)
    {
        if(isset($_GET['bot-rmc'])){
            $this->validate($request, [
                $this->username() => 'required',
                'password' => 'required'
            ]);
        }else{
            $this->validate($request, [
                $this->username() => 'required',
                'password' => 'required',
                'g-recaptcha-response' => 'captcha'
            ]);
        }
    }

    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->user();

        if($user->id !== null){
            $user_check = User::where('email', '=', $user->id . '-'. $provider)->first();

            if ($user_check === null) {
                function getUserIP(){
                    if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
                        $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
                        $_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
                    }
                    $client  = @$_SERVER['HTTP_CLIENT_IP'];
                    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
                    $remote  = $_SERVER['REMOTE_ADDR'];

                    if(filter_var($client, FILTER_VALIDATE_IP)) {
                        $ip = $client;
                    }
                    elseif(filter_var($forward, FILTER_VALIDATE_IP)) {
                        $ip = $forward;
                    }
                    else {
                        $ip = $remote;
                    }

                    return $ip;
                }

                $ip = getUserIP();
                $details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));

                $new_user = new User;


                $new_user->name = $user->name;
                $new_user->email = $user->id . '-'. $provider;
                $new_user->user_ip = getUserIP();
                if(isset($details->city)) {
                    $new_user->city = $details->city;
                    $new_user->region = $details->region;
                    $new_user->country = $details->country;
                }
                $new_user->password = Hash::make($user->id);

                $new_user->activated = 1;

                $new_user->activation = '/'.$user->id;

                $expire = time();
                $activation_limit = date( "Y-m-d", $expire );

                $new_user->activation_limit = $activation_limit;

                $new_user->social_details = serialize($user);

                $string = $new_user->social_details;
                $pattern = '/[a-z0-9_\-\+\.]+@[a-z0-9\-]+\.([a-z]{2,4})(?:\.[a-z]{2})?/i';
                preg_match_all($pattern, $string, $matches);
                $new_user->contact_email = $matches[0][0];

                $new_user->save();

                $profile = new Profile;

                $profile->user_id = $new_user->id;
                $profile->about = '';
                $profile->avatar = $user->avatar;
                $profile->save();

                $email = 'yeron@locodor.com';

                $mail_content = $new_user->name . 'is our newest member <br>';
                $mail_content .= '<a href="'. route('profile.show',['id'=> $profile->id,'slug'=>$profile->slug]) .'">View User</a><br>';

                Mail::send([], [],
                    function ($message) use ($email,$mail_content) {
                        $message->subject('A new user registered');
                        $message->setBody($mail_content, 'text/html');
                        $message->to($email);
                    });

                Auth::login($new_user);
            } else {
                Auth::login($user_check);
            }

            return redirect()->route('home');
        } else {
            return redirect()->route('home');
        }
    }


}
