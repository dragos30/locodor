<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Profile;
use App\Lead;
use App\ActivityLog;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Auth;
use Mail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';
    protected $fetchedUser = '/';

    public function redirectPath()
    {
        if($this->fetchedUser != '/'){
            return $this->fetchedUser;
        } else {
            return '/user-profile/'. Auth::user()->profile->id.'/'. Auth::user()->profile->slug;
        }

    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');

        if(isset($_GET['quick-email'])){
            function getUserIP(){
                if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
                    $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
                    $_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
                }
                $client  = @$_SERVER['HTTP_CLIENT_IP'];
                $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
                $remote  = $_SERVER['REMOTE_ADDR'];

                if(filter_var($client, FILTER_VALIDATE_IP)) {
                    $ip = $client;
                }
                elseif(filter_var($forward, FILTER_VALIDATE_IP)) {
                    $ip = $forward;
                }
                else {
                    $ip = $remote;
                }

                return $ip;
            }

            $ip = getUserIP();
            $details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));


            $lead = new Lead;
            $lead->email = $_GET['quick-email'];
            $lead->user_ip = getUserIP();
            if(isset($details->city)) {
                $lead->city = $details->city;
                $lead->region = $details->region;
                $lead->country = $details->country;
            }

            $lead->save();
        }
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        if(isset($_GET['bot-rmc'])){
            return Validator::make($data, [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|string|min:6'
            ]);
        } else {
            return Validator::make($data, [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|string|min:6',
                'g-recaptcha-response' => 'required|captcha'
            ]);
        }


    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {




        if($_POST['email']){
            $data['email'] = $_POST['email'];
            $data['name'] = $_POST['name'];
            $data['password'] = $_POST['password'];
        }

//        if($data['url'] != null){
//            $this->fetchedUser = '/results-get?url=' . $data['url'];
//        }

        function getUserIP(){
            if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
                $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
                $_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
            }
            $client  = @$_SERVER['HTTP_CLIENT_IP'];
            $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
            $remote  = $_SERVER['REMOTE_ADDR'];

            if(filter_var($client, FILTER_VALIDATE_IP)) {
                $ip = $client;
            }
            elseif(filter_var($forward, FILTER_VALIDATE_IP)) {
                $ip = $forward;
            }
            else {
                $ip = $remote;
            }

            return $ip;
        }

        $ip = getUserIP();
        $details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));

        $user = new User;


        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->user_ip = getUserIP();
        if(isset($details->city)) {
            $user->city = $details->city;
            $user->region = $details->region;
            $user->country = $details->country;
        }
        $user->password = Hash::make($data['password']);

        $activation_link = Hash::make(strtotime($user->created_at).$user->name);
        $activation_link = str_replace(' ', '-', $activation_link);
        $activation_link = preg_replace('/[^A-Za-z0-9\-]/', '', $activation_link);

        $user->activation = $activation_link;

        $expire = time() + 30 * 24 * 60 *60;
        $activation_limit = date( "Y-m-d", $expire );

        $user->activation_limit = $activation_limit;

        $user->contact_email = $user->email;

        $user->save();

        $profile = new Profile;

        $profile->user_id = $user->id;
        $profile->about = '';

        $profile->slug = str_slug($user->name, '-');

        $random_avatar = rand(1, 6);
        $profile->avatar = '/uploads/avatars/'.$random_avatar.'.png';
        $profile->save();


        $mail_data = [
            'link' => $activation_link,
            'user_id' => $user->id
        ];

        Mail::to($data['email'])->send(new \App\Mail\RegisterConfirmation($mail_data));

        $email = 'yeron@locodor.com';

        $mail_content = $user->name . 'is our newest member <br>';
        $mail_content .= '<a href="'. route('profile.show',['id'=> $profile->id,'slug'=>$profile->slug]) .'">View User</a><br>';

        Mail::send([], [],
            function ($message) use ($email,$mail_content) {
                $message->subject('A new user registered');
                $message->setBody($mail_content, 'text/html');
                $message->to($email);
            });


        // Create activity log for new user
        $activity = new ActivityLog;

        $activity->user_id = $user->id;
        $activity->action_id = $user->id;
        $activity->action_name = 'new_user';

        $activity->save();

        return $user;
    }
}
