<?php

namespace App\Http\Controllers;

use App\Mail\OrderConfirmation;
use Illuminate\Http\Request;
use Auth;
use Mail;
use Stripe\Stripe;
use Stripe\Charge;
use App\Project;
use App\Backer;
use App\ActivityLog;
use App\ProjectSync;
use App\Watcher;
use App\Notification;
use App\User;

class BackersController extends Controller
{
//    public function pay(){
//        Stripe::setApiKey("sk_test_I0VLF5rDroIsoQhHLgws5xFQ");
//
//        $project = Project::find(request()->project_id);
//
//        $project->current_amount += request()->price;
//        $project->funding_percent = round($project->current_amount / $project->project_goal * 100);
//        $project->save();
//
//        $charge = Charge::create([
//            'amount' => request()->price*100,
//            'currency' => 'usd',
//            'description' => request()->description,
//            'source' => request()->stripeToken
//        ]);
//
//
//        $backer = new Backer;
//        $backer->user_id = Auth::check() ? Auth::id() : 0;
//        $backer->level_id = request()->level_id;
//        $backer->project_id = request()->project_id;
//        $backer->amount = request()->price;
//        $backer->save();
//
//        Mail::to($backer->user->email)->send(new OrderConfirmation($backer));
//
//        $activity_check = ActivityLog::where('action_id',$project->id)->where('action_name','project_funded')->first();
//
//        if(($project->current_amount + $project->projectSync->amount) >= $project->project_goal && $activity_check == null){
//            $activity = new ActivityLog;
//
//            if(Auth::check()){
//                $activity->user_id = Auth::id();
//            } else {
//                $activity->user_id = 99999;
//            }
//            $activity->action_id = $project->id;
//            $activity->action_name = 'project_funded';
//            $activity->save();
//        }
//
//        $activity2 = new ActivityLog;
//        if(Auth::check()){
//            $activity2->user_id = Auth::id();
//        } else {
//            $activity2->user_id = 99999;
//        }
//        $activity2->action_id = $project->id;
//        $activity2->action_name = 'new_backer';
//        $activity2->save();
//
//        if(($project->current_amount + $project->projectSync->amount) >= $project->project_goal){
//            $project->status = 'funded';
//            $project->save();
//        }
//
//        return redirect()->back();
//    }

    public function new_backer(){
        $project = Project::find(request()->project_id);

        if(!isset(request()->data)){
            $email = 'yeron@locodor.com';

            $mail_content = 'Fraudulent Backing attempt / Failed backing attempt<br>';
            $mail_content .= Auth::check() ? Auth::user()->name : 'A Guest' . ' tried to back the project ' . $project->title . ' but he was missing the payment confirmation details';

            Mail::send([], [],
                function ($message) use ($email,$mail_content) {
                    $message->subject('Fraudulent Backing attempt / Failed backing attempt');
                    $message->setBody($mail_content, 'text/html');
                    $message->to($email);
                });

            dd('Something went wrong');
        }

        $project->current_amount += request()->price;
        if($project->projectSync){
            $project->funding_percent = floor((( $project->current_amount + $project->projectSync->amount ) / $project->project_goal)*100);
        } else {
            $project->funding_percent = floor($project->current_amount / $project->project_goal * 100);
        }
        $project->save();

        if(($project->current_amount + $project->projectSync->amount) >= $project->project_goal){
            $project->status = 'funded';
            $project->save();
        }

        $backer = new Backer;
        $backer->user_id = Auth::check() ? Auth::id() : 0;
        $backer->level_id = request()->level_id;
        $backer->project_id = request()->project_id;
        $backer->amount = request()->price;
        $backer->data = serialize(request()->data);
        $backer->save();

        $watchers = Watcher::where('project_id',request()->project_id)->get();

        foreach($watchers as $watcher){
            $notification = New Notification;

            $notification->user_id = $watcher->user_id;
            $notification->action_id = $watcher->project_id;
            $notification->action_name = 'watcher_new_backer';
            $notification->status = 'new';
            $notification->save();
        }


        if(Auth::check()){
            Mail::to($backer->user->contact_email)->send(new OrderConfirmation($backer));
        }

        $activity2 = new ActivityLog;
        if(Auth::check()){
            $activity2->user_id = Auth::id();
        } else {
            $activity2->user_id = 99999;
        }
        $activity2->action_id = $backer->id;
        $activity2->action_name = 'new_backer';
        $activity2->save();



        $email = 'yeron@locodor.com';

        $mail_content = 'A new backer <br>';
        $mail_content .= Auth::check() ? $backer->user->name : 'A Guest' . ' has backed the project ' . $backer->project->title . ' with $' . $backer->amount . ' for ' . $backer->level->title;

        Mail::send([], [],
            function ($message) use ($email,$mail_content) {
                $message->subject('There is a new backer');
                $message->setBody($mail_content, 'text/html');
                $message->to($email);
            });

        return 'LOCO'. $backer->id .'-' . request()->data['orderID'];
    }

    public function admin_new_backer(Request $request){
        if(isset($request->bot_id)){
            $user = User::find($request->bot_id);
        }else{
            $user = Auth::user();
        }

        $project = Project::find(request()->project_id);

        $project->current_amount += request()->price;
        if($project->projectSync){
            $project->funding_percent = floor((( $project->current_amount + $project->projectSync->amount ) / $project->project_goal)*100);
        } else {
            $project->funding_percent = floor($project->current_amount / $project->project_goal * 100);
        }
        $project->save();

        if(($project->current_amount + $project->projectSync->amount) >= $project->project_goal){
            $project->status = 'funded';
            $project->save();
        }

        $backer = new Backer;
        $backer->user_id = $user->id;
        $backer->level_id = request()->level_id;
        $backer->project_id = request()->project_id;
        $backer->amount = request()->price;
        $backer->data = 'admin';
        $backer->save();

        $watchers = Watcher::where('project_id',request()->project_id)->get();

        foreach($watchers as $watcher){
            $notification = New Notification;

            $notification->user_id = $watcher->user_id;
            $notification->action_id = $watcher->project_id;
            $notification->action_name = 'watcher_new_backer';
            $notification->status = 'new';
            $notification->save();
        }


        if(Auth::check()){
            Mail::to($backer->user->contact_email)->send(new OrderConfirmation($backer));
        }

        $activity2 = new ActivityLog;
        if(Auth::check()){
            $activity2->user_id = $user->id;
        } else {
            $activity2->user_id = 99999;
        }
        $activity2->action_id = $backer->id;
        $activity2->action_name = 'new_backer';
        $activity2->save();

        return 'LOCO'. $backer->id .'-' . $backer->level->id . '-' . $backer->created_at->timestamp;
    }
}
