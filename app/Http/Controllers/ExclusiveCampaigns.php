<?php

namespace App\Http\Controllers;

use App\CampaignGeneralPlan;
use Illuminate\Http\Request;
use Auth;
use App\CampaignTask;
use App\Project;
use App\CampaignTeamMember;
use App\CampaignPerk;
use App\CampaignWeekPlan;

class ExclusiveCampaigns extends Controller
{
    public function campaign_tasks($id){
        $project = Project::find($id);
        if($project->user_id != Auth::id() || !$project->exclusive){
            return redirect()->back();
        }

        $campaign_tasks = CampaignTask::where('user_id',Auth::id())->where('project_id',$id)->get();
        if($campaign_tasks->count() === 0){
            for($i = 0; $i<34 ;$i++){
                $task = new CampaignTask;
                $task->user_id = Auth::id();
                $task->project_id = $id;
                $task->row_index = $i;
                $task->status = null;
                $task->assigned_to = null;
                $task->deadline = null;
                $task->notes = null;

                switch ($i) {
                    case 0:
                        $task->task = "Research your audience(who, why and where)	";
                        break;
                    case 1:
                        $task->task = "Legal matters (bank account and legal structure)	";
                        break;
                    case 2:
                        $task->task = "Get your team together	";
                        break;
                    case 3:
                        $task->task = "Research and choose the platform	";
                        break;
                    case 4:
                        $task->task = "Decide on the realistic goal	";
                        break;
                    case 5:
                        $task->task = "Brainstorm campaign ideas with your team	";
                        break;
                    case 6:
                        $task->task = "Build a timeline	";
                        break;
                    case 7:
                        $task->task = "Research your community and its online activity	";
                        break;
                    case 8:
                        $task->task = "Start growing your online and offline communities	";
                        break;
                    case 9:
                        $task->task = "Research potential partners	";
                        break;
                    case 10:
                        $task->task = "Research potential press connections	";
                        break;
                    case 11:
                        $task->task = "Research potential networks	";
                        break;
                    case 12:
                        $task->task = "Home your campaign pitch(what, why and to whom)	";
                        break;
                    case 13:
                        $task->task = "Test your pitch	";
                        break;
                    case 14:
                        $task->task = "Create a budget	";
                        break;
                    case 15:
                        $task->task = "Collect campaigns media (images, videos)	";
                        break;
                    case 16:
                        $task->task = "Determine perks	";
                        break;
                    case 17:
                        $task->task = "Test your perks with your target audience	";
                        break;
                    case 18:
                        $task->task = "Prepare a publishing plan for the campaign	";
                        break;
                    case 19:
                        $task->task = "Get your team to evaluate their availability	";
                        break;
                    case 20:
                        $task->task = "Prepare press releases / an EPK Website	";
                        break;
                    case 21:
                        $task->task = "Contact your networks to secure a 20-30% seed investment	";
                        break;
                    case 22:
                        $task->task = "Start soft launch (if applicable)	";
                        break;
                    case 23:
                        $task->task = "Press release about the launch	";
                        break;
                    case 24:
                        $task->task = "Keep growing your network	";
                        break;
                    case 25:
                        $task->task = "Launch your email campaign	";
                        break;
                    case 26:
                        $task->task = "Weekly email campaigns	";
                        break;
                    case 27:
                        $task->task = "Analize and tweak perks	";
                        break;
                    case 28:
                        $task->task = "Thank your supporters	";
                        break;
                    case 29:
                        $task->task = "Daily campaign updates (interviews, behind the scenes...)	";
                        break;
                    case 30:
                        $task->task = "Keep your partners informed about the campaign	";
                        break;
                    case 31:
                        $task->task = "Inform supporters about the campaign results	";
                        break;
                    case 32:
                        $task->task = "Deliver Perks	";
                        break;
                    case 33:
                        $task->task = "Send a post campaign press release	";
                        break;
                }

                $task->save();
            };

            $campaign_tasks = CampaignTask::where('user_id',Auth::id())->where('project_id',$id)->get();
        }

        $campaign_team_members = CampaignTeamMember::where('project_id',$id)->orderBy('created_at','desc')->get();
        $campaign_perks = CampaignPerk::where('project_id',$id)->orderBy('created_at','desc')->get();
        $campaign_weeks = CampaignWeekPlan::where('project_id',$id)->orderBy('created_at','desc')->get();
        $campaign_plans = CampaignGeneralPlan::where('project_id',$id)->orderBy('created_at','desc')->get();

        return view('pages.campaignTips')
            ->with('campaign_tasks',$campaign_tasks)
            ->with('campaign_perks',$campaign_perks)
            ->with('campaign_weeks',$campaign_weeks)
            ->with('campaign_plans',$campaign_plans)
            ->with('campaign_team_members',$campaign_team_members);
    }

    public function store_campaign_tasks(Request $request){

        foreach($request->row_index as $index){
            $updated_task = CampaignTask::where('project_id',$request->project_id)->where('user_id',Auth::id())->where('row_index',$index)->get()->first();


                $updated_task->status = $request->task_status[$index];

            $updated_task->assigned_to = $request->task_assigned[$index];
            $updated_task->deadline = $request->task_deadline[$index];
            $updated_task->notes = $request->task_note[$index];
            $updated_task->save();
        }

        return redirect()->back();
    }

    public function store_campaign_team(Request $request){
        $members = CampaignTeamMember::where('project_id',$request->project_id)->get();
        if($members->count() !== 0){
            foreach($members as $member){
                $member->delete();
            }
        }

        for($i = 0;$i < count($request->member_name);$i++){
            $member = new CampaignTeamMember;

            $member->project_id = $request->project_id;
            $member->name = $request->member_name[$i];
            $member->role = $request->member_role[$i];
            $member->availability = $request->member_availability[$i];
            $member->social_media_link = $request->member_link[$i];
            $member->email = $request->member_email[$i];
            $member->phone = $request->member_phone[$i];
            $member->notes = $request->member_note[$i];
            $member->save();
        }

        return redirect()->back();
    }

    public function store_campaign_perks(Request $request){
        $perks = CampaignPerk::where('project_id',$request->project_id)->get();
        if($perks->count() !== 0){
            foreach($perks as $perk){
                $perk->delete();
            }
        }

        for($i = 0;$i < count($request->perk_price);$i++){
            $perk = new CampaignPerk;
            $perk->project_id = $request->project_id;
            $perk->price = $request->perk_price[$i];
            $perk->cost = $request->perk_cost[$i];
            $perk->goal = $request->sales_goal[$i];
            $perk->name = $request->perk_name[$i];
            $perk->description = $request->perk_description[$i];
            $perk->tested = $request->perk_tested[$i];
            $perk-> notes= $request->perk_notes[$i];
            $perk->save();
        }

        return redirect()->back();
    }

    public function store_week_plan(Request $request){
        $weeks = CampaignWeekPlan::where('project_id',$request->project_id)->get();
        if($weeks->count() !== 0){
            foreach($weeks as $week){
                $week->delete();
            }
        }

        for($i = 0;$i < count($request->focus);$i++){
            $week = new CampaignWeekPlan;

            $week->project_id = $request->project_id;
            $week->focus = $request->focus[$i];
            $week->monday_channel1 = $request->social_channel_monday_1[$i];
            $week->monday_channel2 = $request->social_channel_monday_2[$i];
            $week->monday_email = $request->email_monday[$i];
            $week->tuesday_channel1 = $request->social_channel_tuesday_1[$i];
            $week->tuesday_channel2 = $request->social_channel_tuesday_2[$i];
            $week->tuesday_email = $request->email_tuesday[$i];
            $week->wednesday_channel1 = $request->social_channel_wednesday_1[$i];
            $week->wednesday_channel2 = $request->social_channel_wednesday_2[$i];
            $week->wednesday_email = $request->email_wednesday[$i];
            $week->thursday_channel1 = $request->social_channel_thursday_1[$i];
            $week->thursday_channel2 = $request->social_channel_thursday_2[$i];
            $week->thursday_email = $request->email_thursday[$i];
            $week->friday_channel1 = $request->social_channel_friday_1[$i];
            $week->friday_channel2 = $request->social_channel_friday_2[$i];
            $week->friday_email = $request->email_friday[$i];
            $week->saturday_channel1 = $request->social_channel_saturday_1[$i];
            $week->saturday_channel2 = $request->social_channel_saturday_2[$i];
            $week->saturday_email = $request->email_saturday[$i];
            $week->sunday_channel1 = $request->social_channel_sunday_1[$i];
            $week->sunday_channel2 = $request->social_channel_sunday_2[$i];
            $week->sunday_email = $request->email_sunday[$i];
            $week->save();
        }
        return redirect()->back();
    }

    public function store_general_plan(Request $request){
        $plans = CampaignGeneralPlan::where('project_id',$request->project_id)->get();
        if($plans->count() !== 0){
            foreach($plans as $plan){
                $plan->delete();
            }
        }

        for($i = 0;$i < count($request->role);$i++){
            $plan = new CampaignGeneralPlan;

            $plan->project_id = $request->project_id;
            $plan->status = $request->task_status[$i];
            $plan->roles = $request->role[$i];
            $plan->availability = $request->availability[$i];
            $plan->pricing = $request->pricing[$i];
            $plan->commission = $request->commission[$i];
            $plan->general = $request->general[$i];
            $plan->focus = $request->focus[$i];
            $plan->social_media = $request->social_media[$i];
            $plan->email = $request->email[$i];

            $plan->save();
        }
        return redirect()->back();
    }

    public function make_exclusive($id){
        $project = Project::find($id);

        $project->exclusive = true;
        $project->save();

        return redirect()->back();
    }
}
