<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserPost;
use App\ActivityLog;
use App\Notification;
use Auth;
use Session;
use Embed\Embed;

class UserPostsController extends Controller
{
    public function store(Request $request){

        $this->validate($request, [
            'content' => 'required'
        ]);

        $image = $request->file;

//        if ($request->hasFile('file')) {
//            $image_new_name = time().$image->getClientOriginalName();
//            $image_new_name = preg_replace('/\s+/', '', $image_new_name);
//            $image->move('uploads/activityPosts',$image_new_name);
//        }

        $user_post = new UserPost;


        $text = strip_tags($request->content,'<a><ul><ol><li><b><u><i><img>');
        $parsedLinks = [];
        $reg_exUrl = '/(?:(?:https?|ftp):\/\/)(?:(?:[^\s()<>]+|\((?:[^\s()<>]+|(?:\([^\s()<>]+\)))?\))+(?:\((?:[^\s()<>]+|(?:\(?:[^\s()<>]+\)))?\)|[^\s`!()\[\]{};:\'\".,<>?«»“”‘’]))?/';
        preg_match_all($reg_exUrl, $text, $matches);
        foreach($matches[0] as $match){
            $info = Embed::create($match);
            $parsedLinks[] = $info;
        }
        $text = preg_replace($reg_exUrl, '', $text);
        if(count($parsedLinks)){
            if($parsedLinks[0]->description){
                $finalText = '<a href="'. $parsedLinks[0]->url .'" rel="nofollow" target="_blank"><img src="'. $parsedLinks[0]->image .'" width="100%"><div class="post-url-title">'. $parsedLinks[0]->title .'</div><div class="post-url-description">'. $parsedLinks[0]->description .'</div>'.'</a><p class="original-user-comment">'.$text.'</p>';
            } else {
                $finalText = '<a href="'. $parsedLinks[0]->url .'" rel="nofollow" target="_blank"><img src="'. $parsedLinks[0]->image .'" width="100%"><div class="post-url-title">'. $parsedLinks[0]->title .'</div>'.'</a><p class="original-user-comment">'.$text.'</p>';
            }
        }else{
            $finalText = $text;
        }


        $user_post->content = $finalText;

//        if ($request->hasFile('file')) {
//            $user_post->image = 'uploads/activityPosts/' . $image_new_name;
//        }
        $user_post->image = preg_replace('/\s+/', '%20', $request->file);
        $user_post->user_id = Auth::id();

        $user_post->save();



        $activity = new ActivityLog;

        $activity->user_id = Auth::id();
        $activity->action_id = $user_post->id;
        $activity->action_name = "user_post_create";

        $activity->save();


        function get_string_between($string, $start, $end){
            $string = ' ' . $string;
            $ini = strpos($string, $start);
            if ($ini == 0) return '';
            $ini += strlen($start);
            $len = strpos($string, $end, $ini) - $ini;
            return substr($string, $ini, $len);
        }

        preg_match_all("/<a[^>]*>([^<]+)<\/a>/",$finalText,$mentions);


        $users_ids = [];

        foreach($mentions[0] as $mention){
            $users_ids[] = get_string_between($mention, 'user-profile/', '/');
        }

        foreach($users_ids as $user){
            $notification = new Notification;

            $notification->user_id = $user;
            $notification->action_id = $activity->id;
            $notification->action_name = 'mention';
            $notification->status = 'new';

            $notification->save();
        }

        Session::flash('success','You comment was succesfully posted');

        return redirect()->route('activity');
    }

    public function user_post_delete($id){
        $post = UserPost::find($id);
        $activityLog = ActivityLog::where('action_id',$id)->where('action_name','user_post_create')->first();
        $notifications = Notification::where('action_id',$activityLog->id)->where('action_name','mention')->get();

        if(isset($notifications)){
            foreach($notifications as $notification){
                $notification->delete();
            }
        }

        foreach($activityLog->activityLikes as $like){
            $like->delete();
        }

        foreach($activityLog->activityComments as $comment){
            $comment->delete();
        }

        $activityLog->delete();
        $post->delete();

        Session::flash('User Post deleted succesfully');

        return redirect()->back();
    }
}
