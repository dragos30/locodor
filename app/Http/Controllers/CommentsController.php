<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use App\CommentLike;
use App\User;
use App\ActivityLog;
use App\Project;
use App\Notification;
use App\Watcher;
use Session;
use Auth;

class CommentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'comment' => 'required'
        ]);

//        $comment = Comment::create([
//            'user_id' => Auth::id(),
//            'project_id' => $request->project_id,
//            'comment' => $request->comment
//        ]);

        $text = $request->comment;
        $reg_exUrl = '/(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9]\.[^\s]{2,})/';
        if (preg_match_all($reg_exUrl, $text, $matches)) {
            foreach ($matches[0] as $i => $match) {
                $text = str_replace(
                    $match,
                    '<a href="'.$matches[0][$i].'" rel="nofollow">'.$matches[0][$i].'</a>',
                    $text
                );
            }
        }

        $project = Project::find($request->project_id);

        $comment = new Comment;
        $comment->user_id = Auth::id();
        $comment->project_id = $request->project_id;
        $comment->comment = strip_tags($text,'<a>');
        $comment->save();

        $watchers = Watcher::where('project_id',request()->project_id)->get();

        foreach($watchers as $watcher){
            $notification = New Notification;

            $notification->user_id = $watcher->user_id;
            $notification->action_id = $comment->id;
            $notification->action_name = 'watcher_new_comment';
            $notification->status = 'new';
            $notification->save();
        }





        $project->comments_number++;
        $project->save();

        $activity = new ActivityLog;

        $activity->user_id = Auth::id();
        $activity->action_id = $comment->id;
        $activity->action_name = "project_comment";

        $activity->save();

        $notification = New Notification;

        $notification->user_id = $project->user_id;
        $notification->action_id = $comment->id;
        $notification->action_name = 'project_new_comment';
        $notification->status = 'new';
        $notification->save();

        Session::flash('success','Comment posted');


        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $comment = Comment::find($id);
        return view('comments.edit')
            ->with('comment',$comment)
            ->with('user',User::find(Auth::id()));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $comment = Comment::find($id);

        $comment->comment = $request->comment;
        $comment->save();

        Session::flash('success','Comment updated');

        return redirect()->route('project.show',['id' => $comment->project->id,'slug' => $comment->project->slug]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comment= Comment::find($id);



        $activity = ActivityLog::where('action_name','project_comment')->where('action_id',$id)->get()->first();
        if(isset($activity)){
            $activity->delete();
        }

        foreach($comment->commentLikes as $like){
            $like->delete();
        }

        $comment->delete();

        $notifications_watchers = Notification::where('action_id',$id)->where('action_name','watcher_new_comment')->get();
        $notifications_owners = Notification::where('action_id',$id)->where('action_name','project_new_comment')->get();
        $notifications = $notifications_watchers->merge($notifications_owners);

        foreach($notifications as $notification){
            $notification->delete();
        }


        $project = $comment->project;
        $project->comments_number = $project->comments_number - 1;
        $project->save();

        Session::flash('success','Comment erased');

        return redirect()->back();
    }

    public function like($id){
        $like = new CommentLike;

        $like->user_id = Auth::id();
        $like->comment_id = $id;

        $like->save();

        Session::flash('success','Comment liked');

        return redirect()->back();
    }

    public function unlike($id){

        $like = CommentLike::where('comment_id',$id)->where('user_id',Auth::id())->first();

        $like->delete();

        return redirect()->back();
    }
}
