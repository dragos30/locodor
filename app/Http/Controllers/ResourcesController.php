<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Resource;
use Session;

class ResourcesController extends Controller
{
    public function index(){
        $resources = Resource::all();
        return view('crowdfunding-resources.index')->with('resources',$resources);
    }

    public function create(){
        return view('crowdfunding-resources.create');
    }

    public function store(Request $request){
        $resource = new Resource;
        $resource->image = $request->image;
        $resource->title = $request->title;
        $resource->content = $request->content;
        $resource->tags = isset($request->tags)? $string = str_replace(' ', '', $request->tags) : null;
        $resource->link = $request->link;
        $resource->slug = str_slug($request->title, '-');
        $resource->save();

        Session::flash('success','Resource created');

        return redirect()->back();
    }

    public function show($id){
        $resource = Resource::find($id);

        return view('crowdfunding-resources.show')->with('resource',$resource);
    }

    public function edit($id){
        $resource = Resource::find($id);
        return view('crowdfunding-resources.edit')->with('resource',$resource);
    }

    public function update($id,Request $request){
        $resource = Resource::find($id);

        $resource->image = $request->image;
        $resource->title = $request->title;
        $resource->content = $request->content;
        $resource->link = $request->link;
        $resource->tags = isset($request->tags)? $string = str_replace(' ', '', $request->tags) : null;
        $resource->slug = str_slug($request->title, '-');
        $resource->save();

        Session::flash('success','Resource updated');

        return redirect()->back();
    }

    public function delete($id){
        $resource = Resource::find($id);
        $resource->delete();
        return redirect()->back();
    }
}
