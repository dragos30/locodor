<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use Cookie;
use Illuminate\Support\Facades\DB;
use App\Project;
use App\Discussion;
use App\Backer;
use App\Level;
use App\Post;
use App\User;
use App\Profile;
use App\UserPost;
use App\Friend;
use App\Meta;
use App\Notification;
use App\Resource;

class PagesController extends Controller
{
    public function chatroom(){
        return view('pages.chatroom');
    }

    public function results(){
        $projects = Project::where('title','like', '%' . request('query') . '%')->get();
        $discussions = Discussion::where('title','like', '%' . request('query') . '%')->get();
        $levels = Level::where('title','like', '%' . request('query') . '%')->get();
        $posts = Post::where('title','like', '%' . request('query') . '%')->get();
        $users = User::where('name','like', '%' . request('query') . '%')->get();
        $resources = Resource::where('title','like','%' . request('query') . '%')->get();


        $friend1 = Friend::where('user1_id',Auth::id())->where('status','accepted')->pluck('user2_id')->toArray();
        $friend2 = Friend::where('user2_id',Auth::id())->where('status','accepted')->pluck('user1_id')->toArray();

        $friends = array_merge($friend1,$friend2);
        $sentFriendRequests = Friend::where('user1_id',Auth::id())->where('status','pending')->pluck('user2_id')->toArray();
        $receivedFriendRequests = Friend::where('user2_id',Auth::id())->where('status','pending')->pluck('user1_id')->toArray();

        return view('pages.results')
            ->with('query', request('query'))
            ->with('projects',$projects)
            ->with('discussions',$discussions)
            ->with('levels',$levels)
            ->with('resources',$resources)
            ->with('users',$users)
            ->with('friends',$friends)
            ->with('sentFriendRequests',$sentFriendRequests)
            ->with('receivedFriendRequests',$receivedFriendRequests)
            ->with('posts',$posts);
    }

    public function homepage(){

        $slider_projects = Project::where('approval','approved')->inRandomOrder()->get()->take(3);
        $featured_projects = Project::where('approval','approved')->orderBy('created_at','desc')->get()->take(3);
        $recent_backers = Backer::orderby('created_at','desc')->take(24)->get();
        $users = User::orderby('created_at','desc')->take(8)->get();
        $recent_posts = Post::orderby('created_at','desc')->get()->take(4);
        $popular_userposts = UserPost::orderby('likes','desc')->get()->take(4);
        $projects = Project::where('approval','approved')->inRandomOrder()->take(6)->get();
        $tom = User::find(2406);

        $friend1 = Friend::where('user1_id',Auth::id())->where('status','accepted')->pluck('user2_id')->toArray();
        $friend2 = Friend::where('user2_id',Auth::id())->where('status','accepted')->pluck('user1_id')->toArray();

        $friends = array_merge($friend1,$friend2);
        $sentFriendRequests = Friend::where('user1_id',Auth::id())->where('status','pending')->pluck('user2_id')->toArray();
        $receivedFriendRequests = Friend::where('user2_id',Auth::id())->where('status','pending')->pluck('user1_id')->toArray();

        $meta_title = Meta::where('page','homepage')->where('meta','title')->get()->first();
        $meta_description = Meta::where('page','homepage')->where('meta','description')->get()->first();

//        if(Cookie::get('popup') != true){
//            echo Cookie::get('popup');
//        }

        $cookie = cookie('popup', 'displayed', 1440);




        return view('welcome')
            ->with('cookie',$cookie)
            ->with('meta_title',$meta_title->value)
            ->with('meta_description',$meta_description->value)
            ->with('featured_projects',$featured_projects)
            ->with('slider_projects',$slider_projects)
            ->with('recent_backers',$recent_backers)
            ->with('users',$users)
            ->with('tom',$tom)
            ->with('recent_posts',$recent_posts)
            ->with('projects',$projects)
            ->with('friends',$friends)
            ->with('sentFriendRequests',$sentFriendRequests)
            ->with('receivedFriendRequests',$receivedFriendRequests)
            ->with('popular_userposts',$popular_userposts);
    }

    public function how_it_works(){
        $tom = User::find(2406);
        return view('pages.howitworks')
            ->with('tom',$tom);
    }

    public function members(){
//        $users = User::orderBy('created_at','desc')->get();

        if(isset($_GET['sort']) && $_GET['sort'] == 'crowdfunders'){
            $users = User::join('projects', 'users.id', '=', 'projects.user_id')
                ->select('users.*')
                ->distinct()
                ->paginate(60);
        }

        if(isset($_GET['sort']) && $_GET['sort'] == 'backers'){
            $users = User::join('backers', 'users.id', '=', 'backers.user_id')
                ->select('users.*')
                ->distinct()
                ->paginate(60);
        }

        if(!isset($_GET['sort'])){
            $users = User::orderby('created_at','desc')->paginate(60);
        }





//        $loggedUserFriends = Friend::where('user1_id',Auth::id())->orWhere('user2_id',Auth::id())->get();
//        $loggedUserFriendsId = array();
//        $receivedFriendRequests = array();
//        foreach($loggedUserFriends as $friend){
//            if($friend->user1_id == Auth::id()){
//                array_push($loggedUserFriendsId,$friend->user2_id);
//            } else {
//                array_push($loggedUserFriendsId,$friend->user1_id);
//                if($friend->status == 'pending'){
//                    $receivedFriendRequests[$friend->user1_id] = $friend->id;
//                }
//            }
//        }

        $friend1 = Friend::where('user1_id',Auth::id())->where('status','accepted')->pluck('user2_id')->toArray();
        $friend2 = Friend::where('user2_id',Auth::id())->where('status','accepted')->pluck('user1_id')->toArray();

        $friends = array_merge($friend1,$friend2);
        $sentFriendRequests = Friend::where('user1_id',Auth::id())->where('status','pending')->pluck('user2_id')->toArray();
        $receivedFriendRequests = Friend::where('user2_id',Auth::id())->where('status','pending')->pluck('user1_id')->toArray();

        $meta_title = Meta::where('page','projectsIndex')->where('meta','title')->get()->first();
        $meta_description = Meta::where('page','projectsIndex')->where('meta','description')->get()->first();

        return view('pages.members')
            ->with('meta_title',$meta_title)
            ->with('meta_description',$meta_description)
            ->with('users',$users)
            ->with('friends',$friends)
            ->with('sentFriendRequests',$sentFriendRequests)
            ->with('receivedFriendRequests',$receivedFriendRequests);
    }

    public function notifications(){
        $all_notifications = Notification::where('user_id',Auth::id())->orderby('created_at','desc')->get();

        return view('pages.notifications')->with('all_notifications',$all_notifications);
    }

    public function desktop_notifications(){
        return view('pages.desktopNotifications');
    }

    public function test(){
        return view('pages.test');
    }

    public function tom(){
        return view('pages.tom');
    }

    public function campaign_tips(){
        return view('pages.campaignTips');
    }

    public function evaluate_campaign(){
        return view('pages.evaluateCampaign');
    }

    public function evaluate(){
        return view('pages.evaluate');
    }

    public function campaign_tasks_example(){
        return view('pages.campaignTipsExample');
    }
}