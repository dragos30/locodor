<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use Session;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard')
            ->with('user',Auth::user());
    }

    public function activate($activation_link,$user_id){

        $user = User::find($user_id);

        $activation_limit = strtotime($user->activation_limit);

        if($user->activation == $activation_link && $activation_limit > time()){
            $user->activated = 1;
            $user->save();

            return view('pages.activation')->with('activated',1);
        } else {
            return view('pages.activation')->with('activated',0);
        }
    }
}
