<?php

namespace App\Http\Controllers;

use App\ActivityLog;
use App\Collaborator;
use App\EmailTemplate;
use App\Jobs\SendAdminEmails;
use App\Jobs\SendUnreadNotificationsEmailJob;
use App\Mail\OrderConfirmation;
use App\Watcher;
use Illuminate\Http\Request;
use App\Locodoruser;
use App\User;
use App\Profile;
use App\Project;
use App\Backer;
use App\Post;
use App\ProjectSync;
use App\Meta;
use App\Affiliate;
use App\Product;
use App\Conversation;
use App\Message;
use App\Notification;
use App\Rank;
use App\Help;
use App\ProjectSlide;
use App\SubscribedUser;
use App\AllEmails;
use App\Lead;
use App\Level;
use Illuminate\Support\Facades\Route;
use Mail;
use Auth;
use Session;
use Illuminate\Support\Facades\Artisan;


class AdminsDashboardController extends Controller
{
    public function index(){
        $projects = Project::where('approval','pending')->orderby('updated_at','desc')->get();
        $draft_projects = Project::where('approval','draft')->orderby('updated_at','desc')->get();
        $products = Product::where('status','pending')->orderby('updated_at','desc')->get();
        $requests = Help::orderBy('created_at','desc')->get();
        $admins = User::where('admin',1)->get();

        return view('admin.index')
            ->with('admins',$admins)
            ->with('requests',$requests)
            ->with('projects',$projects)
            ->with('draft_projects',$draft_projects)
            ->with('products',$products);
    }

    public function switch_account(Request $request){
        Auth::loginUsingId($request->admin_id);

        return redirect()->back();
    }

    public function projects(){
        $projects = Project::orderby('updated_at','desc')->get();
        $backers = Backer::orderby('created_at','desc')->get();
        return view('admin.projects')->with('projects',$projects);
    }

    public function posts(){
        $posts = Post::orderby('created_at','desc')->get();
        return view('admin.posts')->with('posts',$posts);
    }

    public function user_search(){
        $users = User::where('name','like', '%' . request('user_query') . '%')->orWhere('email','like', '%' . request('user_query') . '%')->get();

        return view('admin.userSearch')->with('users',$users);
    }

    public function users_table(){
        $users = User::orderby('created_at','desc')->get();

        return view('admin.usersTable')->with('users',$users);
    }

    public function user($id){
        $user = User::find($id);
        return view('admin.user')->with('user',$user);
    }

    public function import_users(){
        $users = Locodoruser::all();


        foreach($users as $u){
            $checkEmail = User::where('email',$u->user_email)->get();

            if($checkEmail->count() != 1){
                $user = new User;


                $user->name = $u->display_name;
                $user->email = $u->user_email;

                $user->password = $u->user_pass;
                $user->user_ip = '127.0.0.1';

                $user->activation = 'wordpress';
                $user->activated = 1;

                $expire = time() + 30 * 24 * 60 *60;
                $activation_limit = date( "Y-m-d", $expire );

                $user->activation_limit = $activation_limit;

                $user->save();

                $profile = new Profile;

                $profile->user_id = $user->id;
                $profile->about = '';
                $profile->avatar = '/uploads/avatars/no-avatar.png';
                $profile->save();
            }


        }


        return view('admin.importusers')->with('users',$users);
    }

    public function project_syncs(){
        $projects = Project::where('approval','approved')->get();
        return view('admin.syncs')->with('projects',$projects);
    }

    public function project_sync($id){
        $project = project::Find($id);
        $sync = ProjectSync::where('project_id',$id)->get()->first();
        if($sync == null){
            $sync = new ProjectSync;
            $sync->project_id = $id;
            $sync->views = 0;
            $sync->backers = 0;
            $sync->amount = 0;
            $sync->save();
        }

        return view('admin.project_sync')
            ->with('project',$project)
            ->with('sync',$sync);
    }

    public function add_sync(Request $request){
        $sync = ProjectSync::where('project_id',$request->project_id)->get()->first();

        $sync->project_id = $request->project_id;
        $sync->views = $request->views;
        $sync->backers = $request->backers;
        $sync->amount = $request->amount;

        $sync->save();

        $project = Project::find($request->project_id);
        $project->funding_percent = round($project->current_amount + $request->amount / $project->project_goal * 100);
        $project->save();

        return redirect()->back();
    }

    public function change_project_author($id){
        $project = Project::find($id);
        $users = User::orderBy('name','asc')->get();

        return view('admin.projectAuthorChange')
            ->with('project',$project)
            ->with('users',$users);
    }

    public function save_author_change(Request $request){
        $project = Project::find($request->project_id);

        $project->user_id = $request->user_id;
        $project->save();

        return redirect()->back();
    }

    public function meta_tags(){
        $meta_tags = Meta::all();

        return view('admin.metaTags')->with('meta_tags',$meta_tags);
    }

    public function save_meta(Request $request){
        $meta = Meta::where('page',$request->page)->where('meta',$request->meta)->get()->first();

        $meta->value = $request->value;
        $meta->save();

        return redirect()->back();
    }

    public function email(){
        $templates = EmailTemplate::all();
        return view('admin.email')->with('templates',$templates);
    }

    public function send_mails(Request $request){

        $records = explode(PHP_EOL, $request->emails);

        foreach($records as $record){
            if (preg_match('/"([^"]+)"/', $record, $m) != 0) {
                $string_to_remove = $m[0];
                $emails = explode(",", $m[1]);


                foreach($emails as $email){
                    $new_record = str_replace ( $string_to_remove , $email , $record );

                    $current = explode(",", $new_record);
                    array_unshift($current , $request->body);
                    array_unshift($current , $request->subject);


                    SendAdminEmails::dispatch($current)->delay(now()->addSeconds(1));
                }

            } else {
                $current = explode(",", $record);
                array_unshift($current , $request->body);
                array_unshift($current , $request->subject);

                SendAdminEmails::dispatch($current)->delay(now()->addSeconds(1));
            }




        }

        Artisan::call('queue:work', ['--stop-when-empty' => true,'--tries'=>3]);


        return redirect()->back();

    }

    public function save_email_template(Request $request){
        $template = new EmailTemplate;
        $template->name = $request->name;
        $template->content = $request->content;
        $template->save();

        return redirect()->back();
    }

    public function delete_email_template($id){
        $template = EmailTemplate::find($id);
        $template->delete();

        return 'success';
    }

    public function announcement(){
        return view('admin.announcement');
    }

    public function send_announcement(Request $request){

        $users = User::all()->pluck('id')->toArray();

        foreach($users as $id){
            $conversation = Conversation::where('user1_id',1)->where('user2_id',$id)->orWhere('user1_id',$id)->where('user2_id',1)->first();

            if(!isset($conversation)){
                $conversation = new Conversation;
                $conversation->user1_id = Auth::id();
                $conversation->user2_id = $id;
                $conversation->save();
            }

            $message = new Message;

            $message->from = 1;
            $message->to = $id;
            $message->conversation_id = $conversation->id;
            $message->message = $request->message;
            $message->status = 1;
            $message->save();

            $notification = New Notification;

            $notification->user_id = $id;
            $notification->action_id = $conversation->id;
            $notification->action_name = 'conversation_new_message';
            $notification->status = 'new';
            $notification->save();
        }


        return redirect()->back();
    }

    public function ranks(){
        $users = User::all();
        $ranks = Rank::all();

        return view('admin.ranks')
            ->with('users',$users)
            ->with('ranks',$ranks);
    }

    public function rank_store(Request $request){
        $rank = new Rank;
        $rank->name = $request->name;
        $rank->icon = $request->icon;
        $rank->save();

        return redirect()->back();
    }

    public function rank_update(Request $request){
        $rank = Rank::find($request->rank_id);

        $rank->name = $request->name;
        $rank->icon = $request->icon;
        $rank->save();

        return redirect()->back();
    }

    public function set_rank(Request $request){
        $user = User::find($request->user);

        $user->rank_id = $request->rank;
        $user->save();

        return redirect()->back();
    }

    public function got_idea(Request $request){

        if($request->capcha != ''){
            $email = 'yeron@locodor.com';

            $mail_content = 'Name: '.$request->name . "<br>";
            $mail_content .= 'Email: '.$request->email . "<br>";
            $mail_content .= 'Idea: '. $request->idea . "<br>";
            if($request->campaign != null){
                $mail_content .= 'Old Campaign Link: '. $request->campaign;
            }

            Mail::send([], [],
                function ($message) use ($email,$mail_content) {
                    $message->subject('A new Idea has been submitted');
                    $message->setBody($mail_content, 'text/html');
                    $message->to($email);
                });

            return redirect()->back();
        }else{
            return false;
        }
    }

    public function emails_unread_notifications(){

        $users_ids = Notification::where('status','new')->distinct()->pluck('user_id')->all();
        $delay_seconds=1;

        foreach($users_ids as $id){
            SendUnreadNotificationsEmailJob::dispatch($id)
                ->delay(now()->addSeconds($delay_seconds));
            $delay_seconds++;
        }

//        $id = 1;
//
//        SendUnreadNotificationsEmailJob::dispatch($id)
//            ->delay(now()->addSeconds(1));

        Session::flash('success','Emails queued for sending');
        return redirect()->back();
    }

    public function need_help(Request $request){
        $help = new Help;

        if(isset($request->user_id)) {
            $user = User::find($request->user_id);
            $help->user_id = $user->id;
        }

        if(isset($request->email)){
            $help->email = $request->email;
            $help->message = $request->message;
        }else{
            $help->email = $user->email;
        }
        $help->save();

        $email = 'yeron@locodor.com';

        $mail_content = 'There is a new help request <br>';
        $mail_content .= 'from:'.$help->email;

        Mail::send([], [],
            function ($message) use ($email,$mail_content) {
                $message->subject('New Help Request');
                $message->setBody($mail_content, 'text/html');
                $message->to($email);
            });

        Session::flash('success','We received you message! Thank you, we will contact you ASAP');

        return redirect()->back();
    }

    public function help_requests(){
        $requests = Help::orderBy('created_at','desc')->get();

        return view('admin.helpRequests')->with('requests',$requests);
    }

    public function projectsSlider(){
        $projects = Project::where('approval','approved')->get();
        $slides = ProjectSlide::all();
        return view('admin.projectsSlider')
            ->with('projects',$projects)
            ->with('slides',$slides);
    }

    public function add_project_slide(Request $request){

        $slide = new ProjectSlide;
        $slide->project_id = $request->project_id;
        $slide->background1 = $request->background1;
        $slide->background2 = $request->background2;
        $slide->order = $request->order;
        $slide->save();

        return redirect()->back();

    }

    public function delete_project_slide($id){
        $slide = ProjectSlide::find($id);
        $slide->delete();

        return redirect()->back();
    }

    public function check_projects_status(){
        $projects = Project::all();

        foreach($projects as $project){
            if(isset($project->projectSync->amount)){
                if(($project->current_amount + $project->projectSync->amount) >= $project->project_goal){
                    $project->status = 'funded';
                    $project->save();
                }

                if(($project->current_amount + $project->projectSync->amount) < $project->project_goal && time() < $project->end_date){
                    $project->status = 'failed';
                    $project->save();
                }
            }else{
                if($project->current_amount >= $project->project_goal){
                    $project->status = 'funded';
                    $project->save();
                }

                if($project->current_amount < $project->project_goal && time() < $project->end_date){
                    $project->status = 'failed';
                    $project->save();
                }
            }

            if($project->projectSync){
                $project->funding_percent = floor((( $project->current_amount + $project->projectSync->amount ) / $project->project_goal)*100);
            } else {
                $project->funding_percent = floor($project->current_amount / $project->project_goal * 100);
            }
        }

        return redirect()->back();
    }

    public function fetch_all_emails(){
        AllEmails::truncate();

        $users = User::all();
        foreach($users as $user){
            if (filter_var($user->contact_email, FILTER_VALIDATE_EMAIL)) {
                AllEmails::updateOrCreate(
                    ['email' => $user->contact_email]
                );
            }
        }

        $users = Lead::all();
        foreach($users as $user){
            if (filter_var($user->email, FILTER_VALIDATE_EMAIL)) {
                AllEmails::updateOrCreate(
                    ['email' => $user->email]
                );
            }
        }

        $users = Help::all();
        foreach($users as $user){
            if (filter_var($user->email, FILTER_VALIDATE_EMAIL)) {
                AllEmails::updateOrCreate(
                    ['email' => $user->email]
                );
            }
        }

        $users = Collaborator::all();
        foreach($users as $user){
            if (filter_var($user->email, FILTER_VALIDATE_EMAIL)) {
                AllEmails::updateOrCreate(
                    ['email' => $user->email]
                );
            }
        }

        $users = SubscribedUser::all();
        foreach($users as $user){
            if (filter_var($user->email, FILTER_VALIDATE_EMAIL)) {
                AllEmails::updateOrCreate(
                    ['email' => $user->email]
                );
            }
        }
    }

    public function export_emails(){
        return view('admin.exportEmails');
    }

    public function download_exported_emails(){
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename="emails.csv"');
        $data = array();

        $emails = AllEmails::all();
        foreach($emails as $email){
            array_push($data,$email->email.',');
        }

        $fp = fopen('php://output', 'wb');
        foreach ( $data as $line ) {
            $val = explode(",", $line);
            fputcsv($fp, $val);
        }
        fclose($fp);
    }



    public function run_once(){

    }

    public function take_tour(Request $request){
        $help = new Help;
        $help->email = $request->email;
        $help->message = 'Wants to take a tour of the website';
        $help->save();

        $email = 'yeron@locodor.com';

        $mail_content = 'Somebody wants to take a tour of the website<br>';
        $mail_content .= 'Reach out to him at this email address: '.$request->email;

        Mail::send([], [],
            function ($message) use ($email,$mail_content) {
                $message->subject('Somebody wants to take a tour');
                $message->setBody($mail_content, 'text/html');
                $message->to($email);
            });

        return 'success';
    }

    public function exit_feedback(Request $request){
        $help = new Help;
        $help->email = $request->email;
        $help->message = 'Exit message: '. $request->message;
        $help->save();

        $email = 'yeron@locodor.com';

        $mail_content = 'Somebody left an exit message<br>';
        $mail_content .= 'Message: '.$request->message .'<br>';
        $mail_content .= 'You can reach out to him: '.$request->email;

        Mail::send([], [],
            function ($message) use ($email,$mail_content) {
                $message->subject('Somebody left an exit message');
                $message->setBody($mail_content, 'text/html');
                $message->to($email);
            });

        return 'success';
    }

    public function store_subscribed_user(Request $request){
        $user = New SubscribedUser;
        $user->email = $request->email;
        $user->save();

        return 'success';
    }

    public function admin_backers(){
        $backers = Backer::orderBy('created_at','desc')->get();

        return view('admin.backers')->with('backers',$backers);
    }

    public function complete_jobs(){
        Artisan::call('queue:work', ['--stop-when-empty' => true,'--tries'=>3]);

        return "done";
    }

    public function set_bots_avatars(){
        if (($handle = fopen(asset('locodor_users.csv'), "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
//                dd($data);
                $user = User::where('email',$data[1])->get()->first();
                if(isset($data[5])){
                    $user->country = $data[5];
                }
                if(isset($data[6])){
                    $user->city = ucfirst($data[6]);
                }
                $user->save();
                $profile = Profile::where('user_id',$user->id)->get()->first();
                $profile->avatar = $data[4];
                if($data[3] == 'male'){
                    $profile->gender = 1;
                }elseif($data[3] == 'female'){
                    $profile->gender = 2;
                }
                $profile->save();
            }
            fclose($handle);
        }

        Session::flash('success','Avatars Set');

        return redirect()->back();
    }

    public function bots_auto_pledge(Request $request){
        $bots_no = $request->bots_no;
        $pledges_no = $request->pledges_no;

        //Get all users from CSV
        $csvfile = "locodor_users.csv";
        $file_handle = fopen($csvfile, "r");
        $users_csv = array();

        // Add to array each row in csv
        while (!feof($file_handle) ) {
            $users_csv[] = fgetcsv($file_handle, 1024);
        }
        fclose($file_handle);




        while($bots_no){
            $current_pledges_left = $pledges_no;

            //Get random index from the CSV generated array
            $random_row_index = array_rand($users_csv);
            //Get random row from the CSV generated array
            $user_csv = $users_csv[$random_row_index];

            //Get the coresponding user
            $user = User::where('email',$user_csv[1])->firstOrFail();



            while($current_pledges_left){
                $random_project = Project::where('approval','approved')->has('levels')->inRandomOrder()->first();
                $random_level = Level::where('project_id',$random_project->id)->inRandomOrder()->first();

                $project = Project::find($random_project->id);

                $project->current_amount += $random_level->price;
                if($project->projectSync){
                    $project->funding_percent = floor((( $project->current_amount + $project->projectSync->amount ) / $project->project_goal)*100);
                } else {
                    $project->funding_percent = floor($project->current_amount / $project->project_goal * 100);
                }
                $project->save();

                if(($project->current_amount + $project->projectSync->amount) >= $project->project_goal){
                    $project->status = 'funded';
                    $project->save();
                }

                $backer = new Backer;
                $backer->user_id = $user->id;
                $backer->level_id = $random_level->id;
                $backer->project_id = $random_project->id;
                $backer->amount = $random_level->price;
                $backer->data = 'bot';
                $backer->save();

                $watchers = Watcher::where('project_id',$random_project->id)->get();

                foreach($watchers as $watcher){
                    $notification = New Notification;

                    $notification->user_id = $watcher->user_id;
                    $notification->action_id = $watcher->project_id;
                    $notification->action_name = 'watcher_new_backer';
                    $notification->status = 'new';
                    $notification->save();
                }

                $activity2 = new ActivityLog;

                $activity2->user_id = $user->id;

                $activity2->action_id = $backer->id;
                $activity2->action_name = 'new_backer';
                $activity2->save();



                $current_pledges_left--;
            }
            $bots_no--;
        }



        Session::flash('success','Pledges made');

        return redirect()->back();
    }
}
