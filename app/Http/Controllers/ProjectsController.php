<?php

namespace App\Http\Controllers;


use App\Mail\CollaborateInvitation;
use App\Notification;
use App\ProjectSlide;
use App\Subcategory;
use App\Watcher;
use Illuminate\Http\Request;
use App\Project;
use App\Level;
use App\Comment;
use App\User;
use App\ActivityLog;
use App\Category;
use App\ProjectView;
use App\Product;
use App\Collaborator;
use Intervention\Image\Facades\Image;
use Spatie\LaravelImageOptimizer\Facades\ImageOptimizer;
use Stripe\Stripe;
use Stripe\Charge;
use Carbon\Carbon;
use App\Country;
use App\Backer;
use App\ProjectSync;
use App\Meta;
use Session;
use Auth;
use Mail;
use DOMDocument;
use Illuminate\Support\Str;
use Goutte\Client;


class ProjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($filter = null,$value= null)
    {
        $categories = Category::all();

        $featured = Project::all()->take(3);

        $featured_projects = Project::where('approval','approved')->inRandomOrder()->take(6)->get();
        $featured_products = Product::where('status','approved')->whereNotNull('link')->take(6)->get();
        $trending_projects = Project::where('approval','approved')->inRandomOrder()->take(6)->get();


        $projects = Project::where('approval','approved');

        if(!$_GET){
            $projects->whereIn('status',['funded','failed','ongoing'])->orderBy('status');
        }

        if(isset($_GET['category'])){
            $projects->where('category_id',$_GET['category']);
        }

        if(isset($_GET['tags'])){
            $projects->where(function ($query) {
                for ($i = 0; $i < count($_GET['tags']); $i++){
                    $query->orWhere('tags', 'like',  '%' . $_GET['tags'][$i] .'%');
                }
            });
        }

        if(isset($_GET['general'])){
            $general = $_GET['general'];
            if($general == 'new'){
                $projects->orderBy('created_at','desc');
            }

            if($general == 'funded '){
                $projects->where('status', 'funded')->orderBy('funding_percent','desc');
            }

            if($general == 'trending'){
                $projects->orderBy('views', 'desc');
            }

            if($general == 'almost'){
                $projects->where('funding_percent', '<=', 100)->where('funding_percent', '>=', 70);
            }

            if($general == 'purchase-now'){
                $projects->has('products')->orWhere('affiliate_link','!=','');
            }

            if($general == 'ending'){
                $limit = time() + 7 * 24 * 60 *60;
                $projects->where('end_date', '<=', $limit);
            }

            if($general == 'ended'){
                $projects->where('end_date', '<=', time())->orderBy('end_date','desc');
            }

            if($general == 'exclusive'){
                $projects->where('exclusive',1);
            }
        }

        if(isset($_GET['query'])){
            $projects->where('title','like', '%' . $_GET['query'] . '%')->orWhere('subtitle','like', '%' . $_GET['query'] . '%');
        }

        $projects = $projects->get()->take(12);

        $slides = ProjectSlide::orderBy('order','asc')->get();


        $meta_title = Meta::where('page','projectsIndex')->where('meta','title')->get()->first();
        $meta_description = Meta::where('page','projectsIndex')->where('meta','description')->get()->first();


        return view('projects.index')
            ->with('meta_description',$meta_description->value)
            ->with('meta_title',$meta_title->value)
            ->with('categories',$categories)
            ->with('projects', $projects)
            ->with('filter',$filter)
            ->with('slides',$slides)
            ->with('value',$value)
            ->with('featured',$featured)
            ->with('featured_projects',$featured_projects)
            ->with('featured_products',$featured_products)
            ->with('trending_projects',$trending_projects)
            ->with('user',User::find(Auth::id()));
    }

    public function products($id){
        $project = Project::find($id);
        $products = Product::where('project_id',$id)->where('status','approved')->get();

        return view('projects.products')
            ->with('project',$project)
            ->with('products',$products);
    }

    public function load_more_projects($from,$filter = null,$value= null){

        $projects = Project::where('approval','approved');
        if(count($_GET) == 1){
            $projects->whereIn('status',['funded','failed','ongoing'])->orderBy('status');
        }

        if(isset($_GET['category'])){
            $projects->where('category_id',$_GET['category']);
        }

        if(isset($_GET['tags'])){
            $projects->where(function ($query) {
                for ($i = 0; $i < count($_GET['tags']); $i++){
                    $query->orWhere('tags', 'like',  '%' . $_GET['tags'][$i] .'%');
                }
            });
        }

        if(isset($_GET['general'])){
            $general = $_GET['general'];
            if($general == 'new'){
                $projects->orderBy('created_at','desc');
            }

            if($general == 'funded '){
                $projects->where('status', 'funded')->orderBy('funding_percent','desc');
            }

            if($general == 'trending'){
                $projects->orderBy('views', 'desc');
            }

            if($general == 'almost'){
                $projects->where('funding_percent', '<=', 100)->where('funding_percent', '>=', 70);
            }

            if($general == 'purchase-now'){
                $projects->has('products')->orWhere('affiliate_link','!=','');
            }

            if($general == 'ending'){
                $limit = time() + 7 * 24 * 60 *60;
                $projects->where('end_date', '<=', $limit);
            }

            if($general == 'ended'){
                $projects->where('end_date', '<=', time())->orderBy('end_date','desc');
            }

            if($general == 'exclusive'){
                $projects->where('exclusive',1);
            }
        }

        if(isset($_GET['query'])){
            $projects->where('title','like', '%' . $_GET['query'] . '%')->orWhere('subtitle','like', '%' . $_GET['query'] . '%');
        }

        $projects->paginate(12);
        $projects = $projects->forPage($_GET['page'],12)->get();


        $misc = [
            ['tag'=> 'Tech', 'link' => 'tech'],
            ['tag' => 'Robotics','link' => 'robotics'],
            ['rank' => 'service provider'],
            ['rank' => 'angel investor'],
            ['rank' => 'influencer'],
            ['random-card' => 'socialcrowd']
        ];

        $miscIndex = ($from - 2) % count($misc);

        if(isset($misc[$miscIndex])){
            $miscCollection = collect($misc[$miscIndex]);
            $projects->push($miscCollection);
        }

        return view('projects.loadMoreProjects')
            ->with('projects',$projects);
    }

    public function created(){
        $projects_drafts = Project::where('user_id',Auth::id())->where('approval','draft')->get();
        $projects_approved = Project::where('user_id',Auth::id())->where('approval','approved')->get();
        $projects_pending = Project::where('user_id',Auth::id())->where('approval','pending')->get();
        $collaborating = Collaborator::where('user_id',Auth::id())->pluck('project_id')->toArray();

        $projects_collaborating = Project::whereIn('id',$collaborating)->get();

        return view('projects.created')
            ->with('projects_approved',$projects_approved)
            ->with('projects_pending',$projects_pending)
            ->with('projects_collaborating',$projects_collaborating)
            ->with('projects_drafts',$projects_drafts);
    }

    public function exclusive_projects(){
        $projects = Project::where('user_id',Auth::id())->where('exclusive',1)->get();
        return view('projects.exclusive')->with('projects',$projects);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('projects.create')->with('categories',$categories);
    }

    public function create2()
    {
        $categories = Category::all();
        return view('projects.create2')->with('categories',$categories);
    }

    public function  create3(){
        $categories = Category::select('id','name')->get();
        $categories = json_encode($categories->toArray());
        return view('projects.create3')->with('categories',$categories);
    }

    public function create_start()
    {
        return view('projects.create-start');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(isset($request->submit)){
            $this->validate($request, [
                'category_id' => 'required',
                'project_type' => 'required',
                'ready' => 'required',
                'title' => 'required|max:255',
                'slide' => 'required',
                'project_goal' => 'required',
                'start_date' => 'required|date',
                'end_date' => 'required|date',
                'content' => 'required',
            ]);
        }
        if ($request->project_id) {
            $project = Project::where('id', $request->project_id)->first();

            foreach($project->levels as $level) {
                $level->delete();
            }
        } else {
            $project = new Project;
        }

        $slides='';
        $thumbnail = '';
        if(is_array($request->slide)){
            foreach($request->slide as $index => $image) {
                if (is_object($image)) {
                    $filename = time().$image->getClientOriginalName();
                    $filename = str_replace('_','-',$filename);
                    $filename = str_replace(' ','-',$filename);

                    $userId = Auth::id();
                    $filepath = "photos/$userId/";
                    if (!file_exists($filepath)) {
                        mkdir($filepath.'thumbs', 666, true);
                    }
                    Image::make($image)->save("$filepath$filename");
                    Image::make($image)->resize(300, 200)->save("$filepath/thumbs/$filename");

                    ImageOptimizer::optimize(public_path() . "/$filepath$filename");
                    ImageOptimizer::optimize(public_path() . "/$filepath/thumbs/$filename");

                    if (!$index) {
                        $thumbnail = "$filepath/thumbs/$filename";
                    }
                    $slides .= "$filepath$filename,";
                } else {
                    if (!$index) {
                        $thumbnail = $image;
                    }
                    $slides .= $image.',';
                }
            }

            $slides = substr($slides, 0, -1);

        }

        $project->first_project = $request->first_project ?? null;
        $project->category_id = $request->category_id ?? null;
        $project->tags = isset($request->tags) ? $string = str_replace(' ', '', $request->tags) : null;
        $project->previous_url = $request->previous_url ?? null;
        $project->upload_other = $request->upload_other ?? null;
        $project->ready = $request->ready ?? null;
        $project->title = $request->title ?? null;
        $project->slug = str_slug($project->title, '-');
        $project->project_goal = $request->project_goal ?? 1;
        $project->subtitle = $request->subtitle ?? null;
        $project->slides = $slides != '' ? $slides : null;
        $project->image = $thumbnail;
        $project->youtube_url = $request->youtube_url ?? null;
        $project->affiliate_link = $request->affiliate_link ?? null;
        if(isset($request->save_draft)){
            $project->approval = 'draft';
        }
        if(isset($request->submit)){
            $project->approval = 'pending';
        }
        $project->start_date = isset($request->start_date)? strtotime($request->start_date) : null;
        $project->end_date = isset($request->end_date)? strtotime($request->end_date) : null;
        $project->project_type = $request->project_type ?? null;
        $project->shipping_location = $request->shipping_location ?? null;
        $project->website = $request->p_website ?? null;


        // Detect youtube videos and transform them into iframe
        $content = $request->content ?? null;
        $search     = '/(http|https)\:\/\/(www)?\.youtube\.com\/watch\?v=([a-zA-Z0-9]+)/smi';
        if(preg_match($search, $content, $url)) {
            $content = preg_replace($search, '<iframe width="100%" height="400px" src="' . $url[0] . '" frameborder="0" allowfullscreen></iframe>', $content);

            $search = '/youtube\.com\/watch\?v=([a-zA-Z0-9]+)/smi';
            $replace = "youtube.com/embed/$1";
            $content = preg_replace($search, $replace, $content);
        } else {
            $content =  isset($request->content)? $request->content : null;
        }

        $project->content = $content;
        $project->user_id = Auth::id();

        $project->save();

        for($i = 0;$i < count($request->level_desc);$i++){
            if(isset($request->level_title[$i]) && isset($request->level_price[$i]) && isset($request->level_desc[$i])){
                $level = new Level;
                $level->project_id = $project->id;
                $level->user_id = Auth::id();
                $level->title = $request->level_title[$i];
                $level->price = $request->level_price[$i];
                $level->shipping_cost = isset($request->shipping_cost[$i]) ? $request->shipping_cost[$i] : 0;
                $level->description = $request->level_desc[$i];
                $level->save();
            }
        }

        $collaborators = $request->collaborators;
        $collaborators_emails = explode(",", preg_replace('/\s+/', '', $collaborators));

        foreach($collaborators_emails as $email){
            if(filter_var($email, FILTER_VALIDATE_EMAIL)){

                $collaborator = new Collaborator;
                $collaborator->email = $email;
                $collaborator->token = Str::random(32);;
                $collaborator->project_id = $project->id;
                $collaborator->save();

                Mail::to($email)->send(new CollaborateInvitation($collaborator->id));
            }
        }

        $email = 'yeron@locodor.com';

        $mail_content = 'A project has been created <br>';
        $mail_content .= '<a href="'. route('project.show',['id'=> $project->id,'slug'=>$project->slug]) .'">View Project</a><br>';
        $mail_content .= 'And approve it from the <a href="www.locodor.com/admin/dashboard">Dashboard</a> if it meets our standards';

        Mail::send([], [],
            function ($message) use ($email,$mail_content) {
                $message->subject('A project has been updated');
                $message->setBody($mail_content, 'text/html');
                $message->to($email);
            });

        Session::flash('success','Project created');

        return redirect('/projects?project_type=all');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug,$id)
    {
        if($slug == 'dummy'){
            $project = Project::find($id);
             return redirect()->route('project.show',['id'=> $id,'slug'=> $project->slug]);
        }

        $project = Project::find($id);

        $project_comments = $project->comments->pluck('id')->toArray();

        $notifcations_backers = Notification::where('user_id',Auth::id())->where('action_id',$id)->where('status','new')->where('action_name','watcher_new_backer')->get();
        $notifcations_comments = Notification::where('user_id',Auth::id())->whereIn('action_id',$project_comments)->where('status','new')->where('action_name','watcher_new_comment')->get();
        $notifcations_project_owner_notification = Notification::where('user_id',Auth::id())->whereIn('action_id',$project_comments)->where('status','new')->where('action_name','project_new_comment')->get();


        $notifcations = $notifcations_backers->merge($notifcations_comments)->merge($notifcations_project_owner_notification);

        foreach($notifcations as $notification){
            $notification->status = 'seen';
            $notification->save();
        }

        if($project->approval == "draft"){
            if(Auth::check() == false && Auth::id() != $project->user_id && Auth::user()->admin == false){
                Session::flash('success','You do not have permission to access this page');
                return redirect()->route('home');
            }
        }
        $sync = ProjectSync::where('project_id',$id)->get()->first();
        if($sync == null){
            $sync = new ProjectSync;
            $sync->project_id = $id;
            $sync->views = 0;
            $sync->backers = 0;
            $sync->amount = 0;
            $sync->save();
        }

        if($project->approval == 'approved' || Auth::id() == $project->user_id || Auth::user()->admin){
            if(Auth::check()){
                $view = new ProjectView;
                $view->user_id = Auth::id();
                $view->project_id = $id;
                $view->save();
            } else {
                $view = new ProjectView;
                $view->user_id = '123456';
                $view->project_id = $id;
                $view->save();
            }


            $project->views = $project->views + 1;
            $project->save();

            $comments = Comment::where('project_id',$id)->get();
            $level = Level::where('project_id',$id)->get();


            $liked = array();
            foreach($comments as $comment){
                foreach($comment->commentLikes as $like){
                    if($like->user_id == Auth::id()){
                        array_push($liked,$like->comment_id);
                    }
                }
            }

            $backers = Backer::where('project_id',$id)->get();

            $meta_title = $project->title;
            $meta_description = htmlspecialchars_decode(preg_replace( "/\r|\n/", "", substr(strip_tags($project->content), 0, 155) ));
            $meta_image = $project->image;

            $activity_check = ActivityLog::where('action_id',$project->id)->where('action_name','project_ended')->first();

            if(time() > $project->end_date && $activity_check == null && $project->approval == 'approved'){
                $activity = new ActivityLog;

                $activity->user_id = $project->user->id;
                $activity->action_id = $project->id;
                $activity->action_name = 'project_ended';
                $activity->save();
            }

            $notifcations = Notification::where('user_id',$project->user_id)->where('action_id',$project->id)->where('action_name','project_new_comment')->get();

            foreach($notifcations as $notifcation){
                $notifcation->status = 'seen';
                $notifcation->save();
            }

            $slides = explode(',',$project->slides);
            if ($slides[0] == "") {
                $slides[0] = $project->image;
            }

            $youtube_thumbnail = null;
            if ($project->youtube_url){
                $parts = parse_url($project->youtube_url);
                if (isset($parts['query'])) {
                    parse_str($parts['query'], $query);
                    $youtube_thumbnail = $query['v'];
                }
            }
            return view('projects.show')
                ->with('meta_title',$meta_title)
                ->with('meta_description',$meta_description)
                ->with('meta_image',$meta_image)
                ->with('project',$project)
                ->with('sync',$sync)
                ->with('levels',$level)
                ->with('comments',$comments)
                ->with('user',User::find(Auth::id()))
                ->with('liked',$liked)
                ->with('slides',$slides)
                ->with('backers',$backers)
                ->with('youtube_thumbnail',$youtube_thumbnail);
        } else {
            Session::flash('success','You do not have permission to access this page');
            return redirect()->back();
        }


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $project = Project::find($id);
        $categories = Category::select('id','name')->get();
        $categories = json_encode($categories->toArray());
        $levels = $project->levels;
        $collaborators_ids = array();
        foreach($project->collaborators as $collaborator){
            if($collaborator->user_id != null){
                array_push($collaborators_ids,$collaborator->user_id);
            }
        }

        $slides = explode(',',$project->slides);

        if($project->user_id == Auth::id() || Auth::user()->admin || in_array(Auth::id(),$collaborators_ids)){
            return view('projects.create3')
                ->with('project',$project)
                ->with('categories',$categories)
                ->with('slides',$slides)
                ->with('project_levels',$levels);
        } else {
            Session::flash('success','This is not your project, you can\'t edit it');
            return redirect()->back();
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(isset($request->submit)){
            $this->validate($request, [
                'category_id' => 'required',
                'project_type' => 'required',
                'ready' => 'required',
                'title' => 'required|max:255',
                'image' => 'required',
                'project_goal' => 'required',
                'start_date' => 'required|date',
                'end_date' => 'required|date',
                'content' => 'required',
            ]);
        }

        $project = Project::find($id);

        $slides='';
        foreach($request->image as $image){
            $slides .= $image . ',';
        }
        $slides = substr($slides, 0, -1);

        $project->first_project = isset($request->first_project)? $request->first_project : null;
        $project->category_id = isset($request->category_id)? $request->category_id : null;
        $project->tags = isset($request->tags)? $string = str_replace(' ', '', $request->tags) : null;
        $project->previous_url = isset($request->previous_url)? $request->previous_url : null;
        $project->upload_other = isset($request->upload_other)? $request->upload_other : null;
        $project->ready = isset($request->ready)? $request->ready : null;
        $project->title = isset($request->title)? $request->title : 'Draft Project';
        $project->slug = str_slug($project->title, '-');
        $project->project_goal = isset($request->project_goal)? $request->project_goal : 1;
        $project->subtitle = isset($request->subtitle)? $request->subtitle : null;
        $project->slides = $slides != '' ? $slides : null;
        $project->image = isset($request->image)? $request->image[0] : null;
        $project->youtube_url = isset($request->youtube_url)? $request->youtube_url : null;
        $project->affiliate_link = isset($request->affiliate_link)? $request->affiliate_link : null;
        if(isset($request->save_draft)){
            $project->approval = 'draft';
        }
        if(isset($request->submit)){
            $project->approval = 'pending';
        }
        $project->start_date = isset($request->start_date)? strtotime($request->start_date) : null;
        $project->end_date = isset($request->end_date)? strtotime($request->end_date) : null;
        $project->project_type = isset($request->project_type)? $request->project_type : null;
        $project->shipping_location = isset($request->shipping_location)? $request->shipping_location : null;
        $project->website = isset($request->p_website)? $request->p_website : null;


        // Detect youtube videos and transform them into iframe
        $content = isset($request->content)? $request->content : null;
        $search     = '/(http|https)\:\/\/(www)?\.youtube\.com\/watch\?v=([a-zA-Z0-9]+)/smi';

        if(preg_match($search, $content, $url)) {
            $content = preg_replace($search, '<iframe width="100%" height="400px" src="' . $url[0] . '" frameborder="0" allowfullscreen></iframe>', $content);

            $search = '/youtube\.com\/watch\?v=([a-zA-Z0-9]+)/smi';
            $replace = "youtube.com/embed/$1";
            $content = preg_replace($search, $replace, $content);
        } else {
            $content =  isset($request->content)? $request->content : null;
        }

        $project->content = $content;

        $project->save();

        $j = 0;
        foreach($project->levels as $level){
            $level->delete();
            $j++;
        }

        for($i = 0;$i < count($request->level_desc);$i++){

            if(isset($request->level_title[$i]) && isset($request->level_price[$i]) && isset($request->level_desc[$i])){
                $level = new Level;
                $level->project_id = $project->id;
                $level->user_id = Auth::id();
                $level->title = $request->level_title[$i];
                $level->price = $request->level_price[$i];
                $level->shipping_cost = isset($request->shipping_cost[$i]) ? $request->shipping_cost[$i] : 0;
                $level->description = $request->level_desc[$i];
                $level->save();
            }
        }

        $collaborators = $request->collaborators;
        $collaborators_emails = explode(",", preg_replace('/\s+/', '', $collaborators));

        foreach($collaborators_emails as $email){
            if(filter_var($email, FILTER_VALIDATE_EMAIL)){

                $collaborator = new Collaborator;
                $collaborator->email = $email;
                $collaborator->token = Str::random(32);;
                $collaborator->project_id = $project->id;
                $collaborator->save();

                Mail::to($email)->send(new CollaborateInvitation($collaborator->id));
            }
        }


        $email = 'yeron@locodor.com';

        $mail_content = 'A project has been updated <br>';
        $mail_content .= '<a href="'. route('project.show',['id'=> $id,'slug'=>$project->slug]) .'">View Project</a><br>';
        $mail_content .= 'And approve it from the <a href="www.locodor.com/admin/dashboard">Dashboard</a> if it meets our standards';

        Mail::send([], [],
            function ($message) use ($email,$mail_content) {
                $message->subject('A project has been updated');
                $message->setBody($mail_content, 'text/html');
                $message->to($email);
            });

        Session::flash('success','Project updated');

        return redirect()->route('project.show',['id'=>$project->id,'slug'=>$project->slug]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $project = Project::find($id);

        $levels = Level::where('project_id',$id)->get();

        foreach($project->backers as $backer){
            $backer->delete();
        }

        if(Auth::id() == $project->user_id || Auth::user()->admin){

            $views = ProjectView::where('project_id',$project->id);
            $views->delete();

            //Delete from activity logs
            $activity = ActivityLog::where('action_name','project_create')->where('action_id',$id)->first();
            if($activity != null){
                $activity->delete();
            }

            $activity = ActivityLog::where('action_name','project_funded')->where('action_id',$id)->first();
            if($activity != null){
                $activity->delete();
            }

            $activity = ActivityLog::where('action_name','project_ended')->where('action_id',$id)->first();
            if($activity != null){
                $activity->delete();
            }

            $activities = ActivityLog::where('action_name','new_backer')->where('action_id',$id)->get();
            if($activities != null) {
                foreach ($activities as $activity) {
                    $activity->delete();
                }
            }

            // Delete comments of project and activity log
            $comments = Comment::where('project_id',$id)->get();
            if($comments != null){
                foreach($comments as $comment){
                    $activity = ActivityLog::where('action_name','project_comment')->where('action_id',$comment->id)->first();
                    $activity->delete();
                    // Delete likes of comments and comment likes activity
                    foreach($comment->commentLikes as $like){
                        $activity = ActivityLog::where('action_name','comment_like')->where('action_id',$like->id)->first();
                        if(isset($activity)){
                            $activity->delete();
                        }
                        $like->delete();
                    }
                    $comment->delete();
                }
            }

            if($levels != null){
                foreach($levels as $level){
                    $level->delete();
                }
            }

            $project->delete();
        } else {
            Session::flash('failed','You dont have permission to do that!');
            return redirect()->back();
        }

        return redirect()->route('projects.created');
    }

    public function project_url(){
        return view('projects.projecturl');
    }

    public function results(){
        $page = file_get_contents(request()->url);
        if(isset($_GET['url'])){
            $page = file_get_contents($_GET['url']);
        }

        $client = new Client();
        $crawler = $client->request('GET', request()->url);

        $projectHeader = json_decode($crawler->filter('#react-project-header')->first()->attr('data-initial'));
        $endDate = $projectHeader->project->deadlineAt;
        $title = $projectHeader->project->name;
        $subtitle = $projectHeader->project->description;
        $image = $projectHeader->project->imageUrl;
        $affiliate = $projectHeader->project->url;
        $goal = $projectHeader->project->goal->amount;
        $category = 'General';
        $currency ='USD';

        //Content
        preg_match('/<div class="col col-8 description-container">([\w\W]*)<div class="col col-4 max-w62">/', $page, $matches);
        $full_description = '';
        if(array_key_exists(1,$matches)){
            $full_description = $matches[1];
        }

        preg_match_all('/<li class="hover-group js-reward-available pledge--available pledge-selectable-sidebar" data-reward-id=.*?About <span>.+?(([0-9]|\.)*)<\/span>.*?<h3 class="pledge__title">(.*?)<\/h3>\n<div class="pledge__reward-description pledge__reward-description--expanded">(.*?)<a class="pledge/s' , $page , $levels , PREG_OFFSET_CAPTURE);
        $numOfLevels = 0;
        if(array_key_exists(1,$levels)){
            $numOfLevels = count($levels[1]);
        }

        $actualLevels = array();
        for ($i = 0; $i <$numOfLevels ; $i++)
            $actualLevels[$i] = array($levels[3][$i][0],str_replace(',','',$levels[1][$i][0]),$levels[4][$i][0]);

        Session::flash('name',$title);
        Session::flash('category',$category);
        Session::flash('description',$subtitle);
        Session::flash('goal',$goal);
        Session::flash('currency',$currency);
        Session::flash('full_description',$full_description);
        Session::flash('deadline',$endDate);
        Session::flash('numOfLevels',$numOfLevels);
        Session::flash('url',$affiliate);
        Session::flash('image',$image);
        $levels_flash = [];
        for ($i = 0; $i <$numOfLevels ; $i++){
            $levels_flash[$i]['title'] = $actualLevels[$i][0];
            $levels_flash[$i]['price'] = $actualLevels[$i][1];
            $levels_flash[$i]['description'] = $actualLevels[$i][2];
        }
        Session::flash('levels_flash', $levels_flash);

        return redirect()->route('admin.create');
    }

    public function locodor_url(){
        return view('projects.locodorurl');
    }

    public function results_locodor(){
        $page = file_get_contents($_POST['url']);
        preg_match('/<h1 class="title">(.*?)<\/h1>/', $page ,  $name, PREG_OFFSET_CAPTURE);
        preg_match('/<img src="(.*?)"/', $page ,  $image, PREG_OFFSET_CAPTURE);
        preg_match('/<div class="id-product-funding">Pledged of \$(.*?) Goal<\/div>/' , $page , $goal , PREG_OFFSET_CAPTURE);
        preg_match('/<div class="id-progress-raised"> \$(.*?) <\/div>/' , $page , $plaged , PREG_OFFSET_CAPTURE);
        preg_match('/<div class="id-product-total">(.*?)<\/div>/' , $page , $pladgers , PREG_OFFSET_CAPTURE);
        preg_match_all('/<div class="id-level-title"><span>(.*?):<\/span> \$(.*?)<\/div>.*?<div class="id-level-desc">(.*?)<\/div>/s' , $page , $levels , PREG_SET_ORDER);

        try{
            preg_match('/<div class="id-widget-year">(.*?)<\/div>/' , $page , $year , PREG_OFFSET_CAPTURE);
            $year = $year[1][0];
            preg_match('/<div class="id-widget-month">(.*?)<\/div>/' , $page , $month , PREG_OFFSET_CAPTURE);
            $month = $month[1][0];
            preg_match('/<div class="id-widget-day">(.*?)<\/div>/' , $page , $day , PREG_OFFSET_CAPTURE);
            $day = $day[1][0];
            $date = $year.','.$month.','.$day;

        }catch (\Exception $e) {
            $date = FALSE;
        }

        $full_description = substr($page , strpos ($page,'<div class="ignitiondeck id-creatorprofile">')+strlen('<div class="ignitiondeck id-creatorprofile">'));
        $enderindex = strpos($full_description,'</div');
        $openerindex = strpos($full_description,'<div');
        while($openerindex < $enderindex){
            $enderindex = strpos($full_description,'</div',$enderindex+1);
            $openerindex = strpos($full_description,'<div',$openerindex+1);
        }
        $full_description =substr($full_description, $enderindex);
        $enderindex = 0;
        $openerindex = 0;
        while($openerindex <= $enderindex){
            $enderindex = strpos($full_description,'</div',$enderindex+1);
            $openerindex = strpos($full_description,'<div',$openerindex+1);
        }
        $full_description =substr($full_description, 7,$enderindex);

        preg_match('/<h2 class="subtitle">(.*?)<\/h2>/' , $page , $subtitle , PREG_OFFSET_CAPTURE);



//        $pladgers = $pladgers[1][0];
//        $plaged = $plaged[1][0];
        $goal = str_replace(",","",$goal[1][0]);
        $image = $image[1][0];
        $name = $name[1][0];
        if( isset($subtitle[1][0])){
            $subtitle = $subtitle[1][0];
        } else {
            $subtitle = " ";
        }
        $date = $date;
        $levels = $levels;
        $numOfLevels = count($levels);

        Session::flash('url',$_POST['url']);
        Session::flash('name',$name);
        Session::flash('description',$subtitle);
        Session::flash('goal',$goal);
        Session::flash('image',$image);
        Session::flash('full_description',$full_description);
        Session::flash('numOfLevels',$numOfLevels);
        $levels_flash = [];
        for ($i = 0; $i <$numOfLevels ; $i++){
            $levels_flash[$i]['title'] = $levels[$i][1];
            $levels_flash[$i]['price'] = str_replace(',','',$levels[$i][2]);
            $levels_flash[$i]['description'] = $levels[$i][3];
        }
        Session::flash('levels_flash', $levels_flash);
//        Session::flash('levels',$levels);

        return redirect()->route('admin.create');
//            ->with('name',$name)
//            ->with('category',$category)
//            ->with('description',$description)
//            ->with('goal',$goal)
//            ->with('currency',$currency)
//            ->with('full_description',$full_description)
//            ->with('numOfLevels',$numOfLevels)
//            ->with('levels',$levels);
    }

    public function results_indiegogo(Request $request){
        $context = stream_context_create(
            array(
                "http" => array(
                    "header" => "User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36"
                )
            )
        );
        if(isset($_GET['url'])){
            $page = file_get_contents($_GET['url'],false, $context);
        } else {
            $page = file_get_contents($request->url, false, $context);
        }

        $category = 'General';
        $currency ='USD';

        //Title
        preg_match('/"project_title":"(.*?)"/', $page, $matches);
        $name = $matches[1];

        //Project ID
        preg_match('/"project_id":(.*?)};/', $page, $matches);
        $project_id = $matches[1];

        //Content
        $content_link = "https://www.indiegogo.com/private_api/campaigns/".$project_id."/description";
        $content_page = file_get_contents($content_link, false, $context);
        $full_description = json_decode($content_page,true);
        $full_description = $full_description['response']['description_html'];

        //Goal
        preg_match('/"campaign_goal_amount":"(.*?)"/', $page, $matches);
        $goal = $matches[1];

        // Subtitle
        preg_match('/"tagline":"(.*?)",/', $page, $matches);
        $description = $matches[1];


        preg_match('/"campaign_days_left":(.*?),"/', $page, $matches);
        $days_left = $matches[1];
        $deadline = 86400 * $days_left + time();


        preg_match('/gon\.perks=([\s\S]*);gon\.secret_perk_status=/', $page, $matches);
        $levels = json_decode($matches[1]);


        foreach($levels as $key => $level){
            $actualLevels[$key] = $level;
        }


        Session::flash('name',$name);
        Session::flash('category',$category);
        Session::flash('description',$description);
        Session::flash('goal',$goal);
        Session::flash('currency',$currency);
        Session::flash('full_description',$full_description);
        Session::flash('deadline',$deadline);
        Session::flash('numOfLevels',count($levels));
        Session::flash('url',request()->url);
        $levels_flash = [];
        foreach($levels as $key => $level){
            $levels_flash[$key]['title'] = $level->label;
            $levels_flash[$key]['price'] = $level->amount;
            $levels_flash[$key]['description'] = $level->description;
        };
        Session::flash('levels_flash', $levels_flash);
//        Session::flash('levels',$levels);

        return redirect()->route('admin.create');
    }

    public function adminCreate(){
        $categories = Category::all();
        return view('projects.adminCreate')->with('categories',$categories);
    }

    public function approve($id){
        $project = Project::find($id);
        $project->approval = 'approved';
        $project->save();

        $old_activity = ActivityLog::where('action_id',$project->id)->where('action_name','project_create')->get()->first();

        if($old_activity == null){
            $activity = new ActivityLog;

            $activity->user_id = $project->user_id;
            $activity->action_id = $project->id;
            $activity->action_name = "project_create";

            $activity->save();
        }



        Session::flash('success','Project Approved');

        return redirect()->back();
    }

    public function drafts(){
        $projects = Project::where('user_id',Auth::id())->where('approval','draft')->get();

        return view('projects.drafts')->with('projects',$projects);
    }

    public function become_collaborator($token){
        $collaborator = Collaborator::where('token',$token)->get()->first();
        if($collaborator->user_id == null){
            $collaborator->user_id = Auth::id();
            $collaborator->save();
        }else{
            Session::flash('success','Link expired');
            return redirect('/');
        }

        return redirect()->route('project.show',['id'=>$collaborator->project->id,'slug'=>$collaborator->project->slug]);
    }

    public function watch($id){
        $watcher = new Watcher;
        $watcher->project_id = $id;
        $watcher->user_id = Auth::id();
        $watcher->save();

        $activity = new ActivityLog;

        $activity->user_id = Auth::id();
        $activity->action_id = $id;
        $activity->action_name = "project_watch";

        $activity->save();

        return redirect()->back();
    }

    public function unwatch($id){
        $watcher = Watcher::where('user_id',Auth::id())->where('project_id',$id)->get()->first();
        $watcher->delete();

        $activity = ActivityLog::where('action_name','project_watch')->where('action_id',$id)->where('user_id',Auth::id())->get()->first();
        $activity->delete();

        return redirect()->back();
    }


}
