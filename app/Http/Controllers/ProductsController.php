<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Affiliate;
use App\Product;
use App\Project;
use Auth;
use Session;

class ProductsController extends Controller
{
    public function affiliate(){
        $products = Affiliate::paginate(15);

        return view('products.affiliate')->with('products',$products);
    }

    public function index(){
        $products = Product::where('status','approved')->orderby('created_at','desc')->paginate(15);

        return view('products.index')->with('products',$products);
    }

    public function create(){
        $projects = Project::where('user_id',Auth::id())->where('status','funded')->get();

        return view('products.create')
            ->with('projects',$projects);
    }

    public function store(Request $request){

        if(Auth::user()->admin){
            $this->validate($request, [
                'title' => 'required|max:255',
                'price' => 'required',
                'description' => 'required',
                'image' => 'required',
            ]);

            $product = new Product;

            $product->title = $request->title;
            $product->price = $request->price;
            $product->description = $request->description;
            $product->project_id = isset($request->project_id) ? $request->project_id : 0;
            $product->link = isset($request->link) ? $request->link : null;
            $product->image = $request->image;
            $product->user_id = Auth::id();
            $product->status = 'approved';

            $product->save();
        }else{
            $this->validate($request, [
                'title' => 'required|max:255',
                'price' => 'required',
                'description' => 'required',
                'project_id' => 'required',
                'image' => 'required',
            ]);

            $product = new Product;

            $product->title = $request->title;
            $product->price = $request->price;
            $product->description = $request->description;
            $product->project_id = $request->project_id;
            $product->image = $request->image;
            $product->user_id = Auth::id();
            $product->status = 'pending';

            $product->save();
        }





        Session::flash('success','Product submitted for review!');

        return redirect()->route('products');
    }

    public function destroy($id){
        $product = Product::find($id);
        $product->delete();

        Session::flash('success','Product erased');

        return redirect()->back();
    }

    public function edit($id){
        $product = Product::find($id);
        $projects = Project::where('user_id',Auth::id())->get();

        return view('products.edit')
            ->with('product',$product)
            ->with('projects',$projects);
    }

    public function update(Request $request,$id){
        $product = Product::find($id);
        $product->project_id = $request->project_id;
        $product->image = $request->image;
        $product->title = $request->title;
        $product->price = $request->price;
        $product->description = $request->description;
        $product->status = 'pending';
        $product->save();

        Session::flash('success','Product Updated');
        return redirect()->back();
    }

    public function created(){
        $products_approved = Product::where('user_id',Auth::id())->where('status','approved')->get();
        $products_pending = Product::where('user_id',Auth::id())->where('status','pending')->get();

        return view('products.created')
            ->with('products_approved',$products_approved)
            ->with('products_pending',$products_pending);
    }

    public function approve($id){
        $product = Product::find($id);
        $product->status = 'approved';
        $product->save();

        Session::flash('succes','Product approved');

        return redirect()->back();
    }

    public function show($id){
        $product = Product::find($id);

        $suggestions = Product::inRandomOrder()->take(4)->get();

        return view('products.show')
            ->with('product',$product)
            ->with('suggestions',$suggestions);
    }
}
