<?php

namespace App\Http\Controllers;

use App\Mail\NewMessageNotification;
use Illuminate\Http\Request;
use App\Conversation;
use App\Message;
use App\User;
use App\Friend;
use App\Notification;
use App\Meta;
use Mail;
use Auth;

class ConversationsController extends Controller
{
    public function index(){

        $conversations = Conversation::where('user1_id',Auth::id())->orWhere('user2_id',Auth::id())->get();
        $friends = Friend::where('user1_id',Auth::id())->where('status','accepted')->orWhere('user2_id',Auth::id())->where('status','accepted')->get();

        if(isset($_GET['r'])){
            $recipient = User::find($_GET['r']);
        }else{
            $recipient = [];
        }

        if(isset($_GET['c'])){
            $messages = Message::where('conversation_id',$_GET['c'])->orderBy('created_at','asc')->get();

            //Mark notifications as seen
            $notifications = Notification::where('user_id',Auth::id())->where('action_id',$_GET['c'])->where('action_name','conversation_new_message')->get();
            foreach($notifications as $notification){
                $notification->status = 'seen';
                $notification->save();
            }

            foreach($messages as $message){
                if($message->to == Auth::id()){
                    $message->status = 2;
                    $message->save();
                }
            }
        } else {
            $messages = [];
        }

        $meta_title = Meta::where('page','projectsIndex')->where('meta','title')->get()->first();
        $meta_description = Meta::where('page','projectsIndex')->where('meta','description')->get()->first();

        return view('conversations.index')
            ->with('meta_title', $meta_title)
            ->with('meta_description', $meta_description)
            ->with('friends',$friends)
            ->with('conversations',$conversations)
            ->with('messages',$messages)
            ->with('recipient',$recipient);
    }

    public function start_conversation($id){
        $conversation = Conversation::where('user1_id',Auth::id())->where('user2_id',$id)->orWhere('user1_id',$id)->where('user2_id',Auth::id())->first();

        if(!isset($conversation)){
            $conversation = new Conversation;
            $conversation->user1_id = Auth::id();
            $conversation->user2_id = $id;
            $conversation->save();
        }

        return redirect('/conversations?c='.$conversation->id.'&r='.$id);
    }

    public function delete_conversation($id){
        $conversation = Conversation::find($id);
        $messages = Message::where('conversation_id',$id);
        $conversation->delete();
        $messages->delete();
        $notifications = Notification::where('action_name','conversation_new_message')->where('action_id',$conversation->id)->get();
        foreach($notifications as $notification){
            $notification->delete();
        }

        return redirect('/conversations');
    }

    public function message_store(Request $request){
        $text = $request->message;
        $reg_exUrl = '/(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9]\.[^\s]{2,})/';
        if (preg_match_all($reg_exUrl, $text, $matches)) {
            foreach ($matches[0] as $i => $match) {
                $text = str_replace(
                    $match,
                    '<a href="'.$matches[0][$i].'" rel="nofollow" target="_blank">'.$matches[0][$i].'</a>',
                    $text
                );
            }
        }

        $message = new Message;

        $message->from = Auth::id();
        $message->to = $request->to;
        $message->conversation_id = $request->conv_id;
        $message->message = strip_tags($text,'<a>');
        $message->status = 1;
        $message->save();

        $notification = New Notification;

        $notification->user_id = $request->to;
        $notification->action_id = $request->conv_id;
        $notification->action_name = 'conversation_new_message';
        $notification->status = 'new';
        $notification->save();

        $receiver = User::find($request->to);

        if(Auth::user()->admin == true){
            Mail::to($receiver->contact_email)->send(new NewMessageNotification($message->to));
        };

        return redirect()->back();
    }
}
