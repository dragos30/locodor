<?php

namespace App\Http\Controllers;

use App\Mail\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class ContactController extends Controller
{
    public function contact(Request $request) {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required'
        ]);

        Mail::to('yeron@locodor.com')
            ->cc('dragoscimpean@gmail.com')
            ->queue(new Contact($request));


        Session::flash('success','Message sent');
        return redirect()->back();
    }
}
