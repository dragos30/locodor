<?php

namespace App\Http\Controllers;

use App\ProfileReply;
use Illuminate\Http\Request;
use App\Profile;
use App\User;
use App\ActivityLog;
use App\ActivityComment;
use App\Project;
use App\Friend;
use App\ProjectView;
use App\Notification;
use App\ProfilePost;
use App\Watcher;
use Auth;
use Mockery\Matcher\Not;
use Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class ProfilesController extends Controller
{



    public function show($id,$slug){
        $profile= Profile::find($id);
        $user = User::find($profile->user->id);
        $activity = ActivityLog::where('user_id',$user->id)->get();
        $activity_comments = ActivityComment::all();
        $projects = Project::where('approval','approved')->where('user_id',$user->id)->get();
        $friends = Friend::where('user1_id',$user->id)->where('status','accepted')->orWhere('user2_id',$user->id)->where('status','accepted')->get();
        //Sent friend requests
        $pending_friends = Friend::where('user1_id',$user->id)->where('status','pending')->get();
        //received friend requests
        $friend_requests = Friend::where('user2_id',$user->id)->where('status','pending')->get();
        $profile_posts = ProfilePost::where('user2',$user->id)->orderBy('created_at','desc')->get();
        $watched_projects = Watcher::where('user_id',$user->id)->get();


        if(File::exists('photos/'.$id)) {
            $media = File::files('photos/'.$id);
        } else {
            $media = array();
        }

        if($user->id == Auth::id()){
            $notifications = Notification::where('user_id',$user->id)->where('action_name','new_profile_post')->get();
            if($notifications != null){
                foreach($notifications as $notification){
                    $notification->status = 'seen';
                    $notification->save();
                }
            }
        }

        $profile_posts_ids = ProfilePost::where('user2',$user->id)->pluck('id')->all();


        $replies_ids = ProfileReply::whereIn('profile_post_id',$profile_posts_ids)->pluck('id')->all();

        $notifications = Notification::whereIn('action_id',$replies_ids)->where('action_name','new_profile_reply')->where('status','new')->where('user_id',Auth::id())->get();

        foreach($notifications as $notification){
            if($notification != null){
                $notification->status = 'seen';
                $notification->save();
            }
        }

        $liked = array();
        if(Auth::check()){
            foreach($activity as $a){
                foreach($a->activityLikes as $like){
                    if($like->user_id == Auth::id()){
                        array_push($liked,$a->id);
                    }
                }
            }
        }


        $recent_ids = array();
        $recent_viewed_id = ProjectView::select('project_id')->where('user_id',$user->id)->distinct()->get();
        foreach($recent_viewed_id as $id){
            array_push($recent_ids,$id->project_id);
        }
        $recent_viewed = Project::findMany($recent_ids);


        // Used to verify if user is already friend
        $loggedUserFriendsId = array();
        if(Auth::check()){
            $loggedUserFriends = Friend::where('user1_id',Auth::id())->orWhere('user2_id',Auth::id())->get();
            foreach($loggedUserFriends as $friend){
                if($friend->user1_id == Auth::id()){
                    array_push($loggedUserFriendsId,$friend->user2_id);
                } else {
                    array_push($loggedUserFriendsId,$friend->user1_id);
                }
            }
        }

        if(isset($_GET['notification'])){
            $notification = Notification::find($_GET['notification']);
            $notification->status = 'seen';
            $notification->save();
        }

        $meta_title = 'Crowdfunding User | '.$user->name;
        $meta_description = 'Crowdfunding Member of Locodor | ' . $user->name . " | " . $user->profile->about;
//        dd($rss_news);

        return view('profiles.show')
            ->with('meta_description',$meta_description)
            ->with('watched_projects',$watched_projects)
            ->with('meta_title',$meta_title)
            ->with('user',$user)
            ->with('profile_posts',$profile_posts)
            ->with('activity',$activity)
            ->with('liked',$liked)
            ->with('activity_comments',$activity_comments)
            ->with('projects',$projects)
            ->with('friends',$friends)
            ->with('pending_friends',$pending_friends)
            ->with('friend_requests',$friend_requests)
            ->with('loggedUserFriendsId',$loggedUserFriendsId)
            ->with('recent_viewed',$recent_viewed)
            ->with('media',$media);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user= User::find(Auth::id());

        return view('profiles.edit')->with('user',$user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $profile = Profile::find($id);
        $user = User::find($profile->user_id);

        $user->name = $request->name;
        $user->country = $request->country;
        $user->city = $request->city;
        $profile->about = $request->about;
        $profile->facebook = $request->facebook;
        $profile->linkedin = $request->linkedin;
        $profile->twitter = $request->twitter;
        $profile->kickstarter = $request->kickstarter;
        $profile->indiegogo = $request->indiegogo;
        $profile->angellist = $request->angellist;
        $profile->gender = $request->gender;
        $profile->birth = $request->birth;
        $profile->phone = $request->phone;

        if ($request->avatar) {
            $profile->avatar = $request->avatar;
        }

        $user->save();
        $profile->save();

        Session::flash('Profile updated');

        return redirect()->route('profile.show',['id'=>$user->profile->id,'slug' => $user->profile->slug]);
    }

    public function add_friend($id){
        $friend = Friend::where('user1_id',$id)->where('user2_id',Auth::id())->orWhere('user2_id',$id)->where('user1_id',Auth::id())->get();
        if(Auth::id() != $id && count($friend) == 0){
            $friend = new Friend;
            $friend->user1_id = Auth::id();
            $friend->user2_id = $id;
            $friend->status = 'pending';
            $friend->save();

            $notification = New Notification;

            $notification->user_id = $id;
            $notification->action_id = $friend->id;
            $notification->action_name = 'new_friend_request';
            $notification->status = 'new';
            $notification->save();

            Session::flash('success','Friend request sent!');
        } else {
            Session::flash('success','You are already friends');
        }

        return redirect()->back();
    }

    public function accept_friend($id){
        $friend = Friend::where('user1_id',$id)->where('user2_id',Auth::id())->first();
        $friend->status = 'accepted';
        $friend->save();

        $notification = Notification::where('action_id',$friend->id)->where('action_name','new_friend_request')->first();
        $notification->status = 'seen';
        $notification->save();

        Session::flash('success','Friend request accepted');

        return redirect()->back();
    }

    public function remove_friend($id){
        $friend = Friend::where('user1_id',Auth::id())->where('user2_id',$id)->orWhere('user1_id',$id)->where('user2_id',Auth::id())->first();
        if($friend != null){
            $friend->delete();
            Session::flash('success','Friend removed');
        } else {
            Session::flash('success','Can\'t remove from friend list because you are not friends. Refresh the page and try again.');
        }


        $notification = Notification::where('action_id',$friend->id)->where('action_name','new_friend_request')->first();
        if($notification != null){
            $notification->delete();
        }



        return redirect()->back();
    }

    public function store_profile_post(Request $request){
        $text = $request->message;
        $reg_exUrl = '/(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9]\.[^\s]{2,})/';
        if (preg_match_all($reg_exUrl, $text, $matches)) {
            foreach ($matches[0] as $i => $match) {
                $text = str_replace(
                    $match,
                    '<a href="'.$matches[0][$i].'" rel="nofollow" target="_blank">'.$matches[0][$i].'</a>',
                    $text
                );
            }
        }

        $post = new ProfilePost;
        $post->user1 = Auth::id();
        $post->user2 = $request->user2;
        $post->message = strip_tags($text,'<a>');;
        $post->save();

        $notification = new Notification;
        $notification->user_id = $request->user2;
        $notification->action_id = $post->id;
        $notification->action_name = 'new_profile_post';
        $notification->status = 'new';
        $notification->save();

        $activity = new ActivityLog;
        $activity->user_id = Auth::id();
        $activity->action_id = $post->id;
        $activity->action_name = 'new_profile_post';
        $activity->save();

        return redirect()->back();
    }

    public function delete_profile_post($id){

        $post = ProfilePost::find($id);

        if(!(Auth::user()->admin || $post->user1 == Auth::id() || $post->user2 == Auth::id())){
            dd('You don\'t have permission to do that!');
        }

        foreach($post->replies as $reply){
            $notifications = Notification::where('action_id',$reply->id)->where('action_name','new_profile_reply')->get();
            if($notifications != null){
                foreach($notifications as $notification){
                    $notification->delete();
                }
            }
            $reply->delete();
        }

        $notifications = Notification::where('action_id',$post->id)->where('action_name','new_profile_post')->get();

        if($notifications != null){
            foreach($notifications as $notification){
                $notification->delete();
            }
        }

        $activity = ActivityLog::where('action_name','new_profile_post')->where('action_id',$post->id)->get()->first();
        $activity->delete();

        $post->delete();

        return redirect()->back();
    }

    public function profile_reply_store(Request $request){

        $text = $request->reply;
        $reg_exUrl = '/(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9]\.[^\s]{2,})/';
        if (preg_match_all($reg_exUrl, $text, $matches)) {
            foreach ($matches[0] as $i => $match) {
                $text = str_replace(
                    $match,
                    '<a href="'.$matches[0][$i].'" rel="nofollow" target="_blank">'.$matches[0][$i].'</a>',
                    $text
                );
            }
        }

        $reply = new ProfileReply;
        $reply->profile_post_id = $request->post_id;
        $reply->user_id = Auth::id();
        $reply->reply = strip_tags($text,'<a>');
        $reply->save();

        if($reply->post->user1 != $reply->post->user2){


            if( $reply->post->user1 == Auth::id() ){


                $notification1 = new Notification;
                $notification1->user_id = $reply->post->user2;
                $notification1->action_id = $reply->id;
                $notification1->action_name = 'new_profile_reply';
                $notification1->status = 'new';
                $notification1->save();

            }elseif( $reply->post->user2 == Auth::id() ){

                $notification1 = new Notification;
                $notification1->user_id = $reply->post->user1;
                $notification1->action_id = $reply->id;
                $notification1->action_name = 'new_profile_reply';
                $notification1->status = 'new';
                $notification1->save();

            }elseif($reply->post->user1 != Auth::id() && $reply->post->user2 != Auth::id()){

                $notification1 = new Notification;
                $notification1->user_id = $reply->post->user2;
                $notification1->action_id = $reply->id;
                $notification1->action_name = 'new_profile_reply';
                $notification1->status = 'new';
                $notification1->save();

                $notification1 = new Notification;
                $notification1->user_id = $reply->post->user1;
                $notification1->action_id = $reply->id;
                $notification1->action_name = 'new_profile_reply';
                $notification1->status = 'new';
                $notification1->save();

            }

        }elseif( $reply->post->user1 != Auth::id() ){

            $notification1 = new Notification;
            $notification1->user_id = $reply->post->user1;
            $notification1->action_id = $reply->id;
            $notification1->action_name = 'new_profile_reply';
            $notification1->status = 'new';
            $notification1->save();

        }


        $post = ProfilePost::find($reply->post->id);
        foreach($post->replies->unique('user_id') as $r){
            if($r->user_id != Auth::id()){
                if($r->post->user1 != $r->user_id && $r->post->user2 != $r->user_id){
                    $notification = new Notification;
                    $notification->user_id = $r->user_id;
                    $notification->action_id = $r->id;
                    $notification->action_name = 'new_profile_reply';
                    $notification->status = 'new';
                    $notification->save();
                }
            }
        }

        if($request->page == "profile-show"){
            $returned_reply =
            '<p>
                    <img src="'.$reply->user->profile->avatar.'" class="profile-reply-avatar">
                    <a href="'. route('profile.show',['id'=> $reply->user->profile->id, 'slug'=>$reply->user->profile->slug]) .'"><span class="profile-reply-username">'.$reply->user->name.'</span></a>
                    <span class="profile-reply">'.$reply->reply.'</span>
                    <a href="'. route('profile.reply.delete',['id'=>$reply->id]) .'" class="delete-reply float-right"><i class="fas fa-times"></i></a>
            </p>';
        } else {
            $returned_reply =
                '<p class="activity-comment" data-comment-id="'.$reply->id.'" id="comment-'.$reply->id.'">
                    <img class="hover-image" src="'.$reply->user->profile->avatar.'" width="40" height="40" alt="Crowdfunding User Avatar">
                    <a class="activity-comment-username" href="'. route('profile.show',['id'=> $reply->user->profile->id, 'slug'=>$reply->user->profile->slug]) .'">'.$reply->user->name.'</a>
                    <span>'.$reply->reply.'</span>
                    <br>
                    <a href="'. route('profile.reply.delete',['id'=>$reply->id]) .'"><span class="float-right activity-reply-delete"><i class="fas fa-times"></i></span></a>
                </p>';
        }



        return $returned_reply;
    }

    public function reply_delete($id){
        $reply = ProfileReply::find($id);

        if(!(Auth::user()->admin || $reply->user_id == Auth::id() || $reply->post->user2 == Auth::id())){
            dd('You don\'t have permission to do that!');
        }

        $notifications = Notification::where('action_id',$reply->id)->where('action_name','new_profile_reply')->get();
        if($notifications != null){
            foreach($notifications as $notification){
                $notification->delete();
            }
        }

        $reply->delete();

        return redirect()->back();
    }
}
