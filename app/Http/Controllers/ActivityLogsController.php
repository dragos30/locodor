<?php

namespace App\Http\Controllers;

use App\ActivityLog;
use App\ActivityLike;
use App\User;
use App\Project;
use App\Category;
use App\ActivityComment;
use App\Friend;
use App\Meta;
use App\Notification;
use Session;
use Auth;
use Illuminate\Http\Request;

class ActivityLogsController extends Controller
{
    public function index(){
        $mention = '';
        if(Auth::check()){
            if(isset($_GET['id'])){
                $mention = User::find($_GET['id']);
            }
        } else {
            return redirect('/login');
        }

        $activity = ActivityLog::orderBy('created_at','desc')->get();

        $users = User::all();
        $our_users = User::orderby('created_at','desc')->take(8)->get();
        $user = User::find(Auth::id());
        $projects = Project::where('approval','approved')->orderBy('created_at','desc')->take(12)->get();
        $categories = Category::all();
        $all_friends =  Friend::where('user1_id',Auth::id())->orWhere('user2_id',Auth::id())->get();


        $loggedUserFriends = Friend::where('user1_id',Auth::id())->orWhere('user2_id',Auth::id())->get();
        $loggedUserFriendsId = array();
        foreach($loggedUserFriends as $friend){
            if($friend->user1_id == Auth::id()){
                array_push($loggedUserFriendsId,$friend->user2_id);
            } else {
                array_push($loggedUserFriendsId,$friend->user1_id);
            }
        }


        $liked = array();
        foreach($activity as $a){
            foreach($a->activityLikes as $like){
                if($like->user_id == Auth::id()){
                    array_push($liked,$a->id);
                }
            }
        }

        $activity_comments = ActivityComment::all();

        $activity_items = array();
        $activity_i = 0;
        $project_i = 0;

        for($i = 0; $i < $activity->count(); $i++){
            if($i == 3 or $i == 6 or $i == 9 or $i == 12){
                array_push($activity_items, $i);
            }elseif ($i % 5 == 1){
                if(count($projects) > $project_i){
                    array_push($activity_items, $projects[$project_i]);
                    $project_i++;
                }
            } else {
                array_push($activity_items, $activity[$activity_i]);
                $activity_i++;
            }
        }


        $new_activity_items = array();
        $range = range(0,11);
        foreach($range as $r){
            array_push($new_activity_items, $activity_items[$r]);
        }
        $activity_items = $new_activity_items;

        $meta_title = Meta::where('page','socialCrowd')->where('meta','title')->get()->first();
        $meta_description = Meta::where('page','socialCrowd')->where('meta','description')->get()->first();

        return view('activities.index')
            ->with('mention',$mention)
            ->with('meta_title',$meta_title->value)
            ->with('meta_description',$meta_description->value)
            ->with('activity_items',$activity_items)
            ->with('user',$user)
            ->with('users',$users)
            ->with('our_users',$our_users)
            ->with('projects',$projects)
            ->with('categories',$categories)
            ->with('liked',$liked)
            ->with('all_friends',$all_friends)
            ->with('loggedUserFriendsId',$loggedUserFriendsId)
            ->with('activity_comments',$activity_comments);
    }

    public function load_more_activities($from){
        $activity = ActivityLog::orderBy('created_at','desc')->get();

        $users = User::all();
        $our_users = User::orderby('created_at','desc')->take(8)->get();
        $user = User::find(Auth::id());
        $projects = Project::where('approval','approved')->inRandomOrder()->get();
        $categories = Category::all();
        $all_friends =  Friend::where('user1_id',Auth::id())->orWhere('user2_id',Auth::id())->get();


        $loggedUserFriends = Friend::where('user1_id',Auth::id())->orWhere('user2_id',Auth::id())->get();
        $loggedUserFriendsId = array();
        foreach($loggedUserFriends as $friend){
            if($friend->user1_id == Auth::id()){
                array_push($loggedUserFriendsId,$friend->user2_id);
            } else {
                array_push($loggedUserFriendsId,$friend->user1_id);
            }
        }


        $liked = array();
        foreach($activity as $a){
            foreach($a->activityLikes as $like){
                if($like->user_id == Auth::id()){
                    array_push($liked,$a->id);
                }
            }
        }

        $activity_comments = ActivityComment::all();

        $activity_items = array();
        $activity_i = 0;
        $project_i = 0;

        for($i = 0; $i < $activity->count(); $i++){
            if($i == 3 or $i == 6){
                array_push($activity_items, $i);
            }elseif ($i % 5 == 1){
                if(count($projects) > $project_i){
                    array_push($activity_items, $projects[$project_i]);
                    $project_i++;
                }
            } else {
                array_push($activity_items, $activity[$activity_i]);
                $activity_i++;
            }
        }

        $new_activity_items = array();
        $range = range($from,$from+11);
        foreach($range as $r){
            array_push($new_activity_items, $activity_items[$r]);
        }
        $activity_items = $new_activity_items;

        return view('activities.loadMoreActivities')
            ->with('activity_items',$activity_items)
            ->with('user',$user)
            ->with('users',$users)
            ->with('our_users',$our_users)
            ->with('projects',$projects)
            ->with('categories',$categories)
            ->with('liked',$liked)
            ->with('all_friends',$all_friends)
            ->with('loggedUserFriendsId',$loggedUserFriendsId)
            ->with('activity_comments',$activity_comments);

    }

    public function actions(){

        $activity = ActivityLog::where('action_name','<>','user_post_create')->orderBy('created_at','desc')->get();

        $users = User::all();
        $our_users = User::orderby('created_at','desc')->take(9)->get();
        $user = User::find(Auth::id());
        $projects = Project::where('approval','approved')->orderBy('id', 'desc')->take(5)->get();
        $categories = Category::all();
        $all_friends =  Friend::where('user1_id',Auth::id())->orWhere('user2_id',Auth::id())->get();


        $loggedUserFriends = Friend::where('user1_id',Auth::id())->orWhere('user2_id',Auth::id())->get();
        $loggedUserFriendsId = array();
        foreach($loggedUserFriends as $friend){
            if($friend->user1_id == Auth::id()){
                array_push($loggedUserFriendsId,$friend->user2_id);
            } else {
                array_push($loggedUserFriendsId,$friend->user1_id);
            }
        }


        $liked = array();
        foreach($activity as $a){
            foreach($a->activityLikes as $like){
                if($like->user_id == Auth::id()){
                    array_push($liked,$a->id);
                }
            }
        }

        return view('activities.actions')
            ->with('activity',$activity)
            ->with('user',$user)
            ->with('users',$users)
            ->with('our_users',$our_users)
            ->with('projects',$projects)
            ->with('categories',$categories)
            ->with('liked',$liked)
            ->with('all_friends',$all_friends)
            ->with('loggedUserFriendsId',$loggedUserFriendsId);
    }

    public function show($id){
        $a = ActivityLog::find($id);

        //Mark notifications as seen
        $notifications = Notification::where('user_id',Auth::id())->where('action_id',$a->id)->where('action_name','mention')->get();
        foreach($notifications as $notification){
            $notification->status = 'seen';
            $notification->save();
        }

        $users = User::all();
        $user = User::find(Auth::id());
        $projects = Project::orderBy('id', 'desc')->take(5)->get();
        $categories = Category::all();

        $liked = array();
        foreach($a->activityLikes as $like){
            if($like->user_id == Auth::id()){
                array_push($liked,$a->id);
            }
        }

        $activity_comments = ActivityComment::where('activity_id',$id)->get();

        $our_users = User::orderby('created_at','desc')->take(8)->get();
        $all_friends =  Friend::where('user1_id',Auth::id())->orWhere('user2_id',Auth::id())->get();

        return view('activities.show')
            ->with('our_users',$our_users)
            ->with('all_friends',$all_friends)
            ->with('a',$a)
            ->with('user',$user)
            ->with('users',$users)
            ->with('projects',$projects)
            ->with('categories',$categories)
            ->with('liked',$liked)
            ->with('activity_comments',$activity_comments);
    }

    public function activity_like($id){

        $activity = ActivityLog::find($id);
        if($activity->userpost){
            $activity->userpost->likes = $activity->userpost->likes + 1;
            $activity->userpost->save();

            $activity_like = new ActivityLike;

            $activity_like->user_id = Auth::id();
            $activity_like->activity_id = $id;
            $activity_like->save();

            Session::flash('success','Post liked!');
        } else {
            $activity_like = new ActivityLike;

            $activity_like->user_id = Auth::id();
            $activity_like->activity_id = $id;
            $activity_like->save();
            Session::flash('success','Activity liked!');
        }


        return 'success';
    }

    public function activity_unlike($id){

        $activity = ActivityLog::find($id);
        if($activity->userpost){
            $activity->userpost->likes = $activity->userpost->likes - 1;
            $activity->userpost->save();

            $activity_like = ActivityLike::where('activity_id',$id)->where('user_id',Auth::id())->first();
            $activity_like->delete();

            Session::flash('success','Post unliked!');
        } else {
            $activity_like = ActivityLike::where('activity_id',$id)->where('user_id',Auth::id())->first();
            $activity_like->delete();

            Session::flash('success','Activity unliked!');
        }

        Session::flash('success','Activity unliked!');
        return 'success';
    }

    public function activity_comment(){
        $comment = new ActivityComment;

        $comment->activity_id = request('activity_id');
        $comment->user_id = Auth::id();

        $text = request('comment');
        $reg_exUrl = '/(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9]\.[^\s]{2,})/';
        if (preg_match_all($reg_exUrl, $text, $matches)) {
            foreach ($matches[0] as $i => $match) {
                $text = str_replace(
                    $match,
                    '<a href="'.$matches[0][$i].'" rel="nofollow">'.$matches[0][$i].'</a>',
                    $text
                );
            }
        }

        $user_comment = strip_tags($text,'<a><ul><ol><li><b><u><i>');

        $comment->comment = $user_comment;

        $comment->save();

        $returned_comment =
            '<p class="activity-comment" data-comment-id="'.$comment->id.'" id="comment-'.$comment->id.'">
                    <img class="hover-image" src="'. asset($comment->user->profile->avatar) .'" width="40" height="40" alt="Crowdfunding User Avatar">
                    <a class="activity-comment-username" href="'. route('profile.show',['id'=> $comment->user->profile->id,'slug' => $comment->user->profile->slug]) .'">'.$comment->user->name.'</a>
                    <span>'.$comment->comment.'</span>
                    <br>
                    <a href="'. route('activity.comment.delete',['id'=> $comment->id]) .'"><span class="float-right activity-comment-delete"><i class="fas fa-times"></i></span></a>
            </p>';

        return $returned_comment;
    }

    public function activity_comment_delete($id){
        $comment = ActivityComment::find($id);
        $comment->delete();

        Session::Flash('Comment erased');

        return redirect()->back();
    }


}
