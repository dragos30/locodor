<?php

namespace App\Http\Middleware;

use Illuminate\Cookie\Middleware\EncryptCookies as Middleware;

use Closure;
use Iflylabs\iFlyChat;
use Auth;
use App\User;

class iFlyChatIntegration extends Middleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */



    public function handle($request, Closure $next, $guard = null)
    {
        $user = User::find(Auth::id());

        $APP_ID = '80fbe802-b015-4a6c-9576-b874bd831861';
        $API_KEY = 'kPjKYPl7JlminrqozzVNjaJ7BfAWtUFU3mImux1bC9sW67207';



        $iflychat = new iFlyChat($APP_ID, $API_KEY);

        if(Auth::check()){
            $flyUser = array(
                'user_name' => $user->name, // string(required)
                'user_id' => "{{$user->id}}", // string (required)
                'is_admin' => $user->admin, // boolean (optional)
                'user_avatar_url' => asset($user->profile->avatar), // string (optional)
                'user_profile_url' => route('profile.show',['id'=>$user->profile->id,'slug'=>$user->profile]), // string (optional)
            );

            $iflychat->setUser($flyUser);
        }

        $iflychat_code = $iflychat->getHtmlCode();

        /** @var Response $response */
        $response = $next($request);
        $content = $response->getContent();
        if (($head = mb_strpos($content, '</footer>')) !== false) {
            $response->setContent(mb_substr($content, 0, $head) . $iflychat_code . mb_substr($content, $head));
        }
        return $response;
    }

}

