@extends('layouts.app')

@section('content')
    <div class="container">
        <h1 class="text-center">Forum</h1>
        @if(Auth::check() && $user->admin)
            <a class="btn btn-dark topic-create-btn" href="{{ route('topic.create') }}">Create topic</a>
        @endif

        <div class="row forum-container">
            <div class="col-md-12">
                <h3><b>Topics</b></h3>
            </div>
            @foreach($topics as $topic)
                <div class="col-md-12">
                    <hr>
                    <h3>
                        <a href="{{ route('topic.show',['id' => $topic->id]) }}">{{ $topic->name }}</a>
                        @if(Auth::check() && $user->admin)
                            <a href="{{ route('topic.edit',['id' => $topic->id]) }}" class="float-right"><i class="fas fa-pen"></i></a>
                            <a href="{{ route('topic.delete',['id' => $topic->id]) }}" class="float-right"><i class="fas fa-trash"></i></a>
                        @endif
                    </h3>

                </div>
            @endforeach
        </div>
    </div>
@endsection