@extends('layouts.app')

@section('content')
    <div class="container">
        <h1 class="text-center">Topic: <b>{{ $topic->name }}</b></h1>
        @if(Auth::check())
            <a class="btn btn-dark topic-create-btn" href="{{ route('discussion.create',['topic_id'=>$topic->id]) }}">Create Discussion</a>
        @endif
        <div class="row forum-container">
            @foreach($discussions as $discussion)
                <div class="col-md-12">
                    <h3>
                        <a href="{{ route('discussion.show',['id' => $discussion->id]) }}"> {{ $discussion->title }}</a>
                        @if($discussion->user->id == Auth::id())
                            <a href="{{ route('discussion.edit',['id' => $discussion->id]) }}" class="float-right"><i class="fas fa-pen"></i></a>
                            <a href="{{ route('discussion.delete',['id' => $discussion->id]) }}" class="float-right"><i class="fas fa-trash"></i></a>
                        @endif
                    </h3>
                    <hr>
                </div>
            @endforeach
        </div>
    </div>
@endsection