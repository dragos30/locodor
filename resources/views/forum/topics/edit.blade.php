@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="{{ route('topic.update',['id' => $topic->id]) }}" method="post">
            {{ csrf_field() }}

            <div class="form-group">
                <label for="name">Topic name</label>
                <input class="form-control" type="text" name="name" placeholder="Topic name..." value="{{ $topic->name }}">
            </div>

            <button type="submit" class="btn btn-dark">Update Topic Name</button>
        </form>
    </div>
@endsection