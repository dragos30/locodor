@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="{{ route('topic.store') }}" method="post">
            {{ csrf_field() }}

            <div class="form-group">
                <label for="name">Topic name</label>
                <input class="form-control" type="text" name="name" placeholder="Topic name...">
            </div>

            <button type="submit" class="btn btn-dark">Create Topic</button>
        </form>
    </div>
@endsection