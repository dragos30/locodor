</div>
<footer>
    <div class="container-fluid footer-main-container">
        <div class="container footer-container">
            <div class="row">
                <div class="col-md-4 footer-col-1">
                    <h2>What to do now?</h2>
                    <div class="simple-line"></div>
                    <p><a href="{{ route('projects') }}">Explore Projects</a></p>
                    <p><a href="{{ route('project.create-start') }}">Create a new Project</a></p>
                    <p><a href="{{ route('activity') }}">Join the Social Community</a></p>
                    <p><a href="{{ route('posts') }}">Read the latest Crowdfunding news</a></p>
                </div>
                <div class="col-md-4  footer-col-2 text-center">
                    <img src="{{ asset('uploads/logo-locodor-crowdfunding.png') }}">
                </div>
                <div class="col-md-4  footer-col-3">
                    <h2>Contact Us</h2>
                    <h5>IT and R&D Department:</h5>
                    <div class="simple-line"></div>
                    <p>Wilshire Blvd. #1114, Los Angeles, USA</p>
                    <p>Phone: <a href="tel:0014242563306">+1-424-2563306</a></p>
                    <p>Email: <a href="mailto:contact@locodor.com">contact@locodor.com</a></p>
                    <br>
                    <p>Be-All Alon Towers, Tel Aviv, IL</p>
                    <p>Phone: <a href="tel:00972772491111">+972-77-2491111</a></p>
                    <p>Email: <a href="mailto:contact@locodor.com">contact@locodor.com</a></p>
                </div>
            </div>
        </div>
    </div>
</footer>
@if($_SERVER['REQUEST_URI'] !== '/')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    {{-- Slick --}}
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
@endif
<script src="{{asset('js/magnific-popup.js')}}"></script>
<script>
    @if(Session::has('success'))
        toastr.success('{{ Session::get('success') }}');
    @endif
    @if($_SERVER['REQUEST_URI'] == '/activity')
        tribute.attach(document.getElementById('user-post-editor'));
        tribute.attach($('.userpost-comment-div'));
    @endif
</script>
@if(strpos($_SERVER['REQUEST_URI'],'/products') !== false || strpos($_SERVER['REQUEST_URI'],'/projects') !== false || strpos($_SERVER['REQUEST_URI'],'/user-profile') !== false )
    <script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>
    <script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
@endif
</body>
</html>