

    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Expires" content="30" />

    {{--General Meta Data--}}
    <title>@if(isset( $meta_title->value )) {{ $meta_title->value }}  @elseif(isset( $meta_title )) {{ $meta_title }} @else Crowdfunding for Business | Crowdfunding Platforms | Locodor @endif</title>
    <meta name='description' itemprop='description' content='@if(isset($meta_description->value)) {{ $meta_description->value }}@elseif(isset($meta_description)) {{$meta_description}} @else Locodor is a powerful crowdfunding social network platform / site that allows individuals & businesses to sahre their ideas with a global community of inventors, innovators, designers & tinkerers to seek funding from their supporters, funders and investors who belive in them and their ideas. @endif'/>
    <meta name='keywords' content='Crowdfunding Sites, Crowdfunding for Business, Crowdfunding Platforms, Crowdfunding for Startups, How Does Crowdfunding Work' />

    {{--Facebook Metas--}}
    <meta property="og:type" content="article" />
    <meta property='og:title' content='@if(isset( $meta_title->value )) {{ $meta_title->value }}  @elseif(isset( $meta_title )) {{ $meta_title }} @else Crowdfunding for Business | Crowdfunding Platforms | Locodor @endif'/>
    @if(isset($meta_image))
        <meta property='og:image' content='{{ asset($meta_image) }}'/>
    @endif
    <meta property='og:description' content='@if(isset($meta_description->value)) {{ $meta_description->value }}@elseif(isset($meta_description)) {{$meta_description}} @else Locodor is a powerful crowdfunding social network platform / site that allows individuals & businesses to sahre their ideas with a global community of inventors, innovators, designers & tinkerers to seek funding from their supporters, funders and investors who belive in them and their ideas. @endif'/>
    <meta property='og:url' content='<?php echo "https://locodor.com" . $_SERVER['REQUEST_URI']; ?>'/>
