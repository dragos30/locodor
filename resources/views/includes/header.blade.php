<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('includes.meta')

    <!-- BEGIN SHAREAHOLIC CODE -->
    <link rel='preload' href='https://apps.shareaholic.com/assets/pub/shareaholic.js' as='script' />
    <script data-cfasync="false" async src="https://apps.shareaholic.com/assets/pub/shareaholic.js" data-shr-siteid="f0479fae020363d6fab04bafcb3e3bd9"></script>
    <!-- END SHAREAHOLIC CODE -->

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="shortcut icon" href="{{ asset('/uploads/favicon.ico') }}" type="image/x-icon">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">

    <!-- Scripts -->
    @if(strpos($_SERVER['REQUEST_URI'],'/crowdfunding-project') !== false || strpos($_SERVER['REQUEST_URI'],'/products/') !== false || strpos($_SERVER['REQUEST_URI'],'/product/') !== false)
        <script src="https://checkout.stripe.com/checkout.js"></script>
        <script src="https://www.paypalobjects.com/api/checkout.js"></script>
    @endif
    <script src="{{ asset('js/custom-app.js') }}"></script>
    <script src="{{ asset('js/homedialogs.js') }}"></script>
    @if($_SERVER['REQUEST_URI'] !== "/")

        <script src="/vendor/laravel-filemanager/js/lfm.js"></script>



        <!-- TinyMCE -->
        <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=3nopjb5t6vxykdeuctzqtzx704x0ehyw5y091siwqd4mbyt3"></script>
        <script>
            var editor_config = {
                path_absolute : "/",
                selector: "textarea#editor",
                plugins: [
                    "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                    "searchreplace wordcount visualblocks visualchars code fullscreen",
                    "insertdatetime media nonbreaking save table contextmenu directionality",
                    "emoticons template paste textcolor colorpicker textpattern"
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
                relative_urls: false,
                file_browser_callback : function(field_name, url, type, win) {
                    var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                    var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

                    var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                    if (type == 'image') {
                        cmsURL = cmsURL + "&type=Images";
                    } else {
                        cmsURL = cmsURL + "&type=Files";
                    }

                    tinyMCE.activeEditor.windowManager.open({
                        file : cmsURL,
                        title : 'Filemanager',
                        width : x * 0.8,
                        height : y * 0.8,
                        resizable : "yes",
                        close_previous : "no"
                    });
                }
            };

            tinymce.init(editor_config);
        </script>

        <!-- Google adsense -->
        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <script>
            (adsbygoogle = window.adsbygoogle || []).push({
                google_ad_client: "ca-pub-4486204494926986",
                enable_page_level_ads: true
            });
        </script>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

        {{-- Fonts --}}
        <link href="https://fonts.googleapis.com/css?family=Crete+Round" rel="stylesheet">
    @endif

    {!! NoCaptcha::renderJs() !!}


    @if($_SERVER['REQUEST_URI'] == '/activity')
            <link rel="stylesheet" href="js/tribute.css" />
            <script src="js/tribute.js"></script>
    @endif

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-89811218-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-89811218-1');
    </script>



    <!-- Push notifications / OneSignal -->
    <link rel="manifest" href="{{ asset('/manifest.json') }}" />
    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
    <script>
        var OneSignal = window.OneSignal || [];
        OneSignal.push(function() {
            OneSignal.init({
                appId: "15a28bad-07fe-4bc5-91a2-6930c959c840",
            });
        });
    </script>



    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/magnific-popup.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,700&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/project-templates.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    @yield('funky-theme-head')
</head>
<body>

<div id="app">
    <nav class="navbar navbar-expand-xl navbar-light navbar-laravel">
        <div class="container-fluid">
            <a class="navbar-brand" href="{{ url('/') }}">
                <img width="200px" src='{{ asset('/uploads/logo-locodor-crowdfunding.png') }}'>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">

                    <!-- Authentication Links -->
                    @guest
                        <li class="nav-item">
                            <a class="nav-link nav-btn" href="{{ route('home') }}">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-btn" href="{{ route('projects') }}">Projects</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-btn" href="{{ route('activity') }}">Social Crowd</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-btn" href="{{ route('posts') }}">News</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-btn" href="{{ route('login') }}?d">{{ __('Login') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-btn" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                <i class="fas fa-bell notification-icon"></i><span class="caret"></span>
                                <p class="notifications-count">{{ count($notifications) }}</p>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                @if(count($notifications))
                                    @foreach($notifications as $notification)
                                        @switch($notification->action_name)
                                            @case("conversation_new_message")
                                                <?php
                                                if($notification->conversation->user1_id == Auth::id()){
                                                    $r = $notification->conversation->user2_id;
                                                } else {
                                                    $r = $notification->conversation->user1_id;
                                                }
                                                ?>
                                                <a class="dropdown-item" href="{{ route('conversations') }}?c={{$notification->action_id}}&r={{$r}}">
                                                    You have a new message.
                                                </a>
                                            @break
                                            @case("project_new_comment")
                                                <a class="dropdown-item" href="{{ route('project.show',['id' => $notification->comment->project->id,'slug' => $notification->comment->project->slug]) }}">
                                                    You have a new comment on your project.
                                                </a>
                                            @break
                                            @case("new_friend_request")
                                            <a class="dropdown-item" href="{{ route('profile.show',['id' => Auth::user()->profile->id,'slug' => Auth::user()->profile->slug]) }}?friends=1&request=1&notification={{$notification->id}}">
                                                You have a new friend request.
                                            </a>
                                            @break
                                            @case("mention")
                                            <a class="dropdown-item" href="{{ route('activity.show',['id' => $notification->action_id]) }}">
                                                You have been mentioned in a post.
                                            </a>
                                            @break
                                            @case("new_profile_post")
                                            <a class="dropdown-item" href="{{ route('profile.show',['id' => Auth::user()->profile->id,'slug' => Auth::user()->profile->slug]) }}">
                                                Somebody wrote on your timeline.
                                            </a>
                                            @break
                                            @case('new_profile_reply')
                                            <a class="dropdown-item" href="{{ route('profile.show',['id' => $notification->profile_reply->post->user_2->profile->id,'slug' => $notification->profile_reply->post->user_2->profile->slug]) }}">
                                                There's a new reply on a profile message.
                                            </a>
                                            @break
                                            @case('watcher_new_backer')
                                            <a class="dropdown-item" href="{{ route('project.show',['id' => $notification->action_id,'slug' => $notification->project->slug]) }}">
                                                There is a new backer for "{{ str_limit($notification->project->title,30) }}"
                                            </a>
                                            @break
                                            @case('watcher_new_comment')
                                            <a class="dropdown-item" href="{{ route('project.show',['id' => $notification->comment->project->id,'slug' => $notification->comment->project->slug]) }}">
                                                There is a new comment for "{{ str_limit($notification->comment->project->title,30) }}"
                                            </a>
                                            @break
                                        @endswitch
                                    @endforeach
                                @else
                                    <p class="dropdown-item">
                                        There is nothing new
                                    </p>
                                @endif
                                <a class="dropdown-item" href="{{ route('notifications') }}"><b>View all notifications</b></a>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-btn" href="{{ route('home') }}">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-btn" href="{{ route('projects') }}">Projects</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-btn" href="{{ route('activity') }}">Social Crowd</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-btn" href="{{ route('conversations') }}">Conversations</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-btn" href="{{ route('posts') }}">News</a>
                        </li>

                        <li class="nav-item dropdown">
                            <a id="navbarDropdown2" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                <img src="{{ asset(Auth::user()->profile->avatar) }}" class="menu-avatar"><span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('profile.show',['id' => Auth::user()->profile->id,'slug' => Auth::user()->profile->slug ]) }}">
                                    My Profile
                                </a>
                                <a class="dropdown-item" href="{{ route('projects.created') }}">
                                    My Projects
                                </a>
                                <a class="dropdown-item" href="{{ route('exclusive.projects') }}">
                                    Exclusive Projects
                                </a>
                                <a class="dropdown-item" href="{{ route('products.created') }}">
                                    My Products
                                </a>
                                <a class="dropdown-item" href="{{ route('project.create-start') }}">
                                    Create Project
                                </a>
                                <a class="dropdown-item" href="{{ route('product.create') }}">
                                    Create Product
                                </a>
                                @if(Auth::user()->admin)
                                    <a class="dropdown-item" href="{{ route('admin.dashboard') }}">
                                        Dashboard
                                    </a>
                                @endif
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                    <li class="nav-item search-menu">
                        <form action="{{ route('results') }}" method="get" class="search-form">
                            <div class="has-feedback">
                                <label for="search" class="sr-only">Search</label>
                                <input type="text" class="form-control" name="query" id="search" placeholder="Search">
                            </div>
                        </form>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
