@foreach($activity_items as $a)
    @if(isset($a->action_name))
        @switch($a->action_name)
            @case("project_create")
            <div class="activity-item" data-activity-id="{{ $a->id }}">
                <div>
                    <p class="action-text">
                        <a href="{{ route('profile.show',[ 'id' => $a->user->profile->id, 'slug' => $a->user->profile->slug ]) }}">{{ $a->user->name }}</a>
                        created a project
                    </p>
                </div>
                <div class="user-action" role="alert">
                    <p>
                        <a href="{{ route('project.show',['id'=>$a->project->id,'slug'=>$a->project->slug]) }}" rel="nofollow" target="_blank">
                            <img src="{{ $a->project->image }}" width="100%">
                        </a>
                    </p>
                    <div class="post-url-title">
                        <a href="{{ route('project.show',['id'=>$a->project->id,'slug'=>$a->project->slug]) }}" rel="nofollow" target="_blank">{{$a->project->title}}</a>
                    </div>
                    <div class="post-url-description">
                        <a href="{{ route('project.show',['id'=>$a->project->id,'slug'=>$a->project->slug]) }}" rel="nofollow" target="_blank">{{$a->project->subtitle}}</a>
                    </div>
                </div>
                @if(Auth::check())
                    <div class="post-interact-container">
                        <a href="{{ route('activity.unlike',['id' => $a->id]) }}" class="activity-interact activity-unlike" @if(!in_array($a->id,$liked)) style="display:none" @endif><i class="far fa-thumbs-down"></i>Unlike</a>
                        <a href="{{ route('activity.like',['id' => $a->id]) }}" class="activity-interact activity-like" @if(in_array($a->id,$liked)) style="display:none" @endif><i class="far fa-thumbs-up"></i> Like</a>
                        <span class="activity-interact comment-btn"><i class="far fa-comment"></i> Comment</span>
                        <span class="activity-likes"><span class="activity-likes-number">{{count($a->activityLikes)}}</span> likes</span>
                    </div>
                @else
                    <div class="post-interact-container">
                        <a href="{{ route('login') }}" class="activity-interact"><u>Sign in</u></a>
                        or
                        <a href="{{ route('register') }}" class="activity-interact"><u>Sign Up</u></a>
                        to comment and like posts.
                        <span class="activity-likes"><span class="activity-likes-number">{{count($a->activityLikes)}}</span> likes</span>
                    </div>
                @endif

                <div class="activity-comments">
                    @foreach($a->activityComments as $comment)
                        <p class="activity-comment" data-comment-id="{{ $comment->id }}" id="comment-{{$comment->id}}">
                            <img class="hover-image" src="{{ $comment->user->profile->avatar }}" width="40" height="40" alt="Crowdfunding User Avatar">
                            <a class="activity-comment-username" href="{{ route('profile.show',[ 'id' => $comment->user->profile->id, 'slug' => $user->profile->slug ]) }}">{{ $comment->user->name }}</a>
                            <span>{!! $comment->comment !!}</span>
                            <br>
                            @if($comment->user_id == Auth::id())
                                <a href="{{ route('activity.comment.delete',['id' => $comment->id ]) }}"><span class="float-right activity-comment-delete"><i class="fas fa-times"></i></span></a>
                            @endif
                        </p>
                    @endforeach
                    {{--<a>--}}
                    {{--<p class="activity-comment load-more">--}}
                    {{--Load More Comments--}}
                    {{--</p>--}}
                    {{--</a>--}}
                </div>
                <div class="activity-comment" id="activity-comment{{$a->id}}">
                    <form action="{{ route('activity.comment') }}" id="comment-form{{$a->id}}" method="post">
                        @csrf
                        <input type="hidden" value="{{ $a->id }}" name="activity_id">
                        <input type="hidden" name="comment" class="userpost-comment-content">
                        <div style="width:100%;height:72px;background-color:white;text-align:left;border:1px solid darkgray" contenteditable="true"  class="userpost-comment-div">
                        </div>
                        <button class="btn btn-dark btn-sm comment-submit-btn float-right" type="submit">Post</button>
                    </form>
                </div>
            </div>
            @break
            @case("discussion_create")
            <div class="activity-item">
                <p>
                    <a href="{{ route('profile.show',[ 'id' => $a->user->profile->id, 'slug' => $a->user->profile->slug ]) }}">{{ $a->user->name }}</a>
                    created a discussion:
                    <a href="{{ route('discussion.show',['id' => $a->action_id]) }}">{{ $a->discussion->title }}</a>
                </p>
                <span class="activity-likes">{{count($a->activityLikes)}} likes</span>


                <a href="{{ route('activity.unlike',['id' => $a->id]) }}" @if(in_array($a->id,$liked)) style="display:none" @endif><i class="far fa-thumbs-down"></i>Unlike</a>
                <a href="{{ route('activity.like',['id' => $a->id]) }}" @if(!in_array($a->id,$liked)) style="display:none" @endif><i class="far fa-thumbs-up"></i>Like</a>
            </div>
            @break
            @case("discussion_reply")
            <div class="activity-item">
                <p>
                    <a href="{{ route('profile.show',[ 'id' => $a->user->profile->id ]) }}">{{ $a->user->name }}</a>
                    replied "{{ $a->reply->content }}" on the discussion
                    <a href="{{ route('discussion.show',['id' => $a->reply->discussion_id ]) }}">{{ $a->reply->discussion->title }}</a>
                </p>
                <span class="activity-likes">{{count($a->activityLikes)}} likes</span>

                @if(in_array($a->id,$liked))
                    <a href="{{ route('activity.unlike',['id' => $a->id]) }}"><i class="far fa-thumbs-down"></i>Unlike</a>
                @else
                    <a href="{{ route('activity.like',['id' => $a->id]) }}"><i class="far fa-thumbs-up"></i>Like</a>
                @endif
            </div>
            @break
            @case("project_comment")
            <div class="activity-item" data-activity-id="{{ $a->id }}">
                <div>
                    <p class="action-text">
                        <a href="{{ route('profile.show',[ 'id' => $a->user->profile->id, 'slug' => $a->user->profile->slug ]) }}">{{ $a->user->name }}</a>
                        commented "{{ $a->comment->comment }}" on the project <a href="{{ route('project.show',['id' => $a->comment->project->id ,'slug' => $a->comment->project->slug]) }}">{{ $a->comment->project->title }}</a>
                    </p>
                </div>
                <div class="user-action" role="alert">
                    <p>
                        <a href="{{ route('project.show',['id'=>$a->comment->project->id,'slug'=>$a->comment->project->slug]) }}" rel="nofollow" target="_blank">
                            <img src="{{ $a->comment->project->image }}" width="100%">
                        </a>
                    </p>
                    <div class="post-url-title">
                        <a href="{{ route('project.show',['id'=>$a->comment->project->id,'slug'=>$a->comment->project->slug]) }}" rel="nofollow" target="_blank">{{$a->comment->project->title}}</a>
                    </div>
                    <div class="post-url-description">
                        <a href="{{ route('project.show',['id'=>$a->comment->project->id,'slug'=>$a->comment->project->slug]) }}" rel="nofollow" target="_blank">{{$a->comment->project->subtitle}}</a>
                    </div>
                </div>
                @if(Auth::check())
                    <div class="post-interact-container">
                        <a href="{{ route('activity.unlike',['id' => $a->id]) }}" class="activity-interact activity-unlike" @if(!in_array($a->id,$liked)) style="display:none" @endif><i class="far fa-thumbs-down"></i>Unlike</a>
                        <a href="{{ route('activity.like',['id' => $a->id]) }}" class="activity-interact activity-like" @if(in_array($a->id,$liked)) style="display:none" @endif><i class="far fa-thumbs-up"></i> Like</a>
                        <span class="activity-interact comment-btn"><i class="far fa-comment"></i> Comment</span>
                        <span class="activity-likes"><span class="activity-likes-number">{{count($a->activityLikes)}}</span> likes</span>
                    </div>
                @else
                    <div class="post-interact-container">
                        <a href="{{ route('login') }}" class="activity-interact"><u>Sign in</u></a>
                        or
                        <a href="{{ route('register') }}" class="activity-interact"><u>Sign Up</u></a>
                        to comment and like posts.
                        <span class="activity-likes"><span class="activity-likes-number">{{count($a->activityLikes)}}</span> likes</span>
                    </div>
                @endif

                <div class="activity-comments">
                    @foreach($a->activityComments as $comment)
                        <p class="activity-comment" data-comment-id="{{ $comment->id }}" id="comment-{{$comment->id}}">
                            <img class="hover-image" src="{{ $comment->user->profile->avatar }}" width="40" height="40" alt="Crowdfunding User Avatar">
                            <a class="activity-comment-username" href="{{ route('profile.show',[ 'id' => $comment->user->profile->id, 'slug' => $user->profile->slug ]) }}">{{ $comment->user->name }}</a>
                            <span>{!! $comment->comment !!}</span>
                            <br>
                            @if($comment->user_id == Auth::id())
                                <a href="{{ route('activity.comment.delete',['id' => $comment->id ]) }}"><span class="float-right activity-comment-delete"><i class="fas fa-times"></i></span></a>
                            @endif
                        </p>
                    @endforeach
                    {{--<a>--}}
                    {{--<p class="activity-comment load-more">--}}
                    {{--Load More Comments--}}
                    {{--</p>--}}
                    {{--</a>--}}
                </div>
                <div class="activity-comment" id="activity-comment{{$a->id}}">
                    <form action="{{ route('activity.comment') }}" id="comment-form{{$a->id}}" method="post">
                        @csrf
                        <input type="hidden" value="{{ $a->id }}" name="activity_id">
                        <input type="hidden" name="comment" class="userpost-comment-content">
                        <div style="width:100%;height:72px;background-color:white;text-align:left;border:1px solid darkgray" contenteditable="true"  class="userpost-comment-div">
                        </div>
                        <button class="btn btn-dark btn-sm comment-submit-btn float-right" type="submit">Post</button>
                    </form>
                </div>
            </div>
            @break
            @case("topic_create")
            <div class="activity-item">
                <p>
                    <a href="{{ route('profile.show',[ 'id' => $a->user->profile->id, 'slug' => $a->user->profile->slug ]) }}">{{ $a->user->name }}</a>
                    created a new topic:
                    <a href="{{ route('topic.show',['id' => $a->action_id ]) }}">{{ $a->topic->name }}</a>
                </p>
                <span class="activity-likes">{{count($a->activityLikes)}} likes</span>

                @if(in_array($a->id,$liked))
                    <a href="{{ route('activity.unlike',['id' => $a->id]) }}"><i class="far fa-thumbs-down"></i>Unlike</a>
                @else
                    <a href="{{ route('activity.like',['id' => $a->id]) }}"><i class="far fa-thumbs-up"></i>Like</a>
                @endif
            </div>
            @break
            @case("discussion_watch")
            <div class="activity-item">
                <p>
                    <a href="{{ route('profile.show',[ 'id' => $a->user->profile->id , 'slug' => $a->user->profile->slug]) }}">{{ $a->user->name }}</a>
                    is watching
                    <a href="{{ route('discussion.show',['id' => $a->watcher->discussion->id ]) }}">{{ $a->watcher->discussion->title }}</a>
                </p>
                <span class="activity-likes">{{count($a->activityLikes)}} likes</span>

                @if(in_array($a->id,$liked))
                    <a href="{{ route('activity.unlike',['id' => $a->id]) }}"><i class="far fa-thumbs-down"></i>Unlike</a>
                @else
                    <a href="{{ route('activity.like',['id' => $a->id]) }}"><i class="far fa-thumbs-up"></i>Like</a>
                @endif
            </div>
            @break
            @case("post_create")
            <div class="activity-item">
                <p>There is a new post on our blog:
                    <a href="{{ route('post.show',['id' => $a->post->id,'slug' => $a->post->slug]) }}">{{ $a->post->title }}</a>
                </p>

                <span class="activity-likes">{{count($a->activityLikes)}} likes</span>

                @if(in_array($a->id,$liked))
                    <a href="{{ route('activity.unlike',['id' => $a->id]) }}"><i class="far fa-thumbs-down"></i>Unlike</a>
                @else
                    <a href="{{ route('activity.like',['id' => $a->id]) }}"><i class="far fa-thumbs-up"></i>Like</a>
                @endif
            </div>
            @break
            @case("new_user")
            <div class="activity-item"  data-activity-id="{{ $a->id }}">
                <p> <a href="{{ route('profile.show',['id' => $a->user->profile->id,'slug' => $a->user->profile->slug]) }}">{{ $a->user->name }}</a> became a member of our Locodor Community. Welcome!</p>

                @if(Auth::check())
                    <div class="post-interact-container">
                        <a href="{{ route('activity.unlike',['id' => $a->id]) }}" class="activity-interact activity-unlike" @if(!in_array($a->id,$liked)) style="display:none" @endif><i class="far fa-thumbs-down"></i>Unlike</a>
                        <a href="{{ route('activity.like',['id' => $a->id]) }}" class="activity-interact activity-like" @if(in_array($a->id,$liked)) style="display:none" @endif><i class="far fa-thumbs-up"></i> Like</a>
                        <span class="activity-interact comment-btn"><i class="far fa-comment"></i> Comment</span>
                        <span class="activity-likes"><span class="activity-likes-number">{{count($a->activityLikes)}}</span> likes</span>
                    </div>
                @else
                    <div class="post-interact-container">
                        <a href="{{ route('login') }}" class="activity-interact"><u>Sign in</u></a>
                        or
                        <a href="{{ route('register') }}" class="activity-interact"><u>Sign Up</u></a>
                        to comment and like posts.
                        <span class="activity-likes"><span class="activity-likes-number">{{count($a->activityLikes)}}</span> likes</span>
                    </div>
                @endif

                <div class="activity-comments">
                    @foreach($a->activityComments as $comment)
                        <p class="activity-comment" data-comment-id="{{ $comment->id }}" id="comment-{{$comment->id}}">
                            <img class="hover-image" src="{{ $comment->user->profile->avatar }}" width="40" height="40" alt="Crowdfunding User Avatar">
                            <a class="activity-comment-username" href="{{ route('profile.show',[ 'id' => $comment->user->profile->id, 'slug' => $user->profile->slug ]) }}">{{ $comment->user->name }}</a>
                            <span>{!! $comment->comment !!}</span>
                            <br>
                            @if($comment->user_id == Auth::id())
                                <a href="{{ route('activity.comment.delete',['id' => $comment->id ]) }}"><span class="float-right activity-comment-delete"><i class="fas fa-times"></i></span></a>
                            @endif
                        </p>
                    @endforeach
                    {{--<a>--}}
                    {{--<p class="activity-comment load-more">--}}
                    {{--Load More Comments--}}
                    {{--</p>--}}
                    {{--</a>--}}
                </div>
                <div class="activity-comment" id="activity-comment{{$a->id}}">
                    <form action="{{ route('activity.comment') }}" id="comment-form{{$a->id}}" method="post">
                        @csrf
                        <input type="hidden" value="{{ $a->id }}" name="activity_id">
                        <input type="hidden" name="comment" class="userpost-comment-content">
                        <div style="width:100%;height:72px;background-color:white;text-align:left;border:1px solid darkgray" contenteditable="true"  class="userpost-comment-div">
                        </div>
                        <button class="btn btn-dark btn-sm comment-submit-btn float-right" type="submit">Post</button>
                    </form>
                </div>
            </div>
            @break
            @case("project_funded")
            <div class="activity-item" data-activity-id="{{ $a->id }}">
                <div>
                    <p class="action-text">
                        <a href="{{ route('profile.show',[ 'id' => $a->user->profile->id, 'slug' => $a->user->profile->slug ]) }}">{{ $a->user->name }}'s</a>
                        project is fully funded!
                    </p>
                </div>
                <div class="user-action"  role="alert">
                    <p>
                        <a href="{{ route('project.show',['id'=>$a->project->id,'slug'=>$a->project->slug]) }}" rel="nofollow" target="_blank">
                            <img src="{{ $a->project->image }}" width="100%">
                        </a>
                    </p>
                    <div class="post-url-title">
                        <a href="{{ route('project.show',['id'=>$a->project->id,'slug'=>$a->project->slug]) }}" rel="nofollow" target="_blank">{{$a->project->title}}</a>
                    </div>
                    <div class="post-url-description">
                        <a href="{{ route('project.show',['id'=>$a->project->id,'slug'=>$a->project->slug]) }}" rel="nofollow" target="_blank">{{$a->project->subtitle}}</a>
                    </div>
                </div>
                @if(Auth::check())
                    <div class="post-interact-container">
                        <a href="{{ route('activity.unlike',['id' => $a->id]) }}" class="activity-interact activity-unlike" @if(!in_array($a->id,$liked)) style="display:none" @endif><i class="far fa-thumbs-down"></i>Unlike</a>
                        <a href="{{ route('activity.like',['id' => $a->id]) }}" class="activity-interact activity-like" @if(in_array($a->id,$liked)) style="display:none" @endif><i class="far fa-thumbs-up"></i> Like</a>
                        <span class="activity-interact comment-btn"><i class="far fa-comment"></i> Comment</span>
                        <span class="activity-likes"><span class="activity-likes-number">{{count($a->activityLikes)}}</span> likes</span>
                    </div>
                @else
                    <div class="post-interact-container">
                        <a href="{{ route('login') }}" class="activity-interact"><u>Sign in</u></a>
                        or
                        <a href="{{ route('register') }}" class="activity-interact"><u>Sign Up</u></a>
                        to comment and like posts.
                        <span class="activity-likes"><span class="activity-likes-number">{{count($a->activityLikes)}}</span> likes</span>
                    </div>
                @endif

                <div class="activity-comments">
                    @foreach($a->activityComments as $comment)
                        <p class="activity-comment" data-comment-id="{{ $comment->id }}" id="comment-{{$comment->id}}">
                            <img class="hover-image" src="{{ $comment->user->profile->avatar }}" width="40" height="40" alt="Crowdfunding User Avatar">
                            <a class="activity-comment-username" href="{{ route('profile.show',[ 'id' => $comment->user->profile->id, 'slug' => $user->profile->slug ]) }}">{{ $comment->user->name }}</a>
                            <span>{!! $comment->comment !!}</span>
                            <br>
                            @if($comment->user_id == Auth::id())
                                <a href="{{ route('activity.comment.delete',['id' => $comment->id ]) }}"><span class="float-right activity-comment-delete"><i class="fas fa-times"></i></span></a>
                            @endif
                        </p>
                    @endforeach
                    {{--<a>--}}
                    {{--<p class="activity-comment load-more">--}}
                    {{--Load More Comments--}}
                    {{--</p>--}}
                    {{--</a>--}}
                </div>
                <div class="activity-comment" id="activity-comment{{$a->id}}">
                    <form action="{{ route('activity.comment') }}" id="comment-form{{$a->id}}" method="post">
                        @csrf
                        <input type="hidden" value="{{ $a->id }}" name="activity_id">
                        <input type="hidden" name="comment" class="userpost-comment-content">
                        <div style="width:100%;height:72px;background-color:white;text-align:left;border:1px solid darkgray" contenteditable="true"  class="userpost-comment-div">
                        </div>
                        <button class="btn btn-dark btn-sm comment-submit-btn float-right" type="submit">Post</button>
                    </form>
                </div>
            </div>
            @break
            @case("new_backer")
            @if(isset($a->backer))
                <div class="activity-item" data-activity-id="{{ $a->id }}">
                    <div>
                        <p class="action-text">
                            @if($a->user_id != 99999 && $a->user_id != 0 && isset($a->backer->user) && isset($a->backer))
                                <a href="{{ route('profile.show',[ 'id' => $a->backer->user->profile->id, 'slug' => $a->backer->user->profile->slug ]) }}">{{ $a->backer->user->name }}</a>
                            @else
                                A guest
                            @endif
                            backed the project <a href="{{ route('project.show',['id' => $a->backer->project->id,'slug' => $a->backer->project->slug]) }}">"{{ $a->backer->project->title }}"</a> with ${{ $a->backer->amount }}
                        </p>
                    </div>
                    <div class="user-action" role="alert">
                        <p>
                            <a href="{{ route('project.show',['id'=>$a->backer->project->id,'slug'=>$a->backer->project->slug]) }}" rel="nofollow" target="_blank">
                                <img src="{{ $a->backer->project->image }}" width="100%">
                            </a>
                        </p>
                        <div class="post-url-title">
                            <a href="{{ route('project.show',['id'=>$a->backer->project->id,'slug'=>$a->backer->project->slug]) }}" rel="nofollow" target="_blank">{{$a->backer->project->title}}</a>
                        </div>
                        <div class="post-url-description">
                            <a href="{{ route('project.show',['id'=>$a->backer->project->id,'slug'=>$a->backer->project->slug]) }}" rel="nofollow" target="_blank">{{$a->backer->project->subtitle}}</a>
                        </div>
                    </div>

                    @if(Auth::check())
                        <div class="post-interact-container">
                            <a href="{{ route('activity.unlike',['id' => $a->id]) }}" class="activity-interact activity-unlike" @if(!in_array($a->id,$liked)) style="display:none" @endif><i class="far fa-thumbs-down"></i>Unlike</a>
                            <a href="{{ route('activity.like',['id' => $a->id]) }}" class="activity-interact activity-like" @if(in_array($a->id,$liked)) style="display:none" @endif><i class="far fa-thumbs-up"></i> Like</a>
                            <span class="activity-interact comment-btn"><i class="far fa-comment"></i> Comment</span>
                            <span class="activity-likes"><span class="activity-likes-number">{{count($a->activityLikes)}}</span> likes</span>
                        </div>
                    @else
                        <div class="post-interact-container">
                            <a href="{{ route('login') }}" class="activity-interact"><u>Sign in</u></a>
                            or
                            <a href="{{ route('register') }}" class="activity-interact"><u>Sign Up</u></a>
                            to comment and like posts.
                            <span class="activity-likes"><span class="activity-likes-number">{{count($a->activityLikes)}}</span> likes</span>
                        </div>
                    @endif

                    <div class="activity-comments">
                        @foreach($a->activityComments as $comment)
                            <p class="activity-comment" data-comment-id="{{ $comment->id }}" id="comment-{{$comment->id}}">
                                <img class="hover-image" src="{{ $comment->user->profile->avatar }}" width="40" height="40" alt="Crowdfunding User Avatar">
                                <a class="activity-comment-username" href="{{ route('profile.show',[ 'id' => $comment->user->profile->id, 'slug' => $user->profile->slug ]) }}">{{ $comment->user->name }}</a>
                                <span>{!! $comment->comment !!}</span>
                                <br>
                                @if($comment->user_id == Auth::id())
                                    <a href="{{ route('activity.comment.delete',['id' => $comment->id ]) }}"><span class="float-right activity-comment-delete"><i class="fas fa-times"></i></span></a>
                                @endif
                            </p>
                        @endforeach
                        {{--<a>--}}
                        {{--<p class="activity-comment load-more">--}}
                        {{--Load More Comments--}}
                        {{--</p>--}}
                        {{--</a>--}}
                    </div>
                    <div class="activity-comment" id="activity-comment{{$a->id}}">
                        <form action="{{ route('activity.comment') }}" id="comment-form{{$a->id}}" method="post">
                            @csrf
                            <input type="hidden" value="{{ $a->id }}" name="activity_id">
                            <input type="hidden" name="comment" class="userpost-comment-content">
                            <div style="width:100%;height:72px;background-color:white;text-align:left;border:1px solid darkgray" contenteditable="true"  class="userpost-comment-div">
                            </div>
                            <button class="btn btn-dark btn-sm comment-submit-btn float-right" type="submit">Post</button>
                        </form>
                    </div>
                </div>
            @endif
            @break
            @case("project_ended")
            <div class="activity-item" data-activity-id="{{ $a->id }}">
                <div>
                    <p class="action-text">
                        <a href="{{ route('profile.show',[ 'id' => $a->user->profile->id, 'slug' => $a->user->profile->slug ]) }}">{{ $a->user->name }}'s</a>
                        project has ended
                    </p>
                </div>
                <div class="user-action"  role="alert">
                    <p>
                        <a href="{{ route('project.show',['id'=>$a->project->id,'slug'=>$a->project->slug]) }}" rel="nofollow" target="_blank">
                            <img src="{{ $a->project->image }}" width="100%">
                        </a>
                    </p>
                    <div class="post-url-title">
                        <a href="{{ route('project.show',['id'=>$a->project->id,'slug'=>$a->project->slug]) }}" rel="nofollow" target="_blank">{{$a->project->title}}</a>
                    </div>
                    <div class="post-url-description">
                        <a href="{{ route('project.show',['id'=>$a->project->id,'slug'=>$a->project->slug]) }}" rel="nofollow" target="_blank">{{$a->project->subtitle}}</a>
                    </div>
                </div>
                @if(Auth::check())
                    <div class="post-interact-container">
                        <a href="{{ route('activity.unlike',['id' => $a->id]) }}" class="activity-interact activity-unlike" @if(!in_array($a->id,$liked)) style="display:none" @endif><i class="far fa-thumbs-down"></i>Unlike</a>
                        <a href="{{ route('activity.like',['id' => $a->id]) }}" class="activity-interact activity-like" @if(in_array($a->id,$liked)) style="display:none" @endif><i class="far fa-thumbs-up"></i> Like</a>
                        <span class="activity-interact comment-btn"><i class="far fa-comment"></i> Comment</span>
                        <span class="activity-likes"><span class="activity-likes-number">{{count($a->activityLikes)}}</span> likes</span>
                    </div>
                @else
                    <div class="post-interact-container">
                        <a href="{{ route('login') }}" class="activity-interact"><u>Sign in</u></a>
                        or
                        <a href="{{ route('register') }}" class="activity-interact"><u>Sign Up</u></a>
                        to comment and like posts.
                        <span class="activity-likes"><span class="activity-likes-number">{{count($a->activityLikes)}}</span> likes</span>
                    </div>
                @endif

                <div class="activity-comments">
                    @foreach($a->activityComments as $comment)
                        <p class="activity-comment" data-comment-id="{{ $comment->id }}" id="comment-{{$comment->id}}">
                            <img class="hover-image" src="{{ $comment->user->profile->avatar }}" width="40" height="40" alt="Crowdfunding User Avatar">
                            <a class="activity-comment-username" href="{{ route('profile.show',[ 'id' => $comment->user->profile->id, 'slug' => $user->profile->slug ]) }}">{{ $comment->user->name }}</a>
                            <span>{!! $comment->comment !!}</span>
                            <br>
                            @if($comment->user_id == Auth::id())
                                <a href="{{ route('activity.comment.delete',['id' => $comment->id ]) }}"><span class="float-right activity-comment-delete"><i class="fas fa-times"></i></span></a>
                            @endif
                        </p>
                    @endforeach
                    {{--<a>--}}
                    {{--<p class="activity-comment load-more">--}}
                    {{--Load More Comments--}}
                    {{--</p>--}}
                    {{--</a>--}}
                </div>
                <div class="activity-comment" id="activity-comment{{$a->id}}">
                    <form action="{{ route('activity.comment') }}" id="comment-form{{$a->id}}" method="post">
                        @csrf
                        <input type="hidden" value="{{ $a->id }}" name="activity_id">
                        <input type="hidden" name="comment" class="userpost-comment-content">
                        <div style="width:100%;height:72px;background-color:white;text-align:left;border:1px solid darkgray" contenteditable="true"  class="userpost-comment-div">
                        </div>
                        <button class="btn btn-dark btn-sm comment-submit-btn float-right" type="submit">Post</button>
                    </form>
                </div>
            </div>
            @break
            @case("new_profile_post")
            <div class="activity-item" data-activity-id="{{ $a->id }}">
                <div>
                    <p class="action-text">
                        <a href="{{ route('profile.show',[ 'id' => $a->user->profile->id, 'slug' => $a->user->profile->slug ]) }}">{{ $a->user->name }}</a>
                        <i class="fas fa-arrow-right"></i>
                        <a href="{{ route('profile.show',[ 'id' => $a->profilePost->user_2->profile->id, 'slug' => $a->profilePost->user_2->profile->slug ]) }}">{{ $a->profilePost->user_2->name }}</a>
                        <a href="{{ route('delete.profile.post',[ 'id'=>$a->profilePost->id ]) }}" class="float-right remove-activity-profile-post" data-profile-post-id="{{ $a->profilePost->id }}"><i class="fas fa-times"></i></a>

                    </p>
                </div>
                <div class="user-post-text" role="alert">
                    <p>
                        {{ $a->profilePost->message }}
                    </p>
                </div>
                @if(Auth::check())
                    <div class="post-interact-container">
                        <a href="{{ route('activity.unlike',['id' => $a->id]) }}" class="activity-interact activity-unlike" @if(!in_array($a->id,$liked)) style="display:none" @endif><i class="far fa-thumbs-down"></i>Unlike</a>
                        <a href="{{ route('activity.like',['id' => $a->id]) }}" class="activity-interact activity-like" @if(in_array($a->id,$liked)) style="display:none" @endif><i class="far fa-thumbs-up"></i> Like</a>
                        <span class="activity-interact comment-btn"><i class="far fa-comment"></i> Comment</span>
                        <span class="activity-likes"><span class="activity-likes-number">{{count($a->activityLikes)}}</span> likes</span>
                    </div>
                @else
                    <div class="post-interact-container">
                        <a href="{{ route('login') }}" class="activity-interact"><u>Sign in</u></a>
                        or
                        <a href="{{ route('register') }}" class="activity-interact"><u>Sign Up</u></a>
                        to comment and like posts.
                        <span class="activity-likes"><span class="activity-likes-number">{{count($a->activityLikes)}}</span> likes</span>
                    </div>
                @endif
                <div class="activity-comments">
                    @foreach($a->profilePost->replies as $comment)
                        <p class="activity-comment" data-comment-id="{{ $comment->id }}" id="comment-{{$comment->id}}">
                            <img class="hover-image" src="{{ $comment->user->profile->avatar }}" width="40" height="40" alt="Crowdfunding User Avatar">
                            <a class="activity-comment-username" href="{{ route('profile.show',[ 'id' => $comment->user->profile->id, 'slug' => $user->profile->slug ]) }}">{{ $comment->user->name }}</a>
                            <span>{!! $comment->reply !!}</span>
                            <br>
                            @if($comment->user_id == Auth::id())
                                <a href="{{ route('profile.reply.delete',['id' => $comment->id]) }}"><span class="float-right activity-reply-delete"><i class="fas fa-times"></i></span></a>
                            @endif
                        </p>
                    @endforeach
                    {{--<a>--}}
                    {{--<p class="activity-comment load-more">--}}
                    {{--Load More Comments--}}
                    {{--</p>--}}
                    {{--</a>--}}
                </div>
                <div class="activity-comment" id="activity-comment{{$a->id}}">
                    <form action="{{ route('profile.reply.store') }}" id="comment-form{{$a->id}}" method="post">
                        @csrf
                        <input type="hidden" value="{{ $a->profilePost->id }}" name="post_id">
                        <input type="hidden" name="reply" class="userpost-comment-content">
                        <div style="width:100%;height:72px;background-color:white;text-align:left;border:1px solid darkgray" contenteditable="true"  class="userpost-comment-div">
                        </div>
                        <button class="btn btn-dark btn-sm profile-post-reply-submit float-right" type="submit">Post</button>
                    </form>
                </div>
            </div>
            @break
            @case('user_post_create')
            <div class="activity-item" data-activity-id="{{ $a->id }}">
                <div class="user-hover-container" id="hover-container{{$a->id}}" onmouseleave="document.getElementById('hover-container{{$a->id}}').style.display = 'none';">
                    <div class="hover-profile">
                        <div class="row">
                            <div class="col-md-3 no-margin">
                                <img class="hover-image" src="{{ $a->user->profile->avatar }}" width="75" height="75" alt="Crowdfuning User Avatar" title="Member's Avatar of the crowdfuning platform">
                            </div>
                            <div class="col-md-9 no-margin">
                                <p><span class="hover-user-name"">{{ $a->user->name }}</span></p>
                                <a href="{{ route('profile.show',['id' => $a->user->profile->id, 'slug' => $a->user->profile->slug]) }}">View profile</a>
                                <p>"{{  $result = substr($a->user->profile->about, 0, 50)}}"</p>
                            </div>
                            <div class="col-md-12">
                                <div class="simple-line"></div>
                                <div class="hover-social">
                                    <p><b>Social</b></p>
                                    @if($a->user->id != Auth::id() && !in_array($a->user->id, $loggedUserFriendsId))
                                        <a href="{{ route('add.friend',['id' => $a->user->id ]) }}">Add Friend </a>
                                    @endif
                                    @if($a->user->id != Auth::id() && in_array($a->user->id, $loggedUserFriendsId))
                                        <a href="{{ route('remove.friend',['id' => $a->user->id ]) }}">Remove Friend </a>
                                    @endif
                                    <p><a href="{{ route('start.conversation',['id' => $a->user->id] ) }}"><i class="far fa-comment"></i> Private Message</a></p>
                                    <p><a href="{{ route('activity',['id' => $a->user->id] ) }}"><i class="far fa-comment"></i> Public Message</a></p>
                                </div>
                                <div class="simple-line"></div>
                                <div class="hover-campaigns">
                                    <p><b>Campaigns</b></p>
                                    <p><a href="#"><i class="far fa-lightbulb"></i>Created Campaigns({{ count($a->user->projects)  }})</a></p>
                                    <p><a href="#"><i class="fab fa-bitcoin"></i>Backed Campaigns(0)</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <p class="user-post-header">
                    <img class="hover-image" src="{{ $a->user->profile->avatar }}" width="40" height="40" alt="Crowdfuning User Avatar" title="Crowdfuning Member Avatar">
                    <a class="activity-username" href="{{ route('profile.show',[ 'id' => $a->user->profile->id, 'slug' => $a->user->profile->slug ]) }}" id="hover-name{{ $a->id }}">{{ $a->user->name }}</a>
                    <span class="activity-user-rank">{!! $a->user->rank->icon !!} <span>{{ $a->user->rank->name }}</span></span>

                    @if(Auth::check())
                        @if(Auth::user()->admin || $user->id == Auth::id())
                            <a href="{{ route('userpost.delete',[ 'id'=>$a->userpost->id ]) }}" class="float-right remove-user-post"><i class="fas fa-times"></i></a>
                        @endif
                    @endif
                </p>
                <script>
                    var element = document.getElementById('hover-name{{ $a->id }}');
                    var displayHover;

                    $('#hint').toggle('slow');
                    element.addEventListener("mouseover",function(){
                        displayHover = setTimeout(function(){
                            document.getElementById('hover-container{{$a->id}}').style.display = 'block';
                        }, 1000);
                    });
                    element.addEventListener("mouseout",function(){
                        clearTimeout(displayHover);
                    });
                    function bigImg(){
                        document.getElementById('hover-container{{$a->id}}').style.display = 'block';
                    }
                </script>
                <div class="user-post-text no-margin" role="alert">
                    <p>{!! $a->userpost->content !!}</p>
                </div>
                @if($a->userpost->image)
                    <img src="{{ asset($a->userpost->image) }}" class="user-post-image no-margin" alt="Crowdfuning Activity Upload User">
                @endif
                @if(Auth::check())
                    <div class="post-interact-container">
                        <a href="{{ route('activity.unlike',['id' => $a->id]) }}" class="activity-interact activity-unlike" @if(!in_array($a->id,$liked)) style="display:none" @endif><i class="far fa-thumbs-down"></i>Unlike</a>
                        <a href="{{ route('activity.like',['id' => $a->id]) }}" class="activity-interact activity-like" @if(in_array($a->id,$liked)) style="display:none" @endif><i class="far fa-thumbs-up"></i> Like</a>
                        <span class="activity-interact comment-btn"><i class="far fa-comment"></i> Comment</span>
                        <span class="activity-likes"><span class="activity-likes-number">{{count($a->activityLikes)}}</span> likes</span>
                    </div>
                @else
                    <div class="post-interact-container">
                        <a href="{{ route('login') }}" class="activity-interact"><u>Sign in</u></a>
                        or
                        <a href="{{ route('register') }}" class="activity-interact"><u>Sign Up</u></a>
                        to comment and like posts.
                        <span class="activity-likes"><span class="activity-likes-number">{{count($a->activityLikes)}}</span> likes</span>
                    </div>
                @endif

                <div class="activity-comments">
                    @foreach($a->activityComments as $comment)
                        <p class="activity-comment" data-comment-id="{{ $comment->id }}" id="comment-{{$comment->id}}">
                            <img class="hover-image" src="{{ $comment->user->profile->avatar }}" width="40" height="40" alt="Crowdfunding User Avatar">
                            <a class="activity-comment-username" href="{{ route('profile.show',[ 'id' => $comment->user->profile->id, 'slug' => $user->profile->slug ]) }}">{{ $comment->user->name }}</a>
                            <span>{!! $comment->comment !!}</span>
                            <br>
                            @if($comment->user_id == Auth::id())
                                <a href="{{ route('activity.comment.delete',['id' => $comment->id ]) }}"><span class="float-right activity-comment-delete"><i class="fas fa-times"></i></span></a>
                            @endif
                        </p>
                    @endforeach
                    {{--<a>--}}
                    {{--<p class="activity-comment load-more">--}}
                    {{--Load More Comments--}}
                    {{--</p>--}}
                    {{--</a>--}}
                </div>
                <div class="activity-comment" id="activity-comment{{$a->id}}">
                    <form action="{{ route('activity.comment') }}" id="comment-form{{$a->id}}" method="post">
                        @csrf
                        <input type="hidden" value="{{ $a->id }}" name="activity_id">
                        <input type="hidden" name="comment" class="userpost-comment-content">
                        <div style="width:100%;height:72px;background-color:white;text-align:left;border:1px solid darkgray" contenteditable="true"  class="userpost-comment-div">
                        </div>
                        <button class="btn btn-dark btn-sm comment-submit-btn float-right" type="submit">Post</button>
                    </form>
                </div>
            </div>
            @break
        @endswitch
    @endif
    @if( isset($a->title) )
        <div class="sponsored-activity sponsored-project">
            <div class="row">
                <div class="col-md-5">
                    <img src="{{ asset($a->image) }}" width="100%" style="max-height: 180px" alt="Crowdfunding Project Featured">
                </div>
                <div class="col-md-7">
                    <p><b><a href="{{ route('project.show',['id' => $a->id,'slug' => $a->slug ]) }}">{{ $a->title }}</a></b></p>
                    <p>{{ $a->subtitle }}</p>
                </div>
            </div>
            <span class="sponsored"><small>Sponsored</small></span>
        </div>
    @elseif(gettype($a) == "integer" && $a == 3)
        <div class="flip-container">
            <div class="flipper">
                <div class="front artist-1">
                    <div class="sponsored-activity menu-as-sponsored">
                        <div class="left-sidebar-auth">
                            <h1 class="activity-sidebar-title">Let us Help!</h1>
                            <p>Get a free consultation regarding your existent or a new campaign.</p>
                            <p>P.S. Don't worry there's no obligations</p>
                            <div class="text-center">
                                <a class="fancy-btn need-help-btn">Contact Us</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="back">
                    <div class="sponsored-activity menu-as-sponsored">
                        <div class="left-sidebar-auth">
                            <form action="{{ route('need.help') }}" method="post">
                                @csrf
                                <input type="hidden" name="user_id" value="{{ Auth::id() }}">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Email address</label>
                                    <input type="email" class="form-control" name="email" placeholder="Enter email">
                                    <small id="emailHelp" class="form-text text-muted">We'll use this email address to contact you.</small>
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlTextarea1">Message</label>
                                    <textarea class="form-control" name="message" rows="3"></textarea>
                                </div>
                                <button type="submit" class="btn btn-dark">Send</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script>
            $( document ).ready(function() {
                $('.need-help-btn').click(function(){
                    $(this).parent().parent().parent().parent().parent().toggleClass("flip");
                });

                $('.need-help-btn').on('click',function(){
                    $.ajax({
                        type: "Post",
                        url: "/need-help",
                        data: {
                            _token: "{{ csrf_token() }}",
                            user_id: {{Auth::id()}}
                        },
                        success: function(result) {
                            console.log('works');
                        },
                        error: function(result) {
                            console.log(result);
                            alert("Ops, something went wrong");
                        }
                    });
                });
            });

        </script>
    @elseif(gettype($a) == "integer" && $a == 6)
        <div class="sponsored-activity sponsored-activity-users menu-as-sponsored">
            <div>
                <h5 class="text-center">Our Members</h5>
                <hr>
                <div class="our-users-container-sponsored">
                    <div class="row">
                        @foreach($our_users as $user)
                            <div class="col-6 text-center">
                                <a href="{{ route('profile.show',['id' => $user->profile->id, 'slug' => $user->profile->slug]) }}"><img src="{{ asset($user->profile->avatar) }}" alt="Crowdfunding Avatar Member"></a><br>
                                <a href="{{ route('profile.show',['id' => $user->profile->id, 'slug' => $user->profile->slug]) }}">{{ $user->name }}</a>
                            </div>
                        @endforeach
                    </div>
                </div>
                <hr>

                <b><a href="{{ route('members') }}">View all</a></b>
            </div>
        </div>
    @elseif(gettype($a) == "integer" && $a == 9)
        <div class="sponsored-activity sponsored-activity-users menu-as-sponsored">
            <div>
                <h5 class="text-center">Get the Title you Deserve</h5>
                <hr>
                <p>
                    I want to become a:
                </p>
                <div class="social-get-rank">
                    <div class="get-rank-social-activity"><p>Super Backer</p></div>
                    <div class="get-rank-social-activity"><p>Service Provider</p></div>
                    <div class="get-rank-social-activity"><p>Angel Investor</p></div>
                </div>
                <script>
                    var clickedProvider = false;
                    $('.get-rank-social-activity').on('click',function(e){
                        var btnClicked = $(this);

                        e.preventDefault();
                        if(clickedProvider == false){
                            clickedProvider = true;
                            var btnClickedText = btnClicked.children().first().text();
                            btnClicked.html('<p style="font-size:16px">Request Sent.<br>Thank you!</p>');
                            $.ajax({
                                type: "Post",
                                url: "/need-help",
                                data: {
                                    _token: "{{ csrf_token() }}",
                                    user_id: {{Auth::id()}},
                                    email: "{{ Auth::user()->email }}",
                                    message: "I want to become a " + btnClickedText
                                },
                                success: function(result) {
                                    console.log('request-sent');
                                },
                                error: function(result) {
                                    console.log(result);
                                    alert("Ops, something went wrong");
                                }
                            });
                        }
                    });
                </script>
            </div>
        </div>
    @elseif(gettype($a) == "integer" && $a == 12)
        <div class="sponsored-activity sponsored-activity-users menu-as-sponsored">
            <div>
                <h5 class="text-center">Projects Popular Tags</h5>
                <hr>
                <p class="popular-tags-social-crowd text-center">
                    <a href="{{ route('projects',['filter' => 'tag','value' => 'Tech']) }}">#Tech</a>
                    <a href="{{ route('projects',['filter' => 'tag','value' => 'Fashion']) }}">#Fashion</a>
                    <a href="{{ route('projects',['filter' => 'tag','value' => 'Gadget']) }}">#Gadget</a>
                    <a href="{{ route('projects',['filter' => 'tag','value' => 'App']) }}">#App</a>
                    <a href="{{ route('projects',['filter' => 'tag','value' => 'fitness']) }}">#Fitness</a>
                </p>
                <hr>

                <b><a href="{{ route('projects') }}">View all</a></b>
            </div>
        </div>
    @endif
@endforeach