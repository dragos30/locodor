@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-3 activity-left-sidebar">
                @if( Auth::check() )
                    <div class="justify-content-center text-center left-sidebar-div1">

                        <a href="{{ route('profile.show',[ 'id' => $user->profile->id, 'slug' => $user->profile->slug ]) }}">
                            <img src="{{ asset($user->profile->avatar) }}" width="70" height="70" class="user-avatar" alt="Crowdfuning User Avatar"><br>
                        </a>
                        <a href="{{ route('profile.show',[ 'id' => $user->profile->id, 'slug' => $user->profile->slug ]) }}">
                            <h4 class="activity-user-name">{{ $user->name }}</h4>
                        </a>
                        <hr>

                        <div class="left-sidebar-menu text-left">
                            <p>Friends <a href="{{ route('profile.show',['id'=> $user->profile->id,'slug' => $user->profile->slug]) }}?friends=1">{{ count($all_friends) }}</a></p>
                        </div>

                    </div>

                    <div class="left-sidebar-div2">
                        <h5 class="text-center">Explore categories</h5>
                        <hr>
                        @foreach($categories as $category)
                            <p><a href="{{ route('projects',['filter' => 'category','value' => $category->id]) }}">{{ $category->name }}</a></p>
                        @endforeach
                        <hr>
                        <b><a href="{{ route('projects') }}?project_type=all">See all</a></b>
                    </div>

                    <div class="left-sidebar-div2">
                        <h5 class="text-center">Popular Tags</h5>
                        <hr>
                        <a href="{{ route('projects') }}?project_tag=robotics">#Robotics</a>
                        <a href="{{ route('projects') }}?project_tag=sci-fi">#Sci-Fi</a>
                        <a href="{{ route('projects') }}?project_tag=fantasy">#Fantasy</a>
                        <a href="{{ route('projects') }}?project_tag=environment">#Environment</a>
                        <a href="{{ route('projects') }}?project_tag=fitness">#Fitness</a>
                        <a href="{{ route('projects') }}?project_tag=wellness ">#Wellness</a>
                        <a href="{{ route('projects') }}?project_tag=gaming">#Gaming</a>
                        <a href="{{ route('projects') }}?project_tag=fashion">#Fashion</a>
                        <a href="{{ route('projects') }}?project_tag=wearable">#Wearable</a>
                        <a href="{{ route('projects') }}?project_tag=productivity">#Productivity</a>
                        <a href="{{ route('projects') }}?project_tag=high-tech">#High-Tech</a>
                        <a href="{{ route('projects') }}?project_tag=arts">#Arts</a>
                        <a href="{{ route('projects') }}?project_tag=bio-tech">#Bio-Tech</a>
                        <a href="{{ route('projects') }}?project_tag=gadgets">#Gadgets</a>
                        <a href="{{ route('projects') }}?project_tag=music">#Music</a>
                        <a href="{{ route('projects') }}?project_tag=apps">#Apps</a>
                        <a href="{{ route('projects') }}?project_tag=smart">#Smart</a>
                        <a href="{{ route('projects') }}?project_tag=3d-printing">#3D Printing</a>
                        <a href="{{ route('projects') }}?project_tag=tech">#Tech</a>
                        <a href="{{ route('projects') }}?project_tag=drones">#Drones</a>
                        <a href="{{ route('projects') }}?project_tag=vr">#VR</a>
                        <a href="{{ route('projects') }}?project_tag=travel">#Travel</a>
                        <a href="{{ route('projects') }}?project_tag=accessories">#Accessories</a>
                        <hr>
                        <b><a href="{{ route('projects') }}?project_type=all">See all</a></b>
                    </div>

                    <div class="left-sidebar-div2">
                        <h5 class="text-center">Our Members</h5>
                        <hr>
                        <div class="our-users-container">
                            @foreach($our_users as $user)
                                <div>
                                    <a href="{{ route('profile.show',['id' => $user->profile->id, 'slug' => $user->profile->slug]) }}"><img src="{{ asset($user->profile->avatar) }}" alt="Crowdfuning Member Avatar" title="Click on it to go to this user profile"></a>
                                    <a href="{{ route('profile.show',['id' => $user->profile->id, 'slug' => $user->profile->slug]) }}">{{ $user->name }}</a>
                                </div>

                            @endforeach
                        </div>
                        <hr>

                        <b><a href="{{ route('members') }}">View all</a></b>
                    </div>
                @else
                    <div class="justify-content-center text-center left-sidebar-auth">
                        <p>You are not logged in!</p>
                        <a href="{{ route('login') }}" class="btn btn-primary">Login</a>
                        <a href="{{ route('register') }}" class="btn btn-primary">Register</a>
                    </div>
                @endif
            </div>

            <div class="col-md-6 activity-main-content">
                    {{--Check the type of action to display the right sentance and info--}}
                @switch($a->action_name)
                    @case("project_create")
                    <div class="activity-item">
                        <p>
                            <a href="{{ route('profile.show',[ 'id' => $a->user->profile->id ]) }}">{{ $a->user->name }}</a>
                            created a project:
                            <a href="{{ route('project.show',['id' => $a->action_id]) }}">{{ $a->project->title }}</a>
                        </p>
                        <span class="activity-likes">{{count($a->activityLikes)}} likes</span>

                        @if(in_array($a->id,$liked))
                            <a href="{{ route('activity.unlike',['id' => $a->id]) }}"><i class="far fa-thumbs-down"></i>Unlike</a>
                        @else
                            <a href="{{ route('activity.like',['id' => $a->id]) }}"><i class="far fa-thumbs-up"></i>Like</a>
                        @endif
                    </div>
                    @break
                    @case("discussion_create")
                    <div class="activity-item">
                        <p>
                            <a href="{{ route('profile.show',[ 'id' => $a->user->id ]) }}">{{ $a->user->name }}</a>
                            created a discussion:
                            <a href="{{ route('discussion.show',['id' => $a->action_id]) }}">{{ $a->discussion->title }}</a>
                        </p>
                        <span class="activity-likes">{{count($a->activityLikes)}} likes</span>

                        @if(in_array($a->id,$liked))
                            <a href="{{ route('activity.unlike',['id' => $a->id]) }}"><i class="far fa-thumbs-down"></i>Unlike</a>
                        @else
                            <a href="{{ route('activity.like',['id' => $a->id]) }}"><i class="far fa-thumbs-up"></i>Like</a>
                        @endif
                    </div>
                    @break
                    @case("discussion_reply")
                    <div class="activity-item">
                        <p>
                            <a href="{{ route('profile.show',[ 'id' => $a->user->id ]) }}">{{ $a->user->name }}</a>
                            replied "{{ $a->reply->content }}" on the discussion
                            <a href="{{ route('discussion.show',['id' => $a->reply->discussion_id ]) }}">{{ $a->reply->discussion->title }}</a>
                        </p>
                        <span class="activity-likes">{{count($a->activityLikes)}} likes</span>

                        @if(in_array($a->id,$liked))
                            <a href="{{ route('activity.unlike',['id' => $a->id]) }}"><i class="far fa-thumbs-down"></i>Unlike</a>
                        @else
                            <a href="{{ route('activity.like',['id' => $a->id]) }}"><i class="far fa-thumbs-up"></i>Like</a>
                        @endif
                    </div>
                    @break
                    @case("project_comment")
                    <div class="activity-item">
                        <p>
                            <a href="{{ route('profile.show',[ 'id' => $a->user->id ]) }}">{{ $a->user->name }}</a>
                            commented "{{ $a->comment->comment }}" on project
                            <a href="{{ route('project.show',['id' => $a->comment->project_id ]) }}">{{ $a->comment->project->title }}</a>
                        </p>
                        <span class="activity-likes">{{count($a->activityLikes)}} likes</span>

                        @if(in_array($a->id,$liked))
                            <a href="{{ route('activity.unlike',['id' => $a->id]) }}"><i class="far fa-thumbs-down"></i>Unlike</a>
                        @else
                            <a href="{{ route('activity.like',['id' => $a->id]) }}"><i class="far fa-thumbs-up"></i>Like</a>
                        @endif
                    </div>
                    @break
                    @case("topic_create")
                    <div class="activity-item">
                        <p>
                            <a href="{{ route('profile.show',[ 'id' => $a->user->id ]) }}">{{ $a->user->name }}</a>
                            created a new topic:
                            <a href="{{ route('topic.show',['id' => $a->action_id ]) }}">{{ $a->topic->name }}</a>
                        </p>
                        <span class="activity-likes">{{count($a->activityLikes)}} likes</span>

                        @if(in_array($a->id,$liked))
                            <a href="{{ route('activity.unlike',['id' => $a->id]) }}"><i class="far fa-thumbs-down"></i>Unlike</a>
                        @else
                            <a href="{{ route('activity.like',['id' => $a->id]) }}"><i class="far fa-thumbs-up"></i>Like</a>
                        @endif
                    </div>
                    @break

                    @case("comment_like")
                    <div class="activity-item">
                        <div class="user-hover-container" id="hover-container{{$a->id}}" onmouseleave="document.getElementById('hover-container{{$a->id}}').style.display = 'none';">
                            <div class="hover-profile">
                                <div class="row">
                                    <div class="col-md-3 no-margin">
                                        <img class="hover-image" src="{{ $a->user->profile->avatar }}" width="75" height="75">
                                    </div>
                                    <div class="col-md-9 no-margin">
                                        <p><span class="hover-user-name"">{{ $a->user->name }}</span></p>
                                        <a href="{{ route('profile.show',['id' => $a->user->id]) }}">View profile</a>
                                        <p>"{{  $result = substr($a->user->profile->about, 0, 50)}}"</p>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="simple-line"></div>
                                        <div class="hover-social">
                                            <p><b>Social</b></p>
                                            <p><a href="#"><i class="fas fa-user-plus"></i>Add friend</a></p>
                                        </div>
                                        <div class="simple-line"></div>
                                        <div class="hover-messaging">
                                            <p><b>Messaging</b></p>
                                            <p><a href="#"><i class="far fa-comment"></i>Send Private</a><a href="#"><i class="far fa-comments"></i>Send Public</a></p>
                                        </div>
                                        <div class="simple-line"></div>
                                        <div class="hover-campaigns">
                                            <p><b>Campaigns</b></p>
                                            <p><a href="#"><i class="far fa-lightbulb"></i>Created Campaigns({{ count($a->user->projects)  }})</a></p>
                                            <p><a href="#"><i class="fab fa-bitcoin"></i>Backed Campaigns(0)</a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <p>
                            <a href="{{ route('profile.show',[ 'id' => $a->user->id ]) }}" id="hover-name{{ $a->id }}" onmouseover="document.getElementById('hover-container{{$a->id}}').style.display = 'block';">{{ $a->user->name }}</a>
                            liked a comment on the project
                            <a href="{{ route('project.show',['id' => $a->commentLike->comment->project->id ]) }}">{{ $a->commentLike->comment->project->title }}</a>
                        </p>
                        <span class="activity-likes">{{count($a->activityLikes)}} likes</span>

                        @if(in_array($a->id,$liked))
                            <a href="{{ route('activity.unlike',['id' => $a->id]) }}"><i class="far fa-thumbs-down"></i>Unlike</a>
                        @else
                            <a href="{{ route('activity.like',['id' => $a->id]) }}"><i class="far fa-thumbs-up"></i>Like</a>
                        @endif
                    </div>
                    @break
                    @case("reply_like")
                    <div class="activity-item">
                        <p>
                            <a href="{{ route('profile.show',[ 'id' => $a->user->id ]) }}">{{ $a->user->name }}</a>
                            liked a reply on the discussion
                            <a href="{{ route('discussion.show',['id' => $a->replyLike->reply->discussion->id ]) }}">{{ $a->replyLike->reply->discussion->title }}</a>
                        </p>
                        <span class="activity-likes">{{count($a->activityLikes)}} likes</span>

                        @if(in_array($a->id,$liked))
                            <a href="{{ route('activity.unlike',['id' => $a->id]) }}"><i class="far fa-thumbs-down"></i>Unlike</a>
                        @else
                            <a href="{{ route('activity.like',['id' => $a->id]) }}"><i class="far fa-thumbs-up"></i>Like</a>
                        @endif
                    </div>
                    @break
                    @case("discussion_like")
                    <div class="activity-item">
                        <p>
                            <a href="{{ route('profile.show',[ 'id' => $a->user->id ]) }}">{{ $a->user->name }}</a>
                            upvoted the discussion
                            <a href="{{ route('discussion.show',['id' => $a->discussionLike->discussion->id ]) }}">{{ $a->discussionLike->discussion->title }}</a>
                        </p>
                        <span class="activity-likes">{{count($a->activityLikes)}} likes</span>

                        @if(in_array($a->id,$liked))
                            <a href="{{ route('activity.unlike',['id' => $a->id]) }}"><i class="far fa-thumbs-down"></i>Unlike</a>
                        @else
                            <a href="{{ route('activity.like',['id' => $a->id]) }}"><i class="far fa-thumbs-up"></i>Like</a>
                        @endif
                    </div>
                    @break
                    @case("discussion_watch")
                    <div class="activity-item">
                        <p>
                            <a href="{{ route('profile.show',[ 'id' => $a->user->id ]) }}">{{ $a->user->name }}</a>
                            is watching
                            <a href="{{ route('discussion.show',['id' => $a->watcher->discussion->id ]) }}">{{ $a->watcher->discussion->title }}</a>
                        </p>
                        <span class="activity-likes">{{count($a->activityLikes)}} likes</span>

                        @if(in_array($a->id,$liked))
                            <a href="{{ route('activity.unlike',['id' => $a->id]) }}"><i class="far fa-thumbs-down"></i>Unlike</a>
                        @else
                            <a href="{{ route('activity.like',['id' => $a->id]) }}"><i class="far fa-thumbs-up"></i>Like</a>
                        @endif
                    </div>
                    @break
                    @case("post_create")
                    <div class="activity-item">
                        <p>There is a new post on our blog:
                            <a href="{{ route('post.show',['id' => $a->post->id]) }}">{{ $a->post->title }}</a>
                        </p>

                        <span class="activity-likes">{{count($a->activityLikes)}} likes</span>

                        @if(in_array($a->id,$liked))
                            <a href="{{ route('activity.unlike',['id' => $a->id]) }}"><i class="far fa-thumbs-down"></i>Unlike</a>
                        @else
                            <a href="{{ route('activity.like',['id' => $a->id]) }}"><i class="far fa-thumbs-up"></i>Like</a>
                        @endif
                    </div>
                    @break
                    @case("user_post_create")
                    <div class="activity-item" data-activity-id="{{ $a->id }}">
                        <div class="user-hover-container" id="hover-container{{$a->id}}" onmouseleave="document.getElementById('hover-container{{$a->id}}').style.display = 'none';">
                            <div class="hover-profile">
                                <div class="row">
                                    <div class="col-md-3 no-margin">
                                        <img class="hover-image" src="{{ $a->user->profile->avatar }}" width="75" height="75" alt="Crowdfuning User Avatar" title="Member's Avatar of the crowdfuning platform">
                                    </div>
                                    <div class="col-md-9 no-margin">
                                        <p><span class="hover-user-name"">{{ $a->user->name }}</span></p>
                                        <a href="{{ route('profile.show',['id' => $a->user->profile->id, 'slug' => $a->user->profile->slug]) }}">View profile</a>
                                        <p>"{{  $result = substr($a->user->profile->about, 0, 50)}}"</p>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="simple-line"></div>
                                        <div class="hover-social">
                                            <p><b>Social</b></p>
                                            @if($a->user->id != Auth::id() && !in_array($a->user->id, $loggedUserFriendsId))
                                                <a href="{{ route('add.friend',['id' => $a->user->id ]) }}">Add Friend </a>
                                            @endif
                                            @if($a->user->id != Auth::id() && in_array($a->user->id, $loggedUserFriendsId))
                                                <a href="{{ route('remove.friend',['id' => $a->user->id ]) }}">Remove Friend </a>
                                            @endif
                                            <p><a href="{{ route('start.conversation',['id' => $a->user->id] ) }}"><i class="far fa-comment"></i> Private Message</a></p>
                                            <p><a href="{{ route('activity',['id' => $a->user->id] ) }}"><i class="far fa-comment"></i> Public Message</a></p>
                                        </div>
                                        <div class="simple-line"></div>
                                        <div class="hover-campaigns">
                                            <p><b>Campaigns</b></p>
                                            <p><a href="#"><i class="far fa-lightbulb"></i>Created Campaigns({{ count($a->user->projects)  }})</a></p>
                                            <p><a href="#"><i class="fab fa-bitcoin"></i>Backed Campaigns(0)</a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <p class="user-post-header">
                            <img class="hover-image" src="{{ $a->user->profile->avatar }}" width="40" height="40" alt="Crowdfuning User Avatar" title="Crowdfuning Member Avatar">
                            <a class="activity-username" href="{{ route('profile.show',[ 'id' => $a->user->profile->id, 'slug' => $a->user->profile->slug ]) }}" id="hover-name{{ $a->id }}">{{ $a->user->name }}</a>
                            <span>posted:</span>
                            @if(Auth::check())
                                @if(Auth::user()->admin || $user->id == Auth::id())
                                    <a href="{{ route('userpost.delete',[ 'id'=>$a->userpost->id ]) }}" class="float-right"><i class="fas fa-times"></i></a>
                                @endif
                            @endif
                        </p>
                        <script>
                            var element = document.getElementById('hover-name{{ $a->id }}');
                            var displayHover;

                            $('#hint').toggle('slow');
                            element.addEventListener("mouseover",function(){
                                displayHover = setTimeout(function(){
                                    document.getElementById('hover-container{{$a->id}}').style.display = 'block';
                                }, 1000);
                            });
                            element.addEventListener("mouseout",function(){
                                clearTimeout(displayHover);
                            });
                            function bigImg(){
                                document.getElementById('hover-container{{$a->id}}').style.display = 'block';
                            }
                        </script>
                        <div class="user-post-text no-margin" role="alert">
                            <p>{!! $a->userpost->content !!}</p>
                        </div>
                        @if($a->userpost->image)
                            <img src="{{ asset($a->userpost->image) }}" class="user-post-image no-margin" alt="Crowdfuning Activity Upload User">
                        @endif
                        @if(Auth::check())
                            <div class="post-interact-container">
                                <a href="{{ route('activity.unlike',['id' => $a->id]) }}" class="activity-interact activity-unlike" @if(!in_array($a->id,$liked)) style="display:none" @endif><i class="far fa-thumbs-down"></i>Unlike</a>
                                <a href="{{ route('activity.like',['id' => $a->id]) }}" class="activity-interact activity-like" @if(in_array($a->id,$liked)) style="display:none" @endif><i class="far fa-thumbs-up"></i> Like</a>
                                <span class="activity-interact comment-btn"><i class="far fa-comment"></i> Comment</span>
                                <span class="activity-likes"><span class="activity-likes-number">{{count($a->activityLikes)}}</span> likes</span>
                            </div>
                        @else
                            <div class="post-interact-container">
                                <a href="{{ route('login') }}" class="activity-interact"><u>Sign in</u></a>
                                or
                                <a href="{{ route('register') }}" class="activity-interact"><u>Sign Up</u></a>
                                to comment and like posts.
                                <span class="activity-likes"><span class="activity-likes-number">{{count($a->activityLikes)}}</span> likes</span>
                            </div>
                        @endif

                        <div class="activity-comments">
                            @foreach($activity_comments as $comment)
                                @if($comment->activity_id == $a->id)
                                    <p class="activity-comment" data-comment-id="{{ $comment->id }}" id="comment-{{$comment->id}}">
                                        <img class="hover-image" src="{{ $comment->user->profile->avatar }}" width="40" height="40" alt="Crowdfunding User Avatar">
                                        <a class="activity-username" href="{{ route('profile.show',[ 'id' => $comment->user->profile->id, 'slug' => $user->profile->slug ]) }}">{{ $comment->user->name }}</a>
                                        <span>{!! $comment->comment !!}</span>
                                        <br>
                                        @if($comment->user_id == Auth::id())
                                            <a href="{{ route('activity.comment.delete',['id' => $comment->id ]) }}"><span class="float-right activity-comment-delete"><i class="fas fa-times"></i></span></a>
                                        @endif
                                    </p>
                                @endif
                            @endforeach
                            {{--<a>--}}
                            {{--<p class="activity-comment load-more">--}}
                            {{--Load More Comments--}}
                            {{--</p>--}}
                            {{--</a>--}}
                        </div>
                        {{--<script>--}}
                        {{--var elements;--}}
                        {{--var currentElements = 0;--}}
                        {{--$( document ).ready(function() {--}}
                        {{--elements = $('.elements').children();--}}
                        {{--});--}}

                        {{--$('#btn').click(function(){--}}
                        {{--currentElements++;--}}
                        {{--$('.test').append(elements.slice(0,currentElements));--}}
                        {{--});--}}
                        {{--</script>--}}
                        <div class="activity-comment" id="activity-comment{{$a->id}}">
                            <form action="{{ route('activity.comment') }}" id="comment-form{{$a->id}}" method="post">
                                @csrf
                                <input type="hidden" value="{{ $a->id }}" name="activity_id">
                                <input type="hidden" name="comment" class="userpost-comment-content">
                                <div style="width:100%;height:72px;background-color:white;text-align:left;border:1px solid darkgray" contenteditable="true"  class="userpost-comment-div">
                                </div>
                                {{--<textarea placeholder="Leave a comment" id="comment-field{{$a->id}}" class="activity-comment-textarea" name="comment"></textarea>--}}
                                <button class="btn btn-dark btn-sm comment-submit-btn float-right" type="submit">Post</button>
                            </form>
                        </div>
                    </div>
                    @break
                @endswitch
            </div>
            <div class="col-md-3 activity-right-sidebar">
                <h4>Locodor</h4>
                <div class="right-sidebar-menu-item">
                    <i class="fas fa-dollar-sign right-sidebar-icon"></i>
                    <span class="right-sidebar-action">Raising Now</span>
                </div>
                <div class="right-sidebar-menu-item">
                    <i class="fas fa-newspaper right-sidebar-icon"></i>
                    <span class="right-sidebar-action">Venture News</span>
                </div>
                <div class="right-sidebar-menu-item">
                    <i class="fas fa-user-tie right-sidebar-icon"></i>
                    <span class="right-sidebar-action">By Locodor</span>
                </div>
                <div class="right-sidebar-menu-item">
                    <i class="fas fa-chart-line right-sidebar-icon"></i>
                    <span class="right-sidebar-action">Popular</span>
                </div>
                <hr>
                <small>Latest projects</small><br>


                @foreach($projects as $project)
                    <div class="latest-project-container">
                        <p><b>{{ $project->created_at->format('l'.' , '. 'j' .' F') }}</b> </p>
                        <div class="right-sidebar-image" style="background-image: url({{ asset($project->image) }})"></div>
                        <?php $title = substr($project->title, 0, 80); ?>
                        <p class="right-sidebar-title"><b>{{ $title }}</b></p><br>
                        <span class="sidebar-project-cat">{{ $project->category->name }}</span>
                    </div>
                    <hr>
                @endforeach
            </div>
        </div>
    </div>
@endsection
