@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 social-community-title">
                <h3 class="text-center">Crowdfunding Community</h3>
                <p class="text-center">Thank you for joining our community of creators and investors.<br>
                    Find unique crowdfunding campaigns, connect with founders & backers,<br>
                    Promote your work and grow your global audience!</p>
            </div>
            <div class="col-md-3 activity-left-sidebar">
                @if( Auth::check() )
                    <div class="justify-content-center text-center left-sidebar-div1">

                        <a href="{{ route('profile.show',[ 'id' => $user->profile->id, 'slug' => $user->profile->slug ]) }}">
                            <img src="{{ asset($user->profile->avatar) }}" width="70" height="70" class="user-avatar" alt="crowdfuning-user-avatar"><br>
                        </a>
                        <a href="{{ route('profile.show',[ 'id' => $user->profile->id, 'slug' => $user->profile->slug ]) }}">
                            <h4 class="activity-user-name">{{ $user->name }}</h4>
                        </a>
                        <hr>
                        {{--<p><a href="{{ route('projects.created') }}" {{count($user->projects) }} projects</p>--}}
                        {{--<p><a href="{{ route('project.create') }}">Create project</a></p>--}}
                        {{--<p><a href="{{ route('projects') }}">Browse projects</a></p>--}}
                        {{--<p><a href="{{ route('posts') }}">Read news</a></p>--}}
                        <div class="left-sidebar-menu text-left">
                            <p>Friends <a href="#">{{ count($all_friends) }}</a></p>
                            <p>Who viewed your profile <a href="#">9</a></p>
                        </div>

                    </div>

                    <div class="left-sidebar-div2">
                        <h5 class="text-center">Your Categories</h5>
                        <hr>
                        @foreach($categories as $category)
                            <p><a href="#">#{{ $category->name }}</a></p>
                        @endforeach
                        <hr>

                        <b><a href="#">Discover more</a></b>
                    </div>

                    <div class="left-sidebar-div2">
                        <h5 class="text-center">Our Members</h5>
                        <hr>
                        <div class="our-users-container">
                            @foreach($our_users as $user)
                                <div>
                                    <a href="{{ route('profile.show',['id' => $user->profile->id, 'slug' => $user->profile->slug]) }}"><img src="{{ asset($user->profile->avatar) }}" alt="crowdfuning-member-avatar"></a>
                                    <a href="{{ route('profile.show',['id' => $user->profile->id, 'slug' => $user->profile->slug]) }}">{{ $user->name }}</a>
                                </div>

                            @endforeach
                        </div>
                        <hr>

                        <b><a href="{{ route('members') }}">View all</a></b>
                    </div>


                @else
                    <div class="left-sidebar-auth">
                        <h1 class="activity-sidebar-title">Locodor Social Crowdfunding</h1>
                        <h3>Connect and Collaborate</h3>
                        <h3>Join the Locodor Community</h3>
                        <div>
                            <a href="#">Get started</a>
                            <a href="{{ route('howitworks') }}" class="float-right">How it works ></a>
                        </div>
                        <form action="/register" method="get" class="text-center">
                            <input class="form-control" type="text" placeholder="Your Email" name="quick-email">
                            <button type="submit" class="home-become-btn">Become one of us!</button>
                        </form>
                    </div>
                    <br>
                    <div class="left-sidebar-div2">
                        <h5 class="text-center">Our Members</h5>
                        <hr>
                        <div class="our-users-container">
                            @foreach($our_users as $user)
                                <div>
                                    <a href="{{ route('profile.show',['id' => $user->profile->id, 'slug' => $user->profile->slug]) }}"><img src="{{ asset($user->profile->avatar) }}" alt="crowdfuning-member-avatar"></a>
                                    <a href="{{ route('profile.show',['id' => $user->profile->id, 'slug' => $user->profile->slug]) }}">{{ $user->name }}</a>
                                </div>

                            @endforeach
                        </div>
                        <hr>

                        <b><a href="{{ route('members') }}">View all</a></b>
                    </div>
                @endif
            </div>


            <script>
                var tribute = new Tribute({
                    values: [
                            @foreach($users as $user)
                        {key: '{{ $user->name }}', value: '{{$user->name}}', id: '{{ $user->id }}'},
                        @endforeach
                    ],
                    selectTemplate: function (item) {
                        return '<a href="/profile/show/'+ item.original.id +'">' + item.original.value + '</a>';
                    }
                })
            </script>

            <script>
                function syncInput(){
                    var input = document.getElementById('user-post-content');
                    input.value = document.getElementById('user-post-editor').innerHTML;
                }
            </script>





            <div class="col-md-6 activity-main-content">
                <div class="activity-menu row">
                    <div class="col-md-6">
                        <a href="{{ route('activity') }}" >Users Posts</a>
                    </div>
                    <div class="col-md-6">
                        <a href="{{ route('actions') }}"  class="active" >Users Activity</a>
                    </div>
                </div>


                @foreach($activity as $a)
                    {{--Check the type of action to display the right sentance and info--}}

                @endforeach
            </div>
            <div class="col-md-3 activity-right-sidebar">
                <h4>Locodor</h4>
                <div class="right-sidebar-menu-item">
                    <a href="{{ route('projects',['filter' => 'sort','value'=>'new']) }}">
                        <i class="fas fa-dollar-sign right-sidebar-icon"></i>
                        <span class="right-sidebar-action">Raising Now</span>
                    </a>
                </div>
                <div class="right-sidebar-menu-item">
                    <a href="{{ route('posts') }}">
                        <i class="fas fa-newspaper right-sidebar-icon"></i>
                        <span class="right-sidebar-action">Venture News</span>
                    </a>
                </div>
                <div class="right-sidebar-menu-item">
                    <a href="{{ route('projects',['filter'=>'sort','value'=>'exclusive']) }}">
                        <i class="fas fa-user-tie right-sidebar-icon"></i>
                        <span class="right-sidebar-action">By Locodor</span>
                    </a>
                </div>
                <div class="right-sidebar-menu-item">
                    <a href="{{ route('projects',['filter'=>'sort','value'=>'trending']) }}">
                        <i class="fas fa-chart-line right-sidebar-icon"></i>
                        <span class="right-sidebar-action">Popular</span>
                    </a>
                </div>
                <hr>
                <small>Latest projects</small><br>


                @foreach($projects as $project)
                    <div class="latest-project-container">
                        <p><b>{{ $project->created_at->format('l'.' , '. 'j' .' F') }}</b> </p>
                        <div class="right-sidebar-image" style="background-image: url({{ asset($project->image) }})"></div>
                        <?php $title = substr($project->title, 0, 65); ?>
                        <p class="right-sidebar-title"><a href="{{ route('project.show',['id' => $project->id,'slug' => $project->slug]) }}"><b>{{ $title }}</b></a></p><br>
                        <span class="sidebar-project-cat">{{ $project->category->name }}</span>
                    </div>
                    <hr>
                @endforeach
            </div>
        </div>
    </div>
@endsection
