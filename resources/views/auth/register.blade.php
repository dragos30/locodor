@extends('layouts.app')

@section('content')
<div class="container register-page">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <img src="{{ asset('uploads/favicon.png') }}" width="50" height="50"><p class="register-title">Create an account</p>
            <hr>
            <form method="POST" action="{{ route('register') }}<?php if(isset($_GET['bot-rmc'])) echo '?bot-rmc'; ?>" aria-label="{{ __('Register') }}">
                @csrf
                <input type="hidden" name="url" value="<?php if(isset($_GET['url'])) echo $_GET['url']; ?>">
                <div class="border-bottom-inputs">
                    <div class="form-group fullname-container">
                        <label for="name">Lets get to know each other</label>

                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="<?php if(isset($_GET['url'])){echo $_GET['name'];}else{old('name');} ?>" required autofocus placeholder="Full name">

                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="email">Email</label>
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="@if( isset($_GET['url']) ) {{ $_GET['email'] }} @elseif( isset($_GET['quick-email'])) {{ $_GET['quick-email'] }} @else {{ old('email') }} @endif
" required placeholder="bill@gates.com">

                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('email') }}</strong>
                    </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="password">Create a password</label>

                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Password">

                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('password') }}</strong>
                    </span>
                        @endif
                    </div>
                </div>

                {{--<div class="form-group username-container">--}}
                    {{--<label for="username">Choose a cool username (optional)</label>--}}

                    {{--<div class="input-group mb-3">--}}
                        {{--<div class="input-group-prepend">--}}
                            {{--<span class="input-group-text pre-input" id="basic-addon1">@</span>--}}
                        {{--</div>--}}
                        {{--<input id="username" type="text" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ old('username') }}" autofocus placeholder="Crowdfunder1234">--}}
                    {{--</div>--}}
                    {{--@if ($errors->has('username'))--}}
                        {{--<span class="invalid-feedback" role="alert">--}}
                                {{--<strong>{{ $errors->first('username') }}</strong>--}}
                            {{--</span>--}}
                    {{--@endif--}}
                {{--</div>--}}

                @if(!isset($_GET['bot-rmc']))
                    <div class="captcha-container">
                        {!! NoCaptcha::display() !!}
                        @if ($errors->has('g-recaptcha-response'))
                            <span class="help-block">
                            <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                        </span>
                        @endif
                    </div>
                @endif

                <div class="text-center">
                    <button type="submit" class="btn bttn btn-primary text-center normal-btn">Register</button>
                </div>

                <p>or join with...</p>
                <div class="social-login-container">
                    <div>
                        <a href="/login/facebook" class="button-facebook login-social-button">
                            <span class="login-icon"><i class="fab fa-facebook-f"></i></span>Continue with Facebook
                        </a>
                    </div>
                    <div>
                        <a href="/login/linkedin" class="button-linkin login-social-button">
                            <span class="login-icon"><i class="fab fa-linkedin-in"></i></span>Continue with Linkedin
                        </a>
                    </div>
                    <div>
                        <a href="/login/google" class="button-google login-social-button">
                            <span class="login-icon"><i class="fab fa-google"></i></span>Continue with Google
                        </a>
                    </div>
                    <div>
                        <a href="/login/twitter" class="button-twitter login-social-button">
                            <span class="login-icon"><i class="fab fa-twitter"></i></span>Continue with Twitter
                        </a>
                    </div>
                </div>


            </form>
        </div>
    </div>
</div>
@endsection
