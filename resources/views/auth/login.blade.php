@extends('layouts.app')

@section('content')
    <div class="container login-container">
        <div class="row justify-content-center">
            <div class="col-md-4">
                @if(!isset($_GET['d']))
                    <div class="join-locodor-popup">
                        <p class="join-locodor-main-text">BE PART OF THE</p>
                        <div class="join-locodor-white-line">
                            <div class="join-locodor-text">
                                <p>CROWDFUNDING COMMUNITY</p>
                            </div>
                        </div>
                        <p class="join-locodor-btn"><a href="{{ route('register') }}">Register Now!</a></p>
                        <span class="join-locodor-popup-close"><i class="fas fa-times"></i></span>
                    </div>
                @endif
                <script>
                    $( document ).ready(function() {
                        setTimeout(function(){

                            $('.join-locodor-popup').fadeIn();
                            var popupInterval = setInterval(function(){
                                if($('.join-locodor-popup').is(':hover')){
                                }else{
                                    $('.join-locodor-popup').fadeOut();
                                    clearInterval(popupInterval);
                                }
                            }, 30000);
                        }, 3000);

                        $('.join-locodor-popup-close').on('click',function(){
                            $('.join-locodor-popup').fadeOut();
                        });
                    });

                </script>
                <a href="{{ route("home") }}">
                    <div class="text-center">
                        <img src="{{ asset('/uploads/favicon.png') }}" alt="Locodor Logo" class="login-logo">
                    </div>
                </a>
                <p class="login-title">Welcome back!</p>
                <form method="POST" action="{{ route('login') }}<?php if(isset($_GET['bot-rmc'])) echo '?bot-rmc'; ?>" aria-label="{{ __('Login') }}">
                    @csrf

                    <div class="border-bottom-inputs">
                        <div class="form-group row">
                            <div class="login-email-div">
                                <input id="email" type="email" placeholder="Email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                            </div>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                            @endif
                        </div>

                        <div class="form-group row">
                            <input id="password" type="password"  placeholder="Password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                            <div class="text-right login-forgot-div">
                                <a href="/password/reset" class="login-forgot">Forgot?</a>
                            </div>

                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="captcha-container">
                        {!! NoCaptcha::display() !!}
                        @if ($errors->has('g-recaptcha-response'))
                            <span class="help-block">
                            <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                        </span>
                        @endif
                    </div>


                    <div class="text-center login-btn-div">
                        <button type="submit" class="btn bttn btn-primary text-center normal-btn">
                            {{ __('Login') }}
                        </button>
                    </div>

                    <div class="remember-me ">
                        <div class="flex-login-container">
                            <div class="flex-login-item">
                                <a href="{{ route('register') }}">Don't have an account?</a>
                            </div>
                            <div class="flex-login-item">
                                <input class="form-check-input" type="checkbox" name="remember" id="remember" checked>

                                <label class="form-check-label" for="remember">
                                    {{ __('Remember Me') }}
                                </label>
                            </div>
                        </div>

                    </div>


                </form>


                <div class="row or-container">
                    <div class="col-md-4 simple-line"></div>
                    <div class="col-md-4 text-center">
                        <b>or</b>
                    </div>
                    <div class="col-md-4 simple-line"></div>
                </div>
                {{--<div class="login-icons text-center">--}}
                    {{--<i class="fab fa-google"></i>--}}
                    {{--<i class="fab fa-twitter"></i>--}}
                    {{--<i class="fab fa-facebook-f"></i>--}}
                    {{--<i class="fab fa-linkedin-in"></i>--}}
                {{--</div>--}}
                <div class="social-login-container">
                    <div>
                        <a href="/login/facebook" class="button-facebook login-social-button">
                            <span class="login-icon"><i class="fab fa-facebook-f"></i></span>Continue with Facebook
                        </a>
                    </div>
                    <div>
                        <a href="/login/linkedin" class="button-linkin login-social-button">
                            <span class="login-icon"><i class="fab fa-linkedin-in"></i></span>Continue with Linkedin
                        </a>
                    </div>
                    <div>
                        <a href="/login/google" class="button-google login-social-button">
                            <span class="login-icon"><i class="fab fa-google"></i></span>Continue with Google
                        </a>
                    </div>
                    <div>
                        <a href="/login/twitter" class="button-twitter login-social-button">
                            <span class="login-icon"><i class="fab fa-twitter"></i></span>Continue with Twitter
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

