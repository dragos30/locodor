@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="{{ route('profile.update',['id' => $user->profile->id]) }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="form-group">
                <div class="row">
                    <div class="col-md-12">
                        <h3>General Info</h3>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <img id="holder" style="margin-top:15px;margin-bottom: 10px;max-height:200px; max-width:356px"/>
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <a id="lfm" data-input="thumbnail" data-preview="holder" class="edit-profile-upload-btn">
                                        <span class="fancy-btn">Upload Avatar</span>
                                    </a>
                                </span>
                                <input id="thumbnail" class="form-control hidden" type="text" name="avatar"/>
                            </div>
                        </div>

                        <script>
                            $('#lfm').filemanager('image');
                        </script>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="about">About me:</label>
                            <textarea class="form-control" id="about" name="about" rows="3">{{ $user->profile->about }}</textarea>
                        </div>

                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-md-4">
                        <label for="name">Name:</label>
                        <input class="form-control" type="text" name="name" placeholder="Full Name" value="{{ $user->name }}">
                    </div>
                    <div class="col-md-4">
                        <label for="country">Country</label>
                        <input class="form-control" type="text" name="country" placeholder="Country" value="{{ $user->country }}">
                    </div>
                    <div class="col-md-4">
                        <label for="city">City</label>
                        <input class="form-control" type="text" name="city" placeholder="City" value="{{ $user->city }}">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-4">
                        <label for="phone">Phone:</label>
                        <input class="form-control" type="number" name="phone" placeholder="Phone number" value="{{ $user->profile->phone }}">
                    </div>
                    <div class="col-md-4">
                        <label for="gender">Gender:</label>
                        <div class="input-group">

                            <select class="custom-select" name="gender">
                                <option value="1" @if($user->profile->gender == 1) selected @endif>Male</option>
                                <option value="2" @if($user->profile->gender == 2) selected @endif>Female</option>
                            </select>
                        </div>


                    </div>
                    <div class="col-md-4">
                        <label for="birth">Birth date:</label>
                        <input class="form-control" type="date" name="birth" placeholder="City" value="{{ $user->profile->birth }}">
                    </div>
                </div>
            </div>

            {{--<div class="form-group">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-md-2">--}}
                        {{--<img src="{{ asset($user->profile->avatar) }}" width="100" height="100"><br>--}}
                    {{--</div>--}}
                    {{--<div class="col-md-10">--}}
                        {{--<label for="avatar">Avatar</label>--}}
                        {{--<input type="file" class="form-control-file" id="avatar" name="avatar">--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}



            <div class="form-group">
                <div class="row">
                    <div class="col-md-12">
                        <h3>Social Media</h3>
                    </div>
                    <div class="col-md-4">
                        <small>Facebook</small>
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" name="facebook" placeholder="Paste here your Facebook profile URL" value="{{$user->profile->facebook}}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <small>LinkedIn</small>
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" name="linkedin" placeholder="Paste here your LinkedIn profile URL" value="{{$user->profile->linkedin}}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <small>Twitter</small>
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" name="twitter" placeholder="Paste here your Twitter profile URL" value="{{$user->profile->twitter}}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <small>Kickstarter</small>
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" name="kickstarter" placeholder="Paste here your Kickstarter profile URL" value="{{$user->profile->kickstarter}}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <small>Indiegogo</small>
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" name="indiegogo" placeholder="Paste here your Indiegogo profile URL" value="{{$user->profile->indiegogo}}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <small>Angel List</small>
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" name="angellist" placeholder="Paste here your Angel List profile URL" value="{{$user->profile->angellist}}">
                        </div>
                    </div>
                </div>
            </div>

            <button type="submit" class="fancy-btn">Update profile</button>
        </form>
    </div>
@endsection