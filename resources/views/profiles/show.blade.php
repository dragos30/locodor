@extends('layouts.app')

@section('content')
    <div class="profile-main">
        <div class="profile-header">
            <div class="container">
                <div class="profile-avatar" style="background: url('{{ asset($user->profile->avatar) }}')">
                </div>
                <p class="profile-username"><span>{{ $user->name }}</span></p>
            </div>
        </div>
        <div class="collapsable-content container">
            <div class="profile-tabs">
                {{--<div class="friend-request-container text-left">--}}
                    {{--@if($user->id != Auth::id() && !in_array($user->id, $loggedUserFriendsId))--}}
                        {{--<a href="{{ route('add.friend',['id' => $user->id ]) }}"><i class="fas fa-user-plus"></i> Add Friend </a>--}}
                    {{--@endif--}}
                    {{--@if($user->id != Auth::id() && in_array($user->id, $loggedUserFriendsId))--}}
                        {{--<a href="{{ route('remove.friend',['id' => $user->id ]) }}"><i class="fas fa-user-times"></i> Remove Friend </a>--}}
                    {{--@endif--}}
                {{--</div>--}}
                <a class="profile-tab-item" data-toggle="collapse" href="#about-tab" role="button" aria-expanded="false" aria-controls="about-tab">About</a>
                <a class="profile-tab-item" data-toggle="collapse" href="#my-campaigns-tab" role="button" aria-expanded="false" aria-controls="my-campaigns-tab">Created Campaigns</a>
                <a class="profile-tab-item" data-toggle="collapse" href="#backed-campaigns-tab" role="button" aria-expanded="false" aria-controls="backed-campaigns-tab">Backed Campaigns</a>
                <a class="profile-tab-item" data-toggle="collapse" href="#recently-viewed-campaigns-tab" role="button" aria-expanded="false" aria-controls="liked-campaigns-tab">Viewed Campaigns</a>
                <a class="profile-tab-item" data-toggle="collapse" href="#friends-tab" role="button" aria-expanded="false" aria-controls="friends-tab">Friends</a>
                <a class="profile-tab-item" data-toggle="collapse" href="#posts-tab" role="button" aria-expanded="false" aria-controls="posts-tab">Activity</a>
                <a class="profile-tab-item" data-toggle="collapse" href="#watching-tab" role="button" aria-expanded="false" aria-controls="watching-tab">Watching</a>
                <a class="profile-tab-item" data-toggle="collapse" href="#media-tab" role="button" aria-expanded="false" aria-controls="media-tab">Media</a>
            </div>
            <div class="collapse @if(empty($_GET)) show @endif" id="about-tab">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="user-details">
                                <div class="main-profile-about">
                                    <h3 class="about-username">{{ $user->name }}</h3>
                                    <p class="user-location">{{$user->country}}, {{$user->city}}</p>
                                </div>

                                <div class="secondary-profile-about">
                                    <span class="profile-rank">{!! $user->rank->icon !!} {{$user->rank->name}}</span>
                                    <p class="profile-about">{{ $user->profile->about }}</p>

                                    <p><b class="profile-label">Phone:</b> <a href="tel:{{$user->profile->phone}}" class="profile-phone">{{$user->profile->phone}}</a></p>

                                    @if($user->profile->facebook != '' || $user->profile->twitter != '' || $user->profile->linkedin != '')
                                        <h4>Social media:</h4>
                                        @if($user->profile->facebook != '')
                                                <a class="facebook-btn" rel="nofollow" href="{{ $user->profile->facebook }}" target="_blank">Facebook</a>
                                        @endif
                                        @if($user->profile->linkedin != '')
                                                <a class="normal-btn linkedin-btn" rel="nofollow" href="{{ $user->profile->linkedin }}" target="_blank">LinkedIn</a>
                                        @endif
                                        @if($user->profile->twitter != '')
                                                <a class="normal-btn twitter-btn" rel="nofollow" href="{{ $user->profile->twitter }}" target="_blank">Twitter</a>
                                        @endif
                                        @if($user->profile->kickstarter != '')
                                            <a class="kickstarter-btn" rel="nofollow" href="{{ $user->profile->kickstarter }}" target="_blank">Kickstarter</a>
                                        @endif
                                        @if($user->profile->indiegogo != '')
                                            <a class="normal-btn indiegogo-btn" rel="nofollow" href="{{ $user->profile->indiegogo }}" target="_blank">Indiegogo</a>
                                        @endif
                                        @if($user->profile->angellist != '')
                                            <a class="normal-btn angellist-btn" rel="nofollow" href="{{ $user->profile->angellist }}" target="_blank">Angel List</a>
                                        @endif
                                    @endif
                                </div>
                                <div class="secondary-profile-about">
                                    <p class="user-stat-p">Backed campaigns: {{ $user->backers->count() }}</p>
                                    <?php
                                    $backed_amount = 0;
                                    foreach($user->backers as $backer){
                                        $backed_amount += $backer->amount;
                                    }
                                    ?>
                                    <p class="user-stat-p">Backed amount: ${{ number_format($backed_amount) }}</p>
                                    <p class="user-stat-p">Created campaigns: {{ $user->projects->count() }}</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="profile-about-buttons">
                                @if(Auth::check())
                                    @if(Auth::user()->profile->id == $user->profile->id)
                                        <a href="{{ route('profile.edit',['id' => $user->profile->id]) }}"  class="btn btn-dark"><i class="fas fa-user-edit"></i> Edit Profile</a>
                                    @else
                                        <a href="{{ route('start.conversation',['id' => $user->id] ) }}" class="btn btn-dark"><i class="far fa-comments"></i>Private Message</a>
                                        <a href="{{ route('activity',['id' => $user->id] ) }}" class="btn btn-dark"><i class="far fa-comments"></i>Public Message</a>

                                        @if($user->id != Auth::id() && !in_array($user->id, $loggedUserFriendsId))
                                            <a href="{{ route('add.friend',['id' => $user->id] ) }}" class="btn btn-dark"><i class="far fas-user-plus"></i> Add Friend</a>

                                        @endif
                                    @endif
                                @endif
                            </div>
                            @if(Auth::check())
                                <div class="profile-post-create">
                                    <form action="{{ route('store.profile.post') }}" method="post">
                                        @csrf
                                        <input type="hidden" value="{{ $user->id }}" name="user2">
                                        <div class="form-group">
                                            <textarea class="form-control" name="message" rows="3" placeholder="Write your message..."></textarea>
                                        </div>
                                        <button class="fancy-btn post-create-submit">Post</button>
                                    </form>
                                </div>
                            @endif
                            <div class="fancy-hr"></div>
                            <div class="profile-posts">
                                @foreach($profile_posts as $post)
                                    <div class="profile-post-container">
                                        <div class="profile-post">
                                            @if($post->user1 == Auth::id() || $post->user2 == Auth::id())
                                                <a href="{{ route('delete.profile.post',['id' => $post->id]) }}" class="remove-profile-post float-right"><i class="fas fa-times"></i></a>
                                            @endif
                                            <div><span class="post-username">{{ $post->user_1->name }}</span> <i class="fas fa-arrow-right"></i> <span class="post-username">{{ $post->user_2->name }}</span></div>
                                            <p class="post-message">{!! $post->message !!}</p>
                                        </div>
                                        <div class="profile-post-replies-container">
                                            @foreach($post->replies as $reply)
                                                <p>
                                                    <img src="{{ asset($reply->user->profile->avatar) }}" class="profile-reply-avatar">
                                                    <a href="{{ route('profile.show',['id' => $reply->user->profile->id,'slug' => $reply->user->profile->slug]) }}"><span class="profile-reply-username">{{ $reply->user->name }}</span></a>
                                                    <span class="profile-reply">{!! $reply->reply !!}</span>
                                                    @if($reply->user_id == Auth::id() || $user->id == Auth::id())
                                                        <a href="{{ route('profile.reply.delete',['id' => $reply->id]) }}" class="delete-reply float-right"><i class="fas fa-times"></i></a>
                                                    @endif
                                                </p>
                                            @endforeach
                                        </div>
                                        @if(Auth::check())
                                            <div class="profile-reply-create">
                                                <form action="{{ route('profile.reply.store') }}" method="post">
                                                    @csrf
                                                    <input type="hidden" name="post_id" value="{{ $post->id }}">
                                                    <div class="form-group">
                                                        <input class="form-control" name="reply" rows="1" placeholder="Write your reply...">
                                                    </div>
                                                    <button type="submit" class="profile-reply-btn" data-profile-post-id="{{ $post->id }}"><i class="fas fa-paper-plane"></i></button>
                                                </form>
                                            </div>
                                        @endif
                                    </div>
                                    <hr>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="collapse @if(isset($_GET['created'])) show @endif" id="my-campaigns-tab">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="{{ route('projects.created') }}" class="btn btn-dark">Manage Campaigns</a>
                        </div>
                        @if(count($user->projects))
                            @foreach($user->projects as $project)
                                @if($project->approval == 'approved')
                                    <div class="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4">
                                        <div class="project">
                                            <a href="{{ route('project.show',[ 'id' => $project->id , 'slug' => $project->slug]) }}"><img src="{{ asset($project->image) }}" width="100%" height="200px"></a>
                                            <div class="project-content">
                                                <h2 class="text-center project-title"> {{ $project->title }}</h2>
                                                <?php
                                                if (strlen($project->subtitle) > 130){
                                                    $subtitle = substr($project->subtitle, 0,120) . '...';
                                                }else{
                                                    $subtitle = $project->subtitle;
                                                }
                                                ?>
                                                <p class="project-subtitle text-center">{{ $subtitle }}</p>
                                                <?php
                                                $project_proc = ($project->current_amount / $project->project_goal * 100);
                                                ?>
                                                <div>
                                                    {{--<a href="{{ route('projects') }}?project_category=1" class="project-category">{{ $project->category->name }}</a>--}}
                                                    <?php
                                                    $start_date = $project->start_date /60/60/24;
                                                    $end_date = $project->end_date /60/60/24;
                                                    $days_left = floor($end_date - $start_date);
                                                    ?>
                                                    <span class="days-left"><?php if($days_left < 1){ echo "Completed"; } else { echo $days_left . "days left";} ?></span>
                                                </div>
                                                <div class="profile-project-bar">
                                                    <div class="profile-inner-bar" style="width:{{$project->funding_percent}}%"><span class="bar-percent">{{ $project->funding_percent }}%</span></div>
                                                </div>
                                                <p class="profile-project-funds"><span class="current_amount">{{ $project->current_amount }}$</span> <span class="project_goal float-right">{{ $project->project_goal }}$</span></p>
                                                <p class="profile-project-funds-text-container"><span class="profile-goal-text">Pledged</span> <span class="profile-goal-text float-right">Goal</span></p>

                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        @else
                            <p class="profile-notice">{{ $user->name }} has not created any campaigns</p>
                        @endif
                    </div>
                </div>

            </div>
            <div class="collapse @if(isset($_GET['backed'])) show @endif" id="backed-campaigns-tab">
                <div class="container">
                    @if(count($user->backers))
                        @foreach($user->backers as $backer)
                            @if(isset($backer->level))
                                <p>Donated ${{ $backer->amount }} on the project <a href="{{ route('project.show',['id' => $backer->project->id, 'slug'=> $backer->project->slug]) }}">{{ $backer->project->title }}</a> for {{ $backer->level->title }}</p>
                            @endif
                        @endforeach
                    @else
                        <p class="profile-notice">{{ $user->name }} didn't backed any campaign yet.</p>
                    @endif
                </div>
            </div>
            <div class="collapse" id="recently-viewed-campaigns-tab">
                <div class="container">
                    @if(count($user->projectViews))
                        @foreach($user->projectViews->reverse() as $view)
                            <p>Viewed <a href="{{ route('project.show',['id' => $view->project->id , 'slug' => $view->project->slug]) }}">{{ $view->project->title }}</a> {{ $view->created_at->diffForHumans() }}</p>
                        @endforeach
                    @else
                        <p class="profile-notice">{{ $user->name }} didn't viewed any campaign yet.</p>
                    @endif
                </div>
            </div>
            <div class="collapse @if(isset($_GET['friends'])) show @endif" id="friends-tab">
                @if(count($friends) == 0)
                    <div class="container">
                        <p class="profile-notice">{{ $user->name }} has no friends in the Locodor Community.</p>
                    </div>
                @endif
                <div>
                    @if(Auth::id() == $user->id)
                        <a href="#" class="friends-tab" id="friends-btn">Friends</a>
                        <a href="#" class="friends-tab" id="requests-btn">Requests</a>
                        <a href="#" class="friends-tab" id="sent-btn">Sent</a>

                        <div class="tab-content">

                            {{--Friend List--}}
                            <div id="friends">

                                <hr>
                                <small>Friend List:</small>
                                <div class="row friend-list">
                                    <div class="col-md-8">
                                        <div class="all-friends">
                                            @foreach($friends as $friend)
                                                @if($friend->user1_id == $user->id)
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <img src="{{ asset($friend->user2->profile->avatar) }}" class="profile-friend-avatar">
                                                        </div>
                                                        <div class="col-md-7">
                                                            <p class="profile-friend-name"><a class="friend-display" href="{{ route('profile.show',['id' => $friend->user2->profile->id, 'slug' => $friend->user2->profile->slug ]) }}">{{ $friend->user2->name }}</a></p>
                                                            <p class="profile-friend-about">{{ substr($friend->user2->profile->about,0,215)}}</p>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <button class="btn btn-secondary dropdown-toggle fancy-btn members-action-btn" type="button" id="dropdownMenuButton-{{ $user->id }}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                <i class="fas fa-check"></i> Friends
                                                            </button>
                                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton-{{ $friend->user2->id }}">
                                                                <a class="dropdown-item" href="{{ route('start.conversation',['id' => $friend->user2->id ]) }}">Message</a>
                                                                <a class="dropdown-item" href="{{ route('profile.show',['id' => $friend->user2->profile->id,'slug'=> $friend->user2->profile->slug]) }}">View profile</a>
                                                                <a class="dropdown-item" href="{{ route('remove.friend',['id' => $friend->user2->id ]) }}" data-user-id="{{$friend->user2->id}}">Unfriend </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @else
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <img src="{{ asset($friend->user1->profile->avatar) }}" class="profile-friend-avatar">
                                                        </div>
                                                        <div class="col-md-7">
                                                            <p class="profile-friend-name"><a class="friend-display" href="{{ route('profile.show',['id' => $friend->user1->profile->id , 'slug' => $friend->user1->profile->slug]) }}">{{ $friend->user1->name }}</a></p>
                                                            <p class="profile-friend-about">{{ substr($friend->user1->profile->about,0,215)}}</p>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <button class="btn btn-secondary dropdown-toggle fancy-btn members-action-btn" type="button" id="dropdownMenuButton-{{ $user->id }}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                <i class="fas fa-check"></i> Friends
                                                            </button>
                                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton-{{ $friend->user1->id }}">
                                                                <a class="dropdown-item" href="{{ route('start.conversation',['id' => $friend->user1->id ]) }}">Message</a>
                                                                <a class="dropdown-item" href="{{ route('profile.show',['id' => $friend->user1->profile->id,'slug'=> $friend->user1->profile->slug]) }}">View profile</a>
                                                                <a class="dropdown-item" href="{{ route('remove.friend',['id' => $friend->user1->id ]) }}" data-user-id="{{$friend->user1->id}}">Unfriend </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endif
                                                <br>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {{--Friend requests--}}
                            <div id="requests" style="display:none">
                                <hr>
                                <small>Friend Requests:</small>
                                @if($user->id == Auth::id() && count($friend_requests))
                                    @foreach($friend_requests as $request)
                                        @if(isset($request->user1->profile->id))
                                            <p>
                                                <a href="{{ route('profile.show',['id' => $request->user1->profile->id , 'slug' => $request->user1->profile->slug]) }}"  class="friend-display">{{ $request->user1->name }}</a>
                                                <a href="{{ route('accept.friend',['id' => $request->user1->id]) }}" class="fancy-btn accept-friend" data-friend-id="{{ $request->user1->id }}">Accept</a>
                                                <a href="{{ route('remove.friend',['id' => $request->user1->id]) }}" class="fancy-btn remove-friend" data-friend-id="{{ $request->user1->id }}">Decline</a>
                                            </p>
                                        @endif
                                    @endforeach
                                @endif
                            </div>

                            {{--Sent Friend Requests--}}
                            <div id="sent" style="display:none">
                                <hr>
                                <small>Sent Friend Requests</small>
                                @if($user->id == Auth::id() && count($pending_friends))
                                    @foreach($pending_friends as $request)
                                        @if(isset($request->user2->profile->id))
                                            <p>
                                                <a href="{{ route('profile.show',['id' => $request->user2->profile->id,
                                             'slug' => $request->user2->profile->slug ]) }}"  class="friend-display">
                                                    {{ $request->user2->name }}</a>
                                            </p>
                                        @endif
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <script>
                            var friendsDiv = $('#friends');
                            var requestsDiv = $('#requests');
                            var sentDiv = $('#sent');

                            $( document ).ready(function() {
                                $('#requests-btn').click(function(e){
                                    e.preventDefault();

                                    friendsDiv.fadeOut('fast', function(){
                                        friendsDiv.css('display','none');
                                        sentDiv.css('display','none');
                                        requestsDiv.css('display','block');
                                        requestsDiv.fadeIn('fast');
                                    })
                                });
                            });

                            $( document ).ready(function() {
                                $('#friends-btn').click(function(e){
                                    e.preventDefault();

                                    requestsDiv.fadeOut('fast', function(){
                                        requestsDiv.css('display','none');
                                        sentDiv.css('display','none');
                                        friendsDiv.css('display','block');
                                        friendsDiv.fadeIn('fast');
                                    })
                                });
                            });

                            $( document ).ready(function() {
                                $('#sent-btn').click(function(e){
                                    e.preventDefault();

                                    requestsDiv.fadeOut('fast', function(){
                                        requestsDiv.css('display','none');
                                        friendsDiv.css('display','none');
                                        sentDiv.css('display','block');
                                        sentDiv.fadeIn('fast');
                                    })
                                });
                            });
                            @if(isset($_GET['request']))
                            friendsDiv.fadeOut('fast', function(){
                                friendsDiv.css('display','none');
                                sentDiv.css('display','none');
                                requestsDiv.css('display','block');
                                requestsDiv.fadeIn('fast');
                            })
                            @endif

                            $('.accept-friend').click(function(e) {
                                e.preventDefault();
                                var btnClicked = $(this);
                                var friendId = btnClicked.data('friend-id');
                                $.ajax({
                                    type: "GET",
                                    url: "/accept-friend/"+friendId,
                                    success: function (result) {
                                        btnClicked.parent().remove();
                                        var allFriends = $('.all-friends').children().remove();
                                        var newFriends = $(result).find('.all-friends');
                                        $('.all-friends').append(newFriends);
                                        console.log('success');

                                    },
                                    error: function (result) {
                                        alert('Something went wrong!');
                                        console.log('failed');
                                    }
                                });
                            });

                            $('.remove-friend').click(function(e) {
                                e.preventDefault();
                                var btnClicked = $(this);
                                var friendId = btnClicked.data('friend-id');
                                $.ajax({
                                    type: "GET",
                                    url: "/remove-friend/"+friendId,
                                    success: function (result) {
                                        btnClicked.parent().remove();
                                        console.log('success');

                                    },
                                    error: function (result) {
                                        alert('Something went wrong!')
                                        console.log('failed');
                                    }
                                });
                            });
                        </script>
                    @else
                        <div class="row friend-list">
                            <div class="col-md-12">
                                @foreach($friends as $friend)
                                    @if(Auth::check())
                                        @if(Auth::id() == $user->id)
                                            @if($friend->user1_id == $user->id)
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <img src="{{ asset($friend->user2->profile->avatar) }}" class="profile-friend-avatar">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <p class="profile-friend-name"><a class="friend-display" href="{{ route('profile.show',['id' => $friend->user2->profile->id, 'slug' => $friend->user2->profile->slug]) }}">{{ $friend->user2->name }}</a></p>
                                                        <p class="profile-friend-about">{{ substr($friend->user2->profile->about,0,215)}}</p>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <a href="{{ route('remove.friend',['id' => $friend->user2->id ]) }}" class="unfriend-btn">Unfriend</a>
                                                        <br>
                                                        <a href="{{ route('profile.show',['id' => $friend->user2->profile->id ]) }}" class="unfriend-btn">View Profile</a>
                                                    </div>
                                                </div>
                                                <hr>
                                            @else
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <img src="{{ asset($friend->user1->profile->avatar) }}" class="profile-friend-avatar">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <p class="profile-friend-name"><a class="friend-display" href="{{ route('profile.show',['id' => $friend->user1->profile->id, 'slug' => $friend->user1->profile->slug ]) }}">{{ $friend->user1->name }}</a></p>
                                                        <p class="profile-friend-about">{{ substr($friend->user1->profile->about,0,215)}}</p>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <a href="{{ route('remove.friend',['id' => $friend->user1->id ]) }}" class="unfriend-btn">Unfriend</a>
                                                        <br>
                                                        <a href="{{ route('profile.show',['id' => $friend->user1->profile->id, 'slug' => $friend->user1->profile->slug ]) }}" class="unfriend-btn">View Profile</a>
                                                    </div>
                                                </div>
                                                <hr>
                                            @endif
                                        @endif
                                    @endif
                                    @if(!Auth::check() || Auth::id() != $user->id)
                                        @if($friend->user1_id == $user->id)
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <img src="{{ asset($friend->user2->profile->avatar) }}" class="profile-friend-avatar">
                                                </div>
                                                <div class="col-md-6">
                                                    <p class="profile-friend-name"><a class="friend-display" href="{{ route('profile.show',['id' => $friend->user2->profile->id, 'slug' => $friend->user2->profile->slug ]) }}">{{ $friend->user2->name }}</a></p>
                                                    <p class="profile-friend-about">{{ substr($friend->user2->profile->about,0,215)}}</p>
                                                </div>
                                                <div class="col-md-3">
                                                    <a href="{{ route('profile.show',['id' => $friend->user2->profile->id , 'slug' => $friend->user2->profile->slug]) }}" class="unfriend-btn">View Profile</a>
                                                </div>
                                            </div>
                                            <hr>
                                        @else
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <img src="{{ asset($friend->user1->profile->avatar) }}" class="profile-friend-avatar">
                                                </div>
                                                <div class="col-md-6">
                                                    <p class="profile-friend-name"><a class="friend-display" href="{{ route('profile.show',['id' => $friend->user1->profile->id , 'slug' => $friend->user1->profile->slug]) }}">{{ $friend->user1->name }}</a></p>
                                                    <p class="profile-friend-about">{{ substr($friend->user1->profile->about,0,215)}}</p>
                                                </div>
                                                <div class="col-md-3">
                                                    <a href="{{ route('profile.show',['id' => $friend->user1->profile->id, 'slug' => $friend->user1->profile->slug ]) }}" class="unfriend-btn">View Profile</a>
                                                </div>
                                            </div>
                                            <hr>
                                        @endif
                                    @endif
                                    <br>
                                @endforeach
                            </div>
                        </div>
                    @endif
                </div>
            </div>

        </div>
            <div class="collapse" id="posts-tab">
                <div class="container profile-activity-container">
                    @if(count($activity))
                        <div class="col-md-6" style="margin:0 auto">
                            @foreach($activity as $a)
                                @switch($a->action_name)
                                    @case("project_create")
                                    <div class="activity-item">
                                        <p>
                                            <a href="{{ route('profile.show',[ 'id' => $a->user->profile->id , 'slug' => $a->user->profile->slug]) }}">{{ $a->user->name }}</a>
                                            created a project:
                                            <a href="{{ route('project.show',['id' => $a->action_id, 'slug' => $a->project->slug]) }}">{{ $a->project->title }}</a>
                                        </p>
                                        <span class="activity-likes">{{count($a->activityLikes)}} likes</span>

                                        @if(in_array($a->id,$liked))
                                            <a href="{{ route('activity.unlike',['id' => $a->id]) }}"><i class="far fa-thumbs-down"></i>Unlike</a>
                                        @else
                                            <a href="{{ route('activity.like',['id' => $a->id]) }}"><i class="far fa-thumbs-up"></i>Like</a>
                                        @endif
                                    </div>
                                    @break
                                    @case("discussion_create")
                                    <div class="activity-item">
                                        <p>
                                            <a href="{{ route('profile.show',[ 'id' => $a->user->profile->id, 'slug' => $a->user->profile->slug ]) }}">{{ $a->user->name }}</a>
                                            created a discussion:
                                            <a href="{{ route('discussion.show',['id' => $a->action_id]) }}">{{ $a->discussion->title }}</a>
                                        </p>
                                        <span class="activity-likes">{{count($a->activityLikes)}} likes</span>

                                        @if(in_array($a->id,$liked))
                                            <a href="{{ route('activity.unlike',['id' => $a->id]) }}"><i class="far fa-thumbs-down"></i>Unlike</a>
                                        @else
                                            <a href="{{ route('activity.like',['id' => $a->id]) }}"><i class="far fa-thumbs-up"></i>Like</a>
                                        @endif
                                    </div>
                                    @break
                                    @case("discussion_reply")
                                    <div class="activity-item">
                                        <p>
                                            <a href="{{ route('profile.show',[ 'id' => $a->user->profile->id, 'slug' => $a->user->profile->slug ]) }}">{{ $a->user->name }}</a>
                                            replied "{{ $a->reply->content }}" on the discussion
                                            <a href="{{ route('discussion.show',['id' => $a->reply->discussion_id ]) }}">{{ $a->reply->discussion->title }}</a>
                                        </p>
                                        <span class="activity-likes">{{count($a->activityLikes)}} likes</span>

                                        @if(in_array($a->id,$liked))
                                            <a href="{{ route('activity.unlike',['id' => $a->id]) }}"><i class="far fa-thumbs-down"></i>Unlike</a>
                                        @else
                                            <a href="{{ route('activity.like',['id' => $a->id]) }}"><i class="far fa-thumbs-up"></i>Like</a>
                                        @endif
                                    </div>
                                    @break
                                    @case("project_comment")
                                    <div class="activity-item">
                                        <p>
                                            <a href="{{ route('profile.show',[ 'id' => $a->user->profile->id, 'slug' => $a->user->profile->slug ]) }}">{{ $a->user->name }}</a>
                                            commented "{{ $a->comment->comment }}" on project
                                            <a href="{{ route('project.show',['id' => $a->comment->project_id,'slug' => $a->comment->project->slug ]) }}">{{ $a->comment->project->title }}</a>
                                        </p>
                                        <span class="activity-likes">{{count($a->activityLikes)}} likes</span>

                                        @if(in_array($a->id,$liked))
                                            <a href="{{ route('activity.unlike',['id' => $a->id]) }}"><i class="far fa-thumbs-down"></i>Unlike</a>
                                        @else
                                            <a href="{{ route('activity.like',['id' => $a->id]) }}"><i class="far fa-thumbs-up"></i>Like</a>
                                        @endif
                                    </div>
                                    @break
                                    @case("topic_create")
                                    <div class="activity-item">
                                        <p>
                                            <a href="{{ route('profile.show',[ 'id' => $a->user->profile->id, 'slug' => $a->user->profile->slug ]) }}">{{ $a->user->name }}</a>
                                            created a new topic:
                                            <a href="{{ route('topic.show',['id' => $a->action_id ]) }}">{{ $a->topic->name }}</a>
                                        </p>
                                        <span class="activity-likes">{{count($a->activityLikes)}} likes</span>

                                        @if(in_array($a->id,$liked))
                                            <a href="{{ route('activity.unlike',['id' => $a->id]) }}"><i class="far fa-thumbs-down"></i>Unlike</a>
                                        @else
                                            <a href="{{ route('activity.like',['id' => $a->id]) }}"><i class="far fa-thumbs-up"></i>Like</a>
                                        @endif
                                    </div>
                                    @break

                                    @case("comment_like")
                                    <div class="activity-item">
                                        <div class="user-hover-container" id="hover-container{{$a->id}}" onmouseleave="document.getElementById('hover-container{{$a->id}}').style.display = 'none';">
                                            <div class="hover-profile">
                                                <div class="row">
                                                    <div class="col-md-3 no-margin">
                                                        <img class="hover-image" src="/{{ $a->user->profile->avatar }}" width="75" height="75">
                                                    </div>
                                                    <div class="col-md-9 no-margin">
                                                        <p><span class="hover-user-name"">{{ $a->user->name }}</span></p>
                                                        <a href="{{ route('profile.show',['id' => $a->user->profile->id, 'slug' => $a->user->profile->slug]) }}">View profile</a>
                                                        <p>"{{  $result = substr($a->user->profile->about, 0, 50)}}"</p>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="simple-line"></div>
                                                        <div class="hover-social">
                                                            <p><b>Social</b></p>
                                                            <p><a href="#"><i class="fas fa-user-plus"></i>Add friend</a></p>
                                                        </div>
                                                        <div class="simple-line"></div>
                                                        <div class="hover-messaging">
                                                            <p><b>Messaging</b></p>
                                                            <p><a href="#"><i class="far fa-comment"></i>Send Private</a><a href="#"><i class="far fa-comments"></i>Send Public</a></p>
                                                        </div>
                                                        <div class="simple-line"></div>
                                                        <div class="hover-campaigns">
                                                            <p><b>Campaigns</b></p>
                                                            <p><a href="#"><i class="far fa-lightbulb"></i>Created Campaigns({{ count($a->user->projects)  }})</a></p>
                                                            <p><a href="#"><i class="fab fa-bitcoin"></i>Backed Campaigns(0)</a></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <p>
                                            <a href="{{ route('profile.show',[ 'id' => $a->user->profile->id, 'slug' => $a->user->profile->slug ]) }}" id="hover-name{{ $a->id }}" onmouseover="document.getElementById('hover-container{{$a->id}}').style.display = 'block';">{{ $a->user->name }}</a>
                                            liked a comment on the project
                                            <a href="{{ route('project.show',['id' => $a->commentLike->comment->project->id , 'slug' => $a->commentLike->comment->project->slug]) }}">{{ $a->commentLike->comment->project->title }}</a>
                                        </p>
                                        <span class="activity-likes">{{count($a->activityLikes)}} likes</span>

                                        @if(in_array($a->id,$liked))
                                            <a href="{{ route('activity.unlike',['id' => $a->id]) }}"><i class="far fa-thumbs-down"></i>Unlike</a>
                                        @else
                                            <a href="{{ route('activity.like',['id' => $a->id]) }}"><i class="far fa-thumbs-up"></i>Like</a>
                                        @endif
                                    </div>
                                    @break
                                    @case("reply_like")
                                    <div class="activity-item">
                                        <p>
                                            <a href="{{ route('profile.show',[ 'id' => $a->user->profile->id, 'slug' => $a->user->profile->slug ]) }}">{{ $a->user->name }}</a>
                                            liked a reply on the discussion
                                            <a href="{{ route('discussion.show',['id' => $a->replyLike->reply->discussion->id ]) }}">{{ $a->replyLike->reply->discussion->title }}</a>
                                        </p>
                                        <span class="activity-likes">{{count($a->activityLikes)}} likes</span>

                                        @if(in_array($a->id,$liked))
                                            <a href="{{ route('activity.unlike',['id' => $a->id]) }}"><i class="far fa-thumbs-down"></i>Unlike</a>
                                        @else
                                            <a href="{{ route('activity.like',['id' => $a->id]) }}"><i class="far fa-thumbs-up"></i>Like</a>
                                        @endif
                                    </div>
                                    @break
                                    @case("discussion_like")
                                    <div class="activity-item">
                                        <p>
                                            <a href="{{ route('profile.show',[ 'id' => $a->user->profile->id, 'slug' => $a->user->profile->slug ]) }}">{{ $a->user->name }}</a>
                                            upvoted the discussion
                                            <a href="{{ route('discussion.show',['id' => $a->discussionLike->discussion->id ]) }}">{{ $a->discussionLike->discussion->title }}</a>
                                        </p>
                                        <span class="activity-likes">{{count($a->activityLikes)}} likes</span>

                                        @if(in_array($a->id,$liked))
                                            <a href="{{ route('activity.unlike',['id' => $a->id]) }}"><i class="far fa-thumbs-down"></i>Unlike</a>
                                        @else
                                            <a href="{{ route('activity.like',['id' => $a->id]) }}"><i class="far fa-thumbs-up"></i>Like</a>
                                        @endif
                                    </div>
                                    @break
                                    @case("discussion_watch")
                                    <div class="activity-item">
                                        <p>
                                            <a href="{{ route('profile.show',[ 'id' => $a->user->profile->id, 'slug' => $a->user->profile->slug ]) }}">{{ $a->user->name }}</a>
                                            is watching
                                            <a href="{{ route('discussion.show',['id' => $a->watcher->discussion->id ]) }}">{{ $a->watcher->discussion->title }}</a>
                                        </p>
                                        <span class="activity-likes">{{count($a->activityLikes)}} likes</span>

                                        @if(in_array($a->id,$liked))
                                            <a href="{{ route('activity.unlike',['id' => $a->id]) }}"><i class="far fa-thumbs-down"></i>Unlike</a>
                                        @else
                                            <a href="{{ route('activity.like',['id' => $a->id]) }}"><i class="far fa-thumbs-up"></i>Like</a>
                                        @endif
                                    </div>
                                    @break
                                    @case("post_create")
                                    <div class="activity-item">
                                        <p>There is a new post on our blog:
                                            <a href="{{ route('post.show',['id' => $a->post->id,'slug' => $a->post->slug]) }}">{{ $a->post->title }}</a>
                                        </p>

                                        <span class="activity-likes">{{count($a->activityLikes)}} likes</span>

                                        @if(in_array($a->id,$liked))
                                            <a href="{{ route('activity.unlike',['id' => $a->id]) }}"><i class="far fa-thumbs-down"></i>Unlike</a>
                                        @else
                                            <a href="{{ route('activity.like',['id' => $a->id]) }}"><i class="far fa-thumbs-up"></i>Like</a>
                                        @endif
                                    </div>
                                    @break
                                @endswitch
                            @endforeach
                        </div>
                    @else
                            <p class="profile-notice">{{ $user->name }} has no activity on Locodor.</p>
                    @endif

                </div>

            </div>
            <div class="collapse" id="watching-tab">
                <div class="container">
                    <div class="row">
                    @if(count($watched_projects))
                        @foreach($watched_projects as $watched)
                            <?php $project = $watched->project; ?>
                            @if($project->approval == 'approved')
                                <div class="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4">
                                    <div class="project">
                                        <a href="{{ route('project.show',[ 'id' => $project->id , 'slug' => $project->slug]) }}"><img src="{{ asset($project->image) }}" width="100%" height="200px"></a>
                                        <div class="project-content">
                                            <h2 class="text-center project-title"> {{ $project->title }}</h2>
                                            <?php
                                            if (strlen($project->subtitle) > 130){
                                                $subtitle = substr($project->subtitle, 0,120) . '...';
                                            }else{
                                                $subtitle = $project->subtitle;
                                            }
                                            ?>
                                            <p class="project-subtitle text-center">{{ $subtitle }}</p>
                                            <?php
                                            $project_proc = ($project->current_amount / $project->project_goal * 100);
                                            ?>
                                            <div>
                                                {{--<a href="{{ route('projects') }}?project_category=1" class="project-category">{{ $project->category->name }}</a>--}}
                                                <?php
                                                $start_date = $project->start_date /60/60/24;
                                                $end_date = $project->end_date /60/60/24;
                                                $days_left = floor($end_date - $start_date);
                                                ?>
                                                <span class="days-left"><?php if($days_left < 1){ echo "Completed"; } else { echo $days_left . "days left";} ?></span>
                                            </div>
                                            <div class="profile-project-bar">
                                                <div class="profile-inner-bar" style="width:{{$project->funding_percent}}%"><span class="bar-percent">{{ $project->funding_percent }}%</span></div>
                                            </div>
                                            <p class="profile-project-funds"><span class="current_amount">{{ $project->current_amount }}$</span> <span class="project_goal float-right">{{ $project->project_goal }}$</span></p>
                                            <p class="profile-project-funds-text-container"><span class="profile-goal-text">Pledged</span> <span class="profile-goal-text float-right">Goal</span></p>

                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    @else
                        <div class="col-md-12">
                            <p class="profile-notice">{{ $user->name }} is not watching any projects</p>
                        </div>
                    @endif
                    </div>
                </div>
            </div>
            <div class="collapse" id="media-tab">
                @if(Auth::id() == $user->id)
                <iframe src="{{ url('laravel-filemanager') }}" style="width: 100%; height: 500px; overflow: hidden; border: none;"></iframe>
                @else
                    <div class="container">
                        <div class="media-grid row">
                            @foreach($media as $index => $image)
                                <div class="col-md-4 grid-image-container">
                                    <img class="grid-image" data-image-index="{{$index}}" src="/photos/{{$user->id}}/{{$image->getFilename()}}" width="100%">
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="light-box-background">
                    </div>
                    <span><i class="fas fa-chevron-left image-lightbox-prev"></i></span>
                    <span><i class="fas fa-chevron-right image-lightbox-next"></i></span>
                    <span><i class="fas fa-times image-lightbox-close"></i></span>
                @endif
                <script>
                    jQuery.fn.center = function () {
                        $('.light-box-background').show();
                        var prevArrow = $('.image-lightbox-prev');
                        var nextArrow =$('.image-lightbox-next');
                        var closeBtn = $('.image-lightbox-close');
                        closeBtn.show();
                        prevArrow.show();
                        nextArrow.show();

                        closeBtn.css('top','0px');
                        closeBtn.css('right','0px');

                        prevArrow.css('left','0px');
                        prevArrow.css("top", ( $(window).height() - $(prevArrow).outerHeight() ) / 2 + "px");

                        nextArrow.css('right','0px');
                        nextArrow.css("top", ( $(window).height() - $(nextArrow).outerHeight() ) / 2 + "px");

                        this.toggleClass('selected-light-box-image');

                        this.css("top", ( $(window).height() - $(this).outerHeight() ) / 2 + "px");
                        this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) +
                            $(window).scrollLeft()) + "px");

                        return this;
                    };

                    $( document ).ready(function() {
                        $('.grid-image').on('click',function(){
                            $(this).center();
                        });
                    });

                    $('.image-lightbox-close').on('click',function(){
                        $('.selected-light-box-image').toggleClass('selected-light-box-image');

                        var prevArrow = $('.image-lightbox-prev');
                        var nextArrow =$('.image-lightbox-next');
                        var closeBtn = $('.image-lightbox-close');

                        $('.light-box-background').hide();
                        closeBtn.hide();
                        prevArrow.hide();
                        nextArrow.hide();
                    });

                    $('.image-lightbox-next').on('click',function(){
                        var currentImageIndex = $('.selected-light-box-image').data('image-index');
                        var numberOfImages = {{ count($media) }};
                        if(currentImageIndex >= numberOfImages -1){
                            $(this).hide();
                        } else {
                            $('.image-lightbox-prev').show();
                            var nextImageIndex = currentImageIndex + 1;

                            var currentImageSelector = 'img[data-image-index="'+currentImageIndex+'"]';
                            var nextImageSelector = 'img[data-image-index="'+nextImageIndex+'"]';

                            var nextImage = $(nextImageSelector);

                            $(currentImageSelector).toggleClass('selected-light-box-image');
                            nextImage.toggleClass('selected-light-box-image');

                            nextImage.css("top", ( $(window).height() - $(nextImage).outerHeight() ) / 2 + "px");
                            nextImage.css("left", Math.max(0, (($(window).width() - $(nextImage).outerWidth()) / 2) +
                                $(window).scrollLeft()) + "px");
                        }

                    });

                    $('.image-lightbox-prev').on('click',function(){
                        var currentImageIndex = $('.selected-light-box-image').data('image-index');
                        if(currentImageIndex == 0){
                            $(this).hide();
                        } else {
                            $('.image-lightbox-next').show();

                            var prevImageIndex = currentImageIndex - 1;

                            var currentImageSelector = 'img[data-image-index="'+currentImageIndex+'"]';
                            var prevImageSelector = 'img[data-image-index="'+prevImageIndex+'"]';

                            var prevImage = $(prevImageSelector);

                            $(currentImageSelector).toggleClass('selected-light-box-image');
                            prevImage.toggleClass('selected-light-box-image');

                            prevImage.css("top", ( $(window).height() - $(prevImage).outerHeight() ) / 2 + "px");
                            prevImage.css("left", Math.max(0, (($(window).width() - $(prevImage).outerWidth()) / 2) +
                                $(window).scrollLeft()) + "px");
                        }
                    });
                </script>
            </div>
        </div>
        <script>
            $('.profile-tab-item').on( "click", function() {
                $( ".show" ).removeClass( "show" );
            });

            $( document ).ready(function() {
                $('.profile-reply-btn').on('click',function(e){
                    e.preventDefault();

                    var btnClicked = $(this);
                    var commentText = btnClicked.prev().children().first().val();
                    var profilePostId = btnClicked.data('profile-post-id');


                    $.ajax({
                        type: "POST",
                        url: "{{ route('profile.reply.store') }}" ,
                        data: {
                            _token: "{{ csrf_token() }}",
                            reply: commentText,
                            post_id: profilePostId,
                            page: 'profile-show'

                        },
                        success: function(result) {
                            btnClicked.parent().parent().prev().append(result);
                            btnClicked.prev().children().first().val('');

                            $('._cursor-onLoad').css({
                                display: 'none',
                            });

                            $('.activity-reply-delete').on('click', function(e){
                                e.preventDefault();
                                $('._cursor-onLoad').css({
                                    display: 'block',
                                });
                                var btnClicked = $(this);
                                var commentID = btnClicked.parent().parent().data('comment-id');
                                console.log(commentID);

                                $.ajax({
                                    type: "GET",
                                    url: "/profile/reply-delete/" + commentID ,
                                    data: {
                                        id: commentID
                                    },
                                    success: function(result) {
                                        btnClicked.parent().parent().remove();
                                        $('._cursor-onLoad').css({
                                            display: 'none',
                                        });
                                    },
                                    error: function(result) {
                                        console.log(result);
                                        alert("Ops, something went wrong");
                                    }
                                });

                            });

                            console.log('success');
                        },
                        error: function(result) {
                            console.log(result);
                            console.log('comment post failed');
                        }
                    });
                });
            });


        </script>
    </div>
@endsection