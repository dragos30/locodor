@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="{{ route('category.update',['id' => $category->id]) }}" method="post">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="name">Category name:</label>
                <input class="form-control" type="text" name="name" placeholder="Category name" value="{{ $category->name }}">
            </div>
            <div class="form-group">
                <label for="icon">Icon - copy paste icon from fontawesome, ex: <span style="color:darkorange">&lt;i class="fas fa-archive">&lt;/i></span></label>
                <input class="form-control" type="text" name="icon" placeholder="&lt;i class=&quot;fas fa-archive&quot;>&lt;/i>">
            </div>
            <div class="col-md-6">
                <div class="form-group">
                        <span class="input-group-btn">
                        <a id="lfm" data-input="thumbnail" data-preview="holder" class="new-upload-btn">
                        Choose
                        </a>
                </div>
                </span>
                <input id="thumbnail" class="form-control" type="hidden" name="image">
            </div>
            <div>
                <img id="holder" style="margin-top:15px;max-height:400px;">
            </div>

            <script>
                $('#lfm').filemanager('image');
            </script>

            <button type="submit" class="btn btn-dark">Update Category</button>
        </form>
    </div>
@endsection