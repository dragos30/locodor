@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="{{ route('subcategory.update',['id' => $subcategory->id]) }}" method="post">
            {{ csrf_field() }}
            <div class="input-group mb-3">
                <select class="custom-select" name="main_category_id">
                    <option selected>Choose Main Category</option>
                    @foreach($categories as $category)
                        <option value="{{$category->id}}" @if($category->id == $subcategory->main_category_id) selected @endif>{{$category->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="name">Category name:</label>
                <input class="form-control" type="text" name="name" placeholder="Category name" value="{{ $subcategory->name }}">
            </div>
            <div class="form-group">
                <label for="icon">Icon - copy paste icon from fontawesome, ex: <span style="color:darkorange">&lt;i class="fas fa-archive">&lt;/i></span></label>
                <input class="form-control" type="text" name="icon" value="{{$subcategory->icon}}" placeholder="&lt;i class=&quot;fas fa-archive&quot;>&lt;/i>">
            </div>

            <button type="submit" class="btn btn-dark">Update Category</button>
        </form>
    </div>
@endsection