@extends('layouts.app')

@section('content')
    <div class="container">
        <h3>Create Category:</h3>
        <form action="{{ route('category.store') }}" method="post">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="name">Category name:</label>
                <input class="form-control" type="text" name="name" placeholder="Category name">
            </div>
            <div class="form-group">
                <label for="icon">Icon - copy paste icon from fontawesome, ex: <span style="color:darkorange">&lt;i class="fas fa-archive">&lt;/i></span></label>
                <input class="form-control" type="text" name="icon" placeholder="&lt;i class=&quot;fas fa-archive&quot;>&lt;/i>">
            </div>
            <div class="col-md-6">
                    <div class="form-group">
                        <span class="input-group-btn">
                        <a id="lfm" data-input="thumbnail" data-preview="holder" class="new-upload-btn">
                        Choose
                        </a>
                    </div>
                </span>
                <input id="thumbnail" class="form-control" type="hidden" name="image">
            </div>
            <div>
                <img id="holder" style="margin-top:15px;max-height:400px;">
            </div>

            <script>
                $('#lfm').filemanager('image');
            </script>

            <button type="submit" class="btn btn-dark">Create Category</button>
        </form>

        <hr>
        <h3>Create Subcategory:</h3>
        <form action="{{ route('subcategory.store') }}" method="post">
            {{ csrf_field() }}

            <div class="input-group mb-3">
                <select class="custom-select" name="main_category_id">
                    <option selected>Choose Main Category</option>
                    @foreach($categories as $category)
                        <option value="{{$category->id}}">{{$category->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="name">Subcategory name:</label>
                <input class="form-control" type="text" name="name" placeholder="Category name">
            </div>
            <div class="form-group">
                <label for="icon">Icon - copy paste icon from fontawesome, ex: <span style="color:darkorange">&lt;i class="fas fa-archive">&lt;/i></span></label>
                <input class="form-control" type="text" name="icon" placeholder="&lt;i class=&quot;fas fa-archive&quot;>&lt;/i>">
            </div>

            <button type="submit" class="btn btn-dark">Create Subcategory</button>
        </form>
    </div>
@endsection