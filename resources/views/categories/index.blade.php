@extends('layouts.app')

@section('content')
    <div class="container">
        <a href="{{ route('category.create') }}" class="btn btn-dark">Create category</a><br><hr>
        <h3>Categories:</h3>
        @foreach($categories as $category)
            <p>
                {!! $category->icon !!}
                {{ $category->name }}
                <a href="{{ route('category.edit',['id' => $category->id ]) }}" class="btn btn-dark float-right">Edit</a>
            </p>
        @endforeach
        <hr>
        <h3>Subcategories:</h3>
        @foreach($subcategories as $subcategory)
            <p>
                {!! $subcategory->icon !!}
                {{ $subcategory->name }} -
                <strong>{{ $subcategory->category->name }}</strong>
                <a href="{{ route('subcategory.edit',['id' => $subcategory->id ]) }}" class="btn btn-dark float-right">Edit</a>
            </p>
        @endforeach
    </div>
@endsection