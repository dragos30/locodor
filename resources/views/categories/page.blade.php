@extends('layouts.app')

@section('content')
    <div class="container categories-page">
        <h1 class="category-title">Featured Categories</h1>
        <div class="row">
            <div class="col-md-6">
                <a href="#">
                  <div class="featured-category-item" style="background:linear-gradient(0deg,rgba(0,0,0,.4),rgba(0,0,0,.4)),url('/uploads/Create.jpg');">
                      <p class="category-text">Lifestyle Products</p>
                      <i class="fas fa-arrow-right category-icon"></i>
                  </div>
                </a>
            </div>
            <div class="col-md-6">
              <a href="#">
                <div class="featured-category-item" style="background:linear-gradient(0deg,rgba(0,0,0,.4),rgba(0,0,0,.4)),url('/uploads/Create.jpg');">
                    <p class="category-text">Lifestyle Products</p>
                    <i class="fas fa-arrow-right category-icon"></i>
                </div>
              </a>
            </div>
            <div class="col-md-6">
              <a href="#">
                <div class="featured-category-item" style="background:linear-gradient(0deg,rgba(0,0,0,.4),rgba(0,0,0,.4)),url('/uploads/Create.jpg');">
                    <p class="category-text">Lifestyle Products</p>
                    <i class="fas fa-arrow-right category-icon"></i>
                </div>
              </a>
            </div>
            <div class="col-md-6">
              <a href="#">
                <div class="featured-category-item" style="background:linear-gradient(0deg,rgba(0,0,0,.4),rgba(0,0,0,.4)),url('/uploads/Create.jpg');">
                    <p class="category-text">Lifestyle Products</p>
                    <i class="fas fa-arrow-right category-icon"></i>
                </div>
              </a>
            </div>
            <div class="col-md-6">
              <a href="#">
                <div class="featured-category-item" style="background:linear-gradient(0deg,rgba(0,0,0,.4),rgba(0,0,0,.4)),url('/uploads/Create.jpg');">
                    <p class="category-text">Lifestyle Products</p>
                    <i class="fas fa-arrow-right category-icon"></i>
                </div>
              </a>
            </div>
            <div class="col-md-6">
              <a href="#">
                <div class="featured-category-item" style="background:linear-gradient(0deg,rgba(0,0,0,.4),rgba(0,0,0,.4)),url('/uploads/Create.jpg');">
                    <p class="category-text">Lifestyle Products</p>
                    <i class="fas fa-arrow-right category-icon"></i>
                </div>
              </a>
            </div>
        </div>
        <h1 class="category-title">All Categories</h1>
        <div class="row all-categories-container">
          <?php 
            $counter = 0;
            $switch = true;  
          ?>
            @foreach($categories as $key => $category)
            
            <?php
              if($switch && $counter == 4){
                $switch = false;
                $counter = 0;
              }
              if($switch == false && $counter == 3){
                $switch = true;
                $counter = 0;
              }
              $counter++;
            ?>

            @if($switch)
              <div class="col-md-3">
                  <a href="#">
                      <div class="featured-category-item" style="background-image:linear-gradient(0deg,rgba(0,0,0,.4),rgba(0,0,0,.4)),url('{{ asset($category->image) }}');">
                          <p class="category-text">{{ $category->name }}</p>
                      </div>
                  </a>
              </div>
            @else
              <div class="col-md-4">
                  <a href="#">
                      <div class="featured-category-item" style="background-image:linear-gradient(0deg,rgba(0,0,0,.4),rgba(0,0,0,.4)),url('{{ asset($category->image) }}');">
                          <p class="category-text">{{ $category->name }}</p>
                      </div>
                  </a>
              </div>
            @endif
            @endforeach
        </div>
    </div>
    
@endsection
