@extends('layouts.app')

@section('content')
    <div class="container">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form action="{{ route('product.store') }}" method="post">
            @csrf

            <div class="form-group">
                <label for="project_id">To what project does the product belong to?</label>
                <select class="form-control normal-input" id="project_id" name="project_id">
                    @if(Auth::user()->admin)
                        <option value="0">None</option>
                    @endif
                    @foreach($projects as $project)
                        <option value="{{ $project->id }}">{{ $project->title }}</option>
                    @endforeach
                </select>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="title">Product Title</label>
                        <input type="text" class="form-control normal-input" id="title" name="title" placeholder="Example: SmartPhone With Back LED lighting">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div>
                            <label>Choose a representative Image</label>
                        </div>
                        <span class="input-group-btn">
                    <a id="lfm" data-input="thumbnail" data-preview="holder" class="new-upload-btn">
                    Choose
                    </a>
                </span>
                        <input id="thumbnail" class="form-control" type="hidden" name="image">
                    </div>
                    <img id="holder" style="margin-top:15px;max-height:400px;">

                    <script>
                        $('#lfm').filemanager('image');
                    </script>
                </div>
            </div>

            <div class="form-group">
                <label for="price">Product Price (USD)</label>
                <input type="number" step="0.01" min="0" class="form-control normal-input" id="price" name="price" placeholder="Example: 12.99">
            </div>

            @if(Auth::user()->admin)
                <div class="form-group">
                    <label for="link">External Link</label>
                    <input type="text" step="0.01" min="0" class="form-control normal-input" id="link" name="link">
                </div>
            @endif

            <div class="form-group">
                <label for="editor">Product description</label>
                <textarea class="form-control" name="description" id="editor" rows="3"></textarea>
            </div>

            <button class="new-upload-btn">Submit</button>
        </form>
    </div>
@endsection