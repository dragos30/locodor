@extends('layouts.app')

@section('content')
    <div class="loader" id="loader">
        <div class="loader-inner">
            <div class="loader-square"></div>
            <div class="loader-square"></div>
            <div class="loader-square"></div>
            <div class="loader-square"></div>
        </div>
    </div>
    <div class="filter-navbar">
        <span class="filter-navbar-right">
            <a href="{{ route('products') }}" class="filter-bar-item purchase-now"><span>All</span></a>
        </span>
    </div>
    <div class="container">
        <div>
            <div class="row projects-container">
                <div class="all-projects col-md-12" id="all-projects">
                <div class="project-row">
                    <div class="grid">
                        @if(count($products))
                            @foreach($products as $product)
                                <div class="grid-item">
                                    <div class="product">
                                        <img src="{{ $product->image }}" width="100%">
                                        @if($product->link == null)
                                            <span class="exclusive-product"><i class="fas fa-award"></i></span>
                                        @endif
                                        <div class="product-description">
                                            <h3>{{ $product->title }}</h3>
                                            <?php
                                            if (strlen($product->description) > 150){
                                                $str = substr($product->description, 0, 147) . '...';
                                            } else {
                                                $str = $product->description;
                                            }

                                            ?>
                                            <p>{!! $str !!}</p>
                                        </div>
                                        <p class="product-price text-center">${{ $product->price }}</p>
                                        <div class="text-center">
                                            @if($product->project_id === null)
                                                <a class="normal-btn" href="{{ $product->link }}">View Product</a>
                                            @else
                                                <a class="normal-btn" href="{{ route('product.show',['id'=>$product->id]) }}">View Product</a>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <!-- test comment -->

                </div>
            </div>
        </div>
    </div>
    <div class="pages-links d-flex">
        <div class="mx-auto">
            {{ $products->appends(Illuminate\Support\Facades\Input::except('page'))->links() }}
        </div>
    </div>
    <script>
        console.clear();

        // Get all the Meters
        var meters = document.querySelectorAll('svg[data-value] .meter');

        meters.forEach(function (path) {
            // Get the length of the path
            var length = path.getTotalLength();
            // console.log(length) once and hardcode the stroke-dashoffset and stroke-dasharray in the SVG if possible
            // or uncomment to set it dynamically
            // path.style.strokeDashoffset = length;
            // path.style.strokeDasharray = length;

            // Get the value of the meter
            var value = parseInt(path.parentNode.getAttribute('data-value'));
            // Calculate the percentage of the total length
            var to = length * ((100 - value) / 100);
            // Trigger Layout in Safari hack https://jakearchibald.com/2013/animated-line-drawing-svg/
            path.getBoundingClientRect();
            // Set the Offset
            path.style.strokeDashoffset = Math.max(0, to);
        });
    </script>
    <script>
        $( document ).ready(function() {
            var $grid = $('.grid').imagesLoaded( function() {
                // init Masonry after all images have loaded
                $grid.masonry({
                    itemSelector: '.grid-item',
                });
                $('#loader').fadeOut( "slow" );
                setTimeout(function(){
                    $('#loader').remove();
                }, 1000);
            });
        });
    </script>
@endsection