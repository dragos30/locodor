@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="{{ route('product.update',['id' => $product->id]) }}" method="post">
            @csrf

            <div class="form-group">
                <label for="project_id">To what project does the product belong to?</label>
                <select class="form-control" id="project_id" name="project_id">
                    @foreach($projects as $project)
                        <option value="{{ $project->id }}" @if($project->id == $product->project_id) selected @endif>{{ $project->title }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <div>
                    <label>Choose a representative Image</label>
                </div>
                <span class="input-group-btn">
                    <a id="lfm" data-input="thumbnail" data-preview="holder" class="new-upload-btn">
                    Choose
                    </a>
                </span>
                <input id="thumbnail" class="form-control" type="hidden" name="image" value="{{ $product->image }}">
            </div>
            <img id="holder" style="margin-top:15px;max-height:400px;">

            <script>
                $('#lfm').filemanager('image');
            </script>

            <div class="form-group">
                <label for="title">Product Title</label>
                <input type="text" class="form-control" id="title" name="title" placeholder="Example: SmartPhone With Back LED lighting" value="{{ $product->title }}">
            </div>

            <div class="form-group">
                <label for="price">Product Price</label>
                <input type="number" step="0.01" min="0" class="form-control" id="price" name="price" placeholder="Price" value="{{ $product->price }}">
            </div>

            <div class="form-group">
                <label for="editor">Product description</label>
                <textarea class="form-control" name="description" id="editor" rows="3">{{ $product->description }}</textarea>
            </div>

            <button type="submit" class="normal-btn">Update</button>
        </form>
    </div>
@endsection