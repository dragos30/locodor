@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @if(count($products_approved))
                <div class="col-md-12">
                    <h4>Products Approved</h4>
                    <div class="fancy-hr"></div>
                </div>
                @foreach($products_approved as $product)
                    <div class="col-md-4">
                        <div class="card mb-4 text-white bg-dark">
                            <img class="card-img-top" src="{{ $product->image }}" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title">{{ $product->title }}</h5>
                                <a href="{{ route('product.show',[ 'id' => $product->id ]) }}" class="btn btn-outline-light btn-sm">View Product</a>
                                <a href="{{ route('product.edit',[ 'id' => $product->id ]) }}" class="btn btn-outline-light btn-sm">Edit</a>
                                <!-- todo: Create warning message before deleteing -->
                                <a href="{{ route('product.delete',[ 'id' => $product->id ]) }}" class="btn btn-outline-danger btn-sm">Delete</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
            @if(count($products_pending))
                <div class="col-md-12">
                    <h4>Projects pending approval</h4>
                </div>
                @foreach($products_pending as $product)
                        <div class="col-md-4">
                            <div class="card mb-4 text-white bg-dark">
                                <img class="card-img-top" src="{{ $product->image }}" alt="Card image cap">
                                <div class="card-body">
                                    <h5 class="card-title">{{ $product->title }}</h5>
                                    <a href="{{ route('product.show',[ 'id' => $product->id ]) }}" class="btn btn-outline-light btn-sm">View Product</a>
                                    <a href="{{ route('product.edit',[ 'id' => $product->id ]) }}" class="btn btn-outline-light btn-sm">Edit</a>
                                    <!-- todo: Create warning message before deleteing -->
                                    <a href="{{ route('product.delete',[ 'id' => $product->id ]) }}" class="btn btn-outline-danger btn-sm">Delete</a>
                                </div>
                            </div>
                        </div>
                @endforeach
            @endif
        </div>
    </div>
@endsection