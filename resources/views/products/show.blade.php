@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <img src="{{ asset($product->image) }}" width="100%">
            </div>
            <div class="col-md-6">
                <h1>{{ $product->title }}</h1>
                <p class="product-show-price">${{$product->price}}</p>
                <hr>
                <p>{!! $product->description !!}</p>

                <div class="button-container text-center">
                    <?php
                        $test = strpos($product->price, '.');
                        if($test){
                            $price = str_replace(".","",$product->price);
                        } else {
                            $price = $product->price * 100;
                        }
                    ?>
                    {{--Paypal Button--}}

                    @if($product->link)
                        <a class="fancy-btn buy-product-btn" href="{{ $product->link }}">Buy Product</a>
                        @else
                            <div class="payment-button">
                                <div id="paypal-button"></div>
                                <div class="text-center">
                                    <img style="max-width:100%;width:250px;" src="{{ asset('uploads/cards.png') }}">
                                </div>
                            </div>

                            <script>
                                paypal.Button.render({

                                    env: 'production', // sandbox | production
                                    style: {
                                        label: 'paypal',
                                        size:  'medium',    // small | medium | large | responsive
                                        shape: 'pill',     // pill | rect
                                        color: 'black',     // gold | blue | silver | black
                                        tagline: false
                                    },

                                    // PayPal Client IDs - replace with your own
                                    // Create a PayPal app: https://developer.paypal.com/developer/applications/create
                                    client: {
                                        sandbox: 'Ae6vLeJ1R-8Fy_gzfZ-1d8EhOO52TUvLc6_JwedwI8lVyyfdbNdT3T8vDRmQKxsdcUZDcFmNCLGOxvdT',
                                        production: 'AacGDpsS8aoAfLdJN4DblsZELEQBD05QwkKFa-XBBmuKPC9plpAdNFiwzesaiuZ3acQ2FCKUtbImr3Fo'
                                    },

                                    // Show the buyer a 'Pay Now' button in the checkout flow
                                    commit: true,

                                    // payment() is called when the button is clicked
                                    payment: function(data, actions) {
                                        return actions.payment.create({
                                            transactions: [{
                                                amount: {
                                                    total: '{{ $product->price }}.00',
                                                    currency: 'USD'
                                                },
                                                description: '{{ $product->title }}',
                                            }]
                                        });
                                    },

                                    // onAuthorize() is called when the buyer approves the payment
                                    onAuthorize: function(data, actions) {

                                        // Make a call to the REST api to execute the payment
                                        return actions.payment.execute().then(function() {
                                            window.alert('Payment Complete!');
                                        });
                                    }

                                }, '#paypal-button');

                            </script>
                    @endif
                </div>
            </div>
            <div class="col-md-12">
                <p class="might-like">You might also like:</p>
                <div class="row">
                    @foreach($suggestions as $suggestion)
                        <div class="col-md-3">
                            <div style="margin-bottom:10px;">
                                <img src="{{ $suggestion->image }}" width="100%">
                                <h3>{{ $suggestion->title }}</h3>
                                <p class="product-show-price">${{ $suggestion->price }}</p>
                                @if($suggestion->link != null)
                                    <a class="fancy-btn" href="{{ $suggestion->link }}">View</a>
                                @else
                                    <a class="fancy-btn" href="{{ route('product.show',['id' => $suggestion->id]) }}">View</a>
                                @endif
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection