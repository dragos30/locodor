@extends('layouts.app')

@section('content')
    <div class="loader" id="loader">
        <div class="loader-inner">
            <div class="loader-line-wrap">
                <div class="loader-line"></div>
            </div>
            <div class="loader-line-wrap">
                <div class="loader-line"></div>
            </div>
            <div class="loader-line-wrap">
                <div class="loader-line"></div>
            </div>
        </div>
    </div>
    <div class="filter-navbar">
        <span class="filter-navbar-right">
            <a href="{{ route('projects') }}?project_timing=exclusive" class="filter-bar-item"><span><i class="fas fa-star"></i> Locodor Exclusive</span></a>
            <a href="{{ route('projects') }}?project_timing=trending" class="filter-bar-item"><span>Trending</span></a>
            <a href="{{ route('projects') }}?project_timing=just_launched" class="filter-bar-item"><span>New</span></a>
            <a href="{{ route('projects') }}?project_timing=nearly_funded" class="filter-bar-item"><span>Almost Funded</span></a>
            <a href="{{ route('projects') }}?project_timing=fully_funded" class="filter-bar-item"><span>Fully Funded</span></a>
            <a href="{{ route('projects') }}?project_timing=ending_soon" class="filter-bar-item"><span>Ending Soon</span></a>
            <a href="{{ route('projects') }}?project_timing=most_talked" class="filter-bar-item"><span>Most Talked About</span></a>
            <a href="{{ route('affiliate') }}" class="filter-bar-item"><span>Sponsors</span></a>
            <a href="{{ route('products') }}" class="filter-bar-item purchase-now"><span>Purchase now!</span></a>
        </span>
    </div>
    <div class="container">
        <div class="row projects-container">
            {{--<div class="col-md-3">--}}
            {{--<h4>Filter results</h4>--}}
            {{--<hr>--}}
            {{--<h4>Category</h4>--}}
            {{--@foreach($categories as $category)--}}
            {{--<p><a href="{{ route('projects') }}?project_category={{$category->id}}">{!! $category->icon !!}  {{ $category->name }}</a></p>--}}
            {{--@endforeach--}}
            {{--<hr>--}}
            {{--<h4>Projects</h4>--}}
            {{--<p><a href="{{ route('projects') }}?project_type=all">All</a></p>--}}
            {{--<p><a href="{{ route('projects') }}?project_timing=just_launched">Just Launched</a></p>--}}
            {{--<p><a href="{{ route('projects') }}?project_timing=trending_world">Trending Worldwide</a></p>--}}
            {{--<p><a href="{{ route('projects') }}?project_timing=trending_near">Trending Near You</a></p>--}}
            {{--<p><a href="{{ route('projects') }}?project_timing=exclusive">Locodor Exclusive</a></p>--}}
            {{--<p><a href="{{ route('projects') }}?project_timing=fully_funded">Fully funded</a></p>--}}
            {{--<p><a href="{{ route('projects') }}?project_timing=nearly_funded">Nearly Funded</a></p>--}}
            {{--<p><a href="{{ route('projects') }}?project_timing=ending_soon">Ending soon</a></p>--}}
            {{--<p><a href="{{ route('projects') }}?project_timing=funding">Purchase Now</a></p>--}}
            {{--<hr>--}}
            {{--<h4>Social Crowd</h4>--}}
            {{--<p><a href="{{ route('projects') }}?project_timing=most_talked">Most talked about</a></p>--}}
            {{--<p><a href="{{ route('projects') }}?project_timing=friends_liked">Liked by Friends</a></p>--}}
            {{--</div>--}}
            @if(Auth::guest())
                <div class="col-md-6 text-center projects-pitch" id="project-pitch">
                    <h1>JOIN THE FIRST SOCIAL<br> CROWDFUNDING NETWORK AROUND</h1>
                    <h3>#TOGETHERISBETTER</h3>
                    <p>Connect and Collaborate with Individuals<br> in the Crowdfunding Industry!</p>
                    <a href="/register">CREATE ACCOUNT <i class="fas fa-arrow-right"></i></a>
                </div>
                <script>
                    $( document ).ready(function() {
                        function myMove() {
                            var elem = document.getElementById("project-pitch");
                            var elem2 = document.getElementById("all-projects");
                            var elem3 = document.getElementById("create-account");
                            var pos = 0;
                            var id = setInterval(frame, 5);
                            function frame() {
                                if (pos == -330) {
                                    elem.style.opacity = '0';
                                    clearInterval(id);
                                    elem3.style.opacity = '1';
                                } else {
                                    pos--;
                                    elem.style.top = pos*2 + 'px';
                                    elem2.style.top = pos + 'px';
                                }
                            }
                        }
                        $( document ).ready(function() {
                            setTimeout(function(){
                                myMove();
                            }, 10000);
                        });
                    });
                </script>
            @endif
            {{--<div class="text-right">--}}
            {{--Sort by date:--}}
            {{--<select>--}}
            {{--<option value="audi">Trending</option>--}}
            {{--<option value="volvo">Most funded</option>--}}
            {{--<option value="saab">Most commented</option>--}}
            {{--<option value="opel">Most recent</option>--}}
            {{--</select>--}}
            {{--Sort by location:--}}
            {{--<select>--}}
            {{--<option value="1">North America</option>--}}
            {{--<option value="2">South America</option>--}}
            {{--<option value="3">Europe</option>--}}
            {{--<option value="4">Africa</option>--}}
            {{--<option value="5">Middle East</option>--}}
            {{--<option value="6">Australia</option>--}}
            {{--<option value="7">Asia</option>--}}
            {{--</select>--}}
            {{--Shipping location:--}}
            {{--<select>--}}
            {{--<option value="1">North America</option>--}}
            {{--<option value="2">South America</option>--}}
            {{--<option value="3">Europe</option>--}}
            {{--<option value="4">Africa</option>--}}
            {{--<option value="5">Middle East</option>--}}
            {{--<option value="6">Australia</option>--}}
            {{--<option value="7">Asia</option>--}}
            {{--</select>--}}
            {{--</div>--}}
            <div class="all-projects col-md-12" id="all-projects">
                @if(Auth::guest())
                    <div class="col-md-12 text-center">
                        <a href="/register" class="create-account" id="create-account">CREATE ACCOUNT <i class="fas fa-arrow-right"></i></a>
                    </div>
                @endif
                <div class="title-filters col-md-12">
                    <a href="#">Exclusive</a>
                    <a href="#">Trending</a>
                    <a href="#">Fresh</a>
                </div>
                <span class="filter-navbar-left">
                    <form action="{{ route('projects') }}" method="get" class="search-form project-index-search">
                        <div class="projects-search">
                            <label for="project_query" class="sr-only">Search</label>
                            <input type="text" class="form-control" name="project_query" id="project_query" placeholder="Search for projects" value="{{ request('project_query') }}">
                        </div>
                    </form>
                </span>
                <div class="project-row">
                    <div class="grid">
                        @if(count($products))
                            @foreach($products as $product)
                                <div class="grid-item">
                                    <div class="product">
                                        <img src="{{ $product->image }}" width="100%">
                                        <h3>{{ $product->name }}</h3>
                                        <p>${{ $product->after_discount }}</p>
                                        <a class="normal-btn" href="{{ $product->link }}">Get Details!</a>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <!-- test comment -->

                </div>
            </div>
        </div>
    </div>
    <div class="pages-links d-flex">
        <div class="mx-auto">
            {{ $products->appends(Illuminate\Support\Facades\Input::except('page'))->links() }}
        </div>
    </div>
    <script>
        console.clear();

        // Get all the Meters
        var meters = document.querySelectorAll('svg[data-value] .meter');

        meters.forEach(function (path) {
            // Get the length of the path
            var length = path.getTotalLength();
            // console.log(length) once and hardcode the stroke-dashoffset and stroke-dasharray in the SVG if possible
            // or uncomment to set it dynamically
            // path.style.strokeDashoffset = length;
            // path.style.strokeDasharray = length;

            // Get the value of the meter
            var value = parseInt(path.parentNode.getAttribute('data-value'));
            // Calculate the percentage of the total length
            var to = length * ((100 - value) / 100);
            // Trigger Layout in Safari hack https://jakearchibald.com/2013/animated-line-drawing-svg/
            path.getBoundingClientRect();
            // Set the Offset
            path.style.strokeDashoffset = Math.max(0, to);
        });
    </script>
    <script>
        $( document ).ready(function() {
            var $grid = $('.grid').imagesLoaded( function() {
                // init Masonry after all images have loaded
                $grid.masonry({
                    itemSelector: '.grid-item',
                });
                $('#loader').fadeOut( "slow" );
                setTimeout(function(){
                    $('#loader').remove();
                }, 1000);
            });
        });
    </script>
@endsection