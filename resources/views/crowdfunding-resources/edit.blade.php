@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="{{ route('resource.update',['id'=>$resource->id]) }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="input-group">
                <span class="input-group-btn">
                     <a id="lfm" data-input="thumbnail" data-preview="holder" class="new-upload-btn">
                        Choose
                     </a>
                </span>
                <input id="thumbnail" class="form-control" type="hidden" name="image" value="{{ $resource->image }}">
            </div>
            <img id="holder" style="margin-top:15px;max-height:400px;">
            <script>
                $('#lfm').filemanager('image');
            </script>

            <div class="form-group">
                <label for="title">Title</label>
                <input class="form-control" type="text" name="title" placeholder="Title of the resource" value="{{ $resource->title }}">
            </div>

            <div class="form-group">
                <label for="content">Content</label>
                <textarea class="form-control" name="content" placeholder="Description" rows="10" id="editor">{{ $resource->content }}</textarea>
            </div>

            <div class="form-group">
                <label for="link">Link to the resource</label>
                <input class="form-control" type="text" name="link" placeholder="Link to resource" value="{{ $resource->link }}">
            </div>

            <div class="form-group">
                <label for="link">Tags:</label>
                <input class="form-control" type="text" name="tags" placeholder="Examples: gadget, tech, programming, design, creative" value="{{ $resource->tags }}">
            </div>


            <button type="submit" class="btn btn-dark">Update resource</button>
        </form>
    </div>
@endsection