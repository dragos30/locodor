@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="post-show-image">
            <img src="{{ asset($resource->image) }}" width="70%">
        </div>
        @if(Auth::check() && Auth::user()->admin)
            <div class="resource-control">
                <a href="{{ route('resource.edit',['id'=>$resource->id]) }}" class="btn btn-dark">Edit</a>
                <a href="{{ route('resource.delete',['id'=>$resource->id]) }}" class="btn btn-dark">Delete</a>
            </div>
            <hr>
        @endif
        <div class="resources-control">
            <a  href="{{ $resource->link }}" class="basic-card-button" target="_blank">Go to Official Website <i class="fas fa-arrow-right"></i></a>
        </div>
        <div class="project-share text-left">
            Share on:
            <a href="http://www.facebook.com/sharer.php?u={{ urlencode(url()->current()) }}" target="_blank">
                <i style="color:#37569b" class="fab fa-facebook-f"></i>
            </a>
            <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url={{ urlencode(url()->current()) }}" target="_blank">
                <i style="color:#0479ba" class="fab fa-linkedin-in"></i>
            </a>
            <a href="https://twitter.com/share?url={{ urlencode(url()->current()) }}" target="_blank">
                <i style="color:#00acf0" class="fab fa-twitter"></i>
            </a>
        </div>
        <h1 class="post-show-title">{{ $resource->title }}</h1>

        <p class="post-show-content">{!! $resource->content !!}</p>
    </div>
@endsection