@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="row">
                    @foreach($resources as $resource)
                        <div class="col-md-4">
                            <div class="basic-card">
                                <a href="{{ route('resource.show',['id' => $resource->id,'slug'=>$resource->slug]) }}">
                                    <img src="{{ asset($resource->image) }}">
                                </a>
                                <div class="basic-card-details">
                                    <h3 class="basic-card-title">
                                        {{ $resource->title }}
                                    </h3>
                                    <p class="basic-card-content">
                                        {{ strip_tags($resource->content) }}
                                    </p>
                                    <a href="{{ $resource->link }}" class="basic-card-button" target="_blank">View</a>
                                    <a href="{{ route('resource.show',['id' => $resource->id,'slug'=>$resource->slug]) }}" class="basic-card-button">Read More</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                        {{--<div class="col-md-4">--}}
                            {{--<div class="basic-card">--}}
                                {{--<a href="{{ route('campaign.tips') }}">--}}
                                    {{--<img src="{{ asset('/uploads/campaign-tips-cover.jpg') }}">--}}
                                {{--</a>--}}
                                {{--<div class="basic-card-details">--}}
                                    {{--<h3 class="basic-card-title">--}}
                                        {{--Welcome to Locodor--}}
                                    {{--</h3>--}}
                                    {{--<p class="basic-card-content">--}}
                                        {{--Here is a handy checklist that covers everything you need to set up your personal account and campaign settings.--}}
                                    {{--</p>--}}
                                    {{--<a href="{{ route('campaign.tips') }}" class="basic-card-button">Get set up</a>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                </div>
            </div>
            <div class="col-md-2">
                <div class="basic-sidebar">
                    <p class="basic-sidebar-title">Useful Links</p>
                    <div class="fancy-hr"></div>
                    <p class="sidebar-link">
                        <a href="{{ asset('uploads/how-to-launch.pdf') }}" target="_blank">Starting your first Campaign</a>
                    </p>
                    <p class="sidebar-link">
                        <a href="{{ route('crowdfunding.resources') }}">Resources</a>
                    </p>
                    <p class="sidebar-link">
                        <a href="{{ route('campaign.tasks.example') }}" target="_blank">Campaign Tasks</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection