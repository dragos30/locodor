@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <script>
                $( document ).ready(function() {
                    $('.show-contacts').on('click',function(){
                        var leftVal = -500;
                        var toggled = 0;
                        var interval = setInterval(function(){
                            if(toggled == 0){
                                $('.hide-contacts').fadeIn( "slow");
                                leftVal = leftVal +10;
                                $('.mobile-conversations-container').css('left',leftVal+'px');
                                if(leftVal >= 0){
                                    clearInterval(interval);
                                    toggled = 1;
                                }
                            }
                        }, 1);
                    })

                    $('.hide-contacts').on('click',function(){
                        var leftVal = 0;
                        var toggled = 1;
                        var interval = setInterval(function(){
                            if(toggled == 1){
                                $('.hide-contacts').fadeOut( "slow");
                                leftVal = leftVal -20;
                                $('.mobile-conversations-container').css('left',leftVal+'px');
                                if(leftVal <= -1000){
                                    clearInterval(interval);
                                    toggled = 0;
                                }
                            }
                        }, 1);
                    })

                });
            </script>
            <div class="only-mobile">
                <div class="show-contacts-container">
                    <a class="show-contacts">Show Contacts</a>
                </div>

                <div class="mobile-conversations-container">
                    <button class="hide-contacts">Hide</button>
                    <h4>Conversations:</h4>
                    @foreach($conversations as $conversation)
                        @if($conversation->user1_id == Auth::id())
                            <a href="/conversations?c={{ $conversation->id  }}&r={{ $conversation->user2_id }}" class="user-conversation-link">
                                <div class="user-conversation delete-conversation-btn">
                                    @if(Auth::user()->admin != 1)
                                        <img src="{{ asset( $conversation->user2->profile->avatar ) }}">
                                    @endif
                                    <?php
                                    if (strlen( $conversation->user2->name ) > 18){
                                        $str = substr( $conversation->user2->name, 0, 15) . '...';
                                    } else {
                                        $str = $conversation->user2->name;
                                    }

                                    ?>
                                    <p class="conv-username">{{ $str }}</p>
                                </div>
                            </a>
                        @else
                            <a href="/conversations?c={{ $conversation->id  }}&r={{ $conversation->user1_id }}" class="user-conversation-link">
                                <div class="user-conversation">
                                    @if(!Auth::user()->admin != 1)
                                        <img src="{{ asset( $conversation->user1->profile->avatar ) }}">
                                    @endif
                                    <?php
                                    if (strlen( $conversation->user1->name ) > 18){
                                        $str = substr( $conversation->user1->name, 0, 15) . '...';
                                    } else {
                                        $str = $conversation->user1->name;
                                    }
                                    ?>
                                    <p class="conv-username">{{ $str }}</p>
                                </div>
                            </a>
                        @endif
                    @endforeach
                    <h4>Friends:</h4>
                    @foreach($friends as $friend)
                        @if($friend->user1_id == Auth::id())
                            <a href="{{ route('start.conversation',['id'=>$friend->user2_id]) }}" class="user-conversation-link">
                                <div class="user-conversation">
                                    <img src="{{ asset( $friend->user2->profile->avatar ) }}">
                                    <?php
                                    if (strlen( $friend->user2->name ) > 18){
                                        $str = substr( $friend->user2->name, 0, 15) . '...';
                                    } else {
                                        $str = $friend->user2->name;
                                    }
                                    ?>
                                    <p class="conv-username">{{ $str }}</p>
                                </div>
                            </a>
                        @else
                            <a href="{{ route('start.conversation',['id'=>$friend->user1_id]) }}" class="user-conversation-link">
                                <div class="user-conversation">
                                    <img src="{{ asset( $friend->user1->profile->avatar ) }}">
                                    <?php
                                    if (strlen( $friend->user1->name ) > 18){
                                        $str = substr( $friend->user1->name, 0, 15) . '...';
                                    } else {
                                        $str = $friend->user1->name;
                                    }
                                    ?>
                                    <p class="conv-username">{{ $str }}</p>
                                </div>
                            </a>
                        @endif
                    @endforeach
                </div>
            </div>
            <div class="col-md-3 conversations-container hide-on-mobile">
                <h4>Conversations:</h4>
                @foreach($conversations as $conversation)
                    @if($conversation->user1_id == Auth::id())
                        <a href="/conversations?c={{ $conversation->id  }}&r={{ $conversation->user2_id }}" class="user-conversation-link">
                            <div class="user-conversation delete-conversation-btn">
                                @if(Auth::user()->admin != 1)
                                    <img src="{{ asset( $conversation->user2->profile->avatar ) }}">
                                @endif

                                <?php
                                if (strlen( $conversation->user2->name ) > 18){
                                    $str = substr( $conversation->user2->name, 0, 15) . '...';
                                } else {
                                    $str = $conversation->user2->name;
                                }

                                ?>
                                <p class="conv-username">{{ $str }}</p>
                            </div>
                        </a>
                    @else
                        <a href="/conversations?c={{ $conversation->id  }}&r={{ $conversation->user1_id }}" class="user-conversation-link">
                            <div class="user-conversation">
                                @if(Auth::user()->admin != 1)
                                    <img src="{{ asset( $conversation->user1->profile->avatar ) }}">
                                @endif
                                <?php
                                if (strlen( $conversation->user1->name ) > 18){
                                    $str = substr( $conversation->user1->name, 0, 15) . '...';
                                } else {
                                    $str = $conversation->user1->name;
                                }
                                ?>
                                <p class="conv-username">{{ $str }}</p>
                            </div>
                        </a>
                    @endif
                @endforeach
                <h4>Friends:</h4>
                @foreach($friends as $friend)
                    @if($friend->user1_id == Auth::id())
                        <a href="{{ route('start.conversation',['id'=>$friend->user2_id]) }}" class="user-conversation-link">
                            <div class="user-conversation">
                                <img src="{{ asset( $friend->user2->profile->avatar ) }}">
                                <?php
                                if (strlen( $friend->user2->name ) > 18){
                                    $str = substr( $friend->user2->name, 0, 15) . '...';
                                } else {
                                    $str = $friend->user2->name;
                                }
                                ?>
                                <p class="conv-username">{{ $str }}</p>
                            </div>
                        </a>
                    @else
                        <a href="{{ route('start.conversation',['id'=>$friend->user1_id]) }}" class="user-conversation-link">
                            <div class="user-conversation">
                                <img src="{{ asset( $friend->user1->profile->avatar ) }}">
                                <?php
                                if (strlen( $friend->user1->name ) > 18){
                                    $str = substr( $friend->user1->name, 0, 15) . '...';
                                } else {
                                    $str = $friend->user1->name;
                                }
                                ?>
                                <p class="conv-username">{{ $str }}</p>
                            </div>
                        </a>
                    @endif
                @endforeach
            </div>
            <script>
                $( document ).ready(function() {
                    var element = document.getElementById("messages-container");
                    element.scrollTop = element.scrollHeight;
                });
            </script>
            <div class="col-md-6 conversation-container">
                @if(isset($_GET['c']))
                    <div class="messages" id="messages-container">
                        <?php $count_messages = 0; ?>
                        @foreach($messages as $message)
                            @if($message->from == Auth::id())
                                <div class="message message-sent">
                                    <p style="border-bottom-right-radius:0">{!! $message->message !!}</p>
                                    <a href="{{ route('profile.show',['id' => Auth::user()->profile->id, 'slug' => Auth::user()->profile->id ] ) }}"><img src="{{ asset(Auth::user()->profile->avatar) }}"></a>
                                    @if($message->status == 2)
                                        <i class="fas fa-check-circle seen" style="color:#ff6600"></i>
                                    @else
                                        <i class="far fa-check-circle"></i>
                                    @endif
                                </div>
                            @else
                                <div class="message message-received">
                                    <a href="{{ route('profile.show',['id' => $recipient->profile->id, 'slug' => $recipient->profile->id ] ) }}"><img src="{{ asset($recipient->profile->avatar) }}"></a>
                                    <p style="border-bottom-left-radius:0">{!! $message->message !!}</p>
                                </div>
                            @endif
                        @endforeach
                    </div>
                    <hr>
                    <form action="{{ route('message.store') }}" method="post">
                        @csrf
                        <input type="hidden" name="conv_id" value="{{ $_GET['c'] }}">
                        <input type="hidden" name="to" value="{{ $_GET['r'] }}">
                        <div class="form-group">
                            <textarea class="form-control conversation-textarea" name="message" placeholder="Type in your message" maxlength="191"></textarea>
                        </div>
                        <button type="submit" class="fancy-btn float-right submit-conversation-message">Send</button>
                    </form>

                    <script>
                        // Check for new messages

                        $( document ).ready(function() {

                            var messagesContainer = $('.messages');
                            setInterval(function(){
                                $.ajax({
                                    type: "GET",
                                    url: '/conversations',
                                    data: {
                                        c: '{{ $_GET['c'] }}',
                                        r: '{{ $_GET['r'] }}'
                                    },
                                    success: function(result) {
                                        response = $(result).find('.messages').children();
                                        console.log(response);
                                        console.log(response.length);
                                        var existentSeenMessage = $('.messages').find('.seen').length;
                                        var lastSeenMessage = $(result).find('.messages').find('.seen').length;
                                        console.log('e:' + existentSeenMessage + '  a:' + lastSeenMessage);
                                        var existentMessages = response.length;
                                        var displayedMessages = $('.messages').children().length;
                                        if(displayedMessages < existentMessages){
                                            messagesContainer.append(response.slice(displayedMessages - existentMessages));
                                        }

                                        if(existentSeenMessage < lastSeenMessage){
                                            for(var i = 0; i < lastSeenMessage - existentSeenMessage; i++){
                                                messagesContainer.children().last().remove();
                                            }
                                            messagesContainer.append(response.slice(existentSeenMessage - lastSeenMessage));
                                        }
                                    },
                                    error: function(result) {
                                        console.log(result);
                                        console.log('There is a problem');
                                    }
                                });
                            }, 3000);

                            $('.submit-conversation-message').on('click',function(e){
                                e.preventDefault();

                                var btnClicked = $(this);
                                btnClicked.css('pointer-events','none');
                                var messageToSend = btnClicked.prev().children().first().val();
                                console.log(messageToSend);

                                $.ajax({
                                    type: "POST",
                                    url: '/message-store',
                                    data: {
                                        _token: "{{ csrf_token() }}",
                                        conv_id: '{{ $_GET['c'] }}',
                                        to: '{{ $_GET['r'] }}',
                                        message: messageToSend
                                    },
                                    success: function(result) {
                                        btnClicked.prev().children().first().val('');
                                        btnClicked.css('pointer-events','auto');
                                        var messagesContainer = $('.messages');
                                        var element = document.getElementById("messages-container");
                                        element.scrollTop = element.scrollHeight+10;
                                        $.ajax({
                                            type: "GET",
                                            url: '/conversations',
                                            data: {
                                                c: '{{ $_GET['c'] }}',
                                                r: '{{ $_GET['r'] }}'
                                            },
                                            success: function(result) {
                                                response = $(result).find('.messages').children();
                                                console.log(response);
                                                console.log(response.length);
                                                var existentSeenMessage = $('.messages').find('.seen').length;
                                                var lastSeenMessage = $(result).find('.messages').find('.seen').length;
                                                console.log('e:' + existentSeenMessage + '  a:' + lastSeenMessage);
                                                var existentMessages = response.length;
                                                var displayedMessages = $('.messages').children().length;
                                                if(displayedMessages < existentMessages){
                                                    messagesContainer.append(response.slice(displayedMessages - existentMessages));
                                                }

                                                if(existentSeenMessage < lastSeenMessage){
                                                    for(var i = 0; i < lastSeenMessage - existentSeenMessage; i++){
                                                        messagesContainer.children().last().remove();
                                                    }
                                                    messagesContainer.append(response.slice(existentSeenMessage - lastSeenMessage));
                                                }

                                                var element = document.getElementById("messages-container");
                                                element.scrollTop = element.scrollHeight+10;
                                            },
                                            error: function(result) {
                                                console.log(result);
                                                console.log('There is a problem1');
                                            }
                                        });
                                    },
                                    error: function(result) {
                                        console.log(result);
                                        console.log('There is a problem2');
                                    }
                                });
                            })
                        });
                    </script>
                @endif
            </div>
            <div class="col-md-3 recipient-container">
                @if(isset($_GET['r']))
                    <div class="recipient-details">
                        <img src="{{ asset($recipient->profile->avatar) }}">
                        <p>{{ $recipient->name }}</p>
                        <p class="recipient-about">{{ $recipient->profile->about }}</p>
                        <a href="{{ route('profile.show',['id'=> $recipient->profile->id, 'slug' => $recipient->profile->slug]) }}" class="fancy-btn">View profile</a>
                    </div>
                    <div class="text-center">
                        <a href="{{ route('delete.conversation',['id' => $_GET['c']]) }}" class="btn btn-dark delete-conversation round-btn">Delete Conversation</a>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
