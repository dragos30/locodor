<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <head>
        <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
        <!--[if gte mso 12]>
        > <style type="text/css">
            > [a.btn {
                padding:15px 22px !important;
                display:inline-block !important;
            }]
            > </style>
        > <![endif]-->
        <title>Wohoo</title>
        <style type="text/css">
            div, p, a, li, td {
                -webkit-text-size-adjust:none;
            }
            .ReadMsgBody {
                width: 100%;
                background-color: #cecece;
            }
            .ExternalClass {
                width: 100%;
                background-color: #cecece;
            }
            body {
                width: 100%;
                height: 100%;
                background-color: #cecece;
                margin:0;
                padding:0;
                -webkit-font-smoothing: antialiased;
            }
            html {
                width: 100%;
            }
            img{
                border:none;
            }
            table td[class=show]{
                display:none !important;
            }
            @media only screen and (max-width: 640px) {
                body {
                    width:auto!important;
                }
                table[class=full] {
                    width:100%!important;
                }
                table[class=devicewidth] {
                    width:100% !important;
                    padding-left:20px !important;
                    padding-right: 20px!important;
                }
                table[class=inner] {
                    width:100%!important;
                    text-align: center!important;
                    clear: both;
                }
                table[class=inner-centerd] {
                    width:78%!important;
                    text-align: center!important;
                    clear: both;
                    float:none !important;
                    margin:0 auto !important;
                }
                table td[class=hide], .hide {
                    display:none !important;
                }
                table td[class=show], .show{
                    display:block !important;
                }
                img[class=responsiveimg]{
                    width:100% !important;
                    height:atuo !important;
                    display:block !important;
                }
                table[class=btnalign]{
                    float:left !important;
                }
                table[class=btnaligncenter]{
                    float:none !important;
                    margin:0 auto !important;
                }
                table td[class=textalignleft]{
                    text-align:left !important;
                    padding:0 !important;
                }
                table td[class=textaligcenter]{
                    text-align:center !important;
                }
                table td[class=heightsmalldevices]{
                    height:45px !important;
                }
                table td[class=heightSDBottom]{
                    height:28px !important;
                }
                table[class=adjustblock]{
                    width:87% !important;
                }
                table[class=resizeblock]{
                    width:92% !important;
                }
                table td[class=smallfont]{
                    font-size:8px !important;
                }
            }
            @media only screen and (max-width: 520px) {
                table td[class=heading]{
                    font-size:24px !important;
                }
                table td[class=heading01]{
                    font-size:18px !important;
                }
                table td[class=heading02]{
                    font-size:27px !important;
                }
                table td[class=text01]{
                    font-size:22px !important;
                }
                table[class="full mhide"], table tr[class=mhide]{
                    display:none !important;
                }
            }
            @media only screen and (max-width: 480px) {
                table {
                    border-collapse: collapse;
                }
                table[id=colaps-inhiret01],
                table[id=colaps-inhiret02],
                table[id=colaps-inhiret03],
                table[id=colaps-inhiret04],
                table[id=colaps-inhiret05],
                table[id=colaps-inhiret06],
                table[id=colaps-inhiret07],
                table[id=colaps-inhiret08],
                table[id=colaps-inhiret09]{
                    border-collapse:inherit !important;
                }
            }
            @media only screen and (max-width: 320px) {
            }

            .notification-item {
                width: 80%;
                font-family: sans-serif;
                text-decoration: none;
                display: block;
                color: black;
                padding: 20px;
                box-shadow: 0 4px 11px 0 rgba(221,221,221,.72);
                transition: 0.5s;
                background-color: white;
                margin: 20px;
                border-left: 5px solid #ff6600;
            }

            .notification-item:hover {
                box-shadow: 0 4px 11px 0 rgba(221,221,221,0);
            }

            p.order-number {
                margin: 20px 0;
                font-family: arial;
                font-size: 20px;
            }
        </style>
    </head>
<body style="background-color: #f8fafc;">
<!--[if mso]>
<style type="text/css">
    body, table, td, a, span {font-family: Arial, Helvetica, sans-serif !important;}
</style>
<![endif]-->
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" class="full mhide">
    <tr>
        <td>
            <table width="600" border="0" cellspacing="0" cellpadding="0" align="center" class="devicewidth">
                <tr>
                    <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" class="full">
                            <tr>
                                <td height="64">&nbsp;</td>
                            </tr>
                        </table>
                    </td></tr></table>
        </td>
    </tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" class="full">
    <tr>
        <td align="center"><table width="600" border="0" cellspacing="0" cellpadding="0" align="center" class="devicewidth">
                <tr>
                    <td><table width="100%" bgcolor="#ffffff" border="0" cellspacing="0" cellpadding="0" align="center" class="full" style="background-color:#ffffff; border-radius:5px 5px 0 0;">
                            <tr>
                                <td><table width="265" align="left" border="0" cellspacing="0" cellpadding="0" class="inner" style="text-align:center;margin-top: -10px;">
                                        <tr>
                                            <td width="28" height="52">&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="28">&nbsp;</td>
                                            <td align="center" valign="middle"><a href="https://locodor.com"><img src="https://locodor.com/uploads/logo-locodor-crowdfunding.png" width="205" height="54" alt="Locodor"></a></td>
                                        </tr>
                                        <tr>
                                        </tr>
                                    </table>
                                    <table width="255" align="right" border="0" cellspacing="0" cellpadding="0" class="inner" style="text-align:center;margin-top: -5px;">
                                        <tr>
                                            <td class="hide" height="22">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td height="10">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td align="center" valign="middle">
                                                <table width="191" align="center" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; text-align:center;">
                                                    <tr>
                                                        <td align="center" bgcolor="#ff6600" height="57" style="border-radius:28px;"><a href="https://locodor.com" style="font-family:'Montserrat', Helvetica, Arial, sans-serif; font-weight:700; font-size:16px; line-height:57px; color:#ffffff; text-decoration:none; text-transform:uppercase; display:block; overflow:hidden; ">GO TO LOCODOR</a></td>
                                                    </tr>
                                                </table>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="40">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table></td>
                </tr>
            </table></td>
    </tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" class="full">
    <tr>
        <td align="center">
            <table width="600" border="0" cellspacing="0" cellpadding="0" align="center" class="devicewidth">
                <tr>
                    <td>
                        <table width="100%"  border="0" cellspacing="0" cellpadding="0" align="center" class="full" style="background: url(images/background.gif) repeat-x left top #f8f8f8; text-align:center;  border-bottom:1px solid #dfdfdf;">
                            @if($backer->user_id == 0)
                                <tr>
                                    <td align="center"><img src="{{ asset('/uploads/avatars/1.png') }}" width="150" height="150" alt="pic" style="border-radius:150px;"></td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <table width="85%" align="center" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; text-align:center; border-bottom:1px solid #dfdfdf;">
                                            <tr>
                                                <td height="27">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="heading" style="font:700 37px 'Montserrat', Helvetica, Arial, sans-serif; color:#ff6600; text-transform:uppercase;">Hello, Thank you for your order!</td>
                                            </tr>
                                            <tr>
                                                <td height="28">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="heading01" style="font:37px  Arial, Helvetica, sans-serif; color:#3a3a3a;">It seems you like Crowdfunding! Register an account <a href="https://locodor.com/register">Here</a>.</td>
                                            </tr>
                                            <tr>
                                                <td height="41">&nbsp;</td>
                                            </tr>
                                        </table>

                                    </td>
                                </tr>
                            @else
                                <tr>
                                    <td align="center"><img src="{{ asset($backer->user->profile->avatar) }}" width="150" height="150" alt="pic" style="border-radius:150px;"></td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <table width="85%" align="center" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; text-align:center; border-bottom:1px solid #dfdfdf;">
                                            <tr>
                                                <td height="27">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="heading" style="font:700 37px 'Montserrat', Helvetica, Arial, sans-serif; color:#ff6600; text-transform:uppercase;">HELLO {{ $backer->user->name }}</td>
                                            </tr>
                                            <tr>
                                                <td height="28">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="heading01" style="font:37px  Arial, Helvetica, sans-serif; color:#3a3a3a;">Thank you for your order!</td>
                                            </tr>
                                            <tr>
                                                <td height="41">&nbsp;</td>
                                            </tr>
                                        </table>

                                    </td>
                                </tr>
                            @endif
                            <tr>
                                <td align="center">
                                    <p class="order-number">Your Order Number is: LOCO{{ $backer->id }}-{{ $orderID }}</p>
                                    <p style="font-family: Arial">Please write down the order number or save this email for reference.</p>
                                </td>
                            </tr>
                        </table>
                    </td></tr>
            </table>
        </td></tr></table>


<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" class="full">
    <tr>
        <td align="center">
            <table width="600" border="0" cellspacing="0" cellpadding="0" align="center" class="devicewidth">
                <tr>
                    <td>
                        <table width="100%" bgcolor="#da1b60" border="0" cellspacing="0" cellpadding="0" align="center" class="full" style="border-radius:0 0 5px 5px;">
                            <tr><td height="18">&nbsp;</td></tr>
                            <tr>
                                <td>
                                    <table width="340" border="0" cellspacing="0" cellpadding="0" align="right" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; text-align:center;" class="inner">
                                        <tr>
                                            <td width="21">&nbsp;</td>
                                            <td>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                                    <tr><td>
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" class="full">
                                                                <tr>
                                                                    <td align="center" style="font:11px Helvetica,  Arial, sans-serif; color:#383838;"><a style="color:#ffffff; text-decoration:none;" href="https://locodor.com/projects"> View all Projects</a> </td>
                                                                    <td style="color:#ffffff;"> | </td>
                                                                    <td align="center" style="font:11px Helvetica,  Arial, sans-serif; color:#383838;"><a style="color:#ffffff; text-decoration:none;" href="https://locodor.com/"> Go to Locodor</a> </td>
                                                                </tr>
                                                                <tr><td height="18">&nbsp;</td></tr>
                                                            </table>

                                                        </td></tr>
                                                </table>

                                            </td>
                                        </tr>
                                    </table>
                                    <table width="230" border="0" cellspacing="0" cellpadding="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; text-align:center;" class="inner">
                                        <tr>
                                            <td width="21">&nbsp;</td>
                                            <td>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                                    <tr><td align="center" style="font:11px Helvetica,  Arial, sans-serif; color:#ffffff;">&copy; 2019, All rights reserved
                                                        </td></tr>
                                                    <tr><td height="18">&nbsp;</td></tr>
                                                </table>

                                            </td>
                                            <td width="21">&nbsp;</td>
                                        </tr>
                                    </table>

                                </td></tr>
                        </table>
                    </td></tr>
                <tr class="mhide">
                    <td height="100">&nbsp;</td>
                </tr>
            </table>
        </td></tr></table>

</body>
</html>