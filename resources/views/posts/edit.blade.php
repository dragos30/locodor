@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="{{ route('post.update',['id' => $post->id]) }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="image">Post image</label>
                <input type="file" class="form-control-file" id="image" name="image">
            </div>
            <div class="form-group">
                <label for="title">Post Title</label>
                <input class="form-control" type="text" name="title" placeholder="Title of the post" value="{{ $post->title }}">
            </div>
            <div class="form-group">
                <label for="content">Content</label>
                <textarea class="form-control" name="content" placeholder="Content of the discusssion" rows="10" id="editor">{{ $post->content }}</textarea>
            </div>

            <button type="submit" class="btn btn-dark">Update post</button>
        </form>
    </div>
@endsection