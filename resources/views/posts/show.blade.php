@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="post-show-container">
            <div class="post-show-image">
                <img src="{{ asset($post->image) }}" width="70%">
            </div>
            <div class="project-share text-left">
                <a href="http://www.facebook.com/sharer.php?u={{ urlencode(url()->current()) }}" target="_blank">
                    <i style="background-color:#37569b" class="fab fa-facebook-f"></i>
                </a>
                <a href="http://pinterest.com/pin/create/button/?url={{url()->current()}}&media={{ urlencode(asset($post->image)) }}&description={{ urlencode($post->title) }}" target="_blank">
                    <i style="background-color:#e60023" class="fab fa-pinterest-p"></i>
                </a>
                <a href="https://twitter.com/share?url={{ urlencode(url()->current()) }}" target="_blank">
                    <i style="background-color:#00acf0" class="fab fa-twitter"></i>
                </a>
            </div>
            <h1 class="post-show-title">{{ $post->title }}</h1>

            <p class="post-show-content">
                {!! $post->content !!}
            </p>
        </div>
    </div>
    <div class="shareaholic-canvas" data-app="recommendations" data-app-id="28239346"></div>
@endsection