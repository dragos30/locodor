@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="row">
                    @foreach($posts as $post)
                        <div class="col-md-4">
                            <div class="basic-card">
                                <img src="{{ asset($post->image) }}">
                                <div class="basic-card-details">
                                    <h3 class="basic-card-title">
                                        {{ $post->title }}
                                    </h3>
                                    <p class="basic-card-content">
                                        {{ strip_tags($post->content) }}
                                    </p>
                                    <a href="{{ route('post.show',['id' => $post->id,'slug'=>$post->slug]) }}" class="basic-card-button">Read More</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col-md-2">
                <div class="basic-sidebar">
                    <p class="basic-sidebar-title">Useful Links</p>
                    <div class="fancy-hr"></div>
                    <p class="sidebar-link">
                        <a href="{{ asset('uploads/how-to-launch.pdf') }}" target="_blank">Starting your first Campaign</a>
                    </p>
                    <p class="sidebar-link">
                        <a href="{{ route('crowdfunding.resources') }}">Resources</a>
                    </p>
                    <p class="sidebar-link">
                        <a href="{{ route('campaign.tasks.example') }}" target="_blank">Campaign Tasks</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection