@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="{{ route('post.store') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="image">Post image</label>
                <input type="file" class="form-control-file" id="image" name="image">
            </div>
            <div class="form-group">
                <label for="title">Post Title</label>
                <input class="form-control" type="text" name="title" placeholder="Title of the post">
            </div>
            <div class="form-group">
                <label for="content">Content</label>
                <textarea class="form-control" name="content" placeholder="Content of the discusssion" rows="10" id="editor"></textarea>
            </div>

            <button type="submit" class="btn btn-dark">Create post</button>
        </form>
    </div>
@endsection