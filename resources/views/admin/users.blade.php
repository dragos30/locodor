@extends('layouts.admin')

@section('content')
    <h1>Users</h1>

    @foreach($users as $user)
        <span class="admin-user-item"><a href="{{ route('profile.show',['id'=>$user->profile->id,'slug' => $user->profile->slug]) }}"><img src="{{ asset($user->profile->avatar) }}" width="50px" height="50px">{{$user->name}}</a></span>
    @endforeach
@endsection