@extends('layouts.admin')

@section('content')
    <h1>Announcement</h1>

    <form action="{{ route('send.announcement') }}" method="post">
        @csrf
        <div class="form-group">
            <label for="editor">Message:</label>
            <textarea class="form-control" placeholder="Message" name="message" maxlength="190"></textarea>
        </div>
        <button type="submit" class="btn btn-dark">Send</button>
    </form>
@endsection