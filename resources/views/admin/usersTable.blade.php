@extends('layouts.admin')

@section('content')
    <h1>Users</h1>

    <table>
        <tr>
            <th>Message</th>
            <th>ID</th>
            <th>Name</th>
            <th>Email</th>
            <th>Activated</th>
            <th>IP</th>
            <th>City</th>
            <th>Region</th>
            <th>Country</th>
            <th>Created at</th>
        </tr>
        @foreach($users as $user)
            <tr style="border-bottom: 1px solid #ff6600;">
                <td><a href="{{ route('start.conversation',['id' => $user->id]) }}" class="btn btn-dark">Message</a></td>
                <td>{{ $user->id }}</td>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->activated }}</td>
                <td>{{ $user->user_ip }}</td>
                <td>{{ $user->city }}</td>
                <td>{{ $user->region }}</td>
                <td>{{ $user->country }}</td>
                <td>{{ $user->created_at->diffForHumans() }}</td>
            </tr>
        @endforeach
    </table>
@endsection