@extends('layouts.admin')

@section('content')
    <h1>Posts <a href="{{ route('post.create') }}" class="btn btn-dark">Add New</a></h1>

    <div class="container">
        <div class="row">
            @foreach($posts as $post)
                <div class="col-md-4">
                    <div class="card mb-4 text-white bg-dark">
                        <img class="card-img-top" src="{{ asset($post->image) }}" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">{{ $post->title }}</h5>
                            <p class="card-text">{{ substr(strip_tags($post->content),0,300) }}</p>
                            <a href="{{ route('post.show',[ 'id' => $post->id,'slug'=>$post->slug ]) }}" class="btn btn-outline-light btn-sm">View Post</a>
                            <a href="{{ route('post.edit',[ 'id' => $post->id ]) }}" class="btn btn-outline-light btn-sm">Edit</a>
                            <a href="{{ route('post.delete',[ 'id' => $post->id ]) }}" class="btn btn-outline-danger btn-sm">Delete</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection