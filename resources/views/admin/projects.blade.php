@extends('layouts.admin')

@section('content')
    <h1>Projects <a href="{{ route('admin.create') }}" class="btn btn-dark">Create project</a></h1>

    <div class="container">
        <div class="row">
            @foreach($projects as $project)
                <div class="col-md-4">
                    <div class="card mb-4 text-white bg-dark">
                        <img class="card-img-top" src="{{ asset($project->image) }}" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">{{ $project->title }}</h5>
                            <p class="card-text">{{ $project->subtitle }}</p>
                            <a href="{{ route('project.show',[ 'id' => $project->id , 'slug' => $project->slug]) }}" class="btn btn-outline-light btn-sm">View Project</a>
                            @if(!$project->exclusive)
                                <a href="{{ route('make.exclusive',[ 'id' => $project->id]) }}" class="btn btn-outline-light btn-sm">Make Exclusive</a>
                            @endif
                            <a href="{{ route('project.edit',[ 'id' => $project->id ]) }}" class="btn btn-outline-light btn-sm">Edit</a>
                            <a href="{{ route('project.delete',[ 'id' => $project->id ]) }}" class="btn btn-outline-danger btn-sm">Delete</a>
                            <a href="{{ route('change.project.author',[ 'id' => $project->id ]) }}" class="btn btn-outline-danger btn-sm">Change Author</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

@endsection