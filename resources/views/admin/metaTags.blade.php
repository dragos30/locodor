@extends('layouts.admin')

@section('content')
    <br>
    <h3>Meta Tags</h3>
    <form method="post" action="{{ route('save.meta') }}">
        @csrf
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="page">Select page:</label>
                    <select class="form-control" id="page" name="page">
                        <option value="homepage">Homepage</option>
                        <option value="projectsIndex">Projects</option>
                        <option value="socialCrowd">Social Crowd</option>
                        <option value="conversations">Conversations</option>
                        <option value="news">News</option>
                        <option value="profileShow">Profile</option>
                        <option value="members">Members</option>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="meta">Select tag:</label>
                    <select class="form-control" id="meta" name="meta">
                        <option value="title">Title</option>
                        <option value="description">Description</option>
                    </select>
                </div>
            </div>
        </div>


        <div class="form-group">
            <label for="value">Value</label>
            <input class="form-control" type="text" placeholder="Type in the value..." id="value" name="value">
        </div>

        <button type="submit" class="btn btn-dark">Save</button>
    </form>
    <hr>
    <div class="meta-tags-table-container">
        <div class="row">
            <div class="col-md-2">
                <b><p>Page</p></b>
            </div>
            <div class="col-md-2">
                <b><p>Tag</p></b>
            </div>
            <div class="col-md-8">
                <b><p>Value</p></b>
            </div>
        </div>
        <hr>
        @foreach($meta_tags as $tag)
            <div class="row">
                <div class="col-md-2">
                    <p>{{$tag->page}}</p>
                </div>
                <div class="col-md-2">
                    <p>{{$tag->meta}}</p>
                </div>
                <div class="col-md-8">
                    <p>{{$tag->value}}</p>
                </div>
            </div>
            <hr>
        @endforeach
    </div>
@endsection