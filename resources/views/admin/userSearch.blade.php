@extends('layouts.admin')

@section('content')
    <form action="{{ route('admin.dashboard.user.search') }}" method="get" class="search-form project-index-search">
        <div class="projects-search">
            <label for="project_query" class="sr-only">Search</label>
            <input type="text" class="form-control" name="user_query" id="project_query" placeholder="Search for users" value="{{ request('project_query') }}">
        </div>
    </form>
    <h1>Users:</h1>
    @foreach($users as $user)
        <span class="admin-user-item"><a href="{{ route('admin.dashboard.user',['id' => $user->id]) }}"><img src="{{ asset($user->profile->avatar) }}" width="50px" height="50px">{{$user->name}}</a></span>
    @endforeach

@endsection