@extends('layouts.admin')

@section('content')
    <h1>Export Emails</h1>
    <hr>
    <a href="{{ route('fetch.all.emails') }}" class="btn btn-dark">Refresh List</a>
    <a href="{{ route('download.exported.emails') }}" class="btn btn-dark">Export</a>
@endsection