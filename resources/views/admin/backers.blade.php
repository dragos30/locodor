@extends('layouts.admin')

@section('content')
    <div>
        <h4>Backers</h4>
        @foreach($backers as $backer)
            <p>
                <a href="{{ route('project.show',['id' => $backer->project->id , 'slug' => $backer->project->slug]) }}">{{ $backer->project->title }}</a>
                was backed by
                <a href="{{ route('profile.show',['id' => $backer->project->user->profile->id, 'slug' => $backer->project->user->profile->slug]) }}">{{ $backer->project->user->name }}</a>
                for level "{{$backer->level_including_trashed()->title}}"  with ${{$backer->amount}} on {{$backer->created_at}}
            </p>
        @endforeach
    </div>
@endsection