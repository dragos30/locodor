@extends('layouts.admin')

@section('content')
    <hr>
    <h3>Set Ranks</h3>
    <form action="{{ route('set.rank') }}" method="post">
        @csrf
        <div class="form-group">
            <label>User</label>
            <select class="form-control" name="user">
                @foreach($users as $user)
                    <option value="{{$user->id}}">{{$user->name}} - {{$user->id}}</option>
                @endforeach
            </select>
            <label>Rank</label>
            <select class="form-control" name="rank">
                @foreach($ranks as $rank)
                    <option value="{{$rank->id}}">{{$rank->name}} - {{$rank->id}}</option>
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-dark">Set</button>
    </form>
    <hr>
    <h3>Ranks</h3>
    @foreach($ranks as $rank)
        <p>
            <span>
                <span class="rank-icon" style="font-size:24px;" data-rank-icon='{{$rank->icon}}'>{!! $rank->icon !!}</span>
                <span class="rank-id" data-rank-id="{{ $rank->id }}">{{ $rank->id }}</span>
                 -
                <span class="rank-name" data-rank-name="{{ $rank->name }}">{{ $rank->name }}</span>
            </span>
            <a href="#" class="btn btn-dark edit-rank">Edit</a>
        </p>
    @endforeach
    <script>
        $('.edit-rank').on('click',function(){
            var btnClicked = $(this);
            var parent = btnClicked.parent();

            var rankIcon = btnClicked.prev().find('.rank-icon').data('rank-icon');
            var rankId = btnClicked.prev().find('.rank-id').data('rank-id');
            var rankName = btnClicked.prev().find('.rank-name').data('rank-name');


            btnClicked.siblings().remove();
            btnClicked.remove();

            var form = '<form action="{{ route('rank.update') }}" method="post">@csrf' +
                '<div class="form-group">' +
                    '<p><a href="https://origin.fontawesome.com/icons?d=gallery&m=free" target="_blank">FontAwesome Icons Website</a></p>'+
                    '<div class="input-group mb-3">' +
                        '<input type="hidden" value="'+rankId+'" name="rank_id">'+
                        '<input type="text" class="form-control" name="name" placeholder="Rank Name" value="'+ rankName +'" aria-describedby="basic-addon">' +
                        '<input type="text" class="form-control" name="icon" placeholder="Rank Icon from fontawesome" aria-describedby="basic-addon">' +
                        '<div class="input-group-append">' +
                            '<button type="submit" class="btn btn-dark input-group-text">Update</button>' +
                        '</div>' +
                    '</div>' +
                '</div>';

            parent.append(form);
        });
    </script>


    <h5>Add rank</h5>
    <form action="{{ route('rank.store') }}" method="post">
        @csrf
        <p><a href="https://origin.fontawesome.com/icons?d=gallery&m=free" target="_blank">Font Awesome Icons Website</a></p>
        <div class="form-group">
            <div class="input-group mb-3">
                <input type="text" class="form-control" name="name" placeholder="Rank Name">
            </div>
            <div class="input-group mb-3">
                <input type="text" class="form-control" name="icon" placeholder='Rank Icon (ex: <i class="fas fa-baby"></i>)'>
            </div>
            <button type="submit" class="btn btn-dark">Save</button>
        </div>

    </form>
@endsection