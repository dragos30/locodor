@extends('layouts.admin')

@section('content')
    <div class="help-request-container">
        @foreach($requests as $request)
            <div class="help-request">
                <span>{{ $request->user->name }}</span> |
                <span>{{ $request->email }}</span> |
                <span><b>{{ $request->message }}</b></span> |
                <span>{{ $request->created_at->diffForHumans() }}</span>
                <hr>
            </div>
        @endforeach
    </div>
@endsection