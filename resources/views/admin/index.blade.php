@extends('layouts.admin')

@section('content')
    <h1 style="display:inline-block">Dashboard</h1>
    <span>
        Logged in as: <b>{{ Auth::user()->name }}</b>, switch to
        <form style="display:inline-block" action="{{route('switch.account') }}" method="post">
            @csrf
            <select name="admin_id">
                @foreach($admins as $admin)
                    <option value="{{ $admin->id }}">{{ $admin->name }}</option>
                @endforeach
            </select>
            <button type="submit" class="btn btn-dark">Confirm</button>
        </form>
    </span>
    <div class="projects-waiting-approval-container">
        <h4>Projects awaiting approval</h4>
        @foreach($projects as $project)
            <p>
                <a href="{{ route('project.show',['id' => $project->id , 'slug' => $project->slug]) }}">{{ $project->title }}</a>
                created by
                <a href="{{ route('profile.show',['id' => $project->user->profile->id, 'slug' => $project->user->profile->slug]) }}">{{ $project->user->name }}</a>
                is waiting approval
                <a href="{{ route('project.approve',['id' => $project->id]) }}" class="btn btn-dark">Approve</a>
                <a href="{{ route('project.delete',['id' => $project->id]) }}" class="btn btn-dark">Destroy</a>
            </p>
        @endforeach
        <hr>
        <h4>Draft Projects</h4>
        @foreach($draft_projects as $project)
            <p>
                <a href="{{ route('project.show',['id' => $project->id , 'slug' => $project->slug]) }}">{{ $project->title }}</a>
                created by
                <a href="{{ route('profile.show',['id' => $project->user->profile->id, 'slug' => $project->user->profile->slug]) }}">{{ $project->user->name }}</a>
                was saved as draft
                <a href="{{ route('project.show',['id' => $project->id,'slug' => $project->slug]) }}" class="btn btn-dark">View</a>
                <a href="{{ route('project.edit',['id' => $project->id]) }}" class="btn btn-dark">Edit</a>
            </p>
        @endforeach
        <hr>
        <h4>Products awaiting approval</h4>
        @foreach($products as $product)
            <p>
                <a href="{{ route('product.show',['id' => $product->id]) }}">{{ $product->title }}</a>
                created by
                <a href="{{ route('profile.show',['id' => $product->user->profile->id, 'slug' => $product->user->profile->slug]) }}">{{ $product->user->name }}</a>
                is waiting approval
                <a href="{{ route('product.approve',['id' => $product->id]) }}" class="btn btn-dark">Approve</a>
                <a href="{{ route('product.delete',['id' => $product->id]) }}" class="btn btn-dark">Destroy</a>
            </p>
        @endforeach
        <hr>
        <h4>Help Requests</h4>
        <div class="help-request-container">
            @foreach($requests as $request)
                <div class="help-request">
                    <span>@if(isset($request->user->name)) {{ $request->user->name }} @else Guest @endif</span> |
                    <span>{{ $request->email }}</span> |
                    <span><b>{{ $request->message }}</b></span> |
                    <span>{{ $request->created_at->diffForHumans() }}</span>
                    <hr>
                </div>
            @endforeach
        </div>
    </div>
@endsection