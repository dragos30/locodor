@extends('layouts.admin')

@section('content')
    <h3>Project current values:</h3>
    <hr>
    <p>Views: {{ $project->views }}</p>
    <p>Backers: {{ count($project->backers) }}</p>
    <p>Amount Raised: ${{ $project->current_amount }}</p>
    <hr>
    <form action="{{ route('add.sync') }}" method="post">
        @csrf
        <input type="hidden" name="project_id" value="{{ $project->id }}">
        <div class="form-group">
            <label for="exampleInputEmail1">Views</label>
            <input type="text" class="form-control" value="{{ $sync->views }}" name="views">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Backers</label>
            <input type="text" class="form-control" value="{{ $sync->backers }}" name="backers">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Amount Raied</label>
            <input type="text" class="form-control" value="{{ $sync->amount }}" name="amount">
        </div>
        <button type="submit" class="btn btn-dark">Submit</button>
    </form>
@endsection