@extends('layouts.admin')

@section('content')
    <br>
    <h3>{{ $project->title }}</h3>
    <p>Current author: {{ $project->user->name }}</p>
    <form action="{{ route('author.change.save') }}" method="post">
        @csrf
        <input type="hidden" value="{{$project->id}}" name="project_id">
        <div class="form-group">
            <label for="sel1">Select user:</label>
            <select class="form-control" id="sel1" name="user_id">
                @foreach($users as $user)
                    <option value="{{ $user->id }}">{{$user->name }} ( id:{{ $user->id }} )</option>
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-dark">Save</button>
    </form>
@endsection