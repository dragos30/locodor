@extends('layouts.admin')

@section('content')
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=3nopjb5t6vxykdeuctzqtzx704x0ehyw5y091siwqd4mbyt3"></script>
    <script>
        var editor_config = {
            path_absolute : "/",
            selector: "textarea#editor",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
            relative_urls: false,
            file_browser_callback : function(field_name, url, type, win) {
                var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

                var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                if (type == 'image') {
                    cmsURL = cmsURL + "&type=Images";
                } else {
                    cmsURL = cmsURL + "&type=Files";
                }

                tinyMCE.activeEditor.windowManager.open({
                    file : cmsURL,
                    title : 'Filemanager',
                    width : x * 0.8,
                    height : y * 0.8,
                    resizable : "yes",
                    close_previous : "no"
                });
            }
        };

        tinymce.init(editor_config);
    </script>

    <h1>Email</h1>

    <form action="{{ route('send.mails') }}" method="post">
        @csrf
        <div class="form-group">
            <label for="to">CSV ( Emails and variables separated by comma ... each row is one record):</label>
            <textarea type="text" class="form-control" id="to" name="emails"></textarea>
        </div>
        <p>You can use the following variables: [[var1]] [[var2]] [[var3]] [[var4]] [[var5]] in the subject and body</p>
        <div class="form-group">
            <label for="subject">Subject:</label>
            <input type="text" class="form-control" id="subject" placeholder="Type the subject of the email" name="subject">
        </div>
        <div class="form-group">
            <label for="editor">Body:</label>
            <textarea class="form-control" id="editor" placeholder="Message" name="body"></textarea>
        </div>
        <button type="submit" class="btn btn-dark">Send</button>
    </form>
    <hr>
    <h3>Templates</h3>
    <div class="dropdown">
        <button class="btn btn-dark dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Insert Template
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            @foreach($templates as $template)
                <a class="dropdown-item template-item" data-template-id="{{ $template->id }}" data-content="{{ $template->content }}">{{ $template->name }}</a>
            @endforeach
        </div>
    </div>
    <small>Hover over template name and press "delete" key to erase a template</small>
    <br>
    <br>
    <form action="{{ route('save.email.template') }}" method="post">
        @csrf
        <input type="hidden" name="content" id="email-template-content">
        <div class="row">
            <div class="col">
                <input class="form-control" type="text" name="name" id="template-name" placeholder="Type here the Template Name and click Save to save the current template">
            </div>
            <div class="col">
                <button type="submit" class="btn btn-dark">Save Template</button>
            </div>

        </div>

    </form>
    <script>
        $('#template-name').on('change',function () {
            var emailContent = tinyMCE.activeEditor.getContent();
            $('#email-template-content').val(emailContent);
        });

        $('.template-item').on('click',function(){
            tinyMCE.activeEditor.setContent($(this).data('content'));
        });

        $(".template-item").on("mouseover",function()
        {
            var hoverBtn = $(this);
            $(document).bind("keyup",function(e) {
                var templateId = hoverBtn.data('template-id');
                hoverBtn.remove();
                if(e.keyCode == 46) {
                    $.ajax({
                        type: "get",
                        url: "/delete-email-template/" + templateId,
                        success: function(result) {
                            console.log('success');
                        },
                        error: function(result) {
                            console.log(result);
                            alert("Ops, something went wrong");
                        }
                    });

                }
            });

        }).on("mouseout",function()
        {
            $(document).unbind("keyup");
        });
    </script>
@endsection