@extends('layouts.admin')

@section('content')
    <h1>Projects <a href="{{ route('admin.create') }}" class="btn btn-dark">Create project</a></h1>
    <hr>
    <div class="container">
        <div class="row">
            <form action="{{ route('add.projects.slider') }}" method="post">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group">
                        <span class="input-group-btn">
                             <a id="lfm1" data-input="thumbnail1" data-preview="holder1" class="new-upload-btn">
                                Choose
                             </a>
                        </span>
                            <input id="thumbnail1" class="form-control" type="hidden" name="background1">
                        </div>
                        <img id="holder1" style="margin-top:15px;max-height:400px;">
                        <script>
                            $('#lfm1').filemanager('image');
                        </script>
                    </div>
                    <div class="col-md-6">
                        <div class="input-group">
                        <span class="input-group-btn">
                             <a id="lfm2" data-input="thumbnail2" data-preview="holder2" class="new-upload-btn">
                                Choose
                             </a>
                        </span>
                            <input id="thumbnail2" class="form-control" type="hidden" name="background2">
                        </div>
                        <img id="holder2" style="margin-top:15px;max-height:400px;">
                        <script>
                            $('#lfm2').filemanager('image');
                        </script>
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Project:</label>
                    <select class="form-control" name="project_id">
                        @foreach($projects as $project)
                            <option value="{{ $project->id }}">{{ $project->title }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <input class="form-control" type="text" placeholder="Order( higher has priority )" name="order">
                </div>
                <button type="submit" class="btn btn-dark">Submit</button>
            </form>
        </div>
        <hr>
        @foreach($slides as $slide)
            <p>{{ $slide->project->title }} (order:{{$slide->order}}) <a href="{{ route('delete.project.slide',['id' => $slide->id]) }}" class="btn btn-dark">Delete</a></p>

            <div class="row">
                <div class="col-md-6"><img src="{{$slide->background1}}" width="100%" height="400px" style="object-fit: cover"></div>
                <div class="col-md-6"><img src="{{$slide->background2}}" width="100%" height="400px" style="object-fit: cover"></div>
            </div>
            <hr>


        @endforeach
    </div>

@endsection