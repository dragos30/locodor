@extends('layouts.admin')

@section('content')
    <h1>User: {{ $user->name }} <a href="{{ route('profile.show',['id' => $user->profile->id,'slug' => $user->profile->slug]) }}" class="btn btn-dark">View profile</a></h1>
    <hr>
    <p>Email: {{$user->email}}</p>

    <h4>User details:</h4>
    <p class="profile-about">{{ $user->profile->about }}</p>
    <p><b class="profile-label">Phone:</b> <a href="#">{{$user->profile->phone}}</a></p>
    <p><b class="profile-label">Country:</b> {{$user->country}}</p>
    <p><b class="profile-label">City:</b> {{$user->city}}</p>
    <p><b>Gender:</b>@if($user->profile->gender == 1) Male @elseif($user->profile->gender == 2) Female @endif</p>
    <h4>Social media</h4>

    @if (strpos('www.facebook.com', $user->profile->facebook))
        <p><b class="profile-label">Facebook:</b> <a href="{{ $user->profile->facebook }}">{{ $user->profile->facebook }}</a></p>
    @else
        <p><b class="profile-label">Facebook:</b> <a href="https://www.facebook.com/{{ $user->profile->facebook }}">https://www.facebook.com/{{ $user->profile->facebook }}</a></p>
    @endif

    @if (strpos('www.linkedin.com', $user->profile->facebook))
        <p><b class="profile-label">Linkedin:</b> <a href="{{ $user->profile->linkedin }}">{{ $user->profile->linkedin }}</a></p>
    @else
        <p><b class="profile-label">Facebook:</b> <a href="https://www.facebook.com/{{ $user->profile->facebook }}">https://www.facebook.com/{{ $user->profile->facebook }}</a></p>
    @endif

    @if (strpos('www.twitter.com', $user->profile->facebook))
        <p><b class="profile-label">Twitter:</b> <a href="{{ $user->profile->twitter }}">{{ $user->profile->twitter }}</a></p>
    @else
        <p><b class="profile-label">Twitter:</b> <a href="https://www.twitter.com/{{ $user->profile->twitter }}">https://www.twitter.com/{{ $user->profile->twitter }}</a></p>
    @endif
    <div class="simple-line"></div>

    <h4>User Activity</h4>
    <h5>Activity user posts</h5>
    <hr>
    <div class="row">
        @foreach($user->user_posts as $post)
            <div class="col-md-3">
                <div class="card mb-4 text-white bg-dark">
                    @if($post->image)
                        <img class="card-img-top" src="{{ asset($post->image) }}" alt="Card image cap">
                    @endif
                    <div class="card-body">
                        <h5 class="card-title">{{ $post->content }}</h5>
                        <!-- todo: Create warning message before deleteing -->
                        <a href="{{ route('userpost.delete',[ 'id' => $post->id ]) }}" class="btn btn-outline-danger btn-sm">Delete</a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

    <div class="simple-line"></div>

    <h5>Activity comments</h5>
    <hr>
    <div class="row">
        @foreach($user->activityComments as $comment)
            <div class="col-md-3">
                <div class="card mb-4 text-white bg-dark">
                    <div class="card-body">
                        <h5 class="card-title">{{ $comment->comment }}</h5>
                        <a href="{{ route('activity.comment.delete',[ 'id' => $comment->id ]) }}" class="btn btn-outline-danger btn-sm">Delete</a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <div class="simple-line"></div>

    <h5>Project comments</h5>
    <hr>
    <div class="row">
        @foreach($user->comments as $comment)
            <div class="col-md-3">
                <div class="card mb-4 text-white bg-dark">
                    <div class="card-body">
                        <h5 class="card-title">{{ $comment->comment }}</h5>
                        <a href="{{ route('comment.delete',[ 'id' => $comment->id ]) }}" class="btn btn-outline-danger btn-sm">Delete</a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

    <div class="simple-line"></div>


@endsection