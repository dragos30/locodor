@extends('layouts.admin')

@section('content')
    <h1>Auto Pledge to Projects</h1>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <hr>
                <form action="{{ route('bots.auto.pledge') }}" method="post">
                    @csrf
                            <div class="form-group">
                                <label>How many Bots you want to run?</label>
                                <input class="form-control" type="number" placeholder="Number of bots..." name="bots_no">
                            </div>
                            <div class="form-group">
                                <label>How many pledges should each bot make?</label>
                                <input class="form-control" type="number" placeholder="Number of pledges" name="pledges_no">
                            </div>
                            <button type="submit" class="btn btn-dark">Run</button>
                </form>
            </div>
        </div>

    </div>

@endsection