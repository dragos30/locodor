@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="{{ route('comment.update',['id' => $comment->id]) }}" method="post">
            {{ csrf_field() }}

            <div class="form-group">
                <label for="name">Comment</label>
                <input class="form-control" type="text" name="comment" placeholder="Comment..." value="{{ $comment->comment }}">
            </div>

            <button type="submit" class="btn btn-dark">Update comment</button>
        </form>
    </div>
@endsection