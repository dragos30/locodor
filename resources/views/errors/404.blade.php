<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>Locodor - Crowdfunding Platform - Something went wrong</title>

    <!-- Google font -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,900" rel="stylesheet">

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">



</head>

<body>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="error-container">
                <div class="error-first-div">
                    <div class="error-second-div">
                        <h1>Oops!</h1>
                    </div>
                    <h2>Page not found!</h2>
                    <p>The page you are trying to reach does not exist or it was permanently moved.</p>
                    <a href="/" class="fancy-btn">Go To Homepage</a>
                    <a href="mailto:contact@locodor.com" class="fancy-btn">Contact Us</a>
                    <p class="error-contact-info">
                        Be-All Alon Towers, Tel Aviv, IL
                        <br>
                        Phone: <a href="tel:00972772491111">+972-77-2491111</a>
                        <br>
                        Email: <a href="mailto:contact@locodor.com">contact@locodor.com</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

</body><!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>
