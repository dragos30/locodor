<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Crete+Round" rel="stylesheet">

    <!-- Scripts -->
    <script src="{{ asset('js/custom-app.js') }}"></script>
    <script src="/vendor/laravel-filemanager/js/lfm.js"></script>

    <!-- TinyMCE -->
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=3nopjb5t6vxykdeuctzqtzx704x0ehyw5y091siwqd4mbyt3"></script>
    <script>
        var editor_config = {
            path_absolute : "/",
            selector: "textarea#editor",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
            templates: [
                {title: 'Basic', description: 'A lightweight basic template for your project. Enjoy!', url: '{{ asset('/templates/basic.html') }}'},
            ],
            relative_urls: false,
            file_browser_callback : function(field_name, url, type, win) {
                var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

                var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                if (type == 'image') {
                    cmsURL = cmsURL + "&type=Images";
                } else {
                    cmsURL = cmsURL + "&type=Files";
                }

                tinyMCE.activeEditor.windowManager.open({
                    file : cmsURL,
                    title : 'Filemanager',
                    width : x * 0.8,
                    height : y * 0.8,
                    resizable : "yes",
                    close_previous : "no"
                });
            }
        };

        tinymce.init(editor_config);
    </script>


    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
</head>
<body  class="create-project-body">
<div class="container">
    <div class="project-create-slider-container">
        <div class="project-create-controls">
            <div class="container">
                <div class="row justify-content-md-center">
                    <div class="col-md-6 text-center">
                        <div class="step-bar">
                            <a href="#" data-slide-index="0">
                                <span id="step-1" class="reached">1</span>
                            </a>
                            <a href="#" data-slide-index="1">
                                <span id="step-2">2</span>
                            </a>
                            <a href="#" data-slide-index="2">
                                <span id="step-3">3</span>
                            </a>
                            <a href="#" data-slide-index="3">
                                <span id="step-4">4</span>
                            </a>
                            <a href="#" data-slide-index="4">
                                <span id="step-5">5</span>
                            </a>
                            <div class="step-line"></div>
                        </div>
                    </div>
                </div>
            </div>
            <a id="form-slider-prev" role="button">
                <span><i class="fas fa-arrow-left"></i> Previous</span>
            </a>
            <a id="form-slider-next" role="button">
                <span>Next <i class="fas fa-arrow-right"></i></span>
            </a>
        </div>
        <form  action="{{ route('project.update',['id' => $project->id]) }}" method="post" enctype="multipart/form-data" id="got-idea">
            @csrf
            <div class="appended">
                <button type="submit" id="save-draft" name="save_draft" value="draft">Save as Draft</button>
            </div>
            <div id="carousel" class="carousel slide" data-ride="carousel">
                <div class="form-slider-container row">
                    {{-- Slide 1 --}}
                    <div class="active col-md-12" data-slide-no="1">
                        @if(Auth::user()->admin)
                            <div class="question">
                                <p>Type in the affilaite link</p>
                                <input type="text" placeholder="Type in here the external affiliate link" name="affiliate_link" value="@if(isset($project->affiliate_link)){{ $project->affiliate_link }}@endif">
                            </div>
                        @endif

                        {{--Question 1--}}
                        @if($errors->get('first_project'))
                            @foreach($errors->get('first_project') as $error)
                                <div class="notice notice-danger">
                                    <strong>{{ $error }}</strong>
                                </div>
                            @endforeach
                        @endif
                        <div class="question question-1">
                            <p>Is this your first crowdfunding campaign?</p>
                            <div class="row">
                                <div class="col-md-3">
                                    <input type="radio" value="1" id="first-yes" name="first_project" @if($project->first_project == 1) checked @endif>
                                    <label for="first-yes">
                                        <span>Y</span>
                                        <span>Yes</span>
                                    </label><br>

                                    <input type="radio" value="2" id="first-no" name="first_project" @if($project->first_project == 2) checked @endif>
                                    <label for="first-no">
                                        <span>N</span>
                                        <span>No</span>
                                    </label><br>
                                    <small class="required-field">This field is required</small>
                                </div>
                            </div>
                        </div>
                        {{--Question 2--}}
                            @if($errors->get('category_id'))
                                @foreach($errors->get('category_id') as $error)
                                    <div class="notice notice-danger">
                                        <strong>{{ $error }}</strong>
                                    </div>
                                @endforeach
                            @endif
                            <script>
                                $( document ).ready(function() {
                                    const categories = JSON.parse('{!! $categories !!}');
                                    const categoriesContainer = $('#categories-container');
                                    const filterCatInput = $('#filter-cat')

                                    filterCatInput.keyup(function() {
                                        const currentValue = $(this).val();
                                        const filteredCategories = categories.filter(function(item){
                                            const lowercaseName = item.name.toLowerCase();
                                            return lowercaseName.includes(currentValue.toLowerCase());
                                        });

                                        categoriesContainer.empty();

                                        filteredCategories.forEach(function(category){

                                            const elementToAppend = `<input type="radio" value="${category.id}" id="category-${category.id}" name="category_id">
                                            <label for="category-${category.id}">
                                            <span>*</span>
                                            <span>${category.name}</span>
                                            </label><br>`;
                                            categoriesContainer.append(elementToAppend);
                                        });
                                        categoriesContainer.append('<small class="required-field">This field is required</small>');
                                    });

                                    filterCatInput.keyup();
                                });
                            </script>
                            <div class="question question-2">

                                <p>What category does your campaign fall into? @</p>

                                <input type="text" id="filter-cat" placeholder="Search categories...">
                                <br>
                                <div class="row">
                                    <div class="col-md-6" id="categories-container">
                                        {{--@foreach($categories as $category)--}}
                                        {{--<input type="radio" value="{{ $category->id }}" id="category-{{$category->id}}" name="category_id" @if(old('category_id') == $category->id) checked @endif>--}}
                                        {{--<label for="category-{{ $category->id }}">--}}
                                        {{--<span>*</span>--}}
                                        {{--<span>{{ $category->name }}</span>--}}
                                        {{--</label><br>--}}
                                        {{--@endforeach--}}
                                    </div>
                                </div>
                            </div>
                        {{--Question 3--}}
                        @if($errors->get('previous_url'))
                            @foreach($errors->get('previous_url') as $error)
                                <div class="notice notice-danger">
                                    <strong>{{ $error }}</strong>
                                </div>
                            @endforeach
                        @endif
                        <div class="question question-3">
                            <p>Please write a few tags related to your project to get more visitors.</p>
                            <p>Write all the tags you can think of separated by a comma.</p>
                            <input type="text" placeholder="Example: cars, automotive, engineering" name="tags" value="@if(isset($project->tags)){{ $project->tags }}@endif">
                            <small class="required-field">This field is required</small>
                        </div>
                        {{--Question 4--}}
                        @if($errors->get('project_type'))
                            @foreach($errors->get('project_type') as $error)
                                <div class="notice notice-danger">
                                    <strong>{{ $error }}</strong>
                                </div>
                            @endforeach
                        @endif
                        <div class="question question-4">
                            <p>What type of crowdfunding campaign do you want to create?</p>
                            <p>Let's get a little more specific</p>

                            <div class="row">
                                <div class="col-md-5">
                                    <input type="radio" value="1" id="type-1" name="project_type" @if($project->project_type == 1) checked @endif>
                                    <label for="type-1">
                                        <span>A</span>
                                        <span>Pre-Orders</span>
                                    </label><br>

                                    <input type="radio" value="2" id="type-2" name="project_type" @if($project->project_type == 2) checked @endif>
                                    <label for="type-2">
                                        <span>B</span>
                                        <span>Reward Based</span>
                                    </label><br>

                                    <input type="radio" value="3" id="type-3" name="project_type" @if($project->project_type == 3) checked @endif>
                                    <label for="type-3">
                                        <span>C</span>
                                        <span>Equity Based</span>
                                    </label><br>

                                    <input type="radio" value="4" id="type-4" name="project_type" @if($project->project_type == 4) checked @endif>
                                    <label for="type-4">
                                        <span>D</span>
                                        <span>Donation Based</span>
                                    </label><br>
                                    <small class="required-field">This field is required</small>
                                </div>
                            </div>
                        </div>
                        {{-- Collaborators --}}
                        <div class="question question-collaborators">
                            <p>If you want to add team members to your project, type their email addresses below separted by a comma</p>
                            <input type="text" placeholder="Example: john.wick@locodor.com,tom@locodor.com,rick@locodor.com" name="collaborators" value="{{ old('collaborators') }}">
                        </div>
                    </div>
                    {{-- Slide 2 --}}
                    <div data-slide-no="2" class="col-md-12">
                        {{--Question 5--}}
                        @if($errors->get('previous_url'))
                            @foreach($errors->get('previous_url') as $error)
                                <div class="notice notice-danger">
                                    <strong>{{ $error }}</strong>
                                </div>
                            @endforeach
                        @endif
                        <div class="question">
                            <p>If you have one, please include your previous campaign URL:</p>
                            <input type="text" placeholder="https://" name="previous_url" value="@if(isset( $project->previous_url )){{ $project->previous_url }}@endif">
                        </div>
                        {{--Question 6--}}
                        @if($errors->get('upload_other'))
                            @foreach($errors->get('upload_other') as $error)
                                <div class="notice notice-danger">
                                    <strong>{{ $error }}</strong>
                                </div>
                            @endforeach
                        @endif
                        <div class="question">
                            <p>Do you plan on uploading your project to other platforms?</p>
                            <div class="row">
                                <div class="col-md-3">
                                    <input type="radio" value="1" id="other-yes" name="upload_other" @if($project->upload_other == 1) checked @endif>
                                    <label for="other-yes">
                                        <span>Y</span>
                                        <span>Yes</span>
                                    </label><br>
                                    <input type="radio" value="2" id="other-no" name="upload_other" @if($project->upload_other == 2) checked @endif>
                                    <label for="other-no">
                                        <span>N</span>
                                        <span>No</span>
                                    </label><br>
                                    <small class="required-field">This field is required</small>
                                </div>
                            </div>
                        </div>
                        {{--Question 7--}}
                        @if($errors->get('ready'))
                            @foreach($errors->get('ready') as $error)
                                <div class="notice notice-danger">
                                    <strong>{{ $error }}</strong>
                                </div>
                            @endforeach
                        @endif
                        <div class="question-ready">
                            <p>From 1 to 10: How ready is your project?</p>
                            <p>Are you fully funded, or are you just starting your crowdfunding journey?</p>
                            <div class="ready-bar">
                                <input type="radio" value="1" id="ready-1" name="ready" @if($project->ready == 1) checked @endif> <label for="ready-1">1</label>
                                <input type="radio" value="2" id="ready-2" name="ready" @if($project->ready == 2) checked @endif> <label for="ready-2">2</label>
                                <input type="radio" value="3" id="ready-3" name="ready" @if($project->ready == 3) checked @endif> <label for="ready-3">3</label>
                                <input type="radio" value="4" id="ready-4" name="ready" @if($project->ready == 4) checked @endif> <label for="ready-4">4</label>
                                <input type="radio" value="5" id="ready-5" name="ready" @if($project->ready == 5) checked @endif> <label for="ready-5">5</label>
                                <input type="radio" value="6" id="ready-6" name="ready" @if($project->ready == 6) checked @endif> <label for="ready-6">6</label>
                                <input type="radio" value="7" id="ready-7" name="ready" @if($project->ready == 7) checked @endif> <label for="ready-7">7</label>
                                <input type="radio" value="8" id="ready-8" name="ready" @if($project->ready == 8) checked @endif> <label for="ready-8">8</label>
                                <input type="radio" value="9" id="ready-9" name="ready" @if($project->ready == 9) checked @endif> <label for="ready-9">9</label>
                                <input type="radio" value="10" id="ready-10" name="ready" @if($project->ready == 10) checked @endif> <label for="ready-10">10</label>
                            </div>
                            <small class="required-field">This field is required</small>
                        </div>

                        {{--Question 8--}}
                        @if($errors->get('title'))
                            @foreach($errors->get('title') as $error)
                                <div class="notice notice-danger">
                                    <strong>{{ $error }}</strong>
                                </div>
                            @endforeach
                        @endif
                        <div class="question">
                            <p>Enter your project title...</p>
                            <input type="text" placeholder="Type your answer here..." name="title" value="@if(isset($project->title)){{ $project->title }}@endif">
                            <small class="required-field">This field is required</small>
                        </div>

                    </div>
                    {{-- Slide 3 --}}
                    <div data-slide-no="3" class="col-md-12">
                        {{--Question 9--}}
                        @if($errors->get('subtitle'))
                            @foreach($errors->get('subtitle') as $error)
                                <div class="notice notice-danger">
                                    <strong>{{ $error }}</strong>
                                </div>
                            @endforeach
                        @endif
                        <div class="question">
                            <p>... and a catchy subtitle.</p>
                            <input type="text" placeholder="Type your answer here..." name="subtitle" value="@if(isset($project->subtitle)){{ $project->subtitle }}@endif">
                            <small class="required-field">This field is required</small>
                        </div>

                        {{--Question 10--}}
                        @if($errors->get('project_goal'))
                            @foreach($errors->get('project_goal') as $error)
                                <div class="notice notice-danger">
                                    <strong>{{ $error }}</strong>
                                </div>
                            @endforeach
                        @endif
                        <div class="question">
                            <p>What is goal you want to reach (USD)?</p>
                            <input type="number" placeholder="Type your answer here..." name="project_goal" value="@if(isset($project->project_goal)){{ $project->project_goal }}@endif">
                            <small class="required-field">This field is required</small>
                        </div>

                        {{--Question 11--}}
                        @if($errors->get('image'))
                            @foreach($errors->get('image') as $error)
                                <div class="notice notice-danger">
                                    <strong>{{ $error }}</strong>
                                </div>
                            @endforeach
                        @endif
                        <div class="question">
                            <p>Now, we need a representative campaign image.</p>
                            <p>Please add a image below.</p>
                            @if(count($slides))
                                @foreach($slides as $key=>$slide)
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                             <a id="lfm{{$key + 1}}" data-input="slideInput{{$key+ 1}}" data-preview="holder{{$key+ 1}}" class="new-upload-btn">
                                                Choose
                                             </a>
                                             <a class="new-upload-btn remove-upload-btn">
                                                Remove
                                             </a>
                                        </span>
                                        <div class="image-holder-container">
                                            <img id="holder{{$key+ 1}}" src="{{ asset($slide) }}" style="margin-top:15px;max-width:100%;width:250px;">
                                        </div>
                                        <input id="slideInput{{$key+ 1}}" value="{{$slide}}" class="form-control" type="hidden" name="image[]">
                                        <small class="required-field">This field is required</small>
                                    </div>
                                @endforeach
                            @else
                                <div class="input-group">
                                <span class="input-group-btn">
                                     <a id="lfm1" data-input="slideInput1" data-preview="holder1" class="new-upload-btn">
                                        Choose
                                     </a>
                                     <a class="new-upload-btn remove-upload-btn">
                                        Remove
                                     </a>
                                </span>
                                    <div class="image-holder-container">
                                        <img id="holder1" style="margin-top:15px;max-width:100%;width:250px;">
                                    </div>
                                    <input id="slideInput1" class="form-control" type="hidden" name="image[]">
                                    <small class="required-field">This field is required</small>
                                </div>
                            @endif
                            <div>
                                <a class="new-upload-btn add-slide-btn">
                                    Add Slide
                                </a>
                            </div>

                            <script>
                                @if(count($slides))
                                    var slideUploadBtnIDNo = {{ count($slides) }};
                                    @foreach($slides as $key=>$slide)
                                        $('#lfm{{$key+1}}').filemanager('image');
                                    @endforeach
                                @else
                                    var slideUploadBtnIDNo = 1;
                                @endif



                                $('#lfm1').filemanager('image');
                                $('.remove-upload-btn').on('click',function(){
                                    $(this).parent().parent().remove();
                                });

                                $('.add-slide-btn').on('click',function(e){
                                    e.preventDefault();
                                    slideUploadBtnIDNo++;
                                    var slideUploadBtnID = 'lfm'+slideUploadBtnIDNo;
                                    var slideUploadBtn =
                                        '<div class="input-group">'+
                                        '<span class="input-group-btn">'+
                                        '<a id="'+slideUploadBtnID+'" data-input="slideInput'+slideUploadBtnIDNo+'" data-preview="holder'+slideUploadBtnIDNo+'" class="new-upload-btn">Choose</a>'+
                                        '<a class="new-upload-btn remove-upload-btn">Remove</a>'+
                                        '</span>'+
                                        '<div class="image-holder-container"><img id="holder'+slideUploadBtnIDNo+'" style="margin-top:15px;max-width:100%;width:250px;"></div>'+
                                        '<input id="slideInput'+slideUploadBtnIDNo+'" class="form-control" type="hidden" name="image[]">'+
                                        '<small class="required-field">This field is required</small>'+
                                        '</div>';

                                    $(this).parent().before(slideUploadBtn);

                                    $('#'+slideUploadBtnID).filemanager('image'+slideUploadBtnIDNo);

                                    $('.remove-upload-btn').on('click',function(){
                                        $(this).parent().parent().remove();
                                    })
                                });
                            </script>
                        </div>

                        <div class="question">
                            <p>If you have a Youtube Presentation Video, please paste the link below</p>
                            <input type="text" placeholder="Type your youtube URL here..." name="youtube_url" @if($project->youtube_url) value="{{ $project->youtube_url }}" @endif>
                        </div>

                        {{--Question 12--}}
                        @if($errors->get('start_date'))
                            @foreach($errors->get('start_date') as $error)
                                <div class="notice notice-danger">
                                    <strong>{{ $error }}</strong>
                                </div>
                            @endforeach
                        @endif
                        <div class="question">
                            <p>What date do you want to start?</p>
                            <input type="date" name="start_date" value="@if(isset($project->start_date)){{ date('Y-m-d',$project->start) }}@endif">
                            <small class="required-field">This field is required</small>
                        </div>

                    </div>
                    {{-- Slide 4 --}}
                    <div data-slide-no="4" class="col-md-12">
                        {{--Question 13--}}
                        @if($errors->get('end_date'))
                            @foreach($errors->get('end_date') as $error)
                                <div class="notice notice-danger">
                                    <strong>{{ $error }}</strong>
                                </div>
                            @endforeach
                        @endif
                        <div class="question">
                            <p>What is the date limit to reach the goal?</p>
                            <input type="date" name="end_date" value="@if(isset($project->end_date)){{ date('Y-m-d',$project->end_date) }}@endif">
                            <small class="required-field">This field is required</small>
                        </div>

                        {{--Question 14--}}
                        @if($errors->get('content'))
                            @foreach($errors->get('content') as $error)
                                <div class="notice notice-danger">
                                    <strong>{{ $error }}</strong>
                                </div>
                            @endforeach
                        @endif
                        <div class="question">
                            <p>Describe your project to us.</p>
                            <p>Be simple and clear.</p>
                            <textarea id="editor" height="500px" name="content">@if(isset($project->content)){{ $project->content }}@endif</textarea>
                            <small class="required-field">This field is required</small>
                        </div>

                        {{--Question 15--}}
                        @if($errors->get('p_website'))
                            @foreach($errors->get('p_website') as $error)
                                <div class="notice notice-danger">
                                    <strong>{{ $error }}</strong>
                                </div>
                            @endforeach
                        @endif
                        <div class="question">
                            <p>Please include your project's website, if you have one:</p>
                            <input type="text" placeholder="https://" name="p_website" value="@if(isset($project->website)){{$project->website}}@endif">
                        </div>

                        {{--Question 16--}}
                        <?php
                        $levels = Session::get('levels_flash');
                        if($levels != null){
                            $levels_number = count($levels);
                        } else {
                            $levels_number = 0;
                        }
                        ?>
                        <div>
                            <div class="question">
                                <p>How many backing levels do you have for your project?</p>
                                <input name="levels" type="number" oninput="doThing()" id="levels" placeholder="Type your answer here..." value="{{ count($project->levels) }}">
                                <small class="required-field">This field is required</small>
                            </div>
                        </div>
                    </div>
                    {{--Slide 5--}}
                    <div data-slide-no="5" class="levels-slide col-md-12">
                        {{-- Question 17+ --}}
                        <div class="all-levels">

                            @if(isset($project_levels))
                                @foreach($project_levels as $level)
                                    <div class="appended-levels">
                                        <div class="form-group question">
                                            <div class="form-group">
                                                <input type="text" placeholder="Title" name="level_title[]" class="form-control" value="{{ $level->title }}">
                                            </div>
                                            <div class="form-group">
                                                <input type="number" placeholder="Price in $" name="level_price[]" class="form-control" value="{{ $level->price }}">
                                            </div>
                                            <div class="form-group">
                                                <input type="number" placeholder="Shipping cost in $" name="shipping_cost[]" class="form-control" value="{{ $level->shipping_cost }}">
                                            </div>
                                            <div class="form-group">
                                                <textarea type="text" name="level_desc[]" placeholder="Type in the level description" class="form-control level-textarea">{{ $level->description }}</textarea>
                                            </div>
                                        </div>
                                        <div>
                                            <a class="remove-level">Remove</a>
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <div class=" empty-level">
                                    <div class="form-group question">
                                        <div class="form-group">
                                            <input type="text" placeholder="Title" name="level_title[]" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <input type="number" placeholder="Price in $" name="level_price[]" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <textarea type="text" name="level_desc[]" placeholder="Type in the level description" class="form-control level-textarea"></textarea>
                                        </div>
                                        <div>
                                            <a class="remove-level">Remove</a>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div>
                            <a class="add-level">Add</a>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    $(document).ready(function(){
        var level =
            '<div class="appended-levels">'+
            '<div class="form-group question">'+
            '<div class="form-group">'+
            '<input type="text" placeholder="Title" name="level_title[]" class="form-control">'+
            '</div>'+
            '<div class="form-group">'+
            '<input type="number" placeholder="Price in $" name="level_price[]" class="form-control">'+
            '</div>'+
            '<div class="form-group">'+
            '<input type="number" placeholder="Shipping cost in $" name="shipping_cost[]" class="form-control">'+
            '</div>'+
            '<div class="form-group">'+
            '<textarea type="text" name="level_desc[]" placeholder="Type in the level description" class="form-control level-textarea"></textarea>'+
            '</div>'+
            '</div>'+
            '<div>'+
            '<a class="remove-level">Remove</a>'+
            '</div>'+
            '</div>';

        $('.remove-level').on('click',function(){
            var btnClicked = $(this);
            btnClicked.parent().parent().remove();
        });

        $('.add-level').on('click',function(){
            $('.all-levels').append(level);

            $('.remove-level').on('click',function(){
                var btnClicked = $(this);
                btnClicked.parent().parent().remove();
            });
        });
    });
    function doThing(){
        $('.appended-levels').remove();
        var level =
            '<div class="appended-levels">'+
                '<div class="form-group question">'+
                    '<div class="form-group">'+
                        '<input type="text" placeholder="Title" name="level_title[]" class="form-control">'+
                    '</div>'+
                    '<div class="form-group">'+
                        '<input type="number" placeholder="Price in $" name="level_price[]" class="form-control">'+
                    '</div>'+
                    '<div class="form-group">'+
                        '<input type="number" placeholder="Shipping cost in $" name="shipping_cost[]" class="form-control">'+
                    '</div>'+
                    '<div class="form-group">'+
                        '<textarea type="text" name="level_desc[]" placeholder="Type in the level description" class="form-control level-textarea"></textarea>'+
                    '</div>'+
                '</div>'+
                '<div>'+
                    '<a class="remove-level">Remove</a>'+
                '</div>'+
            '</div>';
        var i;



        for(i = 0;i < $('#levels').val();i++){
            $('.all-levels').append(level);
        }

        $('.remove-level').on('click',function(){
            var btnClicked = $(this);
            btnClicked.parent().parent().remove();
        });
    }

    var sliderSlick;
    var currentSlide;

    $(document).ready(function(){

        sliderSlick = $('.form-slider-container').slick({
            infinite: false,
            draggable:false,
            slidesToShow: 1,
            swipe:false,
            slidesToScroll: 1,
            arrows:false
            // prevArrow:$('#form-slider-prev'),
            // nextArrow:$('#form-slider-next')
        });


        $('.step-bar a').on('click',function(e){
            e.preventDefault();
            var btnClicked = $(this);
            var slideIndex = btnClicked.data('slide-index');
            if(btnClicked.find('span').hasClass( "reached" )){
                sliderSlick.slick('slickGoTo',slideIndex);
                $('html, body').animate({
                    scrollTop: $("body").offset().top
                }, 500);
            }
        });

        var sliderContainer = $('.form-slider-container');

        $('#form-slider-next').on('click',function () {
            currentSlide = sliderSlick.slick('slickCurrentSlide') + 1;
            var slideAnswered = false;


            switch(currentSlide) {
                case 1:
                    var answeredQuestions = 0;
                    var firstProject = $('input[name="first_project"]');
                    var categoryId = $('input[name="category_id"]');
                    var tags = $('input[name="tags"]');
                    var projectType = $('input[name="project_type"]');

                    if($('input[name="first_project"]:checked').val() != undefined){
                        answeredQuestions++;
                        firstProject.parent().children().last().removeClass('required-text-shown');
                    } else {
                        firstProject.parent().children().last().addClass('required-text-shown');
                    }

                    if($('input[name="category_id"]:checked').val() != undefined){
                        answeredQuestions++;
                        categoryId.parent().children().last().removeClass('required-text-shown');
                        console.log('no cat');
                    }else{
                        categoryId.parent().children().last().addClass('required-text-shown');
                        console.log('cat selected');
                    }

                    if(tags.val() != ""){
                        answeredQuestions++;
                        tags.next().removeClass('required-text-shown');
                    }else{
                        tags.next().addClass('required-text-shown');
                    }

                    if($('input[name="project_type"]:checked').val() != undefined){
                        answeredQuestions++;
                        projectType.parent().children().last().removeClass('required-text-shown');
                    }else{
                        projectType.parent().children().last().addClass('required-text-shown');
                    }

                    if(answeredQuestions >= 4){


                        slideAnswered = true;
                        $('#step-2').addClass('reached');
                        $('.step-line').css('width','90px');

                        $('html, body').animate({
                            scrollTop: $("body").offset().top
                        }, 500);
                    }


                    break;
                case 2:
                    var answeredQuestions = 0;

                    // if($('input[name="previous_url"]').val() != ""){
                    //     answeredQuestions++;
                    // }

                    var uploadOther = $('input[name="upload_other"]');
                    var ready = $('input[name="ready"]');
                    var title = $('input[name="title"]');

                    if($('input[name="upload_other"]:checked').val() != undefined){
                        answeredQuestions++;
                        uploadOther.parent().children().last().removeClass('required-text-shown');
                    }else{
                        uploadOther.parent().children().last().addClass('required-text-shown');
                    }

                    if($('input[name="ready"]:checked').val() != undefined){
                        answeredQuestions++;
                        ready.parent().parent().children().last().removeClass('required-text-shown');
                    }else{
                        ready.parent().parent().children().last().addClass('required-text-shown');
                    }

                    if($('input[name="title"]').val() != ""){
                        answeredQuestions++;
                        title.next().removeClass('required-text-shown');
                    }else{
                        title.next().addClass('required-text-shown');
                    }

                    if(answeredQuestions >= 3){
                        slideAnswered = true;
                        $('#step-3').addClass('reached');
                        $('.step-line').css('width','180px');

                        $('html, body').animate({
                            scrollTop: $("body").offset().top
                        }, 500);
                    }

                    break;

                case 3:
                    var answeredQuestions = 0;

                    var subtitle = $('input[name="subtitle"]');
                    var projectGoal = $('input[name="project_goal"]');
                    var image =$('input[name="image"]');
                    var startDate =$('input[name="start_date"]');

                    if($('input[name="subtitle"]').val() != ""){
                        answeredQuestions++;
                        subtitle.next().removeClass('required-text-shown');
                    }else{
                        subtitle.next().addClass('required-text-shown');
                    }
                    if($('input[name="project_goal"]').val() != ""){
                        answeredQuestions++;
                        projectGoal.next().removeClass('required-text-shown');
                    }else{
                        projectGoal.next().addClass('required-text-shown');
                    }

                    if($('input[name="image"]').val() != ""){
                        answeredQuestions++;
                        image.next().removeClass('required-text-shown');
                    }else{
                        image.next().addClass('required-text-shown');
                    }

                    if($('input[name="start_date"]').val() != ""){
                        answeredQuestions++;
                        startDate.next().removeClass('required-text-shown');
                    }else{
                        startDate.next().addClass('required-text-shown');
                    }

                    if(answeredQuestions >= 4){
                        slideAnswered = true;
                        $('#step-4').addClass('reached');
                        $('.step-line').css('width','270px');

                        $('html, body').animate({
                            scrollTop: $("body").offset().top
                        }, 500);
                    }

                    break;

                case 4:
                    var answeredQuestions = 0;

                    var endDate = $('input[name="end_date"]');
                    var editor = $('#editor');
                    var levels = $('input[name="levels"]');

                    if(endDate.val() != ""){
                        answeredQuestions++;
                        endDate.next().removeClass('required-text-shown');
                    }else{
                        endDate.next().addClass('required-text-shown');
                    }

                    if(tinyMCE.get('editor').getContent() != ""){
                        answeredQuestions++;
                        editor.next().removeClass('required-text-shown');
                    }else{
                        editor.next().addClass('required-text-shown');
                    }

                    // if($('input[name="p_website"]').val() != ""){
                    //     answeredQuestions++;
                    // }

                    if(levels.val() != "0" && $('#levels').val() != ""){
                        answeredQuestions++;
                        levels.next().removeClass('required-text-shown');
                    }else{
                        levels.next().addClass('required-text-shown');
                    }

                    if(answeredQuestions >= 3){
                        slideAnswered = true;
                        $('#step-5').addClass('reached');
                        $('.step-line').css('width','360px');

                        var button = '<button type="submit" id="submit" name="submit" value="submit">Submit</button>';
                        if($('#submit').length == 0){
                            $('.appended').append(button);
                        }


                        $('html, body').animate({
                            scrollTop: $("body").offset().top
                        }, 500);
                    }

                    break;

                case 5:
                    break;
                default:
                // code block
            }

            if(slideAnswered == true){
                sliderSlick.slick('slickNext');
                $('html, body').animate({
                    scrollTop: $("body").offset().top
                }, 500);
            } else {
                event.preventDefault();
            }
        });

        $('#form-slider-prev').on('click',function(){
            sliderSlick.slick('slickPrev');
            $('html, body').animate({
                scrollTop: $("body").offset().top
            }, 500);
        });
    });



    function isValidEmailAddress(emailAddress) {
        var pattern = new RegExp(/^(("[\w-+\s]+")|([\w-+]+(?:\.[\w-+]+)*)|("[\w-+\s]+")([\w-+]+(?:\.[\w-+]+)*))(@((?:[\w-+]+\.)*\w[\w-+]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][\d]\.|1[\d]{2}\.|[\d]{1,2}\.))((25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\.){2}(25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\]?$)/i);
        return pattern.test(emailAddress);
    };
</script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
</body>

</html>