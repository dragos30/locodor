@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @if(count($projects_approved))
                <div class="col-md-12">
                    <h4>Projects Approved</h4>
                    <div class="fancy-hr"></div>
                </div>
                @foreach($projects_approved as $project)
                    <div class="col-md-4">
                        <div class="card mb-4 text-white bg-dark">
                            <img class="card-img-top" src="{{ asset($project->image) }}" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title">{{ $project->title }}</h5>
                                <p class="card-text">{{ $project->subtitle }}</p>
                                @if($project->status == 'funded')
                                    <a href="{{ route('product.create') }}" class="btn btn-outline-light btn-sm">Create Product</a>
                                @else
                                    <a href="{{ route('product.create') }}" class="btn btn-outline-light btn-sm disabled">Create Product</a>
                                @endif
                                <a href="{{ route('project.show',[ 'id' => $project->id, $project->slug ]) }}" class="btn btn-outline-light btn-sm">View Project</a>
                                @if($project->exclusive)
                                    <a href="{{ route('campaign.tasks',['id' => $project->id]) }}" class="btn btn-outline-light btn-sm">Tasks</a>
                                @endif
                                <a href="{{ route('project.edit',[ 'id' => $project->id ]) }}" class="btn btn-outline-light btn-sm">Edit</a>
                                <!-- todo: Create warning message before deleteing -->
                                <a href="{{ route('project.delete',[ 'id' => $project->id ]) }}" class="btn btn-outline-danger btn-sm">Delete</a>

                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
            @if(count($projects_pending))
                <div class="col-md-12">
                    <h4>Projects pending approval</h4>
                </div>
                @foreach($projects_pending as $project)
                    <div class="col-md-4">
                        <div class="card mb-4 text-white bg-dark">
                            <img class="card-img-top" src="{{ asset($project->image) }}" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title">{{ $project->title }}</h5>
                                <p class="card-text">{{ $project->subtitle }}</p>
                                <a href="{{ route('project.show',[ 'id' => $project->id, $project->slug ]) }}" class="btn btn-outline-light btn-sm">View Project</a>
                                @if($project->exclusive)
                                    <a href="{{ route('campaign.tasks',['id' => $project->id]) }}" class="btn btn-outline-light btn-sm">Tasks</a>
                                @endif
                                <a href="{{ route('project.edit',[ 'id' => $project->id ]) }}" class="btn btn-outline-light btn-sm">Edit</a>
                                <!-- todo: Create warning message before deleteing -->
                                <a href="{{ route('project.delete',[ 'id' => $project->id ]) }}" class="btn btn-outline-danger btn-sm">Delete</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
            @if(count($projects_drafts))
                <div class="col-md-12">
                    <h4>Draft projects</h4>
                </div>

                @foreach($projects_drafts as $project)
                    <div class="col-md-4">
                        <div class="card mb-4 text-white bg-dark">
                            <img class="card-img-top" src="{{ asset($project->image) }}" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title">{{ $project->title }}</h5>
                                <p class="card-text">{{ $project->subtitle }}</p>
                                <a href="{{ route('project.show',[ 'id' => $project->id, $project->slug ]) }}" class="btn btn-outline-light btn-sm">View Project</a>
                                <a href="{{ route('project.edit',[ 'id' => $project->id ]) }}" class="btn btn-outline-light btn-sm">Continue</a>
                                <!-- todo: Create warning message before deleteing -->
                                <a href="{{ route('project.delete',[ 'id' => $project->id ]) }}" class="btn btn-outline-danger btn-sm">Delete</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
            @if($projects_collaborating->count())
                <div class="col-md-12">
                    <h4>Projects Collaborating</h4>
                </div>

                @foreach($projects_collaborating as $project)
                    <div class="col-md-4">
                        <div class="card mb-4 text-white bg-dark">
                            <img class="card-img-top" src="{{ asset($project->image) }}" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title">{{ $project->title }}</h5>
                                <p class="card-text">{{ $project->subtitle }}</p>
                                <a href="{{ route('project.show',[ 'id' => $project->id, $project->slug ]) }}" class="btn btn-outline-light btn-sm">View Project</a>
                                <a href="{{ route('project.edit',[ 'id' => $project->id ]) }}" class="btn btn-outline-light btn-sm">Edit</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
@endsection