@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3><b>Products</b> available for purchase from the project: <b>{{ $project->title }}</b></h3>
                @foreach($products as $product)
                    <div class="grid-item">
                        <div class="product">
                            <img src="{{ asset($product->image) }}" width="100%">
                            <div class="product-description">
                                <h3>{{ $product->title }}</h3>
                                <?php
                                if (strlen(strip_tags($product->description)) > 150){
                                    $str = substr(strip_tags($product->description), 0, 147) . '...';
                                } else {
                                    $str = strip_tags($product->description);
                                }

                                ?>
                                <p>{!! $str !!}</p>
                            </div>
                            <p class="product-price text-center">${{ $product->price }}</p>
                            <div class="text-center">
                                @if($product->link != null)
                                    <a class="normal-btn" href="{{ $product->link }}">View Product</a>
                                @else
                                    <a class="normal-btn" href="{{ route('product.show',['id'=>$product->id]) }}">View Product</a>
                                @endif
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <script>
        $( document ).ready(function() {
            var $grid = $('.grid').imagesLoaded( function() {
                // init Masonry after all images have loaded
                $grid.masonry({
                    itemSelector: '.grid-item',
                });
                $('#loader').fadeOut( "slow" );
                setTimeout(function(){
                    $('#loader').remove();
                }, 1000);
            });
        });
    </script>
@endsection