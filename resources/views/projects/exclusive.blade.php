@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @if(count($projects))
                <div class="col-md-12">
                    <h4>Exclusive projects</h4>
                    <div class="fancy-hr"></div>
                </div>
                @foreach($projects as $project)
                    <div class="col-md-4">
                        <div class="card mb-4">
                            <img class="card-img-top" src="{{ asset($project->image) }}" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title">{{ $project->title }}</h5>
                                <div class="tasks-progress-container">
                                    <small class="tasks-progress-text">Progress: {{$project->completed_tasks()->count()}} of 34 Tasks Completed</small>
                                    <small class="tasks-progress-percentage">@if( $project->tasks->count() != 0) {{ floor($project->completed_tasks()->count() / $project->tasks->count() * 100) }}% @else 0% @endif</small>
                                    <div class="tasks-progress-outter">
                                        <div class="tasks-progress-inner" style="width:@if( $project->tasks->count() != 0) {{ floor($project->completed_tasks()->count() / $project->tasks->count() * 100) }}% @else 0% @endif">
                                        </div>
                                    </div>
                                </div>
                                <a href="{{ route('project.show',[ 'id' => $project->id , 'slug' => $project->slug]) }}" class="btn btn-outline-dark btn-sm">View Project</a>
                                @if($project->exclusive)
                                    <a href="{{ route('campaign.tasks',['id' => $project->id]) }}" class="btn btn-outline-dark btn-sm">Tasks</a>
                                @endif
                                <a href="{{ route('project.edit',[ 'id' => $project->id ]) }}" class="btn btn-outline-dark btn-sm">Edit</a>
                                <a href="{{ route('project.delete',[ 'id' => $project->id ]) }}" class="btn btn-outline-danger btn-sm">Delete</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
@endsection