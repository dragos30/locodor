@extends('layouts.app')

@section('content')
{{--{{name}}--}}
{{--{{category}}--}}
{{--{{description}}--}}
{{--{{goal}}--}}
{{--{{currency}}--}}
{{--{{full_description}}--}}
{{--{{numOfLevels}}--}}
{{--{{levels}}--}}


{{--{{ Session::get('category') }}--}}


{{--{{ Session::get('currency') }}--}}

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="container">
        <form action="{{ route('project.store') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
           <div class="row">
               <div class="col-md-4">
                   <div class="form-group">
                       <label for="title">Project Title</label>
                       <input class="form-control" type="text" placeholder="Title" name="title" value="{{ Session::get('name') }}">
                   </div>
               </div>
               <div class="col-md-4">
                   <div class="form-group">
                       <label for="subtitle">Catchy Subtitle</label>
                       <input class="form-control" type="text" placeholder="Subtitle" name="subtitle" value="{{ Session::get('description') }}">
                   </div>
               </div>
               <div class="col-md-4">
                   <div class="form-group">
                       <label for="p_website">Project goal</label>
                       <input class="form-control" type="number" placeholder="$" name="project_goal" value="{{ Session::get('goal') }}">
                   </div>
               </div>
           </div>

            <div class="row">
                <div class="col-md-4">
                    <label for="image">Featured Image</label>
                    <div class="input-group">
                        <span class="input-group-btn">
                             <div class="fileinputs">
                                <input type="hidden" class="file" name="slide[]" value="{{ Session::get('slide') }}">
                                <input type="file" class="file" name="slide[]" value="{{ Session::get('slide') }}">
                                <div class="fakefile">
                                    <a class="new-upload-btn choose-image-btn">Choose</a>
                                </div>
                                 <p class="uploaded-file-name">{{ Session::get('slide') }}</p>
                            </div>
                        </span>
                        <small class="required-field">This field is required</small>
                    </div>
                </div>
                <script>
                    var slideUploadBtnIDNo = 1;
                    $('.remove-upload-btn').on('click',function(){
                        $(this).parent().parent().parent().remove();
                    });

                    $('.file').on('change',function (e) {
                        $(this).next().next().text(e.target.files[0].name);
                    });

                    $('.choose-image-btn').on('click',function () {
                        $(this).parent().prev().trigger('click');
                    });
                </script>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="start_date">Start date</label>
                        <input class="form-control" type="date" placeholder="Start date" name="start_date" value="{{ date("Y-m-d") }}">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="end_date">End date</label>
                        <input class="form-control" type="date" placeholder="End Date" name="end_date" value="<?php echo date('Y-m-d', Session::get('deadline')); ?>">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="aff_link">Affiliate Link</label>
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" placeholder="Affliate Link" name="affiliate_link" id="aff_link" value="{{ Session::get('url') }}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="p_type">First campaign?</label>
                        <select class="form-control" id="p_type" name="project_type">
                            <option value="1">Yes</option>
                            <option value="2">No</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="p_type">What type of Campaign do you want to create?</label>
                        <select class="form-control" id="p_type" name="p_type">
                            <option value="1">Pre-Orders</option>
                            <option value="2">Reward Based</option>
                            <option value="3">Equity Based</option>
                            <option value="4">Donation Based</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="p_website">Project website</label>
                        <input class="form-control" type="text" placeholder="Exemple: www.exemple.com" name="p_website" value="">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <p>What category does your campaign fall into?</p>
                        @foreach($categories as $key => $category)
                            <div class="form-check">
                                <input type="radio" name="category_id" value="{{ $category->id }}" style="visibility: initial" {{ $key === 0 ? "checked" : "" }}>
                                <label for="category{{ $category->id }}"><div><span>{!! $category->icon !!}</span><span>{{ $category->name }}</span></div></label>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="p_type">Where are you going to deliver your product?</label>
                        <select class="form-control" id="p_type" name="shipping_location">
                            <option value="ww">Worldwide</option>
                            <option value="na">North America</option>
                            <option value="sa">South America</option>
                            <option value="eu">Europe</option>
                            <option value="as">Asia</option>
                            <option value="af">Africa</option>
                            <option value="oc">Oceania</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="p_type">How ready is your project?</label>
                        <select class="form-control" id="p_type" name="ready">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="7">8</option>
                            <option value="7">9</option>
                            <option value="7">10</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="p_type">First project?</label>
                        <select class="form-control" id="p_type" name="first_project">
                            <option value="1">Yes</option>
                            <option value="2">No</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="start_date">Previous url</label>
                        <input class="form-control" type="text" placeholder="Previous URL" name="previous_url" value="">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="p_type">Upload other?</label>
                        <select class="form-control" id="p_type" name="upload_other">
                            <option value="1">Yes</option>
                            <option value="2">No</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="Content">Project Content</label>
                <textarea class="form-control" name="content" id="content" placeholder="Tell us about your project." rows="8">{{ Session::get('full_description') }}</textarea>
            </div>

            <?php
                $levels = Session::get('levels_flash');
                if($levels == null){
                    $levels_count = 0;
                } else {
                    $levels_count = count($levels);
                }
            ?>

            <div class="form-group">
                <label for="levels">Project levels number</label>
                <input oninput="doThing()" id="levels" name="levels" class="form-control" type="number" placeholder="Type in the number of levels for your project" value="{{ $levels_count }}">
            </div>
            <hr>

            <div id="levels-container" class="form-group">
                @if($levels != null)
                    @foreach($levels as $level)
                        <div class="form-group">
                            <div class="form-group">
                                <input type="text" placeholder="Title" name="level_title[]" value="{{$level['title']}}" class="form-control">
                            </div>
                            <div class="form-group">
                                <input type="number" placeholder="Price in $" name="level_price[]" class="form-control" value="{{$level['price']}}">
                            </div><div class="form-group">
                                <textarea type="text" name="level_desc[]" placeholder="Type in the level description" class="form-control">{{$level['description']}}</textarea>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>

            <script>
                /* function */
                function doThing(){
                    //clear container on input change
                    var container = document.getElementById("levels-container");
                    while (container.firstChild) {
                        container.removeChild(container.firstChild);
                    }

                    // populate levels based on the input value
                    var levelValue = document.getElementById("levels").value;

                    for(var i = 1;i <= levelValue;i++){

                        //create parent div
                        var levelContainer = document.createElement("div");
                        levelContainer.setAttribute('class','form-group');



                        //create title input

                        //create container for level title
                        var levelTitleContainer = document.createElement("div");
                        levelTitleContainer.classList.add("form-group");

                        //crete input for title
                        var levelTitle = document.createElement("input");
                        levelTitle.setAttribute('type', 'text');
                        levelTitle.setAttribute('placeholder', 'Title');
                        levelTitle.setAttribute('name', 'level_title[]');
                        levelTitle.classList.add("form-control");

                        // append title input to title contaienr
                        levelTitleContainer.appendChild(levelTitle);


                        // Create price input

                        //create container for level price
                        var levelPriceContainer = document.createElement("div");
                        levelPriceContainer.classList.add("form-group");

                        //create price input
                        var levelPrice = document.createElement("input");
                        levelPrice.setAttribute('type', 'number');
                        levelPrice.setAttribute('placeholder', 'Price in $');
                        levelPrice.setAttribute('name', 'level_price[]');
                        levelPrice.classList.add("form-control");

                        // append price input to price container
                        levelPriceContainer.appendChild(levelPrice);


                        // Create Description input

                        //create container for level description
                        var levelDescContainer = document.createElement("div");
                        levelDescContainer.classList.add("form-group");

                        //create description input
                        var levelDesc = document.createElement("textarea");
                        levelDesc.setAttribute('type', 'text');
                        levelDesc.setAttribute('name', 'level_desc[]');
                        levelDesc.setAttribute('placeholder', 'Type in the level description');
                        //var levelDescValue = document.createTextNode("Type in the level description");
                        //levelDesc.appendChild(levelDescValue);
                        levelDesc.classList.add("form-control");

                        // append description input to description container
                        levelDescContainer.appendChild(levelDesc);


                        //Create level input
                        var levelLabel = document.createElement("H4");
                        var text = document.createTextNode("Level " + i);
                        levelLabel.appendChild(text);

                        //append elements to individual level container

                        levelContainer.appendChild(levelLabel);
                        levelContainer.appendChild(levelTitleContainer);
                        levelContainer.appendChild(levelPriceContainer);
                        levelContainer.appendChild(levelDescContainer);

                        //append levels to general container
                        document.getElementById('levels-container').appendChild(levelContainer);

                    }
                }

            </script>
            <button type="submit" class="btn btn-dark" name="submit" value="1">Create project</button>
        </form>
    </div>
@endsection
