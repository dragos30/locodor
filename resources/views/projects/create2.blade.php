<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Crete+Round" rel="stylesheet">

    <!-- Scripts -->
    <script src="{{ asset('js/custom-app.js') }}"></script>
    <script src="/vendor/laravel-filemanager/js/lfm.js"></script>

    <!-- TinyMCE -->
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=3nopjb5t6vxykdeuctzqtzx704x0ehyw5y091siwqd4mbyt3"></script>
    <script>
        var editor_config = {
            path_absolute : "/",
            selector: "textarea#editor",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
            relative_urls: false,
            file_browser_callback : function(field_name, url, type, win) {
                var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

                var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                if (type == 'image') {
                    cmsURL = cmsURL + "&type=Images";
                } else {
                    cmsURL = cmsURL + "&type=Files";
                }

                tinyMCE.activeEditor.windowManager.open({
                    file : cmsURL,
                    title : 'Filemanager',
                    width : x * 0.8,
                    height : y * 0.8,
                    resizable : "yes",
                    close_previous : "no"
                });
            }
        };

        tinymce.init(editor_config);
    </script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
</head>
<body class="create-project-body">
    <div class="container">
        {{--@if ($errors->any())--}}
            {{--<div class="alert alert-danger">--}}
                {{--<ul>--}}
                    {{--@foreach ($errors->all() as $error)--}}
                        {{--<li>{{ $error }}</li>--}}
                    {{--@endforeach--}}
                {{--</ul>--}}
            {{--</div>--}}
        {{--@endif--}}
        <form action="{{ route('project.store') }}" method="post" enctype="multipart/form-data" id="create-project-form">
            @csrf
            <div class="row">
                {{--<div class="col-md-12 question-row">--}}
                    {{--<div class="question">--}}
                        {{--<p>Please Provide us with your First and Last Name *</p>--}}
                        {{--<input type="text" placeholder="Type your answer here...">--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-md-12 question-row">--}}
                    {{--<div class="question">--}}
                        {{--<p>What is the best way to reach you? *</p>--}}
                        {{--<input type="text" placeholder="Type your email here...">--}}
                    {{--</div>--}}
                {{--</div>--}}
                <div class="col-md-12 question-row">
                    @if($errors->get('first_project'))
                        @foreach($errors->get('first_project') as $error)
                            <div class="notice notice-danger">
                                <strong>{{ $error }}</strong>
                            </div>
                        @endforeach
                    @endif
                    <div class="question">
                        <p>Is this your first crowdfunding campaign?</p>
                        <div class="row">
                            <div class="col-md-3">

                                <input type="radio" value="1" id="first-yes" name="first_project" @if(old('first_project') == 'yes') checked @endif>
                                <label for="first-yes">
                                    <span>Y</span>
                                    <span>Yes</span>
                                </label><br>

                                <input type="radio" value="2" id="first-no" name="first_project" @if(Session::get('name')) checked @elseif(old('first_project') == 'no') checked @endif>
                                <label for="first-no">
                                    <span>N</span>
                                    <span>No</span>
                                </label><br>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 question-row">
                    @if($errors->get('category_id'))
                        @foreach($errors->get('category_id') as $error)
                            <div class="notice notice-danger">
                                <strong>{{ $error }}</strong>
                            </div>
                        @endforeach
                    @endif
                    <div class="question">
                        <p>What category does your campaign fall into? @</p>

                        <div class="row">
                            <div class="col-md-6">
                                @foreach($categories as $category)
                                    <input type="radio" value="{{ $category->id }}" id="category-{{$category->id}}" name="category_id" @if(old('category_id') == $category->id) checked @endif>
                                    <label for="category-{{ $category->id }}">
                                        <span>*</span>
                                        <span>{{ $category->name }}</span>
                                    </label><br>
                                @endforeach


                                {{--<input type="radio" value="2" id="category-2" name="category_id" @if(old('category_id') == '2') checked @endif>--}}
                                {{--<label for="category-2">--}}
                                    {{--<span>B</span>--}}
                                    {{--<span>Art, music, games</span>--}}
                                {{--</label><br>--}}

                                {{--<input type="radio" value="3" id="category-3" name="category_id" @if(old('category_id') == '3') checked @endif>--}}
                                {{--<label for="category-3">--}}
                                    {{--<span>C</span>--}}
                                    {{--<span>Community, nature, culture</span>--}}
                                {{--</label><br>--}}

                                {{--<input type="radio" value="4" id="category-4" name="category_id" @if(old('category_id') == '4') checked @endif>--}}
                                {{--<label for="category-4">--}}
                                    {{--<span>D</span>--}}
                                    {{--<span>Other</span>--}}
                                {{--</label><br>--}}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 question-row">
                    @if($errors->get('previous_url'))
                        @foreach($errors->get('previous_url') as $error)
                            <div class="notice notice-danger">
                                <strong>{{ $error }}</strong>
                            </div>
                        @endforeach
                    @endif
                    <div class="question">
                        <p>Please write a few tags related to your project to get more visitors.</p>
                        <p>Write all the tags you can think of separated by a comma.</p>
                        <input type="text" placeholder="Example: cars, automotive, engineering" name="tags" value="{{ old('previous_url') }}">
                    </div>
                </div>
                <div class="col-md-12 question-row">
                    @if($errors->get('project_type'))
                        @foreach($errors->get('project_type') as $error)
                            <div class="notice notice-danger">
                                <strong>{{ $error }}</strong>
                            </div>
                        @endforeach
                    @endif
                    <div class="question">
                        <p>What type of crowdfunding campaign do you want to create?</p>
                        <p>Let's get a little more specific</p>

                        <div class="row">
                            <div class="col-md-5">
                                <input type="radio" value="1" id="type-1" name="project_type" @if(old('project_type') == '1') checked @endif>
                                <label for="type-1">
                                    <span>A</span>
                                    <span>Pre-Orders</span>
                                </label><br>

                                <input type="radio" value="2" id="type-2" name="project_type" @if(old('project_type') == '2') checked @endif>
                                <label for="type-2">
                                    <span>B</span>
                                    <span>Reward Based</span>
                                </label><br>

                                <input type="radio" value="3" id="type-3" name="project_type" @if(old('project_type') == '3') checked @endif>
                                <label for="type-3">
                                    <span>C</span>
                                    <span>Equity Based</span>
                                </label><br>

                                <input type="radio" value="4" id="type-4" name="project_type" @if(old('project_type') == '4') checked @endif>
                                <label for="type-4">
                                    <span>D</span>
                                    <span>Donation Based</span>
                                </label><br>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-md-12 question-row">
                    @if($errors->get('previous_url'))
                        @foreach($errors->get('previous_url') as $error)
                            <div class="notice notice-danger">
                                <strong>{{ $error }}</strong>
                            </div>
                        @endforeach
                    @endif
                    <div class="question">
                        <p>If you have one, please include your previous campaign URL:</p>
                        <input type="text" placeholder="https://" name="previous_url" value="{{ old('previous_url') }}">
                    </div>
                </div>

                <div class="col-md-12 question-row">
                    @if($errors->get('upload_other'))
                        @foreach($errors->get('upload_other') as $error)
                            <div class="notice notice-danger">
                                <strong>{{ $error }}</strong>
                            </div>
                        @endforeach
                    @endif
                    <div class="question">
                        <p>Do you plan on uploading your project to other platforms?</p>

                        <div class="row">
                            <div class="col-md-3">
                                <input type="radio" value="1" id="other-yes" name="upload_other" @if(old('upload_other') == '1') checked @endif>
                                <label for="other-yes">
                                    <span>Y</span>
                                    <span>Yes</span>
                                </label><br>

                                <input type="radio" value="2" id="other-no" name="upload_other" @if(old('upload_other') == '2') checked @endif>
                                <label for="other-no">
                                    <span>N</span>
                                    <span>No</span>
                                </label><br>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-md-12 question-row">
                    @if($errors->get('ready'))
                        @foreach($errors->get('ready') as $error)
                            <div class="notice notice-danger">
                                <strong>{{ $error }}</strong>
                            </div>
                        @endforeach
                    @endif
                    <div class="question-ready">
                        <p>From 1 to 10: How ready is your project?</p>
                        <p>Are you fully funded, or are you just starting your crowdfunding journey?</p>
                        <div class="ready-bar">
                            <input type="radio" value="1" id="ready-1" name="ready" @if(old('ready') == '1') checked @endif> <label for="ready-1">1</label>
                            <input type="radio" value="2" id="ready-2" name="ready" @if(old('ready') == '2') checked @endif> <label for="ready-2">2</label>
                            <input type="radio" value="3" id="ready-3" name="ready" @if(old('ready') == '3') checked @endif> <label for="ready-3">3</label>
                            <input type="radio" value="4" id="ready-4" name="ready" @if(old('ready') == '4') checked @endif> <label for="ready-4">4</label>
                            <input type="radio" value="5" id="ready-5" name="ready" @if(old('ready') == '5') checked @endif> <label for="ready-5">5</label>
                            <input type="radio" value="6" id="ready-6" name="ready" @if(old('ready') == '6') checked @endif> <label for="ready-6">6</label>
                            <input type="radio" value="7" id="ready-7" name="ready" @if(old('ready') == '7') checked @endif> <label for="ready-7">7</label>
                            <input type="radio" value="8" id="ready-8" name="ready" @if(old('ready') == '8') checked @endif> <label for="ready-8">8</label>
                            <input type="radio" value="9" id="ready-9" name="ready" @if(old('ready') == '9') checked @endif> <label for="ready-9">9</label>
                            <input type="radio" value="10" id="ready-10" name="ready" @if(old('ready') == '10') checked @endif> <label for="ready-10">10</label>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 question-row">
                    @if($errors->get('title'))
                        @foreach($errors->get('title') as $error)
                            <div class="notice notice-danger">
                                <strong>{{ $error }}</strong>
                            </div>
                        @endforeach
                    @endif
                    <div class="question">
                        <p>Enter your project title...</p>
                        <input type="text" placeholder="Type your answer here..." name="title" value="@if(Session::get('name')) {{Session::get('name')}} @else{{ old('title') }}@endif">
                    </div>
                </div>
                <div class="col-md-12 question-row">
                    @if($errors->get('subtitle'))
                        @foreach($errors->get('subtitle') as $error)
                            <div class="notice notice-danger">
                                <strong>{{ $error }}</strong>
                            </div>
                        @endforeach
                    @endif
                    <div class="question">
                        <p>... and a catchy subtitle.</p>
                        <input type="text" placeholder="Type your answer here..." name="subtitle" value="@if(Session::get('description')) {{ Session::get('description') }} @else {{ old('subtitle') }} @endif">
                    </div>
                </div>
                <div class="col-md-12 question-row">
                    @if($errors->get('project_goal'))
                        @foreach($errors->get('project_goal') as $error)
                            <div class="notice notice-danger">
                                <strong>{{ $error }}</strong>
                            </div>
                        @endforeach
                    @endif
                    <div class="question">
                        <p>What is goal you want to reach (USD)?</p>
                        <input type="number" placeholder="Type your answer here..." name="project_goal" value="@if(Session::get('goal')) {{ Session::get('goal') }} @else {{ old('project_goal') }} @endif">
                    </div>
                </div>
                <div class="col-md-12 question-row">
                    @if($errors->get('image'))
                        @foreach($errors->get('image') as $error)
                            <div class="notice notice-danger">
                                <strong>{{ $error }}</strong>
                            </div>
                        @endforeach
                    @endif
                    <div class="question">
                        <p>Now, we need a representative campaign image.</p>
                        <p>Please add a image below.</p>
                        {{--<div class="file-upload-group">--}}
                            {{--<span class="image-btn-container">--}}
                                {{--<label for="image" class="new-upload-btn">Choose Image</label>--}}
                            {{--</span>--}}

                            {{--<input type="file" id="image" name="image" onchange="myFunction()">--}}
                            {{--<script>--}}
                                {{--function myFunction(){--}}
                                    {{--if(document.getElementById("image").value != "") {--}}

                                        {{--var path = document.getElementById("image").value;--}}
                                        {{--var filename = path.substring(path.lastIndexOf('\\')+1);--}}

                                        {{--var node = document.createElement("p");--}}
                                        {{--var textnode = document.createTextNode(filename);--}}
                                        {{--node.appendChild(textnode);--}}
                                        {{--node.classList.add('new-file-name');--}}

                                        {{--document.querySelector('.file-upload-group').appendChild(node);--}}
                                    {{--}--}}
                                {{--}--}}
                            {{--</script>--}}
                        {{--</div>--}}
                        <div class="input-group">
                           <span class="input-group-btn">
                             <a id="lfm" data-input="thumbnail" data-preview="holder" class="new-upload-btn">
                                Choose
                             </a>
                           </span>
                            <input id="thumbnail" class="form-control" type="hidden" name="image" value="{{ asset('/uploads/dashboard-cover.jpg') }}">
                        </div>
                        <img id="holder" style="margin-top:15px;max-height:400px;">

                        <script>
                            $('#lfm').filemanager('image');
                        </script>
                    </div>
                </div>

                <div class="col-md-12 question-row">
                    @if($errors->get('start_date'))
                        @foreach($errors->get('start_date') as $error)
                            <div class="notice notice-danger">
                                <strong>{{ $error }}</strong>
                            </div>
                        @endforeach
                    @endif
                    <div class="question">
                        <p>What date do you want to start?</p>
                        <input type="date" name="start_date" value="{{ old('start_date') }}">
                    </div>
                </div>
                <div class="col-md-12 question-row">
                    @if($errors->get('end_date'))
                        @foreach($errors->get('end_date') as $error)
                            <div class="notice notice-danger">
                                <strong>{{ $error }}</strong>
                            </div>
                        @endforeach
                    @endif
                    <div class="question">
                        <p>What is the date limit to reach the goal?</p>
                        <input type="date" name="end_date" value="{{ old('end_date') }}">
                    </div>
                </div>
                <div class="col-md-12 question-row">
                    @if($errors->get('content'))
                        @foreach($errors->get('content') as $error)
                            <div class="notice notice-danger">
                                <strong>{{ $error }}</strong>
                            </div>
                        @endforeach
                    @endif
                    <div class="question">
                        <p>Describe your project to us.</p>
                        <p>Be simple and clear.</p>
                        <textarea id="editor" height="500px" name="content">@if( Session::get('full_description') ) {{ Session::get('full_description') }} @else{{ old('content') }}@endif</textarea>
                    </div>
                </div>

                <div class="col-md-12 question-row">
                    @if($errors->get('p_website'))
                        @foreach($errors->get('p_website') as $error)
                            <div class="notice notice-danger">
                                <strong>{{ $error }}</strong>
                            </div>
                        @endforeach
                    @endif
                    <div class="question">
                        <p>Please include your project's website, if you have one:</p>
                        <input type="text" placeholder="https://" name="p_website" value="{{ old('p_website') }}">
                    </div>
                </div>
                <?php
                    $levels = Session::get('levels_flash');
                    if($levels != null){
                        $levels_number = count($levels);
                    } else {
                        $levels_number = 0;
                    }
                ?>
                <div class="col-md-12 question-row">
                    <div class="question">
                        <p>How many backing levels do you have for your project?</p>
                        <input name="levels" type="number" oninput="doThing()" id="levels" placeholder="Type your answer here..." value="{{ $levels_number }}">
                    </div>
                </div>
                <div class="col-md-12" id="levels-container-new">
                    @if(Session::get('levels_flash'))
                        @foreach($levels as $level)
                            <div class="form-group">
                                <div class="form-group">
                                    <input type="text" placeholder="Title" name="level_title[]" value="{{$level['title']}}" class="form-control">
                                </div><div class="form-group">
                                    <input type="number" placeholder="Price in $" name="level_price[]" class="form-control" value="{{$level['price']}}">
                                </div>
                                <div class="form-group">
                                    <textarea type="text" name="level_desc[]" placeholder="Type in the level description" class="form-control">{{$level['description']}}</textarea>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
                <div class="col-md-12 project-submit-container">
                    <button type="submit" id="submit" name="submit" value="submit">Submit</button>
                    <button type="submit" id="save-draft" name="save_draft" value="draft">Save as Draft</button>
                </div>
            </div>
        </form>
        <script>

        </script>
        @if(Auth::guest())

            <style>
                button#submit {
                    display:none;
                }
            </style>

            <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
                @csrf
                <div class="account-create-project">
                    <div class="question">
                        <p>Lets get to know each other</p>
                        <input name="name" type="text" placeholder="Full Name" id="name">
                    </div>

                    <div class="question">
                        <p>What is your email address</p>
                        <input name="email" type="email" placeholder="Type your answer here..." id="email">
                    </div>

                    <div class="question">
                        <p>Choose a password and we will take care of the rest</p>
                        <input name="password" type="password" placeholder="Type your answer here..." id="password">
                    </div>

                    <button type="submit" id="submit-user">Submit</button>
                </div>
            </form>

            <script>
                $( document ).ready(function() {
                    $("#submit-user").click(function(e) {
                        e.preventDefault();

                        var name= $('#name').val();
                        var email= $('#email').val();
                        var password= $('#password').val();

                        console.log(name);
                        console.log(email);
                        console.log(password);

                        $.ajax({
                            type: "POST",
                            url: "{{ route('register') }}?auto=r",
                            data: {
                                _token: '{{csrf_token()}}',
                                name: name,
                                email: email,
                                password: password
                            },
                            success: function (result) {
                                $('#submit').click();
                                console.log('submited')
                            },
                            error: function (result) {
                                console.log(result);
                                alert('Something went wrong.');
                            }
                        });
                    });
                });
            </script>
        @endif
    </div>
</body>


<script>
    /* function */
    function doThing(){
        //clear container on input change
        var container = document.getElementById("levels-container-new");
        while (container.firstChild) {
            container.removeChild(container.firstChild);
        }

        // populate levels based on the input value
        var levelValue = document.getElementById("levels").value;

        for(var i = 1;i <= levelValue;i++){

            //create parent div
            var levelContainer = document.createElement("div");
            levelContainer.setAttribute('class','form-group');


            //create title input

            //create container for level title
            var levelTitleContainer = document.createElement("div");
            levelTitleContainer.classList.add("form-group");

            //crete input for title
            var levelTitle = document.createElement("input");
            levelTitle.setAttribute('type', 'text');
            levelTitle.setAttribute('placeholder', 'Title');
            levelTitle.setAttribute('name', 'level_title[]');
            levelTitle.setAttribute('value', '');

            levelTitle.classList.add("form-control");

            // append title input to title contaienr
            levelTitleContainer.appendChild(levelTitle);


            // Create price input

            //create container for level price
            var levelPriceContainer = document.createElement("div");
            levelPriceContainer.classList.add("form-group");

            //create price input
            var levelPrice = document.createElement("input");
            levelPrice.setAttribute('type', 'number');
            levelPrice.setAttribute('placeholder', 'Price in $');
            levelPrice.setAttribute('name', 'level_price[]');
            levelPrice.classList.add("form-control");

            // append price input to price container
            levelPriceContainer.appendChild(levelPrice);


            // Create Description input

            //create container for level description
            var levelDescContainer = document.createElement("div");
            levelDescContainer.classList.add("form-group");

            //create description input
            var levelDesc = document.createElement("textarea");
            levelDesc.setAttribute('type', 'text');
            levelDesc.setAttribute('name', 'level_desc[]');
            levelDesc.setAttribute('placeholder', 'Type in the level description');
            //var levelDescValue = document.createTextNode("Type in the level description");
            //levelDesc.appendChild(levelDescValue);
            levelDesc.classList.add("form-control");

            // append description input to description container
            levelDescContainer.appendChild(levelDesc);


            //Create level input
            var levelLabel = document.createElement("H4");
            var text = document.createTextNode("Level " + i);
            levelLabel.appendChild(text);

            //append elements to individual level container

            levelContainer.appendChild(levelLabel);
            levelContainer.appendChild(levelTitleContainer);
            levelContainer.appendChild(levelPriceContainer);
            levelContainer.appendChild(levelDescContainer);

            //append levels to general container
            document.getElementById('levels-container-new').appendChild(levelContainer);


        }
    }

</script>

</html>