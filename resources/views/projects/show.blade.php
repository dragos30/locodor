@extends('layouts.app')

@section('content')
    <div id="sticky-menu">
        <div class="container">
            <span class="project-share">
                <a href="http://www.facebook.com/sharer.php?u={{url()->current()}}" target="_blank">
                    <i style="background-color:#37569b" class="fab fa-facebook-f"></i>
                </a>
                <a href="http://pinterest.com/pin/create/button/?url={{url()->current()}}&media={{ urlencode(asset($project->image)) }}&description={{ urlencode($project->title) }}" target="_blank">
                    <i style="background-color:#e60023" class="fab fa-pinterest-p"></i>
                </a>
                <a href="https://twitter.com/share?url={{url()->current()}}" target="_blank">
                    <i style="background-color:#00acf0" class="fab fa-twitter"></i>
                </a>
                <span>Share it</span>
            </span>
            <span class="sticky-right">
                <a class="unwatch-project" @if(!App\Watcher::watching($project->id))) style="display:none" @endif href="{{ route('unwatch.project',['id' => $project->id ]) }}"><i class="fas fa-heart"></i> Watching</a>
                <a class="watch-project" @if(App\Watcher::watching($project->id))) style="display:none" @endif href="{{ route('watch.project',['id' => $project->id ]) }}"><i class="far fa-heart"></i> Watch</a>
            </span>

            @if(Auth::check())
                <script>
                    $( document ).ready(function() {
                        var clickedWatch = false;
                        $('.watch-project').on('click',function(e){
                            e.preventDefault();
                            var btnClicked = $(this);
                            btnClicked.fadeOut();

                            if(clickedWatch === false){
                                clickedWatch = true;
                                $.ajax({
                                    type: "GET",
                                    url: "{{ route('watch.project',['id' => $project->id]) }}",
                                    success: function (result) {
                                        btnClicked.hide();
                                        btnClicked.prev().show();
                                        clickedWatch = false;
                                    },
                                    error: function (result) {
                                        console.log('something went wrong');
                                    }
                                });
                            }
                        });

                        var clickedUnwatch = false;
                        $('.unwatch-project').on('click',function(e){
                            e.preventDefault();
                            var btnClicked = $(this);
                            btnClicked.fadeOut();

                            if(clickedUnwatch === false){
                                clickedUnwatch = true;
                                $.ajax({
                                    type: "GET",
                                    url: "{{ route('unwatch.project',['id' => $project->id]) }}",
                                    success: function (result) {
                                        btnClicked.hide();
                                        btnClicked.next().show();
                                        clickedUnwatch = false;
                                    },
                                    error: function (result) {
                                        clickedUnwatch.log('something went wrong');
                                    }
                                });
                            }
                        });

                    });

                </script>
            @endif


        </div>
    </div>
    <script src="https://www.paypalobjects.com/api/checkout.js"></script>
    <div class="container">
        <div>
            <div class="row  main-content">
                <div class="col-md-12">
                        <div id="carouselExampleControls" class="carousel slide featured-image-container" data-ride="carousel">
                            <div class="carousel-inner">
                                @foreach($slides as $slide)
                                    <div class="carousel-item">
                                        <div class="project-slide">
                                            <img src="{{ asset($slide) }}" alt="{{ $project->title }}" width="100%">
                                        </div>
                                    </div>
                                @endforeach
                                @if(count($slides) !== 1)
                                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                @endif
                            </div>

                            <div class="carousel-preview-container">
                                @if($project->youtube_url)
                                    <div class="carousel-preview-img popup-youtube" href="{{ $project->youtube_url }}"  style="background-image:url('http://img.youtube.com/vi/{{$youtube_thumbnail}}/0.jpg')">
                                        <div class="play"></div>
                                    </div>
                                @endif
                                @if(count($slides) !== 1 || $project->youtube_url)
                                    @foreach($slides as $key => $slide)
                                        <div class="carousel-preview-img" data-target="#carouselExampleControls" data-slide-to="{{ $key }}" style="background-image:url('{{ asset($slide) }}')"></div>
                                    @endforeach
                                @endif
                            </div>
                </div>

                <div id="test-popup" class="white-popup mfp-with-anim mfp-hide">You may put any HTML here. This is dummy copy. It is not meant to be read. It has been placed here solely to demonstrate the look and feel of finished, typeset text. Only for show. He who searches for meaning here will be sorely disappointed.</div>
                <script type="text/javascript">
                    $(document).ready(function() {
                        $('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
                            type: 'iframe',
                            mainClass: 'mfp-fade',
                            preloader: true,
                        });
                    });

                    $(document).ready(function(){
                        $('.carousel-preview-container').slick({
                            infinite: true,
                            slidesToShow: 4,
                            slidesToScroll: 1,
                            swipeToSlide: true,
                            arrows:false,
                            responsive: [
                                {
                                    breakpoint: 1140,
                                    settings: {
                                        slidesToShow: 3,
                                        slidesToScroll: 1,
                                        infinite: true,
                                        dots: true
                                    }
                                },
                                {
                                    breakpoint: 960,
                                    settings: {
                                        slidesToShow: 2,
                                        slidesToScroll: 1,
                                        infinite: true,
                                        dots: true
                                    }
                                },
                                {
                                    breakpoint: 600,
                                    settings: {
                                        slidesToShow: 1,
                                        slidesToScroll: 1
                                    }
                                }
                            ]
                        });
                    });
                </script>
                @if(($project->current_amount + $sync->amount < $project->project_goal) && $project->end_date < time())
                    <p class="project-notice">Unfurtunately this project was not fully funded before the end date.</p>
                @else
                    <div class="container">
                    <div class="payment-successful-container justify-content-center">
                        <div class="payment-successful">
                            <a class="close-payment-notice"><i class="fas fa-times"></i></a>
                            <p class="thank-you-for-payment">Thank you for your purchase!</p>
                            <div class="fancy-hr"></div>
                            <p class="order-number">Order No: <span></span></p>
                            <p>Please save this Order Number for future reference.</p>
                        </div>
                    </div>
                    <p class="back-this-btn-container">
                        <button class="normal-btn post-comment-btn back-this-btn" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                            Back this project! <i class="fas fa-angle-right"></i>
                        </button>
                    </p>
                    @if(isset($_GET['backing-intent']))
                        <script>
                            $( document ).ready(function() {
                                $('html, body').animate({
                                    scrollTop: $("#collapseExample").offset().top
                                }, 500);
                            });
                        </script>
                    @endif
                    <div class="collapse @if(isset($_GET['backing-intent'])) show @endif" id="collapseExample">
                        <div class="card card-body">
                            <div class="row">
                                @foreach($levels as $level)
                                    <div class="col-md-6">
                                        <div class="level">
                                            <div class="level-title">
                                                <h4>{{ $level->title }}</h4>
                                            </div>
                                            <div class="level-description">
                                                {!! $level->description !!}
                                            </div>
                                            <div class="level-price" style="margin-bottom:0;">
                                                <b>${{ $level->price}}</b>
                                                <small>+ ${{$level->shipping_cost}} shipping cost</small>
                                            </div>
                                            <div class="button-container text-center">
                                                @if(Auth::check() && Auth::user()->admin)
                                                    <div class="pay-as-admin-container">
                                                        <a class="admin-new-backer-btn btn btn-dark" data-project-id="{{ $project->id }}" data-level-id="{{  $level->id }}" data-level-price="{{ $level->price }}">
                                                            Pay as Admin
                                                        </a>
                                                    </div>
                                                @endif

                                                @if($project->affiliate_link == null)
                                                    <div id="paypal-button-top-{{$level->id}}"></div>

                                                    <script>
                                                        paypal.Button.render({

                                                            env: 'production', // sandbox | production
                                                            style: {
                                                                label: 'paypal',
                                                                size:  'responsive',    // small | medium | large | responsive
                                                                shape: 'pill',     // pill | rect
                                                                color: 'black',     // gold | blue | silver | black
                                                                tagline: false,
                                                                fundingicons:true
                                                            },

                                                            // PayPal Client IDs - replace with your own
                                                            // Create a PayPal app: https://developer.paypal.com/developer/applications/create
                                                            client: {
                                                                sandbox: 'Ae6vLeJ1R-8Fy_gzfZ-1d8EhOO52TUvLc6_JwedwI8lVyyfdbNdT3T8vDRmQKxsdcUZDcFmNCLGOxvdT',
                                                                production: 'AacGDpsS8aoAfLdJN4DblsZELEQBD05QwkKFa-XBBmuKPC9plpAdNFiwzesaiuZ3acQ2FCKUtbImr3Fo'
                                                            },

                                                            // Show the buyer a 'Pay Now' button in the checkout flow
                                                            commit: true,

                                                            // payment() is called when the button is clicked
                                                            payment: function(data, actions) {
                                                                return actions.payment.create({
                                                                    transactions: [{
                                                                        amount: {
                                                                            total: '{{ $level->price + $level->shipping_cost }}.00',
                                                                            currency: 'USD'
                                                                        },
                                                                        description: '{{ $level->title }}',
                                                                    }]
                                                                });
                                                            },

                                                            // onAuthorize() is called when the buyer approves the payment
                                                            onAuthorize: function(data, actions) {

                                                                // Make a call to the REST api to execute the payment
                                                                return actions.payment.execute().then(function() {
                                                                    $.ajax({
                                                                        type: "GET",
                                                                        url: "/new-backer",
                                                                        data: {
                                                                            level_id: {{ $level->id }},
                                                                            project_id: {{ $project->id }},
                                                                            price: {{ $level->price + $level->shipping_cost}},
                                                                            data: data
                                                                        },
                                                                        success: function (result) {
                                                                            $('.payment-successful .order-number span').html(result);
                                                                            $('.payment-successful-container').show();
                                                                            $('.close-payment-notice').on('click',function(){
                                                                                $('.payment-successful-container').hide();
                                                                            });
                                                                            console.log('success');

                                                                        },
                                                                        error: function (result) {
                                                                            alert('Something went wrong!');
                                                                            console.log('failed');
                                                                        }
                                                                    });
                                                                });
                                                            }

                                                        }, '#paypal-button-top-{{$level->id}}');

                                                    </script>
                                                @else
                                                    <br>
                                                    <div class="text-center">
                                                        <a href="{{ $level->project->affiliate_link }}" class="normal-btn" target="_blank">Pledge Now</a>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="simple-line"></div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            </div>
                <div class="col-md-8">
                    <!-- Modal -->
                    <div class="modal fade" id="myModal" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-body">
                                    <button type="button" class="close" data-dismiss="modal"><i class="fas fa-times"></i></button>
                                    <h4>Backers:</h4>
                                    @if($project->backers->count() != 0)
                                        @foreach($project->backers as $backer)
                                            @if($backer->user_id != 0 && $backer->user_id != 99999 && isset($backer->user))
                                                <div class="modal-backer">
                                                    <p class="modal-backer">
                                                        <img src="{{ $backer->user->profile->avatar }}" width="50px" height="50px">
                                                        <a class="modal-backer-name" href="{{ route('profile.show',['id' => $backer->user->profile->id, 'slug' => $backer->user->profile->slug])}}" target="_blank">{{ $backer->user->name }}</a>
                                                        <span class="modal-backer-amount">${{ $backer->amount }}</span>
                                                    </p>
                                                </div>
                                            @else
                                                <div class="modal-backer">
                                                    <p class="modal-backer">
                                                        <img src="/uploads/avatars/1.png" width="50px" height="50px">
                                                        <span class="modal-backer-name" target="_blank">Guest</span>
                                                        <span class="modal-backer-amount">${{ $backer->amount }}</span>
                                                    </p>
                                                </div>
                                            @endif
                                        @endforeach
                                    @else
                                        <p class="modal-backer">There are no backers on Locodor</p>
                                    @endif
                                    @if($sync->backers != 0 && $sync->amount != 0)
                                        <p class="modal-backer">
                                            <img src="/uploads/avatars/1.png" width="50px" height="50px">
                                            <span class="modal-backer-name">{{ $sync->backers }} Backers Other Platforms</span>
                                            <span class="modal-backer-amount">${{ $sync->amount }}</span>
                                        </p>
                                    @endif
                                </div>
                            </div>

                        </div>
                    </div>
                    <h1 class="project-title-show">{{ $project->title }}</h1>
                    <h3 class="project-subtitle-show">{{ $project->subtitle }}</h3>
                    {{--Project datails on mobile--}}
                    <div class="project-details levels-container only-mobile">
                        <img class="project-avatar" src="{{ asset( $project->user->profile->avatar ) }}" alt="crowdfunding-project-author">
                        <a href="{{ route('profile.show',['id' => $project->user->profile->id, 'slug' => $project->user->profile->slug]) }}" class="project-author"><p>{{ $project->user->name }}</p></a>
                        <div class="project-info">
                            <div class="detail-item">
                                <a href="#" data-toggle="modal" data-target="#myModal">
                                    <p class="project-detail">{{ count($backers) + $sync->backers }}</p>
                                    <small>backers</small>
                                </a>
                            </div>
                            <div class="detail-item">
                                <p class="project-detail">${{ number_format($project->current_amount + $sync->amount) }}</p>
                                <small>out of ${{ number_format($project->project_goal) }}</small>
                            </div>
                            @if($project->end_date > time())
                                <div class="detail-item">
                                    <p class="project-detail">{{ date('F Y',$project->end_date) }}</p>
                                    <small>is the end date.</small>
                                </div>
                            @else
                                <div class="detail-item">
                                    <p class="project-detail">Ended</p>
                                    <small>on {{ date('F Y',$project->end_date) }}</small>
                                </div>
                            @endif
                        </div>
                        <div class="project-bar">
                            <div class="inner-bar" style="width:{{ floor((( $project->current_amount + $sync->amount ) / $project->project_goal)*100) }}%"><span class="bar-percent">{{ floor((( $project->current_amount + $sync->amount ) / $project->project_goal)*100) }}%</span></div>
                        </div>

                        <div class="project-share">
                            <a href="http://www.facebook.com/sharer.php?u={{url()->current()}}" target="_blank">
                                <i style="background-color:#37569b" class="fab fa-facebook-f"></i>
                            </a>
                            <a href="http://pinterest.com/pin/create/button/?url={{url()->current()}}&media={{ urlencode(asset($project->image)) }}&description={{ urlencode($project->title) }}" target="_blank">
                                <i style="background-color:#e60023" class="fab fa-pinterest-p"></i>
                            </a>
                            <a href="https://twitter.com/share?url={{url()->current()}}" target="_blank">
                                <i style="background-color:#00acf0" class="fab fa-twitter"></i>
                            </a>
                        </div>
                    </div>

                    <div class="jumbotron comments-section" id="comments-section">
                        @if($comments)
                            @foreach($comments as $comment)
                                <div class="row project-comment-row" data-comment-id="{{ $comment->id }}">
                                    <div class="col-md-3 text-center">
                                        <a href="{{ route('profile.show',['id' => $comment->user->profile->id, 'slug' => $comment->user->profile->slug]) }}">
                                            <img width="50" height="50" class="comments-avatar" src="{{ asset( $comment->user->profile->avatar ) }}" alt="crowdfunding-user-activity">
                                            <p class="comment-author">{{ $comment->user->name }}</p>
                                        </a>
                                        <p>{{ $comment->created_at->diffForHumans() }}</p>
                                    </div>
                                    <div class="col-md-7">
                                        <p class="comment-content">{!! $comment->comment !!}</p>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="comment-stats">
                                            @if($comment->user->id == Auth::id())
                                                <a class="edit-btn" href="{{ route('comment.edit',['id'=>$comment->id]) }}"><i class="fas fa-pen-fancy"></i></a>
                                                <a class="delete-btn" href="{{ route('comment.delete',['id'=>$comment->id]) }}"><i class="fas fa-trash"></i></a>
                                            @endif
                                            <a class="unlike-btn" @if(!in_array($comment->id,$liked)) style="display:none" @endif id="unlike-btn{{$comment->id}}" href="{{ route('comment.unlike',[ 'id' => $comment->id ]) }}"><i class="far fa-thumbs-down"></i>unlike</a>
                                            <a class="like-btn"  @if(in_array($comment->id,$liked)) style="display:none" @endif id="like-btn{{$comment->id}}" href="{{ route('comment.like',[ 'id' => $comment->id ]) }}"><i class="far fa-thumbs-up"></i>like</a>
                                            <span class="likes-number"><span id="likes-number{{$comment->id}}">{{ $comment->commentLikes->count() }}</span> likes</span>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                    @if(Auth::check())
                        <form action="{{ route('comment.store') }}" method="post">
                            {{ csrf_field() }}
                            <input name="project_id" type="hidden" value="{{ $project->id }}">
                            <div class="form-group">
                                <textarea class="form-control" type="text" placeholder="Comment..." name="comment" id="comment"></textarea>
                            </div>
                            <div class="form-group">
                                <button class="fancy-btn post-comment-btn" type="submit" id="post-btn">Post</button>
                            </div>
                        </form>
                    @else
                        <form action="{{ route('comment.store') }}" method="post">
                            {{ csrf_field() }}
                            <input name="project_id" type="hidden" value="{{ $project->id }}">
                            <div class="form-group">
                                <textarea class="form-control" type="text" placeholder="Comment..." name="comment" id="comment"></textarea>
                            </div>
                            <div class="form-group">
                                <button class="normal-btn post-comment-btn" style="width:15%" type="submit" id="post-btn">Post</button>
                            </div>
                        </form>
                        <style>
                            button#post-btn {
                                display:none;
                            }
                        </style>
                        <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
                            @csrf
                            <div class="account-create-comment">
                                <input name="name" type="text" placeholder="Full Name" id="name" class="form-control">

                                <input name="email" type="email" placeholder="Email Address" id="email" class="form-control">

                                <input name="password" type="password" placeholder="Password" id="password" class="form-control">

                                <button type="submit" id="submit-user">Post</button>
                            </div>
                        </form>
                        <script>
                            $( document ).ready(function() {
                                $("#submit-user").click(function(e) {
                                    e.preventDefault();

                                    var name= $('#name').val();
                                    var email= $('#email').val();
                                    var password= $('#password').val();

                                    console.log(name);
                                    console.log(email);
                                    console.log(password);

                                    $.ajax({
                                        type: "POST",
                                        url: "{{ route('register') }}?auto=r",
                                        data: {
                                            _token: '{{csrf_token()}}',
                                            name: name,
                                            email: email,
                                            password: password
                                        },
                                        success: function (result) {
                                            $('#post-btn').click();
                                            console.log('submited')
                                        },
                                        error: function (result) {
                                            console.log(result);
                                            alert('Something went wrong.');
                                        }
                                    });
                                });
                            });
                        </script>

                    @endif
                    <div class="content">
                        {!! $project->content !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="project-details levels-container">
                        <img class="project-avatar" src="{{ asset( $project->user->profile->avatar ) }}" alt="crowdfunding-project-author">
                        <a href="{{ route('profile.show',['id' => $project->user->profile->id, 'slug' => $project->user->profile->slug]) }}" class="project-author"><p>{{ $project->user->name }}</p></a>
                        <div class="project-info">
                            <div class="detail-item">
                                <a href="#" data-toggle="modal" data-target="#myModal">
                                    <p class="project-detail">{{ count($backers) + $sync->backers }}</p>
                                    <small>backers</small>
                                </a>
                            </div>
                            <div class="detail-item">
                                <p class="project-detail">${{ number_format($project->current_amount + $sync->amount) }}</p>
                                <small>out of ${{ number_format($project->project_goal) }}</small>
                            </div>
                            @if($project->end_date > time())
                                <div class="detail-item">
                                    <p class="project-detail">{{ date('F Y',$project->end_date) }}</p>
                                    <small>is the end date.</small>
                                </div>
                            @else
                                <div class="detail-item">
                                    <p class="project-detail">Ended</p>
                                    <small>on {{ date('F Y',$project->end_date) }}</small>
                                </div>
                            @endif
                        </div>
                        <div class="project-bar">
                            <div class="inner-bar" style="width:{{ floor((( $project->current_amount + $sync->amount ) / $project->project_goal)*100) }}%"><span class="bar-percent">{{ floor((( $project->current_amount + $sync->amount ) / $project->project_goal)*100) }}%</span></div>
                        </div>

                        <div class="project-share">
                            <a href="http://www.facebook.com/sharer.php?u={{url()->current()}}" target="_blank">
                                <i style="background-color:#37569b" class="fab fa-facebook-f"></i>
                            </a>
                            <a href="http://pinterest.com/pin/create/button/?url={{url()->current()}}&media={{ urlencode(asset($project->image)) }}&description={{ urlencode($project->title) }}" target="_blank">
                                <i style="background-color:#e60023" class="fab fa-pinterest-p"></i>
                            </a>
                            <a href="https://twitter.com/share?url={{url()->current()}}" target="_blank">
                                <i style="background-color:#00acf0" class="fab fa-twitter"></i>
                            </a>
                        </div>
                        @if($project->collaborators->count() != 0)
                            <div class="fancy-hr"></div>
                            <div class="project-collaborators">
                                <h3>Team Members:</h3>
                                @foreach($project->collaborators->unique('user_id') as $collaborator)
                                    @if($collaborator->user_id != null)
                                        <div class="team-member">
                                            <img class="project-avatar" src="{{ asset( $collaborator->user->profile->avatar ) }}" alt="crowdfunding-project-author">
                                            <a href="{{ route('profile.show',['id' => $collaborator->user->profile->id, 'slug' => $collaborator->user->profile->slug]) }}" class="project-author"><p>{{ $collaborator->user->name }}</p></a>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        @endif
                    </div>
                    <div>

                        <?php
                        if($project->tags != null){
                            $tags = explode(',', $project->tags);
                        }

                        ?>
                        @if(isset($tags))
                            <p class="tags-text">Check similar projects:</p>
                            @foreach($tags as $tag)
                                <a href="{{ route('projects',['filter'=>'tag','value'=>trim($tag)]) }}" class="tag-item">#{{ trim($tag) }}</a>
                            @endforeach
                        @endif
                    </div>
                    @if($project->end_date < time() && ($project->current_amount + $sync->amount) < $project->project_goal)

                        <p class="project-completed-text">We are sorry, this project was not fully funded, no backers has been charged. We will keep everyone updated if this campaign is going to be relaunched. Thank you for your support!</p>
                        <div class="what-next-container text-center">
                            <div class="collapse what-next-content" id="collapseExample2">
                                <a href="{{ route('project.create-start') }}" class="normal-btn">Fund your idea</a><br>
                                <a href="{{ route('activity') }}" class="normal-btn">Socialize</a><br>
                                <a href="{{ route('projects') }}" class="normal-btn">Explore projects</a><br>
                                <a href="{{ route('posts') }}" class="normal-btn">Read our tips</a>
                            </div>
                            <div class="what-next-btn">
                                <a data-toggle="collapse" href="#collapseExample2" role="button" aria-expanded="false" aria-controls="collapseExample2">
                                    What do you want to do next?
                                    <br>
                                    <i class="fas fa-angle-down" style="margin-left:0"></i>
                                </a>
                            </div>
                        </div>
                    @endif
                    @if($project->end_date < time() && ($project->current_amount + $sync->amount) > $project->project_goal)
                        <p class="project-completed-text">This project is coming to life. Thank you for showing your support!</p>
                        <div class="what-next-container text-center">
                            <div class="collapse what-next-content" id="collapseExample2">
                                <a href="{{ route('project.create-start') }}" class="normal-btn">Fund your idea</a><br>
                                <a href="{{ route('activity') }}" class="normal-btn">Socialize</a><br>
                                <a href="{{ route('projects') }}" class="normal-btn">Explore projects</a><br>
                                <a href="{{ route('posts') }}" class="normal-btn">Read our tips</a>
                            </div>
                            <div class="what-next-btn">
                                <a data-toggle="collapse" href="#collapseExample2" role="button" aria-expanded="false" aria-controls="collapseExample2">
                                    What do you want to do next?
                                    <br>
                                    <i class="fas fa-angle-down" style="margin-left:0"></i>
                                </a>
                            </div>
                        </div>
                    @endif
                    <div class="all-levels">
                        @if(($project->current_amount + $sync->amount < $project->project_goal) && $project->end_date < time())
                            <p class="project-notice">Unfurtunately this project was not fully funded before the end date. </p>
                        @else
                            @foreach($levels as $level)
                            <div class="level">
                                <div class="level-title">
                                    <h4>{{ $level->title }}</h4>
                                </div>
                                <div class="level-description">
                                    {!! $level->description !!}
                                </div>
                                <div class="level-price">
                                    Pledge ${{ $level->price}}
                                    <small>+ ${{$level->shipping_cost}} shipping cost</small>
                                </div>
                                <div class="button-container text-center">
                                    {{--<form action="{{ route('backer.pay') }}" method="POST">--}}
                                    {{--{{ csrf_field() }}--}}
                                    {{--<input type="hidden" value="{{ $level->price }}" name="price">--}}
                                    {{--<input type="hidden" value="{{ $level->description }}" name="description">--}}
                                    {{--<input type="hidden" value="{{ $project->id }}" name="project_id">--}}
                                    {{--<input type="hidden" value="{{ $level->id }}" name="level_id">--}}
                                    {{--<script--}}
                                    {{--src="https://checkout.stripe.com/checkout.js" class="stripe-button"--}}
                                    {{--data-key="pk_test_gy6tdWHsomojFGRsni6rtHB0"--}}
                                    {{--data-amount="{{ $level->price * 100 }}"--}}
                                    {{--data-name=" {{$level->title}} "--}}
                                    {{--data-description="{{ $level->description }}"--}}
                                    {{--data-image="{{ asset('/uploads/favicon.png') }}"--}}
                                    {{--data-locale="auto">--}}
                                    {{--</script>--}}
                                    {{--</form>--}}
                                    {{--<span>or</span>--}}
                                    {{--Paypal Button--}}
                                    @if(Auth::check() && Auth::user()->admin)
                                        <div class="pay-as-admin-container">
                                            <a class="admin-new-backer-btn btn btn-dark" data-project-id="{{ $project->id }}" data-level-id="{{  $level->id }}" data-level-price="{{ $level->price }}">
                                                Pay as Admin
                                            </a>
                                        </div>
                                    @endif

                                    @if($project->affiliate_link == null)
                                        <div class="payment-button">
                                            <div id="paypal-button-{{$level->id}}"></div>
                                        </div>

                                        <script>
                                            paypal.Button.render({

                                                env: 'production', // sandbox | production
                                                style: {
                                                    label: 'paypal',
                                                    size:  'responsive',    // small | medium | large | responsive
                                                    shape: 'pill',     // pill | rect
                                                    color: 'black',     // gold | blue | silver | black
                                                    tagline: false,
                                                    fundingicons:true
                                                },

                                                // PayPal Client IDs - replace with your own
                                                // Create a PayPal app: https://developer.paypal.com/developer/applications/create
                                                client: {
                                                    sandbox: 'Ae6vLeJ1R-8Fy_gzfZ-1d8EhOO52TUvLc6_JwedwI8lVyyfdbNdT3T8vDRmQKxsdcUZDcFmNCLGOxvdT',
                                                    production: 'AacGDpsS8aoAfLdJN4DblsZELEQBD05QwkKFa-XBBmuKPC9plpAdNFiwzesaiuZ3acQ2FCKUtbImr3Fo'
                                                },

                                                // Show the buyer a 'Pay Now' button in the checkout flow
                                                commit: true,

                                                // payment() is called when the button is clicked
                                                payment: function(data, actions) {
                                                    return actions.payment.create({
                                                        transactions: [{
                                                            amount: {
                                                                total: '{{ $level->price + $level->shipping_cost }}.00',
                                                                currency: 'USD'
                                                            },
                                                            description: '{{ $level->title }}',
                                                        }]
                                                    });
                                                },

                                                // onAuthorize() is called when the buyer approves the payment
                                                onAuthorize: function(data, actions) {

                                                    // Make a call to the REST api to execute the payment
                                                    return actions.payment.execute().then(function() {

                                                        $.ajax({
                                                            type: "GET",
                                                            url: "/new-backer",
                                                            data: {
                                                                level_id: {{ $level->id }},
                                                                project_id: {{ $project->id }},
                                                                price: {{ $level->price + $level->shipping_cost }},
                                                                data: data
                                                            },
                                                            success: function (result) {
                                                                $('.payment-successful .order-number span').html(result);
                                                                $('.payment-successful-container').show();
                                                                $('.close-payment-notice').on('click',function(){
                                                                    $('.payment-successful-container').hide();
                                                                });
                                                                console.log('success');

                                                            },
                                                            error: function (result) {
                                                                console.log(result);
                                                                alert('Something went wrong!');
                                                                console.log('failed');
                                                            }
                                                        });
                                                    });
                                                }

                                            }, '#paypal-button-{{$level->id}}');

                                        </script>
                                    @else
                                        <br>
                                        <div class="text-center">
                                            <a href="{{ $level->project->affiliate_link }}" class="normal-btn" target="_blank">Pledge Now</a>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="simple-line"></div>
                        @endforeach
                        @endif
                    </div>
                </div>
        </div>
    </div>

    <script>
        $( document ).ready(function() {
            $('.carousel-inner').children().first().addClass('active')
        });
    </script>

    @if(Auth::check())
        {{--Delete comment--}}
        <script>
            $( document ).ready(function() {
                deleteComments();
            });

            function deleteComments(){
                $(".delete-btn").click(function(e) {
                    e.preventDefault();
                    var deleteBtnClicked = $(this);
                    var commentParent = deleteBtnClicked.parent().parent().parent();
                    var deleteCommentID = commentParent.data('comment-id');
                    console.log(deleteCommentID);

                    $.ajax({
                        type: "GET",
                        url: "/comment/delete/"+deleteCommentID,
                        data: {
                            id: deleteCommentID
                        },
                        success: function (result) {
                            commentParent.prev().remove();
                            commentParent.remove();
                            console.log('success');
                        },
                        error: function (result) {
                            console.log(result);
                            alert('Something went wrong!');
                        }
                    });
                });
            }
        </script>
        {{--Add comment--}}
        <script>
            $( document ).ready(function() {
                $("#post-btn").click(function(e) {
                    e.preventDefault();
                    var postBtnClicked = $(this);
                    var newCommentContainer = postBtnClicked.parent().parent().prev();
                    var newCommentContent = $("#comment").val();
                    $.ajax({
                        type: "POST",
                        url: "/comment/store",
                        data: {
                            _token: "{{ csrf_token() }}",
                            project_id: {{ $project->id }},
                            comment: newCommentContent
                        },
                        success: function (result) {
                            response = $(result).find('.comments-section').children().last();
                            console.log(response);
                            newCommentContainer.append('<hr>');
                            newCommentContainer.append(response);
                            $('#comment').val("");
                            editComments();
                            deleteComments();
                        },
                        error: function (result) {
                            console.log(result);
                            alert('Something went wrong!')
                            console.log('failed');
                        }
                    });
                });
            });
        </script>

        {{--Edit comment functionality--}}
        <script>
            $( document ).ready(function() {
                editComments();
            });
            function editComments(){
                $(".edit-btn").click(function(e) {
                    e.preventDefault();
                    console.log('1:'+ commentText);
                    var btnClicked = $(this);
                    var commentId = btnClicked.parent().parent().first().data('comment-id');

                    //Hide comment
                    // commentText = btnClicked.parent().prev().children().text();
                    var commentText = btnClicked.parent().prev().children().first().text();
                    btnClicked.parent().prev().children().css('display','none');

                    //Add input
                    var futureInputContainer = btnClicked.parent().prev();
                    var editForm = ""+
                        "<form action='/comment/update/"+commentId+"' method='post'>" +
                        "<div class='form-group'>" +
                        // "<textarea name='comment' class='form-control' type='text'>"+commentText+"</textarea>" +
                        "<div contenteditable='true' class='comment-edit-div'>"+commentText+"</div>"+
                        "<button type='submit' class='normal-btn float-right save-comment' style='padding:0px 10px;border:0'>Save</button>"+
                        "</div>"+
                        "</form>";
                    editForm = $.parseHTML( editForm );
                    futureInputContainer.append( editForm );
                    console.log('2:'+ commentText);


                    $(".save-comment").click(function(e) {
                        var saveCommentBtn = $(this);
                        var textareaContent = saveCommentBtn.prev().text();
                        btnClicked.parent().prev().children().text(textareaContent);
                        e.preventDefault();
                        $.ajax({
                            type: "GET",
                            url: "/comment/update/" + commentId,
                            data: {
                                id: commentId,
                                comment: textareaContent
                            },
                            success: function (result) {
                                console.log('textarea:'+ textareaContent);
                                var commentText = textareaContent;
                                console.log('3:'+ commentText);
                                saveCommentBtn.parent().parent().parent().append(newComment);
                                saveCommentBtn.parent().parent().css('display','none');
                            },
                            error: function (result) {
                                console.log(result);
                                alert("Ops, something went wrong");
                            }
                        });
                    });
                });
            }
        </script>
        {{-- Back porject as admin --}}
        <script>
            $( document ).ready(function() {
                var processing = false;
                $('.admin-new-backer-btn').on('click',function(e){
                    console.log('clicked');
                    e.preventDefault();
                    var levelId = $(this).data('level-id');
                    var projectId = $(this).data('project-id');
                    var levelPrice = $(this).data('level-price');
                    if(processing == false){
                        processing = true;
                        $.ajax({
                            type: "GET",
                            url: "/admin-new-backer",
                            data: {
                                level_id: levelId,
                                project_id: projectId,
                                price: levelPrice
                            },
                            success: function (result) {
                                processing = false;
                                console.log('works');
                                alert('Project backed!')
                            },
                            error: function (result) {
                                console.log(result);
                                alert('Something went wrong!');
                                console.log('failed');
                            }
                        });
                    } else {
                        console.log('processing');
                    }
                })
            });
        </script>
        {{--Like/Unlike functionality--}}
        <script>
            $( document ).ready(function() {
                $(".like-btn").click(function(e) {
                    e.preventDefault();
                    var btnClicked = $(this);
                    btnClicked.css('pointer-events', 'none');
                    var commentId = btnClicked.parent().parent().parent().data('comment-id');
                    $.ajax({
                        type: "GET",
                        url: "/comment/like/"+commentId,
                        success: function(result) {
                            var likesNumber = btnClicked.next().children().first();

                            btnClicked.css("display", "none");
                            btnClicked.prev().css("display", "block");
                            likesNumber.text(parseInt(likesNumber.text())+1);
                            btnClicked.css('pointer-events', 'initial');
                        },
                        error: function(result) {
                            alert("Ops, something went wrong");
                        }
                    });
                });
                $(".unlike-btn").click(function(e) {
                    e.preventDefault();
                    var btnClicked = $(this);
                    btnClicked.css('pointer-events', 'none');
                    var commentId = btnClicked.parent().parent().parent().data('comment-id');

                    $.ajax({
                        type: "GET",
                        url: "/comment/unlike/"+commentId,
                        success: function(result) {
                            var likesNumber = btnClicked.next().next().children().first();

                            btnClicked.next().css("display", "block");
                            btnClicked.css("display", "none");
                            likesNumber.text(parseInt(likesNumber.text())-1);
                            btnClicked.css('pointer-events', 'initial');
                        },
                        error: function(result) {
                            alert('Ops, something went wrong');
                        }
                    });
                });
            });
        </script>
    @endif
@endsection
