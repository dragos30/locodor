<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Crete+Round" rel="stylesheet">

    <!-- Scripts -->
    <script src="{{ asset('js/custom-app.js') }}"></script>


    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
</head>
<body class="create-project-body">
<div class="back-button">
    <a href="{{ route('home') }}"><i class="fas fa-arrow-left"></i> Back to Locodor</a>
</div>
<div class="container">
    <div class="project-start-content">
        <img src="{{ asset('/uploads/create-start.png') }}">
        <p>Start your project or upload your current Crowdfudning Campaign</p>
        <p>Let's jump to it.</p>
        @if(Auth::check())
            <a href="{{ route('project.create3') }}">Start</a>
        @else
            <div class="project-start-register-container">
                <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
                    @csrf
                    <div class="account-create-project">
                        <div class="question">
                            <p>Lets get you started in no time!</p>
                            <input name="name" type="text" placeholder="Please write your name" id="name">
                            <small class="required-field">This field is required</small>
                        </div>

                        <div class="question">
                            <input name="email" type="email" placeholder="Please write your email address" id="email">
                            <small class="required-field">This field is required and must be an Email Address</small>
                        </div>

                        <div class="question">
                            <input name="password" type="password" placeholder="Choose a password" id="password">
                            <small class="required-field">This field is required</small>
                        </div>

                        <button type="submit" id="submit-user">Submit</button>
                        <div class="loader-inline"></div>
                    </div>
                </form>
            </div>
        @endif
    </div>
</div>
<script>
    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }
    $( document ).ready(function() {
        $('.loader-inline').hide();
        $("#submit-user").click(function(e) {
            e.preventDefault();



            var name= $('#name').val();
            var email= $('#email').val();
            var password= $('#password').val();

            if(name != "" && email != "" && password != "") {
                $(this).hide();
                $('.loader-inline').show();


                console.log(name);
                console.log(email);
                console.log(password);

                $.ajax({
                    type: "POST",
                    url: "{{ route('register') }}?auto=r",
                    data: {
                        _token: '{{csrf_token()}}',
                        name: name,
                        email: email,
                        password: password
                    },
                    success: function (result) {
                        window.location.href = "{{ route('project.create3') }}";
                    },
                    error: function (result) {
                        console.log(result);
                        alert('Something went wrong.');
                    }
                });
            } else {
                if(name == ""){
                    $('#name').next().addClass('required-text-shown');
                }else{
                    $('#name').next().removeClass('required-text-shown');
                }

                if(email == "" || !isEmail($('#email').val())){
                    $('#email').next().addClass('required-text-shown');
                }else{
                    $('#email').next().removeClass('required-text-shown');
                }

                if(password == ""){
                    $('#password').next().addClass('required-text-shown');
                }else{
                    $('#password').next().removeClass('required-text-shown');
                }

            }
        });
    });
</script>
</body>
</html>
