@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <form action="{{ route('project.results') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="previous-url">Kickstarter Campaign URL</label>
                        <input name="url" class="form-control form-control-lg" type="text" placeholder="Type in Kickstarter URL">
                    </div>
                    <button type="submit" class="normal-btn bttn">Submit</button>
                </form>
                <hr>
                <form action="{{ route('project.results.indiegogo') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="previous-url">Indiegogo Campaign URL</label>
                        <input name="url" class="form-control form-control-lg" type="text" placeholder="Type in Indiegogo URL">
                    </div>
                    <button type="submit" class="normal-btn bttn">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection