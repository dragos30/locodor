@extends('layouts.app')

@section('content')
    <div class="loader" id="loader">
        <div class="loader-inner">
            <div class="loader-square"></div>
            <div class="loader-square"></div>
            <div class="loader-square"></div>
            <div class="loader-square"></div>
        </div>
    </div>
    <div class="col-md-12 text-center projects-index-intro">
        <h1>Explore Awesome<br class="only-mobile"> Crowdfunding Ideas!</h1>
        <div class="explore-ideas-line"></div>
        <h3>
            Discover the latest Crowdfunding Campaigns.<br>
            Find the Products you've been looking for.
        </h3>
    </div>
    <div class="projects-main-carousel">
        <div id="main-carousel" class="carousel slide carousel-fade" data-ride="carousel">
            <div class="carousel-inner">
                <?php
                $slideCount = 0;
                ?>
                @foreach($slides as $slide)
                    <?php
                    $slideCount++;

                    if($slide->project->projectSync != null){
                        $project_proc = floor((( $slide->project->current_amount + $slide->project->projectSync->amount ) / $slide->project->project_goal)*100);
                        $project_raised_amount = $slide->project->current_amount + $slide->project->projectSync->amount;
                    } else {
                        $project_proc = floor($slide->project->current_amount / $slide->project->project_goal * 100);
                        $project_raised_amount = $slide->project->current_amount;
                    }

                    $start_date = time() /60/60/24;
                    $end_date = $slide->project->end_date /60/60/24;
                    $days_left = floor($end_date - $start_date);
                    ?>
                    <div class="carousel-item text-center slider-div-container">
                        <div class="row">
                            <div class="col-md-6 slider-col-left text-right" style="background-image:linear-gradient( rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)), url('{{ asset($slide->background1) }}')">
                                <div class="slider-div slider-div-left">
                                    <h2>{{ $slide->project->title }}</h2>
                                    <a href="{{ route('project.show',['id' => $slide->project->id,'slug' => $slide->project->slug]) }}" class="featured-project-button">View</a>
                                </div>
                                <div class="slider-projects-info-card text-center only-mobile">
                                    <p class="slider-projects-days-left">@if($days_left > 0){{ $days_left }} days left! @elseif($days_left <= 0 && $project_proc >= 100) Purchase now! @elseif($days_left <= 0 && $project_proc < 100) Ended @endif</p>
                                    <div class="project-bar project-index-proc-bar">
                                        <div class="inner-bar" style="width:{{ $project_proc > 100 ? 100 : $project_proc }}%"><span class="bar-percent">{{ $project_proc ? $project_proc : 0 }}%</span></div>
                                    </div>
                                    <p class="slider-projects-amounts"><span>${{ number_format($project_raised_amount) }} </span>Raised  </p>
                                    <div class="sep-line-100"></div>
                                    <p class="slider-projects-amounts"><span>${{ number_format($slide->project->project_goal) }} </span>Goal </p>
                                </div>
                            </div>
                            <div class="col-md-6 slider-col-right text-left hide-on-mobile" style="background-image:linear-gradient( rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.7)), url('{{ asset($slide->background2) }}')">
                                <div class="slider-div slider-div-right">
                                    <div class="slider-project-info">
                                        <div class="slider-projects-info-card">
                                            <p class="slider-projects-days-left">@if($days_left > 0){{ $days_left }} days left! @elseif($days_left <= 0 && $project_proc >= 100) Purchase now! @elseif($days_left <= 0 && $project_proc < 100) Ended @endif</p>
                                            <div class="project-bar project-index-proc-bar">
                                                <div class="inner-bar" style="width:{{ $project_proc > 100 ? 100 : $project_proc }}%"><span class="bar-percent">{{ $project_proc ? $project_proc : 0 }}%</span></div>
                                            </div>
                                            <p class="slider-projects-amounts"><span>${{ number_format($project_raised_amount) }} </span>Raised  </p>
                                            <div class="sep-line-100"></div>
                                            <p class="slider-projects-amounts"><span>${{ number_format($slide->project->project_goal) }} </span>Goal </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <script>
                $( document ).ready(function() {
                    $('#main-carousel').find('.carousel-item').first().addClass('active');
                });
            </script>
            <a class="carousel-control-prev" href="#main-carousel" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#main-carousel" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
    <div class="container wider-container">
        <div class="row">
            <div class="col-md-12">
                <div class="sliders-menu">
                    <a class="projects-text-filter projects-text-filter-active" id="featured-projects-btn">Featured</a>
                    <a class="projects-text-filter" id="featured-products-btn">Products</a>
                    <a class="projects-text-filter" id="trending-projects-btn">Trending</a>
                </div>
                <script type="text/javascript">
                    $(document).ready(function(){
                        $('.sliders-menu').slick({
                            infinite: true,
                            slidesToShow: 3,
                            slidesToScroll: 1,
                            variableWidth: true,
                            arrows:false,
                            responsive: [
                                {
                                    breakpoint: 1140,
                                    settings: {
                                        slidesToShow: 3,
                                        slidesToScroll: 1,
                                        infinite: true
                                    }
                                },
                                {
                                    breakpoint: 960,
                                    settings: {
                                        slidesToShow: 2,
                                        slidesToScroll: 1,
                                        infinite: true
                                    }
                                },
                            ]
                        });
                    });
                </script>
            </div>
        </div>
        <div class="row">

            <div class="col-md-12" id="featured-projects">
                <div class="slick-controls">
                    <button type="button" class="slick-prev slick-sliders-arrows"><i class="fas fa-angle-left"></i></button>
                    <button type="button" class="slick-next slick-sliders-arrows"><i class="fas fa-angle-right"></i></button>
                </div>
                <div class="multiple-items">
                    @foreach($featured_projects as $project)
                        <div class="project-slick">
                            <div>
                                {{--<a href="{{ route('projects') }}?project_category=1" class="project-category">{{ $project->category->name }}</a>--}}
                                <?php

                                if($project->projectSync){
                                    $project_proc = floor((( $project->current_amount + $project->projectSync->amount ) / $project->project_goal)*100);
                                } else {
                                    $project_proc = floor($project->current_amount / $project->project_goal * 100);
                                }

                                $start_date = time() /60/60/24;
                                $end_date = $project->end_date /60/60/24;
                                $days_left = floor($end_date - $start_date);
                                ?>
                                {{--<span class="project-like"><i class="fas fa-heart"></i></span>--}}
                            </div>
                            <a href="{{ route('project.show',[ 'id' => $project->id,'slug' => $project->slug ]) }}">
                                <img
                                    onerror="this.onerror=null; this.src='{{asset('/uploads/default-img-logo.png')}}';"
                                    src="{{ asset($project->image) }}"
                                    width="100%"
                                    alt="crowdfunding-project"
                                />
                            </a>
                            <div class="project-content">
                                <a href="{{ route('project.show',[ 'id' => $project->id,'slug' => $project->slug ]) }}"><h2 class="project-title"> {{ $project->title }}</h2></a>
                                <?php
                                if (strlen($project->subtitle) > 130){
                                    $subtitle = substr($project->subtitle, 0,120) . '...';
                                }else{
                                    $subtitle = $project->subtitle;
                                }

                                ?>

                                <a href="{{ route('project.show',[ 'id' => $project->id,'slug' => $project->slug ]) }}"><p class="project-subtitle">{{ $subtitle }}</p></a>

                                <div class="project-bar project-index-proc-bar">
                                    @if(($project->current_amount + ($project->projectSync->amount ?? 0) < $project->project_goal) && $project->end_date < time())
                                        <div class="inner-bar" style="width:100%"><span class="bar-percent">Unsuccessful</span></div>
                                    @else
                                        <div class="inner-bar" style="width:{{ $project_proc > 100 ? 100 : $project_proc }}%"><span class="bar-percent">{{ $project_proc }}%</span></div>
                                    @endif
                                </div>
                                <p class="project-funds"><span class="current_amount">$@if($project->projectSync){{ number_format($project->current_amount + $project->projectSync->amount,2) }} @else {{ number_format($project->current_amount) }} @endif</span> <span class="project_goal float-right">${{ number_format($project->project_goal) }}</span></p>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <script type="text/javascript">
                $(document).ready(function(){
                    $('.multiple-items').slick({
                        infinite: true,
                        slidesToShow: 4,
                        slidesToScroll: 1,
                        arrows:true,
                        prevArrow:'.slick-prev',
                        nextArrow:'.slick-next',
                        responsive: [
                            {
                                breakpoint: 1140,
                                settings: {
                                    slidesToShow: 3,
                                    slidesToScroll: 1,
                                    infinite: true,
                                    dots: true
                                }
                            },
                            {
                                breakpoint: 960,
                                settings: {
                                    slidesToShow: 2,
                                    slidesToScroll: 1,
                                    infinite: true,
                                    dots: true
                                }
                            },
                            {
                                breakpoint: 600,
                                settings: {
                                    slidesToShow: 1,
                                    slidesToScroll: 1
                                }
                            }
                        ]
                    });
                });
            </script>
            <div class="col-md-12" id="featured-products">
                <div class="slick-controls">
                    <button type="button" class="slick-prev-product slick-sliders-arrows"><i class="fas fa-angle-left"></i></button>
                    <button type="button" class="slick-next-product slick-sliders-arrows"><i class="fas fa-angle-right"></i></button>
                </div>
                <div class="multiple-products">
                    @foreach($featured_products as $product)
                        <div class="product">
                            <img
                                onerror="this.onerror=null; this.src='{{asset('/uploads/default-img-logo.png')}}';"
                                src="{{ $product->image }}"
                                width="100%"
                            />
                            @if($product->link == null)
                                <span class="exclusive-product"><i class="fas fa-award"></i></span>
                            @endif
                            <div class="product-description">
                                <h3>{{ $product->title }}</h3>
                                <?php
                                if (strlen(strip_tags($product->description)) > 150){
                                    $str = substr(strip_tags($product->description), 0, 147) . '...';
                                } else {
                                    $str = strip_tags($product->description);
                                }

                                ?>
                                <p>{!! $str !!}</p>
                            </div>
                            <p class="product-price text-center">${{ $product->price }}</p>
                            <div class="text-center">
                                @if($product->link != null)
                                    <a class="normal-btn" href="{{ $product->link }}">View Product</a>
                                @else
                                    <a class="normal-btn" href="{{ route('product.show',['id'=>$product->id]) }}">View Product</a>
                                @endif
                            </div>
                        </div>
                    @endforeach
                </div>
                <script>
                    $( document ).ready(function() {
                        $('.multiple-products').slick({
                            infinite: true,
                            slidesToShow: 4,
                            slidesToScroll: 1,
                            arrows: true,
                            prevArrow: '.slick-prev-product',
                            nextArrow: '.slick-next-product',
                            responsive: [
                                {
                                    breakpoint: 1140,
                                    settings: {
                                        slidesToShow: 3,
                                        slidesToScroll: 1,
                                        infinite: true,
                                        dots: true
                                    }
                                },
                                {
                                    breakpoint: 960,
                                    settings: {
                                        slidesToShow: 2,
                                        slidesToScroll: 1,
                                        infinite: true,
                                        dots: true
                                    }
                                },
                                {
                                    breakpoint: 600,
                                    settings: {
                                        slidesToShow: 1,
                                        slidesToScroll: 1
                                    }
                                }
                            ]
                        });
                    });
                </script>
            </div>
            <div class="col-md-12" id="trending-projects">
                <div class="slick-controls">
                    <button type="button" class="slick-prev-trending slick-sliders-arrows"><i class="fas fa-angle-left"></i></button>
                    <button type="button" class="slick-next-trending slick-sliders-arrows"><i class="fas fa-angle-right"></i></button>
                </div>
                <div class="multiple-trending">
                    @foreach($trending_projects as $project)
                        <div class="project-slick">
                            <div>
                                {{--<a href="{{ route('projects') }}?project_category=1" class="project-category">{{ $project->category->name }}</a>--}}
                                <?php

                                if($project->projectSync){
                                    $project_proc = floor((( $project->current_amount + $project->projectSync->amount ) / $project->project_goal)*100);
                                } else {
                                    $project_proc = floor($project->current_amount / $project->project_goal * 100);
                                }

                                $start_date = time() /60/60/24;
                                $end_date = $project->end_date /60/60/24;
                                $days_left = floor($end_date - $start_date);
                                ?>
                                {{--<span class="project-like"><i class="fas fa-heart"></i></span>--}}
                            </div>
                            <a href="{{ route('project.show',[ 'id' => $project->id,'slug' => $project->slug ]) }}">
                                <img
                                    src="{{ asset($project->image) }}"
                                    width="100%"
                                    alt="crowdfunding-project"
                                    onerror="this.onerror=null; this.src='{{asset('/uploads/default-img-logo.png')}}';"
                                />
                            </a>
                            <div class="project-content">
                                <a href="{{ route('project.show',[ 'id' => $project->id,'slug' => $project->slug ]) }}"><h2 class="project-title"> {{ $project->title }}</h2></a>
                                <?php
                                if (strlen($project->subtitle) > 130){
                                    $subtitle = substr($project->subtitle, 0,120) . '...';
                                }else{
                                    $subtitle = $project->subtitle;
                                }

                                ?>

                                <a href="{{ route('project.show',[ 'id' => $project->id,'slug' => $project->slug ]) }}"><p class="project-subtitle">{{ $subtitle }}</p></a>

                                <div class="project-bar project-index-proc-bar">
                                    @if(($project->current_amount + ($project->projectSync->amount ?? 0) < $project->project_goal) && $project->end_date < time())
                                        <div class="inner-bar" style="width:100%"><span class="bar-percent">Unsuccessful</span></div>
                                    @else
                                        <div class="inner-bar" style="width:{{ $project_proc > 100 ? 100 : $project_proc }}%"><span class="bar-percent">{{ $project_proc }}%</span></div>
                                    @endif
                                </div>
                                <p class="project-funds"><span class="current_amount">$@if($project->projectSync){{ number_format($project->current_amount + $project->projectSync->amount) }} @else {{ number_format($project->current_amount) }} @endif</span> <span class="project_goal float-right">${{ number_format($project->project_goal) }}</span></p>
                            </div>
                        </div>
                    @endforeach
                </div>
                <script>
                    $( document ).ready(function() {
                        $('.multiple-trending').slick({
                            infinite: true,
                            slidesToShow: 4,
                            slidesToScroll: 1,
                            arrows: true,
                            prevArrow: '.slick-prev-trending',
                            nextArrow: '.slick-next-trending',
                            responsive: [
                                {
                                    breakpoint: 1140,
                                    settings: {
                                        slidesToShow: 3,
                                        slidesToScroll: 1,
                                        infinite: true,
                                        dots: true
                                    }
                                },
                                {
                                    breakpoint: 960,
                                    settings: {
                                        slidesToShow: 2,
                                        slidesToScroll: 1,
                                        infinite: true,
                                        dots: true
                                    }
                                },
                                {
                                    breakpoint: 600,
                                    settings: {
                                        slidesToShow: 1,
                                        slidesToScroll: 1
                                    }
                                }
                            ]
                        });
                    });
                </script>
            </div>
        </div>
        <script>
            $( document ).ready(function() {
                $('#featured-projects-btn').on('click',function(e){
                    e.preventDefault();


                    $('#featured-projects').fadeIn();
                    $('#featured-products').css('display','none');
                    $('#trending-projects').css('display','none');

                    $(this).addClass('projects-text-filter-active');
                    $('#featured-products-btn').removeClass('projects-text-filter-active');
                    $('#trending-projects-btn').removeClass('projects-text-filter-active');
                });

                $('#featured-products-btn').on('click',function(e){
                    e.preventDefault();
                    $('#featured-projects').css('display','none');
                    $('#featured-products').fadeIn();
                    $('#trending-projects').css('display','none');
                    $('#featured-products').css('height','auto');
                    $('#featured-products').css('visibility','visible');

                    $(this).addClass('projects-text-filter-active');
                    $('#featured-projects-btn').removeClass('projects-text-filter-active');
                    $('#trending-projects-btn').removeClass('projects-text-filter-active');
                });

                $('#trending-projects-btn').on('click',function(e){
                    e.preventDefault();
                    $('#featured-projects').css('display','none');
                    $('#featured-products').css('display','none');
                    $('#trending-projects').fadeIn();
                    $('#trending-projects').css('height','auto');
                    $('#trending-projects').css('visibility','visible');

                    $(this).addClass('projects-text-filter-active');
                    $('#featured-projects-btn').removeClass('projects-text-filter-active');
                    $('#featured-products-btn').removeClass('projects-text-filter-active');
                });
            });
        </script>
        @if($filter != null)
            <script>
                $( document ).ready(function() {
                    $('html, body').animate({
                        scrollTop: $("#projects-container").offset().top
                    }, 1000);
                });
            </script>
        @endif
        <div class="project-filters">
            <div class="search-form project-index-search">
                <div class="projects-search">
                    <label for="project_query" class="sr-only">Search</label>
                    <input type="text" class="form-control" name="project_query" id="project_query" placeholder="Search for projects...">
                </div>
            </div>

            <button id="toggle-filters">Toggle Filters</button>
            <a id="reset-filters" href="{{Route('projects')}}">Reset Filters</a>


            <div class="filters-container">
                <select id="projects-filtering" onchange="updateQueryStringParameter('general')">
                    <option value="">Filters (All Projects)</option>
                    <option value="exclusive">Exclusive</option>
                    <option value="new">New</option>
                    <option value="funded">Fully Funded</option>
                    <option value="trending">Trending</option>
                    <option value="almost">Almost Funded</option>
                    <option value="purchase-now">Purchase Now</option>
                    <option value="ending">Ending Soon</option>
                    <option value="ended">Ended</option>
                </select>
                <select id="projects-tags" onchange="updateQueryStringParameterForTags('tags')">
                    <option value="">Tags (All Projects)</option>
                    <option value="tech">#Tech</option>
                    <option value="robotics">#Robotics</option>
                    <option value="software">#Software</option>
                    <option value="home">#Home</option>
                    <option value="lifestyle">#Lifestyle</option>
                    <option value="health">#Health</option>
                </select>
                <select id="projects-categories" onchange="updateQueryStringParameter('category')">
                    <option value="">All Categories</option>
                    @foreach($categories as $category)
                        <option value="{{$category->id}}">{{ $category->name }}</option>
                    @endforeach
                </select>
            </div>
            <script>
                $("#project_query").on('keyup', function (e) {
                    if (e.keyCode == 13) {
                        updateQueryStringParameter('query',this.value);
                    }
                });

                $("#toggle-filters").on('click',function(e){
                    $(".filters-container").toggle();
                });

                function updateQueryStringParameterForTags(key) {
                    let value = document.getElementById("projects-tags").value;

                    let uri = window.location.href;
                    if(!uri.includes(key+'[]='+value)){
                        console.log('no match');
                        let separator = uri.indexOf('?') !== -1 ? "&" : "?";
                        uri = uri + separator + key + "[]=" + value;
                        window.location.href = uri;
                    }else{
                        console.log('Tag exists')
                    }
                }


                function updateQueryStringParameter(key,value=null) {

                    if(key === "category"){
                        value = document.getElementById("projects-categories").value;
                    }
                    if(key === "general"){
                        value = document.getElementById("projects-filtering").value
                    }
                    let uri = window.location.href;
                    let re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
                    let separator = uri.indexOf('?') !== -1 ? "&" : "?";
                    if (uri.match(re)) {
                        uri = uri.replace(re, '$1' + key + "=" + value + '$2');
                        window.location.href = uri;
                    } else {
                        uri = uri + separator + key + "=" + value;
                        window.location.href = uri;
                    }
                }
            </script>
        </div>
        <div>
            <div class="row projects-container" id="projects-container">
                <div class="all-projects col-md-12" id="all-projects">
                        <div class="grid">
                            @if(count($projects))
                                @foreach($projects as $project)
                                    <div class="grid-item">
                                        <div class="project">
                                            <div>
                                                <?php

                                                if($project->projectSync){
                                                    $project_proc = floor((( $project->current_amount + $project->projectSync->amount ) / $project->project_goal)*100);
                                                } else {
                                                    $project_proc = floor($project->current_amount / $project->project_goal * 100);
                                                }

                                                $start_date = time() /60/60/24;
                                                $end_date = $project->end_date /60/60/24;
                                                $days_left = floor($end_date - $start_date);
                                                ?>
                                                @if(count($project->products))
                                                    <a href="{{ route('project.products',['id' => $project->id]) }}" class="days-left status-purchase-now"><i class="fas fa-shopping-cart"></i></a>
                                                @elseif($project->affiliate_link)
                                                    <a rel="nofollow" target="_blank" href="{{$project->affiliate_link}}"><span class="days-left status-ongoing"><span class="buy-now">Buy</span><hr><span class="buy-now">now!</span></span></a>
                                                @elseif($days_left > 0)
                                                    <span class="days-left status-ongoing"><span class="days-left-number">{{ $days_left }}</span><hr><span class="days-left-text">days left</span></span>
                                                @elseif($days_left <= 0 && $project_proc >= 100)
                                                    <a href="{{ route('project.show',[ 'id' => $project->id,'slug' => $project->slug ]) }}"><span class="days-left status-ongoing"><span class="buy-now">Buy</span><hr><span class="buy-now">now!</span></span></a>
                                                @elseif($days_left <= 0 && $project_proc < 100)
                                                    <span class="days-left status-ongoing"><span class="ended">Ended</span></span>
                                                @endif

                                            </div>
                                            <a href="{{ route('project.show',[ 'id' => $project->id,'slug' => $project->slug ]) }}">
                                                <img
                                                    onerror="this.onerror=null; this.src='{{asset('/uploads/default-img-logo.png')}}';"
                                                    src="{{ asset($project->image) }}"
                                                    width="100%"
                                                    alt="crowdfunding-project"
                                                />
                                            </a>
                                                <div class="project-content">
                                                    <a href="{{ route('project.show',[ 'id' => $project->id,'slug' => $project->slug ]) }}"><h2 class="project-title"> {{ $project->title }}</h2></a>
                                                <?php
                                                if (strlen($project->subtitle) > 130){
                                                    $subtitle = substr($project->subtitle, 0,120) . '...';
                                                }else{
                                                    $subtitle = $project->subtitle;
                                                }

                                                ?>

                                                <div class="project-bar project-index-proc-bar">
                                                    @if(($project->current_amount + ($project->projectSync->amount ?? 0) < $project->project_goal) && $project->end_date < time())
                                                        <div class="inner-bar" style="width:100%"><span class="bar-percent">Unsuccessful</span></div>
                                                    @else
                                                        <div class="inner-bar" style="width:{{ $project_proc > 100 ? 100 : $project_proc }}%"><span class="bar-percent">{{ $project_proc }}%</span></div>
                                                    @endif
                                                </div>
                                                <div class="project-index-stats-container">
                                                    <div class="project-index-stats-left text-left">
                                                        <span class="current_amount">$@if($project->projectSync){{ number_format($project->current_amount + $project->projectSync->amount) }} @else {{ number_format($project->current_amount) }} @endif</span>
                                                    </div>
                                                    <div class="project-index-stats-center">
                                                        <a href="{{ route('project.show',[ 'id' => $project->id,'slug' => $project->slug ]) }}" class="project-index-btn">View</a>
                                                        <a href="#project-modal{{$project->id}}" class="project-index-btn popup-modal">Preview</a>
                                                    </div>
                                                    <div class="project-index-stats-right text-right">
                                                        <span class="project_goal">${{ number_format($project->project_goal) }}</span>
                                                    </div>
                                                </div>
{{--                                                <div class="project-bar project-index-proc-bar">--}}
{{--                                                    <div class="inner-bar" style="width:{{ $project_proc > 100 ? 100 : $project_proc }}%"><span class="bar-percent"></span></div>--}}
{{--                                                </div>--}}
                                            </div>
                                        </div>
                                    </div>
                                    <div id="project-modal{{$project->id}}" class="white-popup-block mfp-hide project-index-popup-content popup-modal-dismiss">
                                        <?php
                                            if($project->slides == null){
                                                $project_image = $project->image;
                                            } else {
                                                $project_image = explode(',',$project->slides);
                                                $project_image = $project_image[0];
                                            }

                                        ?>
                                        <div class="project-index-modal">
                                            <div class="bottom-space featured-image-container">
                                                <a href="{{ route('project.show',[ 'id' => $project->id,'slug' => $project->slug ]) }}">
                                                    <img
                                                        onerror="this.onerror=null; this.src='{{asset('/uploads/default-img-logo.png')}}';"
                                                        src="{{ asset($project_image) }}"
                                                        alt="crowdfunding-project-image"
                                                        class="featured-image"
                                                    />
                                                </a>
                                            </div>
                                            <a href="{{ route('project.show',[ 'id' => $project->id,'slug' => $project->slug ]) }}">
                                                <h2 class="preview-title">{{ $project->title }}</h2>
                                            </a>
                                            <a href="{{ route('project.show',[ 'id' => $project->id,'slug' => $project->slug ]) }}">
                                                <h3 class="preview-subtitle">{{ $project->subtitle }}</h3>
                                            </a>


                                            <div class="fancy-hr"></div>
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <div class="jumbotron comments-section" id="comments-section">
                                                        @if($project->comments)
                                                            @foreach($project->comments as $comment)
                                                                <div class="row project-comment-row" data-comment-id="{{ $comment->id }}">
                                                                    <div class="col-md-3 text-center">
                                                                        <a href="{{ route('profile.show',['id' => $comment->user->profile->id, 'slug' => $comment->user->profile->slug]) }}">
                                                                            <img width="50" height="50" class="comments-avatar" src="{{ asset( $comment->user->profile->avatar ) }}" alt="crowdfunding-user-activity">
                                                                            <p class="comment-author">{{ $comment->user->name }}</p>
                                                                        </a>
                                                                    </div>
                                                                    <div class="col-md-7">
                                                                        <p class="comment-content">{!! $comment->comment !!}</p>
                                                                    </div>
                                                                    <div class="col-md-2">
                                                                        <div class="comment-stats">
                                                                            <span class="likes-number"><span id="likes-number{{$comment->id}}">{{ $comment->commentLikes->count() }}</span> likes</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="project-details levels-container">
                                                        <img class="project-avatar" src="{{ asset( $project->user->profile->avatar ) }}" alt="crowdfunding-project-author">
                                                        <a href="{{ route('profile.show',['id' => $project->user->profile->id, 'slug' => $project->user->profile->slug]) }}" class="project-author"><p>{{ $project->user->name }}</p></a>
                                                        <div class="project-info">
                                                            <div class="detail-item">
                                                                <a href="#" data-toggle="modal" data-target="#myModal">
                                                                    <p class="project-detail">{{ count($project->backers) + isset($project->projectSync) ? $project->projectSync->backers : 0 }}</p>
                                                                    <small>backers</small>
                                                                </a>
                                                            </div>
                                                            <div class="detail-item">
                                                                <p class="project-detail">${{ number_format($project->current_amount + isset($project->projectSync) ? $project->projectSync->amount : 0) }}</p>
                                                                <small>out of ${{ number_format($project->project_goal) }}</small>
                                                            </div>
                                                            @if($project->end_date > time())
                                                                <div class="detail-item">
                                                                    <p class="project-detail">{{ date('F Y',$project->end_date) }}</p>
                                                                    <small>is the end date.</small>
                                                                </div>
                                                            @else
                                                                <div class="detail-item">
                                                                    <p class="project-detail">Ended</p>
                                                                    <small>on {{ date('F Y',$project->end_date) }}</small>
                                                                </div>
                                                            @endif
                                                        </div>
                                                        <div class="project-bar">
                                                            <div class="inner-bar" style="width:{{ floor((( $project->current_amount + isset($project->projectSync) ? $project->projectSync->amount : 0 ) / $project->project_goal)*100) }}%"><span class="bar-percent">{{ floor((( $project->current_amount + isset($project->projectSync) ? $project->projectSync->amount : 0 ) / $project->project_goal)*100) }}%</span></div>
                                                        </div>

                                                        <div class="project-share">
                                                            <a href="http://www.facebook.com/sharer.php?u={{url()->current()}}" target="_blank">
                                                                <i style="background-color:#37569b" class="fab fa-facebook-f"></i>
                                                            </a>
                                                            <a href="http://pinterest.com/pin/create/button/?url={{url()->current()}}&media={{ urlencode(asset($project->image)) }}&description={{ urlencode($project->title) }}" target="_blank">
                                                                <i style="background-color:#e60023" class="fab fa-pinterest-p"></i>
                                                            </a>
                                                            <a href="https://twitter.com/share?url={{url()->current()}}" target="_blank">
                                                                <i style="background-color:#00acf0" class="fab fa-twitter"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12"><a class="project-index-backing-btn" href="{{ route('project.show',[ 'id' => $project->id,'slug' => $project->slug ]) }}?backing-intent">Back this project <i class="fas fa-arrow-right"></i></a></div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                    <div class="grid-item">
                                        <div class="project project-index-create-project">
                                            <p class="text-center tile-title">Crowdfunder?</p>
                                            <div class="text-center">
                                                <a href="{{ route('project.create-start') }}" class="fancy-btn">Create a Project</a>
                                            </div>
                                        </div>
                                    </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <button class="scroll-top"><i class="fas fa-angle-double-up"></i></button>
    <script>
        $(function () {
            $('.popup-modal').magnificPopup({
                type: 'inline',
                preloader: false,
                focus: '#username',
                modal: true
            });

            $(document).on('click', '.popup-modal-dismiss', function (e) {
                $.magnificPopup.close();
            });
        });
    </script>
    <script>
        var masonry;
        $( document ).ready(function() {

            $(document).scroll(function() {
                var y = $(this).scrollTop();
                if (y > 1200) {
                    $('.scroll-top').show();
                } else {
                    $('.scroll-top').hide();
                }
            });

            $('.scroll-top').on('click',function(){
                $('html, body').animate({
                    scrollTop: $("body").offset().top
                }, 1000);
            });



            var $grid = $('.grid').imagesLoaded( function() {
                // init Masonry after all images have loaded
                $grid.masonry({
                    itemSelector: '.grid-item',
                });
                $('#loader').fadeOut( "slow" );
                setTimeout(function(){
                    $('#loader').remove();
                }, 1000);
            });
            masonry = $grid;




            var processing;
            var fromIndex = 2;
            var projectsContiner = $('.grid');
            var popupDisplayed = false;
            let separator = window.location.href.indexOf('?') !== -1 ? "&" : "?";

            $(document).scroll(function(e){

                if (processing)
                    return false;

                if ($(window).scrollTop() >= ($(document).height() - $(window).height())*0.7){
                    processing = true;

                    $.ajax({
                        type: "GET",
                        url: "/load-more-projects"+window.location.search+separator+"page="+fromIndex,
                        success: function (result) {

                            fromIndex = fromIndex + 1;

                            var content = $(result);

                            content.imagesLoaded( function() {
                                $grid.append( content ).masonry( 'appended', content );
                            });

                            $(function () {
                                $('.popup-modal').magnificPopup({
                                    type: 'inline',
                                    preloader: false,
                                    focus: '#username',
                                    modal: true
                                });
                                $(document).on('click', '.popup-modal-dismiss', function (e) {
                                    $.magnificPopup.close();
                                });
                            });

                            processing = false;
                        },
                        error: function (result) {
                            console.log(result);
                            alert('Something went wrong.');
                        },
                    });
                }

                // var elementTarget = document.getElementById("all-projects");
                // if (popupDisplayed === false && window.scrollY > (elementTarget.offsetTop)) {
                //     var crowdfundThisContainer = $('.crowdfunding-this-background');
                //     crowdfundThisContainer.show();
                //
                //     $(crowdfundThisContainer).on('click',function(){
                //         $(this).hide();
                //     });
                //
                //     // Align popup on center
                //     var crowdfundThis = $('.crowdfunding-this');
                //     crowdfundThis.css("top", ( $(window).height() - $(crowdfundThis).outerHeight() ) / 2 + "px");
                //     crowdfundThis.css("left", Math.max(0, (($(window).width() - $(crowdfundThis).outerWidth()) / 2) +
                //         $(window).scrollLeft()) + "px");
                //
                //     popupDisplayed = true;
                // }


            });

        });
    </script>
@endsection
