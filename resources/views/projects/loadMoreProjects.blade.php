@foreach($projects as $project)
    @if(isset($project->title))
        <div class="grid-item">
            <div class="project">
                <div>
                    <?php

                    if($project->projectSync){
                        $project_proc = floor((( $project->current_amount + $project->projectSync->amount ) / $project->project_goal)*100);
                    } else {
                        $project_proc = floor($project->current_amount / $project->project_goal * 100);
                    }

                    $start_date = time() /60/60/24;
                    $end_date = $project->end_date /60/60/24;
                    $days_left = floor($end_date - $start_date);
                    ?>
                    {{--@if($days_left < 1 && $project_proc < 100)--}}
                    {{--<span class="days-left status-failed"><i class="fas fa-times"></i></span>--}}
                    {{--@elseif($days_left < 1)--}}
                    {{--<span class="days-left status-completed"><i class="fas fa-check"></i></span>--}}
                    {{--@else--}}
                    {{--<span class="days-left status-ongoing"><span class="days-left-number">{{ $days_left }}</span><hr><span class="days-left-text">days left</span></span>--}}
                    {{--@endif--}}
                    @if(count($project->products))
                        <a href="{{ route('project.products',['id' => $project->id]) }}" class="days-left status-purchase-now"><i class="fas fa-shopping-cart"></i></a>
                    @else
                        @if($days_left >= 0)
                            <span class="days-left status-ongoing"><span class="days-left-number">{{ $days_left }}</span><hr><span class="days-left-text">days left</span></span>
                        @else
                            <span class="days-left status-ongoing"><span class="days-left-number">0</span><hr><span class="days-left-text">days left</span></span>
                        @endif
                    @endif
                </div>
                <a href="{{ route('project.show',[ 'id' => $project->id,'slug' => $project->slug ]) }}">
                    <img
                        onerror="this.onerror=null; this.src='{{asset('/uploads/default-img-logo.png')}}';"
                        src="{{ asset($project->image) }}"
                        width="100%"
                        alt="crowdfunding-project"
                    />
                </a>
                <div class="project-content">
                    <a href="{{ route('project.show',[ 'id' => $project->id,'slug' => $project->slug ]) }}"><h2 class="project-title"> {{ $project->title }}</h2></a>
                    <?php
                    if (strlen($project->subtitle) > 130){
                        $subtitle = substr($project->subtitle, 0,120) . '...';
                    }else{
                        $subtitle = $project->subtitle;
                    }

                    ?>
                    {{--                                                <a href="{{ route('project.show',[ 'id' => $project->id,'slug' => $project->slug ]) }}"><p class="project-subtitle">{{ $subtitle }}</p></a>--}}


                    <div class="project-bar project-index-proc-bar">
                        <div class="inner-bar" style="width:{{ $project_proc > 100 ? 100 : $project_proc }}%"><span class="bar-percent">{{ $project_proc }}%</span></div>
                    </div>


                    {{--                                                <p class="project-funds"><span class="current_amount">$@if($project->projectSync){{ number_format($project->current_amount + $project->projectSync->amount) }} @else {{ number_format($project->current_amount) }} @endif</span> <span class="project_goal float-right">${{ number_format($project->project_goal) }}</span></p>--}}
                    {{--                                                <p class="project-funds-text-container">--}}
                    {{--                                                    <span class="current_amount_text">Pledged</span>--}}
                    {{--                                                    <span class="project-goal">Goal</span>--}}
                    {{--                                                </p>--}}
                    <div class="project-index-stats-container">
                        <div class="project-index-stats-left text-left">
                            <span class="current_amount">$@if($project->projectSync){{ number_format($project->current_amount + $project->projectSync->amount) }} @else {{ number_format($project->current_amount) }} @endif</span>
                        </div>
                        <div class="project-index-stats-center">
                            <a href="{{ route('project.show',[ 'id' => $project->id,'slug' => $project->slug ]) }}" class="project-index-btn">View</a>
                            <a href="#project-modal{{$project->id}}" class="project-index-btn popup-modal">Preview</a>
                        </div>
                        <div class="project-index-stats-right text-right">
                            <span class="project_goal">${{ number_format($project->project_goal) }}</span>
                        </div>
                    </div>
                    {{--                                                <div class="project-bar project-index-proc-bar">--}}
                    {{--                                                    <div class="inner-bar" style="width:{{ $project_proc > 100 ? 100 : $project_proc }}%"><span class="bar-percent"></span></div>--}}
                    {{--                                                </div>--}}
                </div>
            </div>
        </div>
        <div id="project-modal{{$project->id}}" class="white-popup-block mfp-hide project-index-popup-content popup-modal-dismiss">
            <?php
            if($project->slides == null){
                $project_image = $project->image;
            } else {
                $project_image = explode(',',$project->slides);
                $project_image = $project_image[0];
            }

            ?>
            <div class="project-index-modal">
                <div class="bottom-space featured-image-container">
                    <a href="{{ route('project.show',[ 'id' => $project->id,'slug' => $project->slug ]) }}"><img src="{{ asset($project_image) }}" alt="crowdfunding-project-image" class="featured-image"></a>
                </div>
                <a href="{{ route('project.show',[ 'id' => $project->id,'slug' => $project->slug ]) }}">
                    <h2 class="preview-title">{{ $project->title }}</h2>
                </a>
                <a href="{{ route('project.show',[ 'id' => $project->id,'slug' => $project->slug ]) }}">
                    <h3 class="preview-subtitle">{{ $project->subtitle }}</h3>
                </a>


                <div class="fancy-hr"></div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="jumbotron comments-section" id="comments-section">
                            @if($project->comments)
                                @foreach($project->comments as $comment)
                                    <div class="row project-comment-row" data-comment-id="{{ $comment->id }}">
                                        <div class="col-md-3 text-center">
                                            <a href="{{ route('profile.show',['id' => $comment->user->profile->id, 'slug' => $comment->user->profile->slug]) }}">
                                                <img width="50" height="50" class="comments-avatar" src="{{ asset( $comment->user->profile->avatar ) }}" alt="crowdfunding-user-activity">
                                                <p class="comment-author">{{ $comment->user->name }}</p>
                                            </a>
                                        </div>
                                        <div class="col-md-7">
                                            <p class="comment-content">{!! $comment->comment !!}</p>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="comment-stats">
                                                <span class="likes-number"><span id="likes-number{{$comment->id}}">{{ $comment->commentLikes->count() }}</span> likes</span>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="project-details levels-container">
                            <img class="project-avatar" src="{{ asset( $project->user->profile->avatar ) }}" alt="crowdfunding-project-author">
                            <a href="{{ route('profile.show',['id' => $project->user->profile->id, 'slug' => $project->user->profile->slug]) }}" class="project-author"><p>{{ $project->user->name }}</p></a>
                            <div class="project-info">
                                <div class="detail-item">
                                    <a href="#" data-toggle="modal" data-target="#myModal">
                                        <p class="project-detail">{{ count($project->backers) + $project->projectSync->backers }}</p>
                                        <small>backers</small>
                                    </a>
                                </div>
                                <div class="detail-item">
                                    <p class="project-detail">${{ number_format($project->current_amount + $project->projectSync->amount) }}</p>
                                    <small>out of ${{ number_format($project->project_goal) }}</small>
                                </div>
                                @if($project->end_date > time())
                                    <div class="detail-item">
                                        <p class="project-detail">{{ date('F Y',$project->end_date) }}</p>
                                        <small>is the end date.</small>
                                    </div>
                                @else
                                    <div class="detail-item">
                                        <p class="project-detail">Ended</p>
                                        <small>on {{ date('F Y',$project->end_date) }}</small>
                                    </div>
                                @endif
                            </div>
                            <div class="project-bar">
                                <div class="inner-bar" style="width:{{ floor((( $project->current_amount + $project->projectSync->amount ) / $project->project_goal)*100) }}%"><span class="bar-percent">{{ floor((( $project->current_amount + $project->projectSync->amount ) / $project->project_goal)*100) }}%</span></div>
                            </div>

                            <div class="project-share">
                                <a href="http://www.facebook.com/sharer.php?u={{url()->current()}}" target="_blank">
                                    <i style="background-color:#37569b" class="fab fa-facebook-f"></i>
                                </a>
                                <a href="http://pinterest.com/pin/create/button/?url={{url()->current()}}&media={{ urlencode(asset($project->image)) }}&description={{ urlencode($project->title) }}" target="_blank">
                                    <i style="background-color:#e60023" class="fab fa-pinterest-p"></i>
                                </a>
                                <a href="https://twitter.com/share?url={{url()->current()}}" target="_blank">
                                    <i style="background-color:#00acf0" class="fab fa-twitter"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12"><a class="project-index-backing-btn" href="{{ route('project.show',[ 'id' => $project->id,'slug' => $project->slug ]) }}?backing-intent">Back this project <i class="fas fa-arrow-right"></i></a></div>
                </div>
            </div>
        </div>
    @endif
    @if(isset($project['tag'])  && end($projects) != reset($projects))
        <div class="grid-item">
            <div class="project project-index-tile project-index-tag">
                <p class="text-center tag-title">Popluar Tag</p>
                <p class="text-center tag">{{ $project['tag'] }}</p>
                <div class="text-center">
                    <a href="{{ route('projects',['filter'=> 'tag', 'value' => $project['link'] ]) }}" class="fancy-btn">View Projects</a>
                </div>
            </div>
        </div>
    @endif
    @if(isset($project['random-card'])  && end($projects) != reset($projects))
        <div class="grid-item">
            <div class="project project-index-tile project-index-tag">
                <p class="text-center tag-title">Grow your Network!</p>
                <br>
                <div class="text-center">
                    <a href="{{ route('activity') }}" class="fancy-btn">Join our SocialCrowd</a>
                </div>
            </div>
        </div>
    @endif

    @if(isset($project['rank'])  && end($projects) != reset($projects))
        @switch($project['rank'])
            @case('influencer')
                <div class="grid-item">
                    <div class="flip-container">
                        <div class="flipper">
                            <div class="front front-project-tile artist-1">
                                <div class="project project-index-tile project-index-rank">
                                    <p class="text-center rank-title">Are you a Super Backer?</p>
                                    <p class="text-center rank">Get the title you deserve!</p>
                                    <div class="text-center">
                                        <form action="{{ route('need.help') }}" method="post">
                                            @csrf
                                            @if(Auth::check())
                                                <input type="hidden" name="email" value="t">
                                                <input type="hidden" name="user_id" value="{{ Auth::id() }}">
                                                <input type="hidden" name="message" value="I want to be an Influencner">
                                            @endif
                                            <button type="submit" class="fancy-btn become-influencer">Become an Influencer</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="back back-project-tile">
                                <div class="sponsored-activity menu-as-sponsored">
                                    <div class="left-sidebar-auth">
                                        <form action="{{ route('need.help') }}" method="post">
                                            @csrf
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Email address</label>
                                                <input type="email" class="form-control" name="email" placeholder="Enter email">
                                                <small id="emailHelp" class="form-text text-muted">We'll use this email address to contact you.</small>
                                            </div>
                                            <button type="submit" class="btn btn-dark become-influencer-guest">Send</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if(Auth::check() == false)
                        <script>
                            var clickedInfluencer = false;
                            $( document ).ready(function() {
                                $('.become-influencer').click(function(e){
                                    e.preventDefault();
                                    $(this).parent().parent().parent().parent().parent().toggleClass("flip");
                                });

                                $('.become-influencer-guest').click(function(e){
                                    e.preventDefault();
                                    var btnClicked = $(this);
                                    var guestEmail = btnClicked.prev().find('input').val();

                                    if(clickedInfluencer == false){
                                        clickedInfluencer = true;
                                        $.ajax({
                                            type: "Post",
                                            url: "/need-help",
                                            data: {
                                                _token: "{{ csrf_token() }}",
                                                email: guestEmail,
                                                message: "I want to become an influencer"
                                            },
                                            success: function(result) {
                                                console.log(guestEmail);
                                                btnClicked.parent().parent().parent().parent().parent().toggleClass("flip");
                                            },
                                            error: function(result) {
                                                console.log(result);
                                                alert("Ops, something went wrong");
                                            }
                                        });
                                    }
                                });


                            });
                        </script>
                    @elseif(Auth::check())
                        <script>
                            var clickedInfluencer = false;
                            $('.become-influencer').on('click',function(e){
                                var btnClicked = $(this);

                                e.preventDefault();
                                if(clickedInfluencer == false){
                                    clickedInfluencer = true;
                                    $.ajax({
                                        type: "Post",
                                        url: "/need-help",
                                        data: {
                                            _token: "{{ csrf_token() }}",
                                            user_id: {{Auth::id()}},
                                            email: "{{ Auth::user()->email }}",
                                            message: "I want to become an influencer"
                                        },
                                        success: function(result) {

                                            btnClicked.text('Request Sent. Thank you!');
                                        },
                                        error: function(result) {
                                            console.log(result);
                                            alert("Ops, something went wrong");
                                        }
                                    });
                                }
                            });
                        </script>
                    @endif
                </div>
            @break
            @case('service provider')
                <div class="grid-item">
                    <div class="flip-container">
                        <div class="flipper">
                            <div class="front front-project-tile artist-1">
                                <div class="project project-index-tile project-index-rank">
                                    <p class="text-center rank-title">Are you a Service Provider?</p>
                                    <p class="text-center rank">Sell your services on Locodor!</p>
                                    <div class="text-center">
                                        <form action="{{ route('need.help') }}" method="post">
                                            @csrf
                                            @if(Auth::check())
                                                <input type="hidden" name="email" value="t">
                                                <input type="hidden" name="user_id" value="{{ Auth::id() }}">
                                                <input type="hidden" name="message" value="I want to be a Service Provider">
                                            @endif
                                            <button type="submit" class="fancy-btn become-provider">Become a Service Provider</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="back back-project-tile">
                                <div class="sponsored-activity menu-as-sponsored">
                                    <div class="left-sidebar-auth">
                                        <form action="{{ route('need.help') }}" method="post">
                                            @csrf
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Email address</label>
                                                <input type="email" class="form-control" name="email" placeholder="Enter email">
                                                <small id="emailHelp" class="form-text text-muted">We'll use this email address to contact you.</small>
                                            </div>
                                            <button type="submit" class="btn btn-dark become-provider-guest">Send</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if(Auth::check() == false)
                        <script>
                            var clickedProvider = false;
                            $( document ).ready(function() {
                                $('.become-provider').click(function(e){
                                    e.preventDefault();
                                    $(this).parent().parent().parent().parent().parent().toggleClass("flip");
                                });

                                $('.become-provider-guest').click(function(e){
                                    e.preventDefault();
                                    var btnClicked = $(this);
                                    var guestEmail = btnClicked.prev().find('input').val();

                                    if(clickedProvider == false){
                                        clickedProvider = true;
                                        $.ajax({
                                            type: "Post",
                                            url: "/need-help",
                                            data: {
                                                _token: "{{ csrf_token() }}",
                                                email: guestEmail,
                                                message: "I want to become an influencer"
                                            },
                                            success: function(result) {
                                                console.log(guestEmail);
                                                btnClicked.parent().parent().parent().parent().parent().toggleClass("flip");
                                            },
                                            error: function(result) {
                                                console.log(result);
                                                alert("Ops, something went wrong");
                                            }
                                        });
                                    }
                                });


                            });
                        </script>
                    @elseif(Auth::check())
                        <script>
                            var clickedProvider = false;
                            $('.become-provider').on('click',function(e){
                                var btnClicked = $(this);

                                e.preventDefault();
                                if(clickedProvider == false){
                                    clickedProvider = true;
                                    $.ajax({
                                        type: "Post",
                                        url: "/need-help",
                                        data: {
                                            _token: "{{ csrf_token() }}",
                                            user_id: {{Auth::id()}},
                                            email: "{{ Auth::user()->email }}",
                                            message: "I want to become a Service Provider"
                                        },
                                        success: function(result) {

                                            btnClicked.text('Request Sent. Thank you!');
                                        },
                                        error: function(result) {
                                            console.log(result);
                                            alert("Ops, something went wrong");
                                        }
                                    });
                                }
                            });
                        </script>
                    @endif
                </div>
            @break
            @case('angel investor')
                <div class="grid-item">
                    <div class="flip-container">
                        <div class="flipper">
                            <div class="front front-project-tile artist-1">
                                <div class="project project-index-tile project-index-rank">
                                    <p class="text-center rank-title">Are you a Angel Investor?</p>
                                    <p class="text-center rank">Fund Ideas and make Profit!</p>
                                    <div class="text-center">
                                        <form action="{{ route('need.help') }}" method="post">
                                            @csrf
                                            @if(Auth::check())
                                                <input type="hidden" name="email" value="t">
                                                <input type="hidden" name="user_id" value="{{ Auth::id() }}">
                                                <input type="hidden" name="message" value="I want to be a Angel Investor">
                                            @endif
                                            <button type="submit" class="fancy-btn become-investor">Become a Angel Investor</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="back back-project-tile">
                                <div class="sponsored-activity menu-as-sponsored">
                                    <div class="left-sidebar-auth">
                                        <form action="{{ route('need.help') }}" method="post">
                                            @csrf
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Email address</label>
                                                <input type="email" class="form-control" name="email" placeholder="Enter email">
                                                <small id="emailHelp" class="form-text text-muted">We'll use this email address to contact you.</small>
                                            </div>
                                            <button type="submit" class="btn btn-dark become-investor-guest">Send</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if(Auth::check() == false)
                        <script>
                            var clickedInvestor = false;
                            $( document ).ready(function() {
                                $('.become-investor').click(function(e){
                                    e.preventDefault();
                                    $(this).parent().parent().parent().parent().parent().toggleClass("flip");
                                });

                                $('.become-investor-guest').click(function(e){
                                    e.preventDefault();
                                    var btnClicked = $(this);
                                    var guestEmail = btnClicked.prev().find('input').val();

                                    if(clickedInvestor == false){
                                        clickedInvestor = true;
                                        $.ajax({
                                            type: "Post",
                                            url: "/need-help",
                                            data: {
                                                _token: "{{ csrf_token() }}",
                                                email: guestEmail,
                                                message: "I want to become an Investor"
                                            },
                                            success: function(result) {
                                                console.log(guestEmail);
                                                btnClicked.parent().parent().parent().parent().parent().toggleClass("flip");
                                            },
                                            error: function(result) {
                                                console.log(result);
                                                alert("Ops, something went wrong");
                                            }
                                        });
                                    }
                                });
                            });
                        </script>
                    @elseif(Auth::check())
                        <script>
                            var clickedInvestor = false;
                            $('.become-investor').on('click',function(e){
                                var btnClicked = $(this);

                                e.preventDefault();
                                if(clickedInvestor == false){
                                    clickedInvestor = true;
                                    $.ajax({
                                        type: "Post",
                                        url: "/need-help",
                                        data: {
                                            _token: "{{ csrf_token() }}",
                                            user_id: {{Auth::id()}},
                                            email: "{{ Auth::user()->email }}",
                                            message: "I want to become an Angel Investor"
                                        },
                                        success: function(result) {

                                            btnClicked.text('Request Sent. Thank you!');
                                        },
                                        error: function(result) {
                                            console.log(result);
                                            alert("Ops, something went wrong");
                                        }
                                    });
                                }
                            });
                        </script>
                    @endif
                </div>
            @break
        @endswitch

    @endif
@endforeach
