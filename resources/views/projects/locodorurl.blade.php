@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <form action="{{ route('project.results.locodor') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="previous-url">Locodor Campaign URL</label>
                        <input name="url" class="form-control form-control-lg" type="text" placeholder="Type in Locodor URL">
                    </div>
                    <button type="submit" class="normal-btn bttn">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection