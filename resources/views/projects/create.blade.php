@extends('layouts.app')

@section('content')
    <script>

        document.addEventListener("scroll", makeActive);

        function makeActive(){
            function getPosition(element) {
                var yPosition = 0;

                while(element) {
                    yPosition += (element.offsetTop - element.scrollTop + element.clientTop);
                    element = element.offsetParent;
                }

                return yPosition;
            }

            // Get scroll position
            var height = $(window).scrollTop();

            //get window height and split it in 2
            var windowHeight = window.innerHeight / 2;

            //get position on page in the middle of the screen
            var pagePosition = height+windowHeight;

            var formGroups = document.querySelectorAll('.formGroup');

            for(var i = 0;i < formGroups.length;i++) {
                // console.log(formGroups[i]);



                //formGroups[i].querySelector('input')


                //get distance between element and top of page
                var elementTop = getPosition(formGroups[i]);
                console.log('eleTop:' + elementTop);
                //get element height
                var elementHeight = formGroups[i].offsetHeight;
                //get distance between end of element and top of the page
                var elementBottom = elementTop + elementHeight;
                console.log('eleBottom:' + elementBottom);

                if (pagePosition >= elementTop && pagePosition <= elementBottom) {
                    formGroups[i].classList.remove("form-opacity");
                } else {
                    formGroups[i].classList.add("form-opacity");
                }


            }


// //First element
//             var formGroup1 = document.querySelector('#formGroup1');
//
//             //get distance between element and top of page
//             var elementTop1 = getPosition(formGroup1);
//             console.log('eleTop:'+ elementTop1);
//             //get element height
//             var elementHeight1 = document.getElementById('formGroup1').offsetHeight;
//             //get distance between end of element and top of the page
//             var elementBottom1 = elementTop1 + elementHeight1;
//             console.log('eleBottom:'+ elementBottom1);
//
//
//             if (pagePosition >= elementTop1 && pagePosition <= elementBottom1) {
//                 formGroup1.classList.remove("form-opacity");
//             } else {
//                 formGroup1.classList.add("form-opacity");
//             }
//
//
// //Second element
//             var formGroup2 = document.querySelector('#formGroup2');
//
//             //get distance between element and top of page
//             var elementTop2 = getPosition(formGroup2);
//             console.log('eleTop:'+ elementTop2);
//             //get element height
//             var elementHeight2 = document.getElementById('formGroup2').offsetHeight;
//             //get distance between end of element and top of the page
//             var elementBottom2 = elementTop2 + elementHeight2;
//             console.log('eleBottom:'+ elementBottom2);
//
//
//             if (pagePosition >= elementTop2 && pagePosition <= elementBottom2) {
//                 formGroup2.classList.remove("form-opacity");
//             } else {
//                 formGroup2.classList.add("form-opacity");
//             }
        }
    </script>
    <div class="container project-fields-container" onscroll="makeActive()">
        <form action="{{ route('project.store') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="form-group first_project_parent form-group-parent form-group-parent-radio formGroup"  id="formGroup1">
                <p class="requirement">Is this your first crowdfunding campaign?</p>
                @if($errors->get('first_project'))
                    @foreach($errors->get('first_project') as $error)
                        <div class="notice notice-danger">
                            <strong>{{ $error }}</strong>
                        </div>
                    @endforeach
                @endif
                <div class="text-center">
                    <div class="form-check">
                        <input class="form-check-input first-project-selector" type="radio" name="first_project" value="1" id="first-proj1" @if(old('first_project') == 1) checked @endif>
                        <label class="bttn boom-trigger" for="first-proj1">Yes</label>
                        <span class="custom-tooltip">This is a tooltip</span>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input first-project-selector" type="radio" name="first_project" value="2" id="first-proj2" @if(old('first_project') == 2) checked @endif>
                        <label class="bttn boom-trigger" for="first-proj2">No</label>
                    </div>
                </div>
            </div>
            <img src="{{ asset('uploads/boom.png') }}" width="1000px" class="boom" style="position:fixed;top:150px;z-index:999;display:none">
            <script>
                $('.boom-trigger').on('click',function(){
                    $('.boom').css('display','block');
                    setTimeout(function(){ $('.boom').css('display','none'); }, 700);
                })
            </script>
            <script>
                $('.first-project-selector').on('click', function(e) {
                    var el = $( '#formGroup2' );
                    var elOffset = el.offset().top;
                    var elHeight = el.height();
                    var windowHeight = $(window).height();
                    var offset;

                    if (elHeight < windowHeight) {
                        offset = elOffset - ((windowHeight / 2) - (elHeight / 2));
                    }
                    else {
                        offset = elOffset;
                    }
                    var speed = 700;
                    $('html, body').animate({scrollTop:offset}, speed);
                });
            </script>

            <div class="form-group form-group-parent form-group-cat form-opacity formGroup" id="formGroup2">
                <p class="requirement">What category does your campaign fall into?</p>
                @if($errors->get('category_id'))
                    @foreach($errors->get('category_id') as $error)
                        <div class="notice notice-danger text-left">
                            <strong>{{ $error }}</strong>
                        </div>
                    @endforeach
                @endif
                @foreach($categories as $category)
                    <div class="form-check">
                        <input class="form-check-input category-selector" type="radio" name="category_id" value="{{ $category->id }}" id="category{{$category->id}}" @if(old('category_id') == $category->id) checked @endif>
                        <label class="form-check-label category-selector" for="category{{ $category->id }}"><div class="cat-box"><span class="cat-icon">{!! $category->icon !!}</span><span class="cat-name">{{ $category->name }}</span></div></label>
                    </div>
                @endforeach
            </div>

            <script>
                $('.category-selector').on('click', function(e) {
                    var el = $( '#formGroup3' );
                    var elOffset = el.offset().top;
                    var elHeight = el.height();
                    var windowHeight = $(window).height();
                    var offset;

                    if (elHeight < windowHeight) {
                        offset = elOffset - ((windowHeight / 2) - (elHeight / 2));
                    }
                    else {
                        offset = elOffset;
                    }
                    var speed = 700;
                    $('html, body').animate({scrollTop:offset}, speed);
                });
            </script>

            <div class="form-group form-group-parent form-group-parent-radio form-opacity formGroup" id="formGroup3">
                <p class="requirement">What type of crowdfunding campaign do you want to create?</p>
                @if($errors->get('project_type'))
                    @foreach($errors->get('project_type') as $error)
                        <div class="notice notice-danger">
                            <strong>{{ $error }}</strong>
                        </div>
                    @endforeach
                @endif
                <div class="text-center">
                    <div class="form-check">
                        <input class="form-check-input type-selector" type="radio" name="project_type" value="1" id="proj-type1" @if(old('project_type') == 1) checked @endif>
                        <label class="form-check-label bttn" for="proj-type1">Pre-orders</label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input type-selector" type="radio" name="project_type" value="2" id="proj-type2" @if(old('project_type') == 2) checked @endif>
                        <label class="form-check-label bttn" for="proj-type2">Reward Based</label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input type-selector" type="radio" name="project_type" value="3" id="proj-type3" @if(old('project_type') == 3) checked @endif>
                        <label class="form-check-label bttn" for="proj-type3">Equity Based</label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input type-selector" type="radio" name="project_type" value="4" id="proj-type4" @if(old('project_type') == 4) checked @endif>
                        <label class="form-check-label bttn" for="proj-type4">Donation Based</label>
                    </div>
                </div>
            </div>

            <script>
                $('.type-selector').on('click', function(e) {
                    var el = $( '#locationGroup' );
                    var elOffset = el.offset().top;
                    var elHeight = el.height();
                    var windowHeight = $(window).height();
                    var offset;

                    if (elHeight < windowHeight) {
                        offset = elOffset - ((windowHeight / 2) - (elHeight / 2)) + 100;
                    }
                    else {
                        offset = elOffset - 100;
                    }
                    var speed = 700;
                    $('html, body').animate({scrollTop:offset}, speed);
                });
            </script>

            <div class="form-group form-group-parent form-group-parent-radio form-opacity formGroup" id="locationGroup">
                <p class="requirement">Where are you going to deliver your product?</p>
                @if($errors->get('project_type'))
                    @foreach($errors->get('project_type') as $error)
                        <div class="notice notice-danger">
                            <strong>{{ $error }}</strong>
                        </div>
                    @endforeach
                @endif
                <div class="text-center">
                    <div class="form-check">
                        <input class="form-check-input location-selector" type="radio" name="shipping_location" value="ww" id="location0" @if(old('shipping_location') == "ww") checked @endif>
                        <label class="form-check-label bttn" for="location0">Worldwide</label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input location-selector" type="radio" name="shipping_location" value="na" id="location1" @if(old('shipping_location') == "na") checked @endif>
                        <label class="form-check-label bttn" for="location1">North America</label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input location-selector" type="radio" name="shipping_location" value="sa" id="location2" @if(old('shipping_location') == "sa") checked @endif>
                        <label class="form-check-label bttn" for="location2">South America</label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input location-selector" type="radio" name="shipping_location" value="eu" id="location3" @if(old('shipping_location') == "eu") checked @endif>
                        <label class="form-check-label bttn" for="location3">Europe</label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input location-selector" type="radio" name="shipping_location" value="as" id="location4" @if(old('shipping_location') == "as") checked @endif>
                        <label class="form-check-label bttn" for="location4">Asia</label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input location-selector" type="radio" name="shipping_location" value="af" id="location5" @if(old('shipping_location') == "af") checked @endif>
                        <label class="form-check-label bttn" for="location5">Africa</label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input location-selector" type="radio" name="shipping_location" value="oc" id="location6" @if(old('shipping_location') == "oc") checked @endif>
                        <label class="form-check-label bttn" for="location6">Oceania</label>
                    </div>
                </div>
            </div>

            <script>
                $('.location-selector').on('click', function(e) {
                    var el = $( '#formGroup4' );
                    var elOffset = el.offset().top;
                    var elHeight = el.height();
                    var windowHeight = $(window).height();
                    var offset;

                    if (elHeight < windowHeight) {
                        offset = elOffset - ((windowHeight / 2) - (elHeight / 2)) + 100;
                    }
                    else {
                        offset = elOffset - 100;
                    }
                    var speed = 700;
                    $('html, body').animate({scrollTop:offset}, speed);
                });
            </script>

            <div class="form-group form-group-parent form-opacity formGroup" id="formGroup4">
                <label for="previous_url" class="requirement">If you have one, please include your previous campaign URL:</label>
                @if($errors->get('previous_url'))
                    @foreach($errors->get('previous_url') as $error)
                        <div class="notice notice-danger">
                            <strong>{{ $error }}</strong>
                        </div>
                    @endforeach
                @endif
                <input class="form-control" type="text" placeholder="Exemple: www.exemple.com" name="previous_url" id="previous_url" value="{{ old('previous_url') }}">
                <p class="enter-continue hidden text-right" id="prev_url_continue">Press <strong>ENTER</strong> to continue</p>
            </div>
            <script>
                var elem = document.getElementById('previous_url');
                elem.addEventListener('keypress', function(e){
                    var element = document.getElementById("prev_url_continue");
                    element.classList.remove("hidden");

                    if (e.keyCode == 13) {
                        e.preventDefault();
                        var el = $( '#formGroup5' );
                        var elOffset = el.offset().top;
                        var elHeight = el.height();
                        var windowHeight = $(window).height();
                        var offset;

                        if (elHeight < windowHeight) {
                            offset = elOffset - ((windowHeight / 2) - (elHeight / 2)) + 100;
                        }
                        else {
                            offset = elOffset - 100;
                        }
                        var speed = 700;
                        $('html, body').animate({scrollTop:offset}, speed);
                    }
                });
            </script>


            <div class="form-group form-group-parent form-group-parent-radio form-opacity formGroup" id="formGroup5">
                <p class="requirement">Do you plan on uploading your project to other platforms?</p>
                @if($errors->get('upload_other'))
                    @foreach($errors->get('upload_other') as $error)
                        <div class="notice notice-danger">
                            <strong>{{ $error }}</strong>
                        </div>
                    @endforeach
                @endif
                <div class="text-center">
                    <div class="form-check">
                        <input class="form-check-input upload-other-selector" type="radio" name="upload_other" value="1" id="upload_other1" @if(old('upload_other') == 1) checked @endif>
                        <label class="bttn" for="upload_other1">Yes</label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input upload-other-selector" type="radio" name="upload_other" value="2" id="upload_other2" @if(old('upload_other') == 2) checked @endif>
                        <label class="bttn" for="upload_other2">No</label>
                    </div>
                </div>
            </div>

            <script>
                $('.upload-other-selector').on('click', function(e) {
                    var el = $( '#formGroup6' );
                    var elOffset = el.offset().top;
                    var elHeight = el.height();
                    var windowHeight = $(window).height();
                    var offset;

                    if (elHeight < windowHeight) {
                        offset = elOffset - ((windowHeight / 2) - (elHeight / 2)) + 100;
                    }
                    else {
                        offset = elOffset - 100;
                    }
                    var speed = 700;
                    $('html, body').animate({scrollTop:offset}, speed);
                });
            </script>

            <div class="form-group ready-group form-group-parent form-opacity formGroup" id="formGroup6">
                <p class="requirement">From 1 to 10: How ready is your project?</p>
                @if($errors->get('ready'))
                    @foreach($errors->get('ready') as $error)
                        <div class="notice notice-danger text-left">
                            <strong>{{ $error }}</strong>
                        </div>
                    @endforeach
                @endif
                <div class="ready-btn-container" role="group" aria-label="First group">
                    <div class="form-check">
                        <input class="form-check-input ready-selector" type="radio" name="ready" value="1" id="inlineRadio1" @if(old('ready') == 1) checked @endif>
                        <label class="form-check-label" for="inlineRadio1">1</label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input ready-selector" type="radio" name="ready" value="2" id="inlineRadio2" @if(old('ready') == 2) checked @endif>
                        <label class="form-check-label" for="inlineRadio2">2</label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input ready-selector" type="radio" name="ready" value="3" id="inlineRadio3" @if(old('ready') == 3) checked @endif>
                        <label class="form-check-label" for="inlineRadio3">3</label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input ready-selector" type="radio" name="ready" value="4" id="inlineRadio4" @if(old('ready') == 4) checked @endif>
                        <label class="form-check-label" for="inlineRadio4">4</label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input ready-selector" type="radio" name="ready" value="5" id="inlineRadio5" @if(old('ready') == 5) checked @endif>
                        <label class="form-check-label ready-selector" for="inlineRadio5">5</label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input ready-selector" type="radio" name="ready" value="6" id="inlineRadio6" @if(old('ready') == 6) checked @endif>
                        <label class="form-check-label" for="inlineRadio6">6</label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input ready-selector" type="radio" name="ready" value="7" id="inlineRadio7" @if(old('ready') == 7) checked @endif>
                        <label class="form-check-label" for="inlineRadio7">7</label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input ready-selector" type="radio" name="ready" value="8" id="inlineRadio8" @if(old('ready') == 8) checked @endif>
                        <label class="form-check-label" for="inlineRadio8">8</label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input ready-selector" type="radio" name="ready" value="9" id="inlineRadio9" @if(old('ready') == 9) checked @endif>
                        <label class="form-check-label" for="inlineRadio9">9</label>
                    </div>
                    <div class="form-check ready-last ready-selector">
                        <input class="form-check-input" type="radio" name="ready" value="10" id="inlineRadio10" @if(old('ready') == 10) checked @endif>
                        <label class="form-check-label" for="inlineRadio10">10</label>
                    </div>
                </div>
            </div>

            <script>
                $('.ready-selector').on('click', function(e) {
                    var el = $( '#formGroup7' );
                    var elOffset = el.offset().top;
                    var elHeight = el.height();
                    var windowHeight = $(window).height();
                    var offset;

                    if (elHeight < windowHeight) {
                        offset = elOffset - ((windowHeight / 2) - (elHeight / 2)) + 100;
                    }
                    else {
                        offset = elOffset - 100;
                    }
                    var speed = 700;
                    $('html, body').animate({scrollTop:offset}, speed);
                });
            </script>

            <div class="form-group form-group-parent form-opacity formGroup" id="formGroup7">
                <label for="title" class="requirement">Project Title</label>
                @if($errors->get('title'))
                    @foreach($errors->get('title') as $error)
                        <div class="notice notice-danger">
                            <strong>{{ $error }}</strong>
                        </div>
                    @endforeach
                @endif
                <input class="form-control" type="text" placeholder="Title" name="title"  id="title" value="{{ old('title') }}">
                <p class="enter-continue hidden text-right" id="title_continue">Press <strong>ENTER</strong> to continue</p>
            </div>
            <script>
                var elem = document.getElementById('title');
                elem.addEventListener('keypress', function(e){
                    var element = document.getElementById("title_continue");
                    element.classList.remove("hidden");
                    if (e.keyCode == 13) {
                        e.preventDefault();
                        var el = $( '#formGroup8' );
                        var elOffset = el.offset().top;
                        var elHeight = el.height();
                        var windowHeight = $(window).height();
                        var offset;

                        if (elHeight < windowHeight) {
                            offset = elOffset - ((windowHeight / 2) - (elHeight / 2)) + 100;
                        }
                        else {
                            offset = elOffset - 100;
                        }
                        var speed = 700;
                        $('html, body').animate({scrollTop:offset}, speed);
                    }
                });
            </script>

            <div class="form-group form-group-parent form-opacity formGroup" id="formGroup8">
                <label for="subtitle" class="requirement">Catchy Subtitle</label>
                @if($errors->get('subtitle'))
                    @foreach($errors->get('subtitle') as $error)
                        <div class="notice notice-danger">
                            <strong>{{ $error }}</strong>
                        </div>
                    @endforeach
                @endif
                <input class="form-control" type="text" placeholder="Subtitle" name="subtitle" id="subtitle" value="{{ old('subtitle') }}">
                <p class="enter-continue hidden text-right" id="subtitle_continue">Press <strong>ENTER</strong> to continue</p>
            </div>
            <script>
                var elem = document.getElementById('subtitle');
                elem.addEventListener('keypress', function(e){
                    var element = document.getElementById("subtitle_continue");
                    element.classList.remove("hidden");
                    if (e.keyCode == 13) {
                        e.preventDefault();
                        var el = $( '#formGroup9' );
                        var elOffset = el.offset().top;
                        var elHeight = el.height();
                        var windowHeight = $(window).height();
                        var offset;

                        if (elHeight < windowHeight) {
                            offset = elOffset - ((windowHeight / 2) - (elHeight / 2)) + 100;
                        }
                        else {
                            offset = elOffset - 100;
                        }
                        var speed = 700;
                        $('html, body').animate({scrollTop:offset}, speed);
                    }
                });
            </script>

            <div class="form-group file-upload-group form-opacity formGroup" id="formGroup9">
                <p class="requirement">Choose your project featured image</p>
                @if($errors->get('image'))
                    @foreach($errors->get('image') as $error)
                        <div class="notice notice-danger">
                            <strong>{{ $error }}</strong>
                        </div>
                    @endforeach
                @endif
                <span class="image-btn-container">
                    {{--<span><i class="fas fa-plus"></i></span>--}}
                    <label for="image" class="image-button btn btn-primary">Choose Image</label>
                </span>


                <input type="file" class="form-control-file" id="image" name="image" onchange="myFunction()">
                <script>
                    function myFunction(){
                        if(document.getElementById("image").value != "") {

                            var path = document.getElementById("image").value;
                            var filename = path.substring(path.lastIndexOf('\\')+1);

                            var node = document.createElement("p");
                            var textnode = document.createTextNode(filename);
                            node.appendChild(textnode);
                            node.classList.add('file-name');


                            document.querySelector('.file-upload-group').appendChild(node);

                            var el = $( '#formGroup10' );
                            var elOffset = el.offset().top;
                            var elHeight = el.height();
                            var windowHeight = $(window).height();
                            var offset;

                            if (elHeight < windowHeight) {
                                offset = elOffset - ((windowHeight / 2) - (elHeight / 2)) + 100;
                            }
                            else {
                                offset = elOffset - 100;
                            }
                            var speed = 700;
                            $('html, body').animate({scrollTop:offset}, speed);

                        }
                    }
                </script>
            </div>


            <div class="form-group form-group-parent form-opacity formGroup" id="formGroup10">
                <div class="row">
                    <div class="col-md-6">
                        <label for="start_date" class="requirement">Start date</label>
                        @if($errors->get('start_date'))
                            @foreach($errors->get('start_date') as $error)
                                <div class="notice notice-danger">
                                    <strong>{{ $error }}</strong>
                                </div>
                            @endforeach
                        @endif
                        <input class="form-control" type="date" placeholder="Start date" name="start_date" value="{{ old('start_date') }}">
                    </div>
                    <div class="col-md-6">
                        <label for="end_date" class="requirement">End date</label>
                        @if($errors->get('end_date'))
                            @foreach($errors->get('end_date') as $error)
                                <div class="notice notice-danger">
                                    <strong>{{ $error }}</strong>
                                </div>
                            @endforeach
                        @endif
                        <input class="form-control" type="date" placeholder="End Date" name="end_date" id="end_date" value="{{ old('end_date') }}">
                    </div>
                    {{--<p class="enter-continue hidden text-right" id="date_continue">Click <strong><a class="date-selector">HERE</a></strong> to continue</p>--}}
                </div>
            </div>

            <script>
                var elem = document.getElementById('end_date');
                elem.addEventListener('change', function(e){
                    var el = $( '#formGroup11' );
                    var elOffset = el.offset().top;
                    var elHeight = el.height();
                    var windowHeight = $(window).height();
                    var offset;

                    if (elHeight < windowHeight) {
                        offset = elOffset - ((windowHeight / 2) - (elHeight / 2)) + 100;
                    }
                    else {
                        offset = elOffset - 100;
                    }
                    var speed = 700;
                    $('html, body').animate({scrollTop:offset}, speed);
                });
            </script>

            {{--<script>--}}
                {{--var elem = document.getElementById('end_date');--}}
                {{--elem.addEventListener('change', function(e){--}}
                    {{--var element = document.getElementById("date_continue");--}}
                    {{--element.classList.remove("hidden");--}}
                {{--});--}}
                {{--$('.date-selector').on('click', function(e) {--}}
                    {{--var el = $( '#formGroup11' );--}}
                    {{--var elOffset = el.offset().top;--}}
                    {{--var elHeight = el.height();--}}
                    {{--var windowHeight = $(window).height();--}}
                    {{--var offset;--}}

                    {{--if (elHeight < windowHeight) {--}}
                        {{--offset = elOffset - ((windowHeight / 2) - (elHeight / 2)) + 100;--}}
                    {{--}--}}
                    {{--else {--}}
                        {{--offset = elOffset - 100;--}}
                    {{--}--}}
                    {{--var speed = 700;--}}
                    {{--$('html, body').animate({scrollTop:offset}, speed);--}}
                {{--});--}}
            {{--</script>--}}



            <div class="form-group form-group-parent form-opacity formGroup" id="formGroup11">
                <label for="project_goal" class="requirement">Project Goal</label>
                @if($errors->get('project_goal'))
                    @foreach($errors->get('project_goal') as $error)
                        <div class="notice notice-danger">
                            <strong>{{ $error }}</strong>
                        </div>
                    @endforeach
                @endif
                <input class="form-control" type="number" id="project_goal" placeholder="Project goal in USD" name="project_goal" value="{{ old('project_goal') }}">
                <p class="enter-continue hidden text-right" id="goal_continue">Press <strong>ENTER</strong> to continue</p>
            </div>

            <script>
                var elem = document.getElementById('project_goal');
                elem.addEventListener('keypress', function(e){
                    var element = document.getElementById("goal_continue");
                    element.classList.remove("hidden");
                    if (e.keyCode == 13) {
                        e.preventDefault();
                        var el = $( '#formGroup12' );
                        var elOffset = el.offset().top;
                        var elHeight = el.height();
                        var windowHeight = $(window).height();
                        var offset;

                        if (elHeight < windowHeight) {
                            offset = elOffset - ((windowHeight / 2) - (elHeight / 2)) + 100;
                        }
                        else {
                            offset = elOffset - 100;
                        }
                        var speed = 700;
                        $('html, body').animate({scrollTop:offset}, speed);
                    }
                });
            </script>

            <div class="form-group form-group-parent form-opacity formGroup" id="formGroup12">
                <label for="Content" class="requirement">Project Content</label>
                @if($errors->get('content'))
                    @foreach($errors->get('content') as $error)
                        <div class="notice notice-danger">
                            <strong>{{ $error }}</strong>
                        </div>
                    @endforeach
                @endif
                <textarea class="form-control" name="content" id="editor" placeholder="Tell us about your project." rows="8">{{ old('content') }}</textarea>
                <p class="enter-continue text-right" id="content_continue">Click <strong><a class="date-selector">HERE</a></strong> to continue</p>
            </div>

            <script>
                $('.date-selector').on('click', function(e) {
                    var el = $( '#formGroup13' );
                    var elOffset = el.offset().top;
                    var elHeight = el.height();
                    var windowHeight = $(window).height();
                    var offset;

                    if (elHeight < windowHeight) {
                        offset = elOffset - ((windowHeight / 2) - (elHeight / 2)) + 100;
                    }
                    else {
                        offset = elOffset - 100;
                    }
                    var speed = 700;
                    $('html, body').animate({scrollTop:offset}, speed);
                });
            </script>

            <div class="form-group form-group-parent form-opacity formGroup" id="formGroup13">
                <label for="p_website" class="requirement">Project website</label>
                @if($errors->get('p_website'))
                    @foreach($errors->get('p_website') as $error)
                        <div class="notice notice-danger">
                            <strong>{{ $error }}</strong>
                        </div>
                    @endforeach
                @endif
                <input class="form-control" type="text" placeholder="www.exemple.com" id="p_website" name="p_website" {{ old('p_website') }}>
                <p class="enter-continue hidden text-right" id="website_continue">Press <strong>ENTER</strong> to continue</p>
            </div>

            <script>
                var elem = document.getElementById('p_website');
                elem.addEventListener('keypress', function(e){
                    var element = document.getElementById("website_continue");
                    element.classList.remove("hidden");
                    if (e.keyCode == 13) {
                        e.preventDefault();
                        var el = $( '#formGroup14' );
                        var elOffset = el.offset().top;
                        var elHeight = el.height();
                        var windowHeight = $(window).height();
                        var offset;

                        if (elHeight < windowHeight) {
                            offset = elOffset - ((windowHeight / 2) - (elHeight / 2)) + 100;
                        }
                        else {
                            offset = elOffset - 100;
                        }
                        var speed = 700;
                        $('html, body').animate({scrollTop:offset}, speed);
                    }
                });
            </script>

            <div class="form-group form-group-parent form-opacity formGroup" id="formGroup14">
                <label for="levels" class="requirement">Project levels number</label>
                @if($errors->get('number'))
                    @foreach($errors->get('number') as $error)
                        <div class="notice notice-danger">
                            <strong>{{ $error }}</strong>
                        </div>
                    @endforeach
                @endif
                <input oninput="doThing()" id="levels" name="levels" class="form-control" type="number" placeholder="Type in the number of levels for your project">
            </div>
            <h1></h1>
            <div id="levels-container" class="form-group form-opacity formGroup">

            </div>

            <script>
                doThing();
                /* function */
                function doThing(){
                    //clear container on input change
                    var container = document.getElementById("levels-container");
                    while (container.firstChild) {
                        container.removeChild(container.firstChild);
                    }

                    // populate levels based on the input value
                    var levelValue = document.getElementById("levels").value;

                    for(var i = 1;i <= levelValue;i++){

                        //create parent div
                        var levelContainer = document.createElement("div");
                        levelContainer.setAttribute('class','form-group');


                    //create title input

                        //create container for level title
                        var levelTitleContainer = document.createElement("div");
                        levelTitleContainer.classList.add("form-group");

                        //crete input for title
                        var levelTitle = document.createElement("input");
                        levelTitle.setAttribute('type', 'text');
                        levelTitle.setAttribute('placeholder', 'Title');
                        levelTitle.setAttribute('name', 'level_title[]');
                        levelTitle.setAttribute('value', '');

                        levelTitle.classList.add("form-control");

                        // append title input to title contaienr
                        levelTitleContainer.appendChild(levelTitle);


                    // Create price input

                        //create container for level price
                        var levelPriceContainer = document.createElement("div");
                        levelPriceContainer.classList.add("form-group");

                        //create price input
                        var levelPrice = document.createElement("input");
                        levelPrice.setAttribute('type', 'number');
                        levelPrice.setAttribute('placeholder', 'Price in $');
                        levelPrice.setAttribute('name', 'level_price[]');
                        levelPrice.classList.add("form-control");

                        // append price input to price container
                        levelPriceContainer.appendChild(levelPrice);


                    // Create Description input

                        //create container for level description
                        var levelDescContainer = document.createElement("div");
                        levelDescContainer.classList.add("form-group");

                        //create description input
                        var levelDesc = document.createElement("textarea");
                        levelDesc.setAttribute('type', 'text');
                        levelDesc.setAttribute('name', 'level_desc[]');
                        levelDesc.setAttribute('placeholder', 'Type in the level description');
                        //var levelDescValue = document.createTextNode("Type in the level description");
                        //levelDesc.appendChild(levelDescValue);
                        levelDesc.classList.add("form-control");

                        // append description input to description container
                        levelDescContainer.appendChild(levelDesc);


                        //Create level input
                        var levelLabel = document.createElement("H4");
                        var text = document.createTextNode("Level " + i);
                        levelLabel.appendChild(text);

                        //append elements to individual level container

                        levelContainer.appendChild(levelLabel);
                        levelContainer.appendChild(levelTitleContainer);
                        levelContainer.appendChild(levelPriceContainer);
                        levelContainer.appendChild(levelDescContainer);

                        //append levels to general container
                        document.getElementById('levels-container').appendChild(levelContainer);


                    }
                }

            </script>
            <div class="text-center">
                <button type="submit" class="bttn">Create project</button>
            </div>
        </form>
    </div>
@endsection
