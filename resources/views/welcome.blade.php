@extends('layouts.app')

@section('funky-theme-head')
    <link rel="stylesheet" href="{{ asset('/funky/css/mind-icons-line.css') }}"/>
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,500,600,700,800,900" rel="stylesheet"/>
@endsection

@section('content')
    <header class="locodor-hero-section">
        <div class="overlay"></div>
        <video autoplay muted loop id="myVideo">
            <source src="{{ asset('videos/Locodor-Crowdfunding-Agency-Video-Background-office.webm') }}" type="video/mp4">
            Your browser does not support HTML5 video.
        </video>
        <div class="section full-height container">
{{--            <div class="parallax" style="background-image: url('img/parallax-hero-2.jpg')"></div>--}}
            <div class="grey-fade-over"></div>
            <div class="hero-center-wrap move-bottom z-bigger">
                <div class="container color-white hero-text big-text text-center parallax-fade-top">
                    <div class="row">
                        <div class="col-md-12 mg-auto locodor-hero-text">
                            <p>FULL-SERVICE CROWDFUNDING AGENCY</p>
                            <p class="locodor-hero-secondary-text">Tailor-Made Campaign Creation</p>
{{--                            <h1 class="color-white mb-xl-5">--}}
{{--                                <img width="200px" src='{{ asset('/uploads/logo-locodor-crowdfunding.png') }}'>--}}
{{--                            </h1>--}}
                            <a href="{{ route('howitworks') }}" target="_blank" class="btn btn-primary btn-round btn-long scroll btn-locodor">discover more</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Call to Action Block
    ================================================== -->

    <div class="background-dark hero-cta">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="call-box-1 dark text-center text-md-left">
                        <h5 class="mb-2 hero-cta-text">Yes, I want LOCODOR to Partner with me to help bring success to my project </h5>
                    </div>
                </div>
                <div class="col-md-3 mt-4 mt-md-0">
                    <div class="call-box-1 dark text-center text-md-right">
                        <a href="{{ route('tom') }}" class="btn-outline-locodor" >LET’S TALK >></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Services Block
    ================================================== -->

    <div class="section padding-top-bottom background-grey campaign-creation-section">
        <div class="container">
            <div class="row justify-content-center services-section">
                <div class="col-md-12">
                    <div class="main-title text-center">
                        <div class="main-subtitle-top mb-4 locodor-color">focused on strategy</div>
                        <h3>
                            CAMPAIGN CREATION & MARKETING SERVICES
                            <br>
                            ONE-STOP PARTNER FOR EVERYTHING CROWDFUNDING
                        </h3>
                        <div class="main-subtitle-bottom mt-3">Our full-service, in-house crowdfunding agency brings your products to market<br> via Kickstarter, Indiegogo and other popular platforms.</div>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="col-md-4">
                    <div class="services-box text-center">
                        <i class="funky-ui-icon icon-Retouching"></i>
                        <h5 class="mt-3">Discovery & Strategy</h5>
                        <p class="mt-3 mb-4">
                            Setup, Research & Manage Assets<br>
                            Audit, Evaluate & Create<br>
                            Product Strategy, Industrial Design, Engineering, Prototyping<br>
                            Product Development in-house<br>
                        </p>
                        <a href="#" class="btn-link btn-primary locodor-link">read more</a>
                    </div>
                </div>
                <div class="col-md-4 mt-4 mt-md-0">
                    <div class="services-box text-center">
                        <i class="funky-ui-icon icon-Monitor-phone"></i>
                        <h5 class="mt-3">Pre-Launch </h5>
                        <p class="mt-3 mb-4">
                            Creative Design, Write & Develop campaign<br>
                            Audience reach, influencers,<br>
                            Videography and Photography<br>
                        </p>
                        <a href="#" class="btn-link btn-primary locodor-link">read more</a>
                    </div>
                </div>
                <div class="col-md-4 mt-4 mt-md-0">
                    <div class="services-box text-center">
                        <i class="funky-ui-icon icon-Split-FourSquareWindow"></i>
                        <h5 class="mt-3">Campaign Strategy & Design</h5>
                        <p class="mt-3 mb-4">Define, Target & Customize</p>
                        <a href="#" class="btn-link btn-primary locodor-link">read more</a>
                    </div>
                </div>
                <div class="col-md-4 mt-4">
                    <div class="services-box text-center">
                        <i class="funky-ui-icon icon-Downward"></i>
                        <h5 class="mt-3">Marketing </h5>
                        <p class="mt-3 mb-4">
                            Facebook Ads, Social Media, Email Marketing, Public Relations,<br>
                            Promote, Engage & Optimize<br>
                            Cross Promotions<br>
                        </p>
                        <a href="#" class="btn-link btn-primary locodor-link">read more</a>
                    </div>
                </div>
                <div class="col-md-4 mt-4">
                    <div class="services-box text-center">
                        <i class="funky-ui-icon icon-Duplicate-Window"></i>
                        <h5 class="mt-3">Lead Generation </h5>
                        <p class="mt-3 mb-4">
                            Funding<br>
                            Launch, Convert & Analyze<br>
                        </p>
                        <a href="#" class="btn-link btn-primary locodor-link">read more</a>
                    </div>
                </div>
                <div class="col-md-4 mt-4">
                    <div class="services-box text-center">
                        <i class="funky-ui-icon icon-Support"></i>
                        <h5 class="mt-3">Live Campaign</h5>
                        <p class="mt-3 mb-4">
                            Launch Day<br>
                            Backers, Visitors & Press<br>
                            Post Funding<br>
                            Order management CRM, Fulfillment & 3rd party Distribution & wharehousing<br>
                        </p>
                        <a href="#" class="btn-link btn-primary locodor-link">read more</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid homepage-first-section">
        <div class="inner-first-section">
            <div class="container text-center">
                <p class="welcome-text">Welcome to <span style="color:#ff6600">Locodor</span></p>
                <p class="welcome-subtitle">A Network and Crowdfunding Platform.</p>
                <p class="init-text">Here is what you can do. Take your time and Explore your options!</p>
                <div class="home-4-images">
                    <div class="text-center">
                        <a href="{{ route('project.create-start') }}">
                            <div class="home-img-1 home-img" style="background: linear-gradient(to right,rgba(255, 102, 0, 0.45), rgba(0, 0, 0, 0.7)),url('{{ asset('/uploads/Create.jpg') }}');">
                                <img src="{{ asset('uploads/icons/create.png') }}">
                                <p>Create</p>
                            </div>
                        </a>
                    </div>

                    <div class="text-center">
                        <a href="{{ route('projects') }}">
                            <div class="home-img home-img-2" style="background: linear-gradient(to right,rgba(255, 102, 0, 0.45), rgba(0, 0, 0, 0.7)),url('{{ asset('/uploads/Explore.jpg') }}');">
                                <img src="{{ asset('uploads/icons/explore.png') }}">
                                <p>Explore</p>
                            </div>
                        </a>
                    </div>

                    <div class="text-center">
                        <a href="{{ route('activity') }}">
                            <div class="home-img home-img-3" style="background: linear-gradient(to right,rgba(255, 102, 0, 0.45), rgba(0, 0, 0, 0.7)),url('{{ asset('/uploads/Socialize.jpg') }}');">
                                <img src="{{ asset('uploads/icons/socialize.png') }}">
                                <p>Socialize</p>
                            </div>
                        </a>
                    </div>

                    <div class="text-center">
                        <a href="{{ route('crowdfunding.resources') }}">
                            <div class="home-img home-img-4" style="background: linear-gradient(to right,rgba(255, 102, 0, 0.45), rgba(0, 0, 0, 0.7)),url('{{ asset('/uploads/Resources.jpg') }}');">
                                <img src="{{ asset('uploads/icons/resources.png') }}">
                                <p>Resources</p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="outer-first-section container">
            <div class="row justify-content-md-center">
                <div class="col-md-9">
                    <form action="/register" method="get">
                        {{ csrf_field() }}
                        <div class="input-group mb-3 form-quick-email">
                            <input class="form-control form-control-lg quick-email-left" type="text" placeholder="Enter your email" name="quick-email">
                            <div class="input-group-append">
                                <button type="submit" class="home-become-btn quick-email-right">Become one of us</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-3 text-center">
                    <div>
                        <p class="instructions"><a href="{{route('howitworks')}}">How it works ></a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="home-purple-line"></div>
    <div class="container">
        <div class="row got-idea">
            <div class="col-md-12">
                <p class="info-section-title">Getting Started</p>
            </div>
            <div class="col-md-8">
                <div class="carousel-got-idea">
                    <form method="post" action="{{ route('got.idea') }}" id="got-idea">
                        @csrf
                        <div id="carousel" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner form-slider-container">
                                {{-- Slide 1 --}}
                                <div class="carousel-item active">
                                    <p class="slider-form-text">Hello,<br>
                                        I'm <a href="{{ route('profile.show',['id' => 2403,'slug'=>'tom-at-locodor']) }}" class="form-slider-span">Tom</a>.<i class="fas fa-arrow-right"></i><br>
                                        Let me help you expand your <span class="form-slider-span">Crowdfunding Network</span> and increase your <span class="form-slider-span">Exposure!</span>
                                    </p>
                                    <p class="slider-form-question">What's your name?</p>
                                    <div class="form-group">
                                        <input type="text" class="form-control slider-form-input" placeholder="Write your name..." name="name" id="form-slider-name">
                                        <p class="warning-message" style="display:none">Please provide your name...</p>
                                    </div>
                                </div>
                                {{-- Slide 2 --}}
                                <div class="carousel-item">
                                    <p class="slider-form-text">Nice to meet you <span class="form-slider-username">User</span>.</p>
                                    <p class="slider-form-question">Please describe your Awesome idea in a few words.</p>
                                    <div class="form-group">
                                        <input type="text" class="form-control slider-form-input" placeholder="A few words about your idea..." name="idea" id="form-slider-idea">
                                        <p class="warning-message" style="display:none">Please provide some details about your idea...</p>
                                    </div>
                                    <p class="slider-form-question">And what type of Crowdfunding Campaigns are you interested in.</p>
                                    <div class="form-group">
                                        <input type="text" class="form-control slider-form-input" placeholder="A few words about your interests..." name="interest" id="form-slider-interest">
                                        <p class="warning-message" style="display:none">Please provide some details about your interests...</p>
                                    </div>
                                </div>
                                {{-- Slide 3 --}}
                                <div class="carousel-item">
                                    <p class="slider-form-text">Sounds great, <span class="form-slider-username">User</span>!</p>
                                    <p class="slider-form-question">Do you already have a live Crowdfunding Project?</p>
                                    <div class="form-check form-slider-select">
                                        <input class="form-check-input form-slider-select-input have_campaign" type="radio" name="have_campaign" id="yes" value="1" checked>
                                        <label class="form-check-label form-slider-select-label" for="yes">
                                            Yes
                                        </label>
                                    </div>
                                    <div class="form-check form-slider-select">
                                        <input class="form-check-input form-slider-select-input have_campaign" type="radio" name="have_campaign" id="no" value="2">
                                        <label class="form-check-label form-slider-select-label" for="no">
                                            No
                                        </label>
                                    </div>
                                </div>
                                {{-- Slide 4 --}}
                                <div class="carousel-item">
                                    <p class="slider-form-text">That is great! <span class="form-slider-span">Congratulation!</span></p>
                                    <p class="slider-form-question">Lets have a look at it. Please paste the link of your Campaign.</p>
                                    <div class="form-group">
                                        <input type="text" class="form-control slider-form-input" placeholder="Paste the link of your Crowdfunding Campaign..." name="campaign" id="form-slider-link">
                                        <p class="warning-message" style="display:none">Please provide some details about your idea...</p>
                                    </div>
                                </div>
                                {{-- Slide 5 --}}
                                <div class="carousel-item">
                                    <p class="slider-form-text">Thank you very much, <span class="form-slider-username">User</span>!</p>
                                    <p class="slider-form-question">Please provide your email address and we will reach out to you!</p>
                                    <div class="form-group">
                                        <input type="email" class="form-control slider-form-input" placeholder="Write your email address..." name="email" id="form-slider-email">
                                        <p class="warning-message" style="display:none">Please provide your valid email address...</p>
                                    </div>
                                    <div class="captcha-container">
                                        <div class="text-center">
                                            <span>
                                                {!! NoCaptcha::display(['data-theme' => 'dark']) !!}
                                            </span>
                                        </div>
                                        @if ($errors->has('g-recaptcha-response'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                {{-- Slide 6 --}}
                                <div class="carousel-item">
                                    <p class="slider-form-text">This is it! You've reached the end!</p>
                                    <p class="slider-form-question">Thank you very much for taking your time to reach us. We will be back shortly to help you out!</p>
                                </div>
                            </div>
                        </div>
                        <button type="submit" id="form-slide-submit" style="display: none">submit</button>
                    </form>
                    <a class="form-slider-prev form-slider-controls" id="form-slider-prev" role="button">
                        <span><i class="fas fa-arrow-left"></i> Previous</span>
                    </a>
                    <a class="form-slider-next form-slider-controls" id="form-slider-next" role="button">
                        <span>Next <i class="fas fa-arrow-right"></i></span>
                    </a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="tom-card-container">
                    <img src="{{ asset($tom->profile->avatar) }}">
                    <span>{{ $tom->name }}</span>
                    <div class="fancy-hr"></div>
                    <p><a href="{{ route('start.conversation',['id'=>$tom->id]) }}" class="tom-start-conversation"><i class="far fa-comment"></i>Start Conversation</a></p>
                    <p><a href="{{ route('profile.show',['id' => $tom->profile->id,'slug' => $tom->profile->slug]) }}" class="tom-start-conversation"><i class="far fa-eye"></i>View Profile</a></p>
                    <div class="fancy-hr"></div>
                    <div class="tom-social-icons">
                        <a href="https://www.facebook.com/Locodor/" target="_blank">
                            <i class="fab fa-facebook"></i>
                        </a>
                        <a href="https://www.instagram.com/locodor_crowdfunding/" target="_blank">
                            <i class="fab fa-instagram"></i>
                        </a>
                        <a href="https://twitter.com/@LOCODOR01" target="_blank">
                            <i class="fab fa-twitter-square"></i>
                        </a>
                        <a href="https://www.linkedin.com/company/Locodor" target="_blank">
                            <i class="fab fa-linkedin"></i>
                        </a>
                        <a href="https://ro.pinterest.com/locodor/" target="_blank">
                            <i class="fab fa-pinterest-square"></i>
                        </a>
                        <a href="https://angel.co/locodor" target="_blank">
                            <i class="fab fa-angellist"></i>
                        </a>
                        <a href="https://medium.com/@LOCODOR" target="_blank">
                            <img src="{{ asset('uploads/medium.png')  }}">
                        </a>
                    </div>
                </div>
            </div>
            <script>
                $('.carousel').carousel({
                    interval: false
                });

                $('#form-slider-prev').hide();

                $( document ).ready(function() {
                    var slideNo = 1;

                    $('#form-slider-next').on('click',function(e){
                        e.preventDefault();
                        console.log(slideNo);


                        switch(slideNo) {
                            case 1:
                                if($('#form-slider-name').val() === ""){
                                    $('#form-slider-name').next().css('display','inline');
                                    console.log('1 empty');
                                }else {
                                    $('#form-slider-name').next().css('display','none');

                                    $('.form-slider-username').text($('#form-slider-name').val());

                                    $('.carousel').carousel('next');
                                    $('#form-slider-prev').show();
                                    slideNo++;
                                    console.log(slideNo);
                                }
                                break;
                            case 2:

                                $('#form-slider-idea').next().css('display','none');
                                $('#form-slider-interest').next().css('display','none');

                                $('.carousel').carousel('next');
                                slideNo++;
                                console.log(slideNo);

                                break;
                            case 3:
                                if($('input[name="have_campaign"]:checked').val() === "1"){
                                    $('.carousel').carousel('next');
                                    slideNo++;
                                    console.log(slideNo);
                                }else if($('input[name="have_campaign"]:checked').val() === "2"){
                                    $('.carousel').carousel(4);
                                    slideNo++;
                                    slideNo++;
                                    console.log(slideNo);
                                }
                                break;
                            case 4:
                                if($('#form-slider-link').val() === ""){
                                    $('#form-slider-link').next().css('display','inline');
                                    console.log('4 empty');
                                }else{
                                    $('#form-slider-link').next().css('display','none');

                                    $('.carousel').carousel('next');
                                    slideNo++;
                                    console.log(slideNo);
                                }
                                break;
                            case 5:
                                if($('#form-slider-email').val() === "" || !isValidEmailAddress($('#form-slider-email').val())){
                                    $('#form-slider-email').next().css('display','inline');
                                    console.log('5 empty');
                                }else{
                                    $('#form-slider-email').next().css('display','none');

                                    $('.carousel').carousel('next');
                                    slideNo++;
                                    console.log(slideNo);

                                    var name = $('#form-slider-name').val();
                                    var idea = $('#form-slider-idea').val();
                                    var campaign = $('#form-slider-link').val();
                                    var email = $('#form-slider-email').val();


                                    $.ajax({
                                        type: "POST",
                                        url: "{{ route('got.idea') }}",
                                        data: {
                                            _token: '{{csrf_token()}}',
                                            name: name,
                                            idea: idea,
                                            campaign: campaign,
                                            email: email,
                                            capcha: grecaptcha.getResponse()
                                        },
                                        success: function (result) {
                                            console.log('submited')
                                        },
                                        error: function (result) {
                                            $('.carousel').carousel('prev');
                                            $('#form-slider-next').show();
                                            slideNo--;
                                            alert("Please check I'm not a robot box.");
                                        }
                                    });
                                }
                                break;
                        }

                        if(slideNo === 6){
                            $('#form-slider-next').hide();
                        }
                    });

                    $('#form-slider-prev').on('click',function(e){
                        e.preventDefault();

                        console.log(slideNo);


                        switch(slideNo) {
                            case 1:
                                break;
                            case 2:
                                $('.carousel').carousel('prev');
                                slideNo--;
                                console.log(slideNo);

                                break;
                            case 3:
                                $('.carousel').carousel('prev');
                                slideNo--;
                                console.log(slideNo);

                                break;
                            case 4:
                                $('.carousel').carousel('prev');
                                slideNo--;
                                console.log(slideNo);

                                break;
                            case 5:
                                if($('input[name="have_campaign"]:checked').val() === "1"){
                                    $('.carousel').carousel('prev');
                                    slideNo--;
                                    console.log(slideNo);
                                }else if($('input[name="have_campaign"]:checked').val() === "2"){
                                    $('.carousel').carousel(2);
                                    slideNo--;
                                    slideNo--;
                                    console.log(slideNo);
                                }
                                break;
                            case 6:
                                $('.carousel').carousel('prev');
                                slideNo--;
                                console.log(slideNo);

                                $('#form-slider-next').show();

                                break;
                        }

                        if(slideNo === 1){
                            $('#form-slider-prev').hide();
                        }
                    });


                });

                function isValidEmailAddress(emailAddress) {
                    var pattern = new RegExp(/^(("[\w-+\s]+")|([\w-+]+(?:\.[\w-+]+)*)|("[\w-+\s]+")([\w-+]+(?:\.[\w-+]+)*))(@((?:[\w-+]+\.)*\w[\w-+]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][\d]\.|1[\d]{2}\.|[\d]{1,2}\.))((25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\.){2}(25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\]?$)/i);
                    return pattern.test(emailAddress);
                };
            </script>
        </div>
        <div class="row info-section">
            <div class="col-md-12">
                <p class="info-section-title">Who we are</p>
            </div>
            <div class="col-md-4 info-section-card">
                <h1 class="h1-title">Locodor</h1>
                <span class="small-line"></span>
                <p class="h1-desc">Locodor is a powerful crowdfunding social network platform / site that allows individuals & businesses to share their ideas with a global community of inventors, innovators, designers & tinkerers to seek funding from their supporters, funders and investors who belive in them and their ideas.</p>
            </div>
            <div class="col-md-4 info-section-card">
                <h1 class="h1-title">Crowdfunding Sites</h1>
                <span class="small-line"></span>
                <p class="h1-desc">Crowdfunding Sites and Crowdfunding Platforms offers the users the opportunity to get funds in a short time and finance their idea into becoming a reality. Crowdfunding for Startups is a big plus because, as we all know, the hardest part for creating a successfull startup is getting funded and the Crowdfunding Industry solves this issue.</p>
            </div>
            <div class="col-md-4 info-section-card">
                <h1 class="h1-title">Crowdfunding for Business</h1>
                <span class="small-line"></span>
                <p class="h1-desc">How Does Crowdfunding Work? Simple! You can do Crowdfunding for Business or just to see your Idea come true. All you have to do is to post your Crowdfunding Project on our platform and Socialize with our members. We will take care of the rest and you will get fully funded in no time!</p>
            </div>
        </div>
        <div class="row staff-picks-container">
            <div class="col-md-12 section-title-container">
                <h2  class="home-section-title">New Discoveries</h2>
                <a class="view-all" href="{{ route('projects') }}">View All</a>
                <div class="fancy-hr"></div>
            </div>
            @foreach($featured_projects as $project)
                <div class="col-md-12">
                    <div class="project-love-item clearfix row">
                        <div class="col-md-5">
                            <a class="project-love-image" href="{{ route('project.show',[ 'id'=>$project->id,'slug' => $project->slug ]) }}">
                                <img
                                    class="default-image"
                                    src="{{ asset($project->image) }}"
                                    alt="Crowdfunding Featured Project"
                                    title="{{$project->title}}"
                                    onerror="this.onerror=null; this.src='{{asset('/uploads/default-img-logo.png')}}';"
                                />
                            </a>
                        </div>
                        <div class="col-md-7">
                            <div class="project-love-item-content project-love-box">
                                <a href="{{ route('projects',['filter'=>'category','value' => $project->category->id]) }}" class="category">{{ $project->category->name }}</a>
                                <h3><a href="{{ route('project.show',[ 'id'=>$project->id ,'slug' => $project->slug]) }}">{{ $project->title }}</a></h3>
                                <div class="project-love-description"> {{ $project->subtitle }} </div>
                                <div class="project-love-author">
                                    <div class="author-profile">
                                        <a class="author-avatar" href="{{ route('profile.show',[ 'id'=>$project->user->profile->id,'slug' => $project->user->profile->slug ]) }}"><img src="{{ asset($project->user->profile->avatar) }}" alt="Crowdfunding Project Author Avatar" title="Project Author {{ $project->user->name }}"></a> <a class="author-name" href="{{ route('profile.show',[ 'id'=>$project->user->profile->id, 'slug' => $project->user->profile->slug ]) }}">{{ $project->user->name }}</a>
                                    </div>
                                    {{--<div class="author-address"><span class="ion-location"></span>{{ $project->user->city }} {{ $project->user->region }} {{ $project->user->country }}</div>--}}
                                </div>
                                <div class="process">
                                    <div class="raised"><span style="width:@if($project->projectSync) {{ floor((( $project->current_amount + $project->projectSync->amount ) / $project->project_goal)*100) }}% @else {{$project->funding_percent}}% @endif; max-width: 100%"></span></div>
                                    <div class="process-info">
                                        @if($project->projectSync)
                                            <div class="process-pledged"><span>${{$project->current_amount + $project->projectSync->amount}}</span><br><span>pledged</span></div>
                                            <div class="process-funded"><span>{{ floor((( $project->current_amount + $project->projectSync->amount ) / $project->project_goal)*100) }}%</span><br><span>funded</span></div>
                                            <div class="process-time"><span>{{ count($project->backers) + $project->projectSync->backers }}</span><br><span>backers</span></div>
                                        @else
                                            <div class="process-pledged"><span>${{ $project->current_amount }}</span><br><span>pledged</span></div>
                                            <div class="process-funded"><span>{{$project->funding_percent}}%</span><br><span>funded</span></div>
                                            <div class="process-time"><span>{{ count($project->backers) }}</span><br><span>backers</span></div>
                                        @endif
                                        <div class="process-time"><span>@if(floor( ($project->end_date - time()) / 60 / 60 / 24 ) > 0) {{ floor( ($project->end_date - time()) / 60 / 60 / 24 ) }}</span><br> <span>days left</span> @else <span>campaign</span><br><span>ended</span> @endif</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="row trending-projects-container">
            <div class="col-md-12 section-title-container">
                <h2  class="home-section-title">Trending Projects</h2>
                <a class="view-all" href="{{ route('projects',['filter'=>'sort','value'=>'trending']) }}">View All</a>
                <div class="fancy-hr"></div>
            </div>
            @foreach($projects as $project)
               <div class="col-md-6 col-lg-4 home-project-item">
                   <a href="{{ route('project.show',['id' => $project->id,'slug' => $project->slug]) }}">
                       <img
                           src="{{ asset($project->image) }}"
                           class="home-project-image"
                           alt="Crowdfunding Trending Project"
                           title="{{$project->title}}"
                           onerror="this.onerror=null; this.src='{{asset('/uploads/default-img-logo.png')}}';"
                       />
                   </a>
                   <p class="home-project-category">{{ $project->category->name }}</p>
                   <a href="{{ route('project.show',['id' => $project->id,'slug' => $project->slug]) }}"><h3 class="home-project-title">{{ $project->title }}</h3></a>
                   <?php
                       if (strlen(strip_tags($project->content)) > 230){
                           $str = substr(strip_tags($project->content), 0, 227) . '...';
                       }else{
                           $str = strip_tags($project->content);
                       }
                   ?>
                   <p class="home-project-content">{{ $str }}</p>
                   <div class="home-project-author-container">
                       <a href="{{ route('profile.show',['id' => $project->user->profile->id,'slug' => $project->user->profile->slug]) }}">
                           <img
                               src="{{ asset($project->user->profile->avatar) }}"
                               alt="crowdfunding-user-avatar"
                               title="Crowdfunding Project Author Avatar {{ $project->user->name }} }}"
                               onerror="this.onerror=null; this.src='{{asset('/uploads/favicon.png')}}';"
                           />
                           <span>{{ $project->user->name }}</span>
                       </a>
                   </div>
                    <div class="home-project-progress-bar">
                        <div style="width:@if($project->projectSync) {{ floor((( $project->current_amount + $project->projectSync->amount ) / $project->project_goal)*100) }}% @else {{$project->funding_percent}}% @endif;max-width:100%"></div>
                    </div>
                    <div class="home-project-statistics">
                        @if($project->projectSync)
                            <span>${{$project->current_amount + $project->projectSync->amount}}</span>
                            <span>{{ floor((( $project->current_amount + $project->projectSync->amount ) / $project->project_goal)*100) }}%</span>
                            <span>@if((floor(($project->end_date - time()) /60 /60 / 24)) < 1) Completed! @else {{floor(($project->end_date - time()) /60 /60 / 24)}} days to go @endif</span>
                        @else
                            <span>${{$project->current_amount}}</span>
                            <span>{{$project->funding_percent}}%</span>
                            <span>@if((floor(($project->end_date - time()) /60 /60 / 24)) < 1) Completed! @else {{floor(($project->end_date - time()) /60 /60 / 24)}} days to go @endif</span>
                        @endif
                    </div>
               </div>
            @endforeach
        </div>
        <div class="row">
            <div class="col-md-12 section-title-container">
                <h2 class="home-section-title">Our Newest Awesome Members</h2>
                <a href="{{ route('members') }}" class="view-all">View All</a>
                <div class="fancy-hr"></div>
                <div class="members-sorting">
                    <a href="{{ route('members') }}?sort=crowdfunders">Top Crowdfunders</a>
                    <a href="{{ route('members') }}?sort=backers">Top Backers</a>
                    <a href="{{ route('members') }}">All Members</a>
                </div>
            </div>
            @foreach($users as $user)
                <div class="col-sm-12 col-md-6 col-lg-3 home-user-element">
                    <div class="user-hover-container-home">
                        <div class="hover-profile">
                            <div class="row">
                                <div class="col-md-3 no-margin">
                                    <img
                                        class="hover-image"
                                        src="{{ $user->profile->avatar }}"
                                        width="50"
                                        height="50"
                                        alt="Crowdfunding User Avatar"
                                        title="{{$user->name}}'s Avatar"
                                        onerror="this.onerror=null; this.src='{{asset('/uploads/favicon.png')}}';"
                                    />
                                </div>
                                <div class="col-md-9 no-margin">
                                    <p><span class="hover-user-name"">{{ $user->name }}</span></p>
                                    <a href="{{ route('profile.show',['id' => $user->profile->id, 'slug' => $user->profile->slug]) }}">View profile</a>
                                </div>
                                <div class="col-md-12">
                                    <div class="simple-line"></div>
                                    <div class="hover-social">
                                        <p><b>Social</b></p>
                                        <p>
                                            @if(!in_array($user->id,$friends) && !in_array($user->id,$sentFriendRequests) && !in_array($user->id,$receivedFriendRequests))
                                                <a href="{{ route('add.friend',['id' => $user->id ]) }}" data-user-id="{{$user->id}}">Add Friend </a>
                                            @endif
                                            @if(in_array($user->id,$friends))
                                                <span><i class="fas fa-check"></i> Friends</span>
                                            @endif
                                            @if(in_array($user->id,$sentFriendRequests))
                                                <span>Pending</span>
                                            @endif
                                            @if(in_array($user->id,$receivedFriendRequests))
                                                <a href="{{ route('accept.friend',['id' => $user->id ]) }}" data-user-id="{{$user->id}}">Accept Friend Request</a>
                                                <a href="{{ route('remove.friend',['id' => $user->id ]) }}" data-user-id="{{$user->id}}">Decline Friend Request</a>
                                            @endif
                                        </p>
                                        <p><a href="{{ route('start.conversation',['id' => $user->id] ) }}"><i class="far fa-comment"></i> Private Message</a></p>
                                        <p><a href="{{ route('activity',['id' => $user->id] ) }}"><i class="far fa-comment"></i> Public Message</a></p>
                                    </div>
                                    <div class="simple-line"></div>
                                    <div class="hover-campaigns">
                                        <p><b>Campaigns</b></p>
                                        <p><a href="{{ route('profile.show',['id' => $user->profile->id, 'slug' => $user->profile->slug]) }}?created"><i class="far fa-lightbulb"></i> Created Campaigns({{ count($user->projects)  }})</a></p>
                                        <p><a href="{{ route('profile.show',['id' => $user->profile->id, 'slug' => $user->profile->slug]) }}?backed"><i class="fab fa-bitcoin"></i> Backed Campaigns({{ count($user->backers) }})</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach

            <script>
                $( document ).ready(function() {
                    $(".add-friend").click(function(e) {
                        e.preventDefault();

                        var btnClicked = $(this);
                        btnClicked.css('pointer-events', 'none');
                        var userID = btnClicked.data( "user-id" );

                        $.ajax({
                            type: "GET",
                            url: "/add-friend/"+userID,
                            success: function (result) {
                                btnClicked.css('display', 'none');
                                btnClicked.next().css('display', 'block');
                                btnClicked.css('pointer-events', 'initial');
                            },
                            error: function (result) {
                                console.log(result);
                                alert('Something went wrong.');
                            }
                        });
                    });
                });
            </script>

            <script>
                $( document ).ready(function() {
                    $(".remove-friend").click(function(e) {
                        e.preventDefault();

                        var btnClicked = $(this);
                        btnClicked.css('pointer-events', 'none');
                        var userID = btnClicked.data( "user-id" );

                        $.ajax({
                            type: "GET",
                            url: "/remove-friend/"+userID,
                            success: function (result) {
                                btnClicked.css('display', 'none');
                                btnClicked.prev().css('display', 'block');
                                btnClicked.css('pointer-events', 'initial');
                            },
                            error: function (result) {
                                console.log(result);
                                alert('Something went wrong.');
                            }
                        });
                    });
                });
            </script>

        </div>
        <div class="row home-blog-posts-container">
            <div class="col-md-12 section-title-container">
                <h2 class="home-section-title">Recent News</h2>
                <a href="{{ route('posts') }}" class="view-all">View All</a>
                <div class="fancy-hr"></div>
            </div>
            @foreach($recent_posts as $post)
                <div class="col-sm-12 col-md-6 col-lg-3 blog-post-container">
                    <div class="home-post-container">
                        <img
                            class="home-post-img"
                            src="{{asset($post->image)}}"
                            width="100%"
                            alt="Crowdfunding Article News"
                            title="{{$post->title}}"
                            onerror="this.onerror=null; this.src='{{asset('/uploads/default-img-logo.png')}}';"
                        />
                        <h3>{{ $post->title }}</h3>
                        <p class="blog-post-date"><b>{{ date('F jS Y', strtotime($post->created_at)) }}</b></p>
                        <?php
                        $stripped = strip_tags($post->content);
                        if (strlen($stripped) > 100)
                            $stripped = substr($stripped, 0, 100) . '...';
                        ?>
                        <p class="blog-post-content">{{ $stripped }}</p>
                        <a href="{{ route('post.show',['id' => $post->id,'slug'=>$post->slug]) }}" class="btn btn-primary blog-post-btn">Read more</a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
{{--    @if(Auth::guest())--}}
{{--        <div class="need-help-popup hide-on-mobile">--}}
{{--            <p>We send all kinds of awesome tips & tricks, announcements, and exclusive deals,<br> but you'll only hear from us if you really want to. Can we count you in?</p>--}}
{{--            <p>TELL US WHAT YOU WANT</p>--}}
{{--            <div class="need-help-form">--}}
{{--                <form action="{{ route('store.subscribed.user') }}" method="POST">--}}
{{--                    @csrf--}}
{{--                    <span class="subscribe-option"><label><input type="radio" name="subscribe" value="1" class="subscribe-agree"><span class="radio-checkmark-needhelp"></span> Yes! Count me in :)</label></span>--}}
{{--                    <span class="subscribe-option"><label><input type="radio" name="subscribe" value="0" class="subscribe-disagree"><span class="radio-checkmark-needhelp"></span> No thanks. Ignorance is bliss.<br></label></span>--}}
{{--                    <input class="subscribe-email" required type="email" name="email" placeholder="Type in your email address">--}}
{{--                    <button class="subscribe-button" type="submit">Submit</button>--}}
{{--                    <p class="subscribe-alert-message">Please check the box that you agree to get email notifications from us.</p>--}}
{{--                    <p class="subscribe-email-null-message">Please type in your email address</p>--}}
{{--                </form>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    @endif--}}
    <script>
        $( document ).ready(function() {
            $('.subscribe-button').on('click',function(e){

                e.preventDefault();
                var btnClicked = $(this);
                $('.subscribe-email-null-message').hide()

                if($('input[name="subscribe"]:checked').val() == 1){
                    var email = btnClicked.prev().val();
                    console.log(email);
                    if(email == ""){
                        $('.subscribe-email-null-message').show()
                    } else {
                        $.ajax({
                            type: "POST",
                            url: '/store-subscribed-user',
                            data: {
                                _token: "{{ csrf_token() }}",
                                email: email,
                            },
                            success: function(result) {
                                btnClicked.parent().parent().parent().html('<h1>Thank you for subscribing!</h1><div class="fancy-hr"></div>')
                                setTimeout(function(){ $('.need-help-popup').fadeOut() }, 2000);
                            },
                            error: function(result) {
                                console.log(result);
                                console.log('There is a problem');
                            }
                        });
                    }


                } else {
                    $('.subscribe-alert-message').show();
                }
            });

            $('.subscribe-agree').on('click',function(){
                $('.subscribe-alert-message').hide();
            })

            $('.subscribe-disagree').on('click',function(){
                $('.need-help-popup').fadeOut();
            })


        });
    </script>
    <div class="exit-intent-popup-background hide-on-mobile">
        <div class="exit-intent-popup">
            <div class="exit-left-container"><p>BE THE FIRST TO KNOW WHEN A SUPERBACKER IS ONLINE!</p></div>
            <div class="exit-center-container"></div>
            <div class="exit-right-container">
                <p>Up to 20% more backers for your campaign!</p>
                <a href="{{ route('register') }}">SUBSCRIBE NOW</a>
            </div>
        </div>
    </div>
    <script>

        $( document ).ready(function() {


            @if(!Auth::check())
            var exitPopup = 0;
            document.addEventListener("mouseleave", function(e){
                if( e.clientY < 0 && exitPopup == 0)
                {
                    var popup = $('.exit-intent-popup');

                    $('.exit-intent-popup-background').show();

                    popup.css("top", ( $(window).height() - $(popup).outerHeight() ) / 2 + "px");
                    popup.css("left", Math.max(0, (($(window).width() - $(popup).outerWidth()) / 2) +
                        $(window).scrollLeft()) + "px");

                    exitPopup = 1;


                }
            }, false);
            @endif

            $('.exit-intent-popup-background').on('click',function(){
                $(this).hide();
            });



            setTimeout(function(){

                $('.need-help-popup').fadeIn();
                var popupInterval = setInterval(function(){
                    if($('.need-help-popup input').is(':focus') || $('.need-help-popup').is(':hover')){
                    }else{
                        $('.need-help-popup').fadeOut();
                        clearInterval(popupInterval);
                    }
                }, 30000);
            }, 3000);

            $('#take-tour').on('click',function (e) {
                e.preventDefault();
                var btnClicked = $(this);

                var tourEmail = btnClicked.parent().prev().children().first().val();

                $.ajax({
                    type: "POST",
                    url: "{{ route('take.tour') }}",
                    data: {
                        _token: '{{csrf_token()}}',
                        email: tourEmail
                    },
                    success: function (result) {
                        btnClicked.parent().parent().parent().html('<p>Thank you!</p>\n' +
                            '<div class="fancy-hr"></div>\n' +
                            '<p>We will contact your shortly!</p>');
                        console.log('submited');
                    },
                    error: function (result) {
                        console.log(result);
                        alert('Something went wrong.');
                    }
                });

            });

            $('.need-help-popup-close').on('click',function(){
                $('.need-help-popup').fadeOut();
            });
        });
    </script>
@endsection

@section('funky-theme-footer')
    <!-- JAVASCRIPT
    ================================================== -->
    <script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/royal_preloader.min.js"></script>
    <script type="text/javascript" src="js/tether.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/plugins.js"></script>
    <script type="text/javascript" src="js/custom/custom-media-agency.js"></script>
@endsection
