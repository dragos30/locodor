@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <h1>Locodor's Awesome Members!</h1>
        <div class="simple-line"></div>

        <div class="members-sorting">
            <a href="{{ route('members') }}?sort=crowdfunders">Top Crowdfunders</a>
            <a href="{{ route('members') }}?sort=backers">Top Backers</a>
            <a href="{{ route('members') }}">All Members</a>
        </div>
        <div class="pages-links d-flex">
            <div class="mx-auto">
                {{ $users->appends(Illuminate\Support\Facades\Input::except('page'))->links() }}
            </div>
        </div>
        <div class="row">
            @foreach($users as $user)
                <div class="col-md-4 col-lg-4 col-xl-2 members-user-item text-center">
                    <img src="{{ asset($user->profile->avatar) }}">
                    <p class="user-name"><a href="{{ route('profile.show',['id' => $user->profile->id,'slug'=> $user->profile->slug]) }}">{{ $user->name }}</a></p>
                    <p class="user-about">{{ substr( $user->profile->about , 0,100) }}</p>
                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle fancy-btn members-action-btn" type="button" id="dropdownMenuButton-{{ $user->id }}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Actions
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton-{{ $user->id }}">
                            <a class="dropdown-item" href="{{ route('start.conversation',['id' => $user->id ]) }}">Message</a>
                            <a class="dropdown-item" href="{{ route('profile.show',['id' => $user->profile->id,'slug'=> $user->profile->slug]) }}">View profile</a>
                            @if(!in_array($user->id,$friends) && !in_array($user->id,$sentFriendRequests) && !in_array($user->id,$receivedFriendRequests))
                                <a class="dropdown-item" href="{{ route('add.friend',['id' => $user->id ]) }}" data-user-id="{{$user->id}}">Add Friend </a>
                            @endif
                            @if(in_array($user->id,$friends))
                                <a class="dropdown-item" href="{{ route('remove.friend',['id' => $user->id ]) }}" data-user-id="{{$user->id}}">Unfriend </a>
                            @endif
                            @if(in_array($user->id,$sentFriendRequests))
                                <a class="dropdown-item" href="{{ route('remove.friend',['id' => $user->id ]) }}" data-user-id="{{$user->id}}">Cancel Friend Request</a>
                            @endif
                            @if(in_array($user->id,$receivedFriendRequests))
                                <a class="dropdown-item" href="{{ route('accept.friend',['id' => $user->id ]) }}" data-user-id="{{$user->id}}">Accept Friend Request</a>
                                <a class="dropdown-item" href="{{ route('remove.friend',['id' => $user->id ]) }}" data-user-id="{{$user->id}}">Decline Friend Request</a>
                            @endif
                        </div>
                    </div>
                    {{--<a href="{{ route('add.friend',['id' => $user->id ]) }}" class="member-add add-friend" data-user-id="{{$user->id}}" @if($user->id != Auth::id() && !in_array($user->id, $loggedUserFriendsId)) style="display: inline-block" @endif>Add Friend </a>--}}
                    {{--<a href="{{ route('remove.friend',['id' => $user->id ]) }}"  class="member-add remove-friend" data-user-id="{{$user->id}}" @if($user->id != Auth::id() && in_array($user->id, $loggedUserFriendsId)) style="display: inline-block" @endif>Remove Friend </a>--}}
                    {{--<a href="{{ route('accept.friend',['id' => $user->id ]) }}"  class="member-add accept-friend" data-user-id="{{$user->id}}" @if($user->id != Auth::id() && in_array($user->id, $loggedUserFriendsId) && array_key_exists($user->id,$receivedFriendRequests)) style="display: inline-block" @endif>Accept Friend </a>--}}
                </div>
            @endforeach

                <script>
                    $( document ).ready(function() {
                        $(".accept-friend").click(function(e) {
                            e.preventDefault();

                            var btnClicked = $(this);
                            btnClicked.css('pointer-events', 'none');
                            var userID = btnClicked.data( "user-id" );

                            $.ajax({
                                type: "GET",
                                url: "/accept-friend/"+userID,
                                success: function (result) {
                                    btnClicked.css('display', 'none');
                                },
                                error: function (result) {
                                    console.log(result);
                                    alert('Something went wrong.');
                                }
                            });
                        });
                    });
                </script>

                <script>
                    $( document ).ready(function() {
                        $(".add-friend").click(function(e) {
                            e.preventDefault();

                            var btnClicked = $(this);
                            btnClicked.css('pointer-events', 'none');
                            var userID = btnClicked.data( "user-id" );

                            $.ajax({
                                type: "GET",
                                url: "/add-friend/"+userID,
                                success: function (result) {
                                    btnClicked.css('display', 'none');
                                    btnClicked.next().css('display', 'inline-block');
                                    btnClicked.css('pointer-events', 'initial');
                                },
                                error: function (result) {
                                    console.log(result);
                                    alert('Something went wrong.');
                                }
                            });
                        });
                    });
                </script>

                <script>
                    $( document ).ready(function() {
                        $(".remove-friend").click(function(e) {
                            e.preventDefault();

                            var btnClicked = $(this);
                            btnClicked.css('pointer-events', 'none');
                            var userID = btnClicked.data( "user-id" );

                            $.ajax({
                                type: "GET",
                                url: "/remove-friend/"+userID,
                                success: function (result) {
                                    btnClicked.css('display', 'none');
                                    btnClicked.prev().css('display', 'inline-block');
                                    btnClicked.css('pointer-events', 'initial');
                                },
                                error: function (result) {
                                    console.log(result);
                                    alert('Something went wrong.');
                                }
                            });
                        });
                    });
                </script>
        </div>
    </div>
@endsection