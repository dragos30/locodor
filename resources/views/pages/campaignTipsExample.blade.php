@extends('layouts.simple')

@section('content')
    <div class="container-fluid">
        <h3>Note! This is just a Demo!<br> In Order to benefit from the Campaign Tasks Features you must have an Exclusive Project on Locodor!</h3>
        <h3>For More information contact us at <a href="mailto:contact@locodor.com">contact@locodor.com</a></h3>
        <hr>
        <h1>Campaign Tips</h1>
        <table class="campaign-tips-table-1">
            <tr>
                <td>Status</td>
                <td>Task</td>
                <td>Assigned to</td>
                <td>Deadline</td>
                <td>Notes</td>
            </tr>
            <tr>
                <td colspan="5" style="text-align: center">Pre Campaign</td>
            </tr>
            <tr>
                <td>
                    <select>
                        <option>Ongoing</option>
                        <option>Completed</option>
                    </select>
                </td>
                <td>Research your audience(who, why and where)</td>
                <td><input type="text"></td>
                <td><input type="date"></td>
                <td><textarea rows="1"></textarea></td>
            </tr>
            <tr>
                <td>
                    <select>
                        <option>Ongoing</option>
                        <option>Completed</option>
                    </select>
                </td>
                <td>Legal matters (bank account and legal structure)</td>
                <td><input type="text"></td>
                <td><input type="date"></td>
                <td><textarea rows="1"></textarea></td>
            </tr>
            <tr>
                <td>
                    <select>
                        <option>Ongoing</option>
                        <option>Completed</option>
                    </select>
                </td>
                <td>Get your team together</td>
                <td><input type="text"></td>
                <td><input type="date"></td>
                <td><textarea rows="1"></textarea></td>
            </tr>
            <tr>
                <td>
                    <select>
                        <option>Ongoing</option>
                        <option>Completed</option>
                    </select>
                </td>
                <td>Research and choose the platform</td>
                <td><input type="text"></td>
                <td><input type="date"></td>
                <td><textarea rows="1"></textarea></td>
            </tr>
            <tr>
                <td>
                    <select>
                        <option>Ongoing</option>
                        <option>Completed</option>
                    </select>
                </td>
                <td>Decide on the realistic goal</td>
                <td><input type="text"></td>
                <td><input type="date"></td>
                <td><textarea rows="1"></textarea></td>
            </tr>
            <tr>
                <td>
                    <select>
                        <option>Ongoing</option>
                        <option>Completed</option>
                    </select>
                </td>
                <td>Brainstorm campaign ideas with your team</td>
                <td><input type="text"></td>
                <td><input type="date"></td>
                <td><textarea rows="1"></textarea></td>
            </tr>
            <tr>
                <td>
                    <select>
                        <option>Ongoing</option>
                        <option>Completed</option>
                    </select>
                </td>
                <td>Build a timeline</td>
                <td><input type="text"></td>
                <td><input type="date"></td>
                <td><textarea rows="1"></textarea></td>
            </tr>
            <tr>
                <td>
                    <select>
                        <option>Ongoing</option>
                        <option>Completed</option>
                    </select>
                </td>
                <td>Research your community and its online activity</td>
                <td><input type="text"></td>
                <td><input type="date"></td>
                <td><textarea rows="1"></textarea></td>
            </tr>
            <tr>
                <td>
                    <select>
                        <option>Ongoing</option>
                        <option>Completed</option>
                    </select>
                </td>
                <td>Start growing your online and offline communities</td>
                <td><input type="text"></td>
                <td><input type="date"></td>
                <td><textarea rows="1"></textarea></td>
            </tr>
            <tr>
                <td>
                    <select>
                        <option>Ongoing</option>
                        <option>Completed</option>
                    </select>
                </td>
                <td>Research potential partners</td>
                <td><input type="text"></td>
                <td><input type="date"></td>
                <td><textarea rows="1"></textarea></td>
            </tr>
            <tr>
                <td>
                    <select>
                        <option>Ongoing</option>
                        <option>Completed</option>
                    </select>
                </td>
                <td>Research potential press connections</td>
                <td><input type="text"></td>
                <td><input type="date"></td>
                <td><textarea rows="1"></textarea></td>
            </tr>
            <tr>
                <td>
                    <select>
                        <option>Ongoing</option>
                        <option>Completed</option>
                    </select>
                </td>
                <td>Research potential networks</td>
                <td><input type="text"></td>
                <td><input type="date"></td>
                <td><textarea rows="1"></textarea></td>
            </tr>
            <tr>
                <td>
                    <select>
                        <option>Ongoing</option>
                        <option>Completed</option>
                    </select>
                </td>
                <td>Home your campaign pitch(what, why and to whom)</td>
                <td><input type="text"></td>
                <td><input type="date"></td>
                <td><textarea rows="1"></textarea></td>
            </tr>
            <tr>
                <td>
                    <select>
                        <option>Ongoing</option>
                        <option>Completed</option>
                    </select>
                </td>
                <td>Test your pitch</td>
                <td><input type="text"></td>
                <td><input type="date"></td>
                <td><textarea rows="1"></textarea></td>
            </tr>
            <tr>
                <td>
                    <select>
                        <option>Ongoing</option>
                        <option>Completed</option>
                    </select>
                </td>
                <td>Create a budget</td>
                <td><input type="text"></td>
                <td><input type="date"></td>
                <td><textarea rows="1"></textarea></td>
            </tr>
            <tr>
                <td>
                    <select>
                        <option>Ongoing</option>
                        <option>Completed</option>
                    </select>
                </td>
                <td>Collect campaigns media (images, videos)</td>
                <td><input type="text"></td>
                <td><input type="date"></td>
                <td><textarea rows="1"></textarea></td>
            </tr>
            <tr>
                <td>
                    <select>
                        <option>Ongoing</option>
                        <option>Completed</option>
                    </select>
                </td>
                <td>Determine perks</td>
                <td><input type="text"></td>
                <td><input type="date"></td>
                <td><textarea rows="1"></textarea></td>
            </tr>
            <tr>
                <td>
                    <select>
                        <option>Ongoing</option>
                        <option>Completed</option>
                    </select>
                </td>
                <td>Test your perks with your target audience</td>
                <td><input type="text"></td>
                <td><input type="date"></td>
                <td><textarea rows="1"></textarea></td>
            </tr>
            <tr>
                <td>
                    <select>
                        <option>Ongoing</option>
                        <option>Completed</option>
                    </select>
                </td>
                <td>Prepare a publishing plan for the campaign</td>
                <td><input type="text"></td>
                <td><input type="date"></td>
                <td><textarea rows="1"></textarea></td>
            </tr>
            <tr>
                <td>
                    <select>
                        <option>Ongoing</option>
                        <option>Completed</option>
                    </select>
                </td>
                <td>Get your team to evaluate their availability</td>
                <td><input type="text"></td>
                <td><input type="date"></td>
                <td><textarea rows="1"></textarea></td>
            </tr>
            <tr>
                <td>
                    <select>
                        <option>Ongoing</option>
                        <option>Completed</option>
                    </select>
                </td>
                <td>Prepare press releases / an EPK Website</td>
                <td><input type="text"></td>
                <td><input type="date"></td>
                <td><textarea rows="1"></textarea></td>
            </tr>
            <tr>
                <td>
                    <select>
                        <option>Ongoing</option>
                        <option>Completed</option>
                    </select>
                </td>
                <td>Contact your networks to secure a 20-30% seed investment</td>
                <td><input type="text"></td>
                <td><input type="date"></td>
                <td><textarea rows="1"></textarea></td>
            </tr>
            <tr>
                <td>
                    <select>
                        <option>Ongoing</option>
                        <option>Completed</option>
                    </select>
                </td>
                <td>Start soft launch (if applicable)</td>
                <td><input type="text"></td>
                <td><input type="date"></td>
                <td><textarea rows="1"></textarea></td>
            </tr>
            <tr>
                <td colspan="5">Campaign Launch</td>
            </tr>
            <tr>
                <td>
                    <select>
                        <option>Ongoing</option>
                        <option>Completed</option>
                    </select>
                </td>
                <td>Press release about the launch</td>
                <td><input type="text"></td>
                <td><input type="date"></td>
                <td><textarea rows="1"></textarea></td>
            </tr>
            <tr>
                <td>
                    <select>
                        <option>Ongoing</option>
                        <option>Completed</option>
                    </select>
                </td>
                <td>Keep growing your network</td>
                <td><input type="text"></td>
                <td><input type="date"></td>
                <td><textarea rows="1"></textarea></td>
            </tr>
            <tr>
                <td>
                    <select>
                        <option>Ongoing</option>
                        <option>Completed</option>
                    </select>
                </td>
                <td>Launch your email campaign</td>
                <td><input type="text"></td>
                <td><input type="date"></td>
                <td><textarea rows="1"></textarea></td>
            </tr>
            <tr>
                <td>
                    <select>
                        <option>Ongoing</option>
                        <option>Completed</option>
                    </select>
                </td>
                <td>Weekly email campaigns</td>
                <td><input type="text"></td>
                <td><input type="date"></td>
                <td><textarea rows="1"></textarea></td>
            </tr>
            <tr>
                <td>
                    <select>
                        <option>Ongoing</option>
                        <option>Completed</option>
                    </select>
                </td>
                <td>Analize and tweak perks</td>
                <td><input type="text"></td>
                <td><input type="date"></td>
                <td><textarea rows="1"></textarea></td>
            </tr>
            <tr>
                <td>
                    <select>
                        <option>Ongoing</option>
                        <option>Completed</option>
                    </select>
                </td>
                <td>Thank your supporters</td>
                <td><input type="text"></td>
                <td><input type="date"></td>
                <td><textarea rows="1"></textarea></td>
            </tr>
            <tr>
                <td>
                    <select>
                        <option>Ongoing</option>
                        <option>Completed</option>
                    </select>
                </td>
                <td>Daily campaign updates (interviews, behind the scenes...)</td>
                <td><input type="text"></td>
                <td><input type="date"></td>
                <td><textarea rows="1"></textarea></td>
            </tr>
            <tr>
                <td>
                    <select>
                        <option>Ongoing</option>
                        <option>Completed</option>
                    </select>
                </td>
                <td>Keep your partners informed about the campaign</td>
                <td><input type="text"></td>
                <td><input type="date"></td>
                <td><textarea rows="1"></textarea></td>
            </tr>
            <tr>
                <td colspan="5">Post Campaign</td>
            </tr>
            <tr>
                <td>
                    <select>
                        <option>Ongoing</option>
                        <option>Completed</option>
                    </select>
                </td>
                <td>Inform supporters about the campaign results</td>
                <td><input type="text"></td>
                <td><input type="date"></td>
                <td><textarea rows="1"></textarea></td>
            </tr>
            <tr>
                <td>
                    <select>
                        <option>Ongoing</option>
                        <option>Completed</option>
                    </select>
                </td>
                <td>Deliver Perks</td>
                <td><input type="text"></td>
                <td><input type="date"></td>
                <td><textarea rows="1"></textarea></td>
            </tr>
            <tr>
                <td>
                    <select>
                        <option>Ongoing</option>
                        <option>Completed</option>
                    </select>
                </td>
                <td>Send a post campaign press release</td>
                <td><input type="text"></td>
                <td><input type="date"></td>
                <td><textarea rows="1"></textarea></td>
            </tr>
        </table>

        <div class="fancy-hr"></div>
        <table class="campaign-tips-table-2">
            <tr>
                <td colspan="7">Team Contact List</td>
            </tr>
            <tr>
                <td>Name</td>
                <td>Role</td>
                <td>Availability</td>
                <td>Social Media Link</td>
                <td>Email</td>
                <td>Phone</td>
                <td><textarea rows="1"></textarea></td>
            </tr>
            <tr>
                <td><input type="text" name="member_name[]"></td>
                <td><input type="text" name="member_role[]"></td>
                <td><input type="text" name="member_availability[]"></td>
                <td><input type="text" name="member_link[]"></td>
                <td><input type="text" name="member_email[]"></td>
                <td><input type="text" name="member_phone[]"></td>
                <td><input type="text" name="member_note[]"></td>
                <td><button class="remove-row"><i class="fas fa-times"></i> Remove</button></td>
            </tr>
            <tr>
                <td colspan="7" class="add-row"><i class="far fa-plus-square"></i> Add Row</td>
            </tr>
        </table>

        <div class="fancy-hr"></div>

        <table class="campaign-tips-table-3">
            <tr>
                <td colspan="3">Publishing Plan</td>
                <td colspan="5">Perks Total (cost 1750 deducted)</td>
            </tr>
            <tr>
                <td>Perk Price(USD)</td>
                <td>Perk Cost(USD)</td>
                <td>Sales Goal</td>
                <td>Perk Profit</td>
                <td>Perk Name</td>
                <td>Short Description</td>
                <td>Tested Yes / No</td>
                <td><textarea rows="1"></textarea></td>
            </tr>

            <tr>
                <td><input type="number" name="perk_price[]"></td>
                <td><input type="number" name="perk_cost[]"></td>
                <td><input type="number" name="sales_goal[]"></td>
                <td>1750</td>
                <td><input type="text" name="perk_name[]"></td>
                <td><textarea rows="1" name="perk_description[]"></textarea></td>
                <td>
                    <select name="perk_tested[]">
                        <option value="1">Yes</option>
                        <option value="0">No</option>
                    </select>
                </td>
                <td><textarea rows="1" name="perk_notes[]"></textarea></td>
                <td><button class="remove-row"><i class="fas fa-times"></i> Remove</button></td>
            </tr>
            <tr>
                <td colspan="8" class="add-row"><i class="far fa-plus-square"></i> Add Row</td>
            </tr>
        </table>

        <div class="fancy-hr"></div>

        <table class="campaign-tips-table-4">
            <tr>
                <td colspan="23">Publishing Plan</td>
            </tr>
            <tr>
                <td>WEEK</td>
                <td></td>
                <td colspan="3">Monday</td>
                <td colspan="3">Tuesday</td>
                <td colspan="3">Wednesday</td>
                <td colspan="3">Thursday</td>
                <td colspan="3">Friday</td>
                <td colspan="3">Saturday</td>
                <td colspan="3">Sunday</td>
            </tr>
            <tr>
                <td></td>
                <td>Focus</td>
                <td>Social Channels</td>
                <td>Social Channels</td>
                <td>Email</td>
                <td>Social Channels</td>
                <td>Social Channels</td>
                <td>Email</td>
                <td>Social Channels</td>
                <td>Social Channels</td>
                <td>Email</td>
                <td>Social Channels</td>
                <td>Social Channels</td>
                <td>Email</td>
                <td>Social Channels</td>
                <td>Social Channels</td>
                <td>Email</td>
                <td>Social Channels</td>
                <td>Social Channels</td>
                <td>Email</td>
                <td>Social Channels</td>
                <td>Social Channels</td>
                <td>Email</td>
            </tr>

            <tr>
                <td>Week 1</td>
                <td><input type="text" name="focus[]"></td>
                <td><input type="text" name="social_channel_monday_1[]"></td>
                <td><input type="text" name="social_channel_monday_2[]"></td>
                <td><input type="text" name="email_monday[]"></td>
                <td><input type="text" name="social_channel_tuesday_1[]"></td>
                <td><input type="text" name="social_channel_tuesday_2[]"></td>
                <td><input type="text" name="email_tuesday[]"></td>
                <td><input type="text" name="social_channel_wednesday_1[]"></td>
                <td><input type="text" name="social_channel_wednesday_2[]"></td>
                <td><input type="text" name="email_wednesday[]"></td>
                <td><input type="text" name="social_channel_thursday_1[]"></td>
                <td><input type="text" name="social_channel_thursday_2[]"></td>
                <td><input type="text" name="email_thursday[]"></td>
                <td><input type="text" name="social_channel_friday_1[]"></td>
                <td><input type="text" name="social_channel_friday_2[]"></td>
                <td><input type="text" name="email_friday[]"></td>
                <td><input type="text" name="social_channel_saturday_1[]"></td>
                <td><input type="text" name="social_channel_saturday_2[]"></td>
                <td><input type="text" name="email_saturday[]"></td>
                <td><input type="text" name="social_channel_sunday_1[]"></td>
                <td><input type="text" name="social_channel_sunday_2[]"></td>
                <td><input type="text" name="email_sunday[]"></td>
                <td><button class="remove-row"><i class="fas fa-times"></i> Remove</button></td>
            </tr>

            <tr>
                <td colspan="23" class="add-row add-row-week-plan text-left"><i class="far fa-plus-square"></i> Add Row</td>
            </tr>
        </table>

        <script>

            $( document ).ready(function() {
                $('.add-row').on('click',function () {
                    var html= $(this).parent().prev().clone();
                    console.log(html);
                    $(this).parent().before(html);
                    $('.remove-row').on('click',function () {
                        $(this).parent().parent().remove();
                    });
                });

                $('.remove-row').on('click',function () {
                    $(this).parent().parent().remove();
                });

                var weekNumber = 1;
                $('.add-row-week-plan').on('click',function(){
                    weekNumber++;
                    var week = $(this).parent().prev().children().first().text('Week '+ weekNumber);
                });
            });
        </script>

        <div class="fancy-hr"></div>

        <table class="campaign-tips-table-5">
            <tr>
                <td colspan="9">
                    Publishing Plan
                </td>
            </tr>
            <tr>
                <td>Checklist Sheet</td>
                <td>Team Sheet</td>
                <td></td>
                <td>Perks Sheet</td>
                <td></td>
                <td>General</td>
                <td>Publishing Plan</td>
                <td>Social Media</td>
                <td>Email</td>
            </tr>
            <tr>
                <td>Task Status</td>
                <td>Roles</td>
                <td>Availability</td>
                <td>Pricing</td>
                <td>Comission(%)</td>
                <td></td>
                <td>Focus</td>
                <td></td>
                <td></td>
            </tr>

                <tr>
                    <td>
                        <select name="task_status[]">
                            <option value="open">Open</option>
                            <option value="assigned">Assigned</option>
                            <option value="in-progress">In Progress</option>
                            <option value="done">Done</option>
                        </select>
                    </td>
                    <td><input type="text" name="role[]"></td>
                    <td><input type="number" name="availability[]"></td>
                    <td><input type="number" name="pricing[]"></td>
                    <td><input type="number" name="commission[]"></td>
                    <td>
                        <select name="general[]">
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select>
                    </td>
                    <td><input type="text" name="focus[]"></td>
                    <td><input type="text" name="social_media[]"></td>
                    <td><input type="text" name="email[]"></td>
                    <td><button class="remove-row"><i class="fas fa-times"></i> Remove</button></td>
                </tr>

            <tr>
                <td colspan="9" class="add-row text-left"><i class="far fa-plus-square"></i> Add Row</td>
            </tr>
        </table>
    </div>
@endsection