@extends('layouts.app')

@section('content')
    <div class="evaluate-page container-fluid">
        <div class="row justify-content-center">
            <div class="progress-sidebar col-xl-2">
                <span class="progress-sidebar-toggle"><i class="fas fa-times"></i></span>
                <div class="evaluate-sidebar-title">
                    Progress
                </div>
                <a href="#" data-menu-item="1" class="q-navigation-item">
                    <div>
                        <span class="question-dot question-dot-selected"></span>
                        <span>First Campaign?</span>
                    </div>
                </a>
                <a href="#" data-menu-item="2" class="q-navigation-item">
                    <div>
                        <span class="question-dot"></span>
                        <span>Previous URL</span>
                    </div>
                </a>
                <a href="#" data-menu-item="3" class="q-navigation-item">
                    <div>
                        <span class="question-dot"></span>
                        <span>Reached Goal</span>
                    </div>
                </a>
                <a href="#" data-menu-item="4" class="q-navigation-item">
                    <div>
                        <span class="question-dot"></span>
                        <span>Number of Backers</span>
                    </div>
                </a>
                <a href="#" data-menu-item="5" class="q-navigation-item">
                    <div>
                        <span class="question-dot"></span>
                        <span>Percent Reached</span>
                    </div>
                </a>
                <a href="#" data-menu-item="6" class="q-navigation-item">
                    <div>
                        <span class="question-dot"></span>
                        <span>Project Type</span>
                    </div>
                </a>
                <a href="#" data-menu-item="7" class="q-navigation-item">
                    <div>
                        <span class="question-dot"></span>
                        <span>Crowdfunding Platforms</span>
                    </div>
                </a>
                <a href="#" data-menu-item="8" class="q-navigation-item">
                    <div>
                        <span class="question-dot"></span>
                        <span>Preparing Days</span>
                    </div>
                </a>
                <a href="#" data-menu-item="9" class="q-navigation-item">
                    <div>
                        <span class="question-dot"></span>
                        <span>Estimated Reach</span>
                    </div>
                </a>
                <a href="#" data-menu-item="10" class="q-navigation-item">
                    <div>
                        <span class="question-dot"></span>
                        <span>Goal</span>
                    </div>
                </a>
                <a href="#" data-menu-item="11" class="q-navigation-item">
                    <div>
                        <span class="question-dot"></span>
                        <span>Days</span>
                    </div>
                </a>
                <a href="#" data-menu-item="12" class="q-navigation-item">
                    <div>
                        <span class="question-dot"></span>
                        <span>Money</span>
                    </div>
                </a>
                <a href="#" data-menu-item="13" class="q-navigation-item">
                    <div>
                        <span class="question-dot"></span>
                        <span>Team Members</span>
                    </div>
                </a>
                <a href="#" data-menu-item="14" class="q-navigation-item">
                    <div>
                        <span class="question-dot"></span>
                        <span>Pledges</span>
                    </div>
                </a>
                <a href="#" data-menu-item="15" class="q-navigation-item">
                    <div>
                        <span class="question-dot"></span>
                        <span>Target</span>
                    </div>
                </a>
            </div>
            <div class="col-xl-10">
                <div class="row justify-content-center">
                    <div class="col-md-10 evaluate-container">
                        <span class="progress-sidebar-toggle"><i class="fas fa-bars"></i></span>
                        <div data-q-number="1" class="display-evaluate-question">
                            <div class="evaluate-header">
                                <p>First Campaign?</p>
                            </div>
                            <div class="evaluate-question">
                                <p class="evaluate-text">Is this your first crowdfunding campaign?</p>

                                <input type="radio" name="first_campaign" value="1" id="first-campaign-yes">
                                <label for="first-campaign-yes">Yes</label>
                                <input type="radio" name="first_campaign" value="2" id="first-campaign-no">
                                <label for="first-campaign-no">No</label>
                            </div>
                        </div>

                        <div data-q-number="2">
                            <div class="evaluate-header">
                                <p>Previous URL</p>
                            </div>
                            <div class="evaluate-question">
                                <p class="evaluate-text">If you have one, please include your previous campaign URL:</p>

                                <input type="text" placeholder="Type your URL here..." name="campaign_url">
                            </div>
                        </div>

                        <div data-q-number="3">
                            <div class="evaluate-header">
                                <p>Reached Goal</p>
                            </div>
                            <div class="evaluate-question">
                                <p class="evaluate-text">Did your previous campaign reached the target goal?</p>

                                <input type="radio" name="previous_goal" value="1" id="previous_goal_yes">
                                <label for="previous_goal_yes">Yes</label>
                                <input type="radio" name="previous_goal" value="2" id="previous_goal_no">
                                <label for="previous_goal_no">No</label>
                            </div>
                        </div>

                        <div data-q-number="4">
                            <div class="evaluate-header">
                                <p>Number of Backers</p>
                            </div>
                            <div class="evaluate-question">
                                <p class="evaluate-text">How many backers did you had in your previous campaigns?</p>

                                <input type="number" placeholder="Please write the average number of backers of your previous campaigns" name="backers_number">
                            </div>
                        </div>

                        <div data-q-number="5">
                            <div class="evaluate-header">
                                <p>Percent Reached</p>
                            </div>
                            <div class="evaluate-question">
                                <p class="evaluate-text">What percentage of funding you had on your previous campaigns?</p>

                                <input type="number" placeholder="Please write the average percentage of your previous campaigns" name="previous_percentage">
                            </div>
                        </div>

                        <div data-q-number="6">
                            <div class="evaluate-header">
                                <p>Project Type</p>
                            </div>
                            <div class="evaluate-question">
                                <p class="evaluate-text">What type of crowdfunding campaign do you want to create?</p>

                                <input type="radio" name="type" value="1" id="reward_based">
                                <label for="reward_based">Reward Based</label>
                                <input type="radio" name="type" value="1" id="equity_based">
                                <label for="equity_based">Equity Based</label>
                                <input type="radio" name="type" value="1" id="donation_based">
                                <label for="donation_based">Donation Based</label>
                            </div>
                        </div>

                        <div data-q-number="7">
                            <div class="evaluate-header">
                                <p>Crowdfunding Platforms</p>
                            </div>
                            <div class="evaluate-question">
                                <p class="evaluate-text">On how many crowdfunding platforms do you plan to upload your campaign?</p>

                                <input type="number" placeholder="Please write the number of platforms you used for your previous campaigns" name="platform_numbers">
                            </div>
                        </div>

                        <div data-q-number="8">
                            <div class="evaluate-header">
                                <p>Preparing Days</p>
                            </div>
                            <div class="evaluate-question">
                                <p class="evaluate-text">How many days do you spent on preparing the campaign before making it live?</p>

                                <input type="number" placeholder="Please write hthe average the number of days your previous campaigns were live" name="average_days">
                            </div>
                        </div>

                        <div data-q-number="9">
                            <div class="evaluate-header">
                                <p>Estimated Reach</p>
                            </div>
                            <div class="evaluate-question">
                                <p class="evaluate-text">How many people do you estimate to be able to reach out with your campaign on the launch day?</p>

                                <input type="number" placeholder="Please write the number of platforms you used for your previous campaigns" name="estimated_reach">
                            </div>
                        </div>

                        <div data-q-number="10">
                            <div class="evaluate-header">
                                <p>Goal</p>
                            </div>
                            <div class="evaluate-question">
                                <p class="evaluate-text">What is goal you want to reach (USD)?</p>

                                <input type="number" placeholder="Please write the goal you want to reach in USD..." name="goal">
                            </div>
                        </div>

                        <div data-q-number="11">
                            <div class="evaluate-header">
                                <p>Days</p>
                            </div>
                            <div class="evaluate-question">
                                <p class="evaluate-text">How many days will your campaign have to reach the goal?</p>

                                <input type="number" placeholder="Please write how many days will your campaign be live..." name="days">
                            </div>
                        </div>

                        <div data-q-number="12">
                            <div class="evaluate-header">
                                <p>Money</p>
                            </div>
                            <div class="evaluate-question">
                                <p class="evaluate-text">How much money are you will to invest to promote your campaign?</p>

                                <input type="number" placeholder="Please write how much money (in USD) are you willing to invest to promote your campaign..." name="money">
                            </div>
                        </div>

                        <div data-q-number="13">
                            <div class="evaluate-header">
                                <p>Team Members</p>
                            </div>
                            <div class="evaluate-question">
                                <p class="evaluate-text">How many team members do you got for your campaign?</p>

                                <input type="number" placeholder="Please write how many team members are Contributing to your campaign..." name="team_members">
                            </div>
                        </div>

                        <div data-q-number="14">
                            <div class="evaluate-header">
                                <p>Pledges</p>
                            </div>
                            <div class="evaluate-question">
                                <p class="evaluate-text">How many pledges do you got for your campaign?</p>

                                <input type="number" placeholder="Please write how many pledges you got for your campaign..." name="pledges">
                            </div>
                        </div>

                        <div data-q-number="15">
                            <div class="evaluate-header">
                                <p>Target</p>
                            </div>
                            <div class="evaluate-question">
                                <p class="evaluate-text">Which one of the following continents are you going to target the most?</p>
                                <input type="radio" name="target" value="1" id="target_1">
                                <label for="target_1">North America</label>
                                <input type="radio" name="target" value="2" id="target_2">
                                <label for="target_2">South America</label>
                                <input type="radio" name="target" value="3" id="target_3">
                                <label for="target_3">Europe</label>
                                <input type="radio" name="target" value="4" id="target_4">
                                <label for="target_4">Asia</label>
                                <input type="radio" name="target" value="5" id="target_5">
                                <label for="target_5">Australia</label>
                                <input type="radio" name="target" value="6" id="target_6">
                                <label for="target_6">Africa</label>
                            </div>
                        </div>

                        <a href="#" class="evaluate-prev-btn"><i class="fas fa-arrow-left"></i> Prev</a>
                        <a href="#" class="evaluate-next-btn">Next <i class="fas fa-arrow-right"></i></a>
                        <div class="evaluate-progress-bar-container">
                            <div class="fancy-hr"></div>
                            <div class="row evaluate-statistics">
                                <div class="col-md-6">
                                    <p class="evaluate-steps">Step <span class="evaluate-current-step">1</span> of <span class="evaluate-steps-count">17</span></p>
                                </div>
                                <div class="col-md-6 text-right">
                                    <p class="evaluate-percentage">0%</p>
                                </div>
                            </div>
                            <div class="evaluate-progress-bar">
                                <div class="evaluate-progress-bar-inner">

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <script>
        $( document ).ready(function() {
            var currentStep = 1;
            var progressPercent = 0;
            $('.progress-sidebar').css('height',$('.py-4').outerHeight() + 'px');

            var questionsCount = $('[data-q-number]').length;
            $('.evaluate-steps-count').text(questionsCount);

            // Navigate by menu items
            $('.q-navigation-item').on('click',function(e){
                e.preventDefault();
                var btnClicked = $(this);
                var itemIndex = btnClicked.data('menu-item');

                //Change current step
                currentStep = itemIndex;
                $('.evaluate-current-step').text(currentStep);

                //Calculate percentage
                progressPercent = Math.round((currentStep / questionsCount )*100);
                $('.evaluate-percentage').text(progressPercent + "%");
                $('.evaluate-progress-bar-inner').css('width',progressPercent + '%');

                // Change selected dot
                var questionDot = btnClicked.find('.question-dot');
                var questionDotSelected = $('.question-dot-selected');
                questionDotSelected.toggleClass('question-dot-selected');
                questionDot.toggleClass('question-dot-selected');

                // Display the right question
                var questionSelector = 'div[data-q-number="'+itemIndex+'"]';
                $('.display-evaluate-question').toggleClass('display-evaluate-question');
                $(questionSelector).toggleClass('display-evaluate-question');
            });

            // Navigate by Next/Prev buttons

            $('.evaluate-next-btn').on('click',function(e){
                e.preventDefault();


                var currentQuestion = $('.display-evaluate-question');
                var currentQuestionNumber = currentQuestion.data('q-number');
                var nextQuestionNumber = currentQuestionNumber + 1;
                if(nextQuestionNumber <= questionsCount){

                    //Change current step
                    currentStep = nextQuestionNumber;
                    $('.evaluate-current-step').text(currentStep);

                    //Calculate percentage
                    progressPercent = Math.round((currentStep / questionsCount )*100);
                    $('.evaluate-percentage').text(progressPercent + "%");
                    $('.evaluate-progress-bar-inner').css('width',progressPercent + '%');

                    // change Selected Dot
                    var questionDot = $('[data-menu-item="'+ nextQuestionNumber +'"] .question-dot');
                    var questionDotSelected = $('.question-dot-selected');
                    questionDotSelected.toggleClass('question-dot-selected');
                    questionDot.toggleClass('question-dot-selected');

                    currentQuestion.toggleClass('display-evaluate-question');
                    var nextQuestionSelector = 'div[data-q-number="'+nextQuestionNumber+'"]';
                    $(nextQuestionSelector).toggleClass('display-evaluate-question');
                }

                if(nextQuestionNumber >= questionsCount){
                    $('.evaluate-next-btn').hide();
                }
                $('.evaluate-prev-btn').show();

            });

            $('.evaluate-prev-btn').on('click',function(e){
                e.preventDefault();
                var currentQuestion = $('.display-evaluate-question');
                var currentQuestionNumber = currentQuestion.data('q-number');
                var prevQuestionNumber = currentQuestionNumber - 1;
                if(prevQuestionNumber > 0){
                    // Change current step
                    currentStep = prevQuestionNumber;
                    $('.evaluate-current-step').text(currentStep);

                    //Calculate percentage
                    progressPercent = Math.round((currentStep / questionsCount )*100);
                    $('.evaluate-percentage').text(progressPercent + "%");
                    $('.evaluate-progress-bar-inner').css('width',progressPercent + '%');

                    // Change selected dot
                    var questionDot = $('[data-menu-item="'+ prevQuestionNumber +'"] .question-dot');
                    var questionDotSelected = $('.question-dot-selected');
                    questionDotSelected.toggleClass('question-dot-selected');
                    questionDot.toggleClass('question-dot-selected');

                    currentQuestion.toggleClass('display-evaluate-question');
                    var prevQuestionSelector = 'div[data-q-number="'+prevQuestionNumber+'"]';
                    $(prevQuestionSelector).toggleClass('display-evaluate-question');
                }
                if(prevQuestionNumber < 2){
                    $('.evaluate-prev-btn').hide();
                }

                $('.evaluate-next-btn').show();

            });

            $('.progress-sidebar-toggle').on('click',function(){
                $('.progress-sidebar').fadeToggle();
            })
        });
    </script>
@endsection