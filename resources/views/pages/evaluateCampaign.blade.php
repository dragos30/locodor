@extends('layouts.app')

@section('content')

    <div class="evaluate-campaign-introduction" style="background-image: url('{{ asset('/uploads/login-background.jpg') }}')">
        <h1>test</h1>
    </div>

    <div class="container">
        <form action="{{ route('project.store') }}" method="post" enctype="multipart/form-data" id="create-project-form">
            @csrf
            <div class="row question-row-container">
                <div class="col-md-12 question-row">
                    @if($errors->get('first_project'))
                        @foreach($errors->get('first_project') as $error)
                            <div class="notice notice-danger">
                                <strong>{{ $error }}</strong>
                            </div>
                        @endforeach
                    @endif
                    <div class="question">
                        <p>Is this your first crowdfunding campaign?</p>
                        <div class="row">
                            <div class="col-md-3">

                                <input type="radio" value="1" id="first-yes" name="first_project" @if(old('first_project') == 'yes') checked @endif>
                                <label for="first-yes">
                                    <span>Y</span>
                                    <span>Yes</span>
                                </label><br>

                                <input type="radio" value="2" id="first-no" name="first_project" @if(old('first_project') == 'no') checked @endif>
                                <label for="first-no">
                                    <span>N</span>
                                    <span>No</span>
                                </label><br>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 question-row">
                    @if($errors->get('previous_url'))
                        @foreach($errors->get('previous_url') as $error)
                            <div class="notice notice-danger">
                                <strong>{{ $error }}</strong>
                            </div>
                        @endforeach
                    @endif
                    <div class="question">
                        <p>If you have one, please include your previous campaign URL:</p>
                        <input type="text" placeholder="https://" name="previous_url" value="{{ old('previous_url') }}">
                    </div>
                </div>
                <div class="col-md-12 question-row">
                    @if($errors->get('first_project'))
                        @foreach($errors->get('first_project') as $error)
                            <div class="notice notice-danger">
                                <strong>{{ $error }}</strong>
                            </div>
                        @endforeach
                    @endif
                    <div class="question">
                        <p>Did your previous campaign reached the target goal?</p>
                        <div class="row">
                            <div class="col-md-3">

                                <input type="radio" value="1" name="previous_target_reached" id="previous_target_reached_yes" @if(old('first_project') == 'yes') checked @endif>
                                <label for="first-yes">
                                    <span>Y</span>
                                    <span>Yes</span>
                                </label><br>

                                <input type="radio" value="2" name="previous_target_reached" id="previous_target_reached_no" @if(old('first_project') == 'no') checked @endif>
                                <label for="first-no">
                                    <span>N</span>
                                    <span>No</span>
                                </label><br>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 question-row">
                    @if($errors->get('previous_url'))
                        @foreach($errors->get('previous_url') as $error)
                            <div class="notice notice-danger">
                                <strong>{{ $error }}</strong>
                            </div>
                        @endforeach
                    @endif
                    <div class="question">
                        <p>How many backers did you had in your previous campaigns?</p>
                        <input type="number" placeholder="Please write the average number of backers from previous campaigns" name="previous_backers_no" value="{{ old('previous_url') }}">
                    </div>
                </div>
                <div class="col-md-12 question-row">
                    @if($errors->get('previous_url'))
                        @foreach($errors->get('previous_url') as $error)
                            <div class="notice notice-danger">
                                <strong>{{ $error }}</strong>
                            </div>
                        @endforeach
                    @endif
                    <div class="question">
                        <p>What percentage of funding you had on your previous campaigns?</p>
                        <input type="number" placeholder="Please write the average percentage of funding from the previous campaigns" name="tags" value="{{ old('previous_url') }}">
                    </div>
                </div>
                <div class="col-md-12 question-row">
                    @if($errors->get('project_type'))
                        @foreach($errors->get('project_type') as $error)
                            <div class="notice notice-danger">
                                <strong>{{ $error }}</strong>
                            </div>
                        @endforeach
                    @endif
                    <div class="question">
                        <p>What type of crowdfunding campaign do you want to create?</p>
                        <p>Let's get a little more specific</p>

                        <div class="row">
                            <div class="col-md-5">
                                <input type="radio" value="1" id="type-1" name="project_type" @if(old('project_type') == '1') checked @endif>
                                <label for="type-1">
                                    <span>A</span>
                                    <span>Pre-Orders</span>
                                </label><br>

                                <input type="radio" value="2" id="type-2" name="project_type" @if(old('project_type') == '2') checked @endif>
                                <label for="type-2">
                                    <span>B</span>
                                    <span>Reward Based</span>
                                </label><br>

                                <input type="radio" value="3" id="type-3" name="project_type" @if(old('project_type') == '3') checked @endif>
                                <label for="type-3">
                                    <span>C</span>
                                    <span>Equity Based</span>
                                </label><br>

                                <input type="radio" value="4" id="type-4" name="project_type" @if(old('project_type') == '4') checked @endif>
                                <label for="type-4">
                                    <span>D</span>
                                    <span>Donation Based</span>
                                </label><br>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="col-md-12 question-row">
                    @if($errors->get('previous_url'))
                        @foreach($errors->get('previous_url') as $error)
                            <div class="notice notice-danger">
                                <strong>{{ $error }}</strong>
                            </div>
                        @endforeach
                    @endif
                    <div class="question">
                        <p>On how many crowdfunding platforms do you plan to upload your campaign?</p>
                        <input type="number" placeholder="Type the number of platforms that you are going to use for your campaign" name="tags" value="{{ old('previous_url') }}">
                    </div>
                </div>

                <div class="col-md-12 question-row">
                    @if($errors->get('previous_url'))
                        @foreach($errors->get('previous_url') as $error)
                            <div class="notice notice-danger">
                                <strong>{{ $error }}</strong>
                            </div>
                        @endforeach
                    @endif
                    <div class="question">
                        <p>How many days do you spent on preparing the campaign before making it live? </p>
                        <input type="number" placeholder="Type the number of days spent or willing to spend on preparing your campaign for launch" name="tags" value="{{ old('previous_url') }}">
                    </div>
                </div>

                <div class="col-md-12 question-row">
                    @if($errors->get('previous_url'))
                        @foreach($errors->get('previous_url') as $error)
                            <div class="notice notice-danger">
                                <strong>{{ $error }}</strong>
                            </div>
                        @endforeach
                    @endif
                    <div class="question">
                        <p>How many people do you estimate to be able to reach out with your campaign on the launch day?</p>
                        <input type="number" placeholder="Type the number of people you can reach in the launch day" name="tags" value="{{ old('previous_url') }}">
                    </div>
                </div>


                <div class="col-md-12 question-row">
                    @if($errors->get('project_goal'))
                        @foreach($errors->get('project_goal') as $error)
                            <div class="notice notice-danger">
                                <strong>{{ $error }}</strong>
                            </div>
                        @endforeach
                    @endif
                    <div class="question">
                        <p>What is goal you want to reach (USD)?</p>
                        <input type="number" placeholder="Type your answer here..." name="project_goal" value="@if(Session::get('goal')) {{ Session::get('goal') }} @else {{ old('project_goal') }} @endif">
                    </div>
                </div>

                <div class="col-md-12 question-row">
                    @if($errors->get('previous_url'))
                        @foreach($errors->get('previous_url') as $error)
                            <div class="notice notice-danger">
                                <strong>{{ $error }}</strong>
                            </div>
                        @endforeach
                    @endif
                    <div class="question">
                        <p>How many days will your campaign have to reach the goal?</p>
                        <input type="number" placeholder="Type the number of people you can reach in the launch day" name="tags" value="{{ old('previous_url') }}">
                    </div>
                </div>

                <div class="col-md-12 question-row">
                    @if($errors->get('previous_url'))
                        @foreach($errors->get('previous_url') as $error)
                            <div class="notice notice-danger">
                                <strong>{{ $error }}</strong>
                            </div>
                        @endforeach
                    @endif
                    <div class="question">
                        <p>How much money are you will to invest to promote your campaign?</p>
                        <input type="number" placeholder="Type the amount you are willing to invest in USD to promote your campaign" name="tags" value="{{ old('previous_url') }}">
                    </div>
                </div>

                <div class="col-md-12 question-row">
                    @if($errors->get('previous_url'))
                        @foreach($errors->get('previous_url') as $error)
                            <div class="notice notice-danger">
                                <strong>{{ $error }}</strong>
                            </div>
                        @endforeach
                    @endif
                    <div class="question">
                        <p>How many team members do you got for your campaign?</p>
                        <input type="number" placeholder="Type the number of people that are contribuiting to your campaign" name="tags" value="{{ old('previous_url') }}">
                    </div>
                </div>

                <div class="col-md-12 question-row">
                    @if($errors->get('previous_url'))
                        @foreach($errors->get('previous_url') as $error)
                            <div class="notice notice-danger">
                                <strong>{{ $error }}</strong>
                            </div>
                        @endforeach
                    @endif
                    <div class="question">
                        <p>How many backing levels do you got for your campaign?</p>
                        <input type="number" placeholder="Type the number of backing levels(pledges) available for your campaign" name="tags" value="{{ old('previous_url') }}">
                    </div>
                </div>

                <div class="col-md-12 question-row">
                    @if($errors->get('first_project'))
                        @foreach($errors->get('first_project') as $error)
                            <div class="notice notice-danger">
                                <strong>{{ $error }}</strong>
                            </div>
                        @endforeach
                    @endif
                    <div class="question">
                        <p>Are you willing to reach out to the press regarding your campaign?</p>
                        <div class="row">
                            <div class="col-md-3">

                                <input type="radio" value="1" id="first-yes" name="first_project" @if(old('first_project') == 'yes') checked @endif>
                                <label for="first-yes">
                                    <span>Y</span>
                                    <span>Yes</span>
                                </label><br>

                                <input type="radio" value="2" id="first-no" name="first_project" @if(Session::get('name')) checked @elseif(old('first_project') == 'no') checked @endif>
                                <label for="first-no">
                                    <span>N</span>
                                    <span>No</span>
                                </label><br>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 question-row">
                    @if($errors->get('first_project'))
                        @foreach($errors->get('first_project') as $error)
                            <div class="notice notice-danger">
                                <strong>{{ $error }}</strong>
                            </div>
                        @endforeach
                    @endif
                    <div class="question">
                        <p>Which one of the following continents are you going to target the most?</p>
                        <div class="row">
                            <div class="col-md-3">
                                <input type="radio" value="1" id="first-yes" name="first_project" @if(old('first_project') == 'yes') checked @endif>
                                <label for="first-yes">
                                    <span>1</span>
                                    <span>North America</span>
                                </label><br>

                                <input type="radio" value="2" id="first-no" name="first_project" @if(Session::get('name')) checked @elseif(old('first_project') == 'no') checked @endif>
                                <label for="first-no">
                                    <span>2</span>
                                    <span>South America</span>
                                </label><br>

                                <input type="radio" value="2" id="first-no" name="first_project" @if(Session::get('name')) checked @elseif(old('first_project') == 'no') checked @endif>
                                <label for="first-no">
                                    <span>3</span>
                                    <span>Europe</span>
                                </label><br>

                                <input type="radio" value="2" id="first-no" name="first_project" @if(Session::get('name')) checked @elseif(old('first_project') == 'no') checked @endif>
                                <label for="first-no">
                                    <span>4</span>
                                    <span>Asia</span>
                                </label><br>

                                <input type="radio" value="2" id="first-no" name="first_project" @if(Session::get('name')) checked @elseif(old('first_project') == 'no') checked @endif>
                                <label for="first-no">
                                    <span>5</span>
                                    <span>Australia</span>
                                </label><br>

                                <input type="radio" value="2" id="first-no" name="first_project" @if(Session::get('name')) checked @elseif(old('first_project') == 'no') checked @endif>
                                <label for="first-no">
                                    <span>6</span>
                                    <span>Africa</span>
                                </label><br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="evaluate-progress-bar">
        <div class="container">
            <p>You have 99 questions left</p>
        </div>
    </div>
@endsection
