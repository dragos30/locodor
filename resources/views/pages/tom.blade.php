@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row got-idea justify-content-md-center">
            <div class="col-md-6">
                <div class="carousel-got-idea">
                    <form method="post" action="{{ route('got.idea') }}" id="got-idea">
                        @csrf
                        <div id="carousel" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner form-slider-container">
                                {{-- Slide 1 --}}
                                <div class="carousel-item active">
                                    <p class="slider-form-text">Lets meet each other, I'm <span class="form-slider-span">Tom</span>.</p>
                                    <p class="slider-form-question">What's your name?</p>
                                    <div class="form-group">
                                        <input type="text" class="form-control slider-form-input" placeholder="Write your name..." name="name" id="form-slider-name">
                                        <p class="warning-message" style="display:none">Please provide your name...</p>
                                    </div>
                                </div>
                                {{-- Slide 2 --}}
                                <div class="carousel-item">
                                    <p class="slider-form-text">Nice to meet you <span class="form-slider-username">User</span>.</p>
                                    <p class="slider-form-question">Please describe your Awesome idea in a few words.</p>
                                    <div class="form-group">
                                        <input type="text" class="form-control slider-form-input" placeholder="A few words about your idea..." name="idea" id="form-slider-idea">
                                        <p class="warning-message" style="display:none">Please provide some details about your idea...</p>
                                    </div>
                                    <p class="slider-form-question">And what type of Crowdfunding Campaigns are interested in.</p>
                                    <div class="form-group">
                                        <input type="text" class="form-control slider-form-input" placeholder="A few words about your interests..." name="interest" id="form-slider-interest">
                                        <p class="warning-message" style="display:none">Please provide some details about your interests...</p>
                                    </div>
                                </div>
                                {{-- Slide 3 --}}
                                <div class="carousel-item">
                                    <p class="slider-form-text">Sounds great, <span class="form-slider-username">User</span>!</p>
                                    <p class="slider-form-question">Do you already have a live Crowdfunding Project?</p>
                                    <div class="form-check form-slider-select">
                                        <input class="form-check-input form-slider-select-input have_campaign" type="radio" name="have_campaign" id="yes" value="1" checked>
                                        <label class="form-check-label form-slider-select-label" for="yes">
                                            Yes
                                        </label>
                                    </div>
                                    <div class="form-check form-slider-select">
                                        <input class="form-check-input form-slider-select-input have_campaign" type="radio" name="have_campaign" id="no" value="2">
                                        <label class="form-check-label form-slider-select-label" for="no">
                                            No
                                        </label>
                                    </div>
                                </div>
                                {{-- Slide 4 --}}
                                <div class="carousel-item">
                                    <p class="slider-form-text">That is great! <span class="form-slider-span">Congratulation!</span></p>
                                    <p class="slider-form-question">Lets have a look at it. Please paste the link of your Campaign.</p>
                                    <div class="form-group">
                                        <input type="text" class="form-control slider-form-input" placeholder="Paste the link of your Crowdfunding Campaign..." name="campaign" id="form-slider-link">
                                        <p class="warning-message" style="display:none">Please provide some details about your idea...</p>
                                    </div>
                                </div>
                                {{-- Slide 5 --}}
                                <div class="carousel-item">
                                    <p class="slider-form-text">Thank you very much, <span class="form-slider-username">User</span>!</p>
                                    <p class="slider-form-question">Please provide your email address and we will reach out to you!</p>
                                    <div class="form-group">
                                        <input type="email" class="form-control slider-form-input" placeholder="Write your email address..." name="email" id="form-slider-email">
                                        <p class="warning-message" style="display:none">Please provide your valid email address...</p>
                                    </div>
                                    <div class="captcha-container">
                                        <div class="text-center">
                                            <span>
                                                {!! NoCaptcha::display(['data-theme' => 'dark']) !!}
                                            </span>
                                        </div>
                                        @if ($errors->has('g-recaptcha-response'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                {{-- Slide 6 --}}
                                <div class="carousel-item">
                                    <p class="slider-form-text">This is it! You've reached the end!</p>
                                    <p class="slider-form-question">Thank you very much for taking your time to reach us. We will be back shortly to help you out!</p>
                                </div>
                            </div>
                        </div>
                        <button type="submit" id="form-slide-submit" style="display: none">submit</button>
                    </form>
                    <a class="form-slider-prev form-slider-controls" id="form-slider-prev" role="button">
                        <span><i class="fas fa-arrow-left"></i> Previous</span>
                    </a>
                    <a class="form-slider-next form-slider-controls" id="form-slider-next" role="button">
                        <span>Next <i class="fas fa-arrow-right"></i></span>
                    </a>
                </div>
            </div>
            <script>
                $('.carousel').carousel({
                    interval: false
                });



                $('#form-slider-prev').hide();

                $( document ).ready(function() {
                    var slideNo = 1;

                    $('#got-idea').on('keydown',function (e) {
                        if(e.which === 13) {
                            e.preventDefault();
                            $('#form-slider-next').trigger('click');
                            return false;
                        }
                    });

                    $('#form-slider-next').on('click',function(e){
                        e.preventDefault();

                        switch(slideNo) {
                            case 1:
                                if($('#form-slider-name').val() === ""){
                                    $('#form-slider-name').next().css('display','inline');
                                    console.log('1 empty');
                                }else {
                                    $('#form-slider-name').next().css('display','none');

                                    $('.form-slider-username').text($('#form-slider-name').val());

                                    $('.carousel').carousel('next');
                                    $('#form-slider-prev').show();
                                    slideNo++;
                                    console.log(slideNo);
                                }
                                break;
                            case 2:
                                if($('#form-slider-idea').val() === "" || $('#form-slider-interest').val() === ""){
                                    if($('#form-slider-idea').val() === ""){
                                        $('#form-slider-idea').next().css('display','inline');
                                        console.log('2 empty');
                                    }else{
                                        $('#form-slider-idea').next().css('display','none');
                                    }
                                    if($('#form-slider-interest').val() === ""){
                                        $('#form-slider-interest').next().css('display','inline');
                                        console.log('2 empty');
                                    }else {
                                        $('#form-slider-interest').next().css('display','none');
                                    }

                                }else {
                                    $('#form-slider-idea').next().css('display','none');
                                    $('#form-slider-interest').next().css('display','none');

                                    $('.carousel').carousel('next');
                                    slideNo++;
                                    console.log(slideNo);
                                }
                                break;
                            case 3:
                                if($('input[name="have_campaign"]:checked').val() === "1"){
                                    $('.carousel').carousel('next');
                                    slideNo++;
                                    console.log(slideNo);
                                }else if($('input[name="have_campaign"]:checked').val() === "2"){
                                    $('.carousel').carousel(4);
                                    slideNo++;
                                    slideNo++;
                                    console.log(slideNo);
                                }
                                break;
                            case 4:
                                if($('#form-slider-link').val() === ""){
                                    $('#form-slider-link').next().css('display','inline');
                                    console.log('4 empty');
                                }else{
                                    $('#form-slider-link').next().css('display','none');

                                    $('.carousel').carousel('next');
                                    slideNo++;
                                    console.log(slideNo);
                                }
                                break;
                            case 5:
                                if($('#form-slider-email').val() === "" || !isValidEmailAddress($('#form-slider-email').val())){
                                    $('#form-slider-email').next().css('display','inline');
                                    console.log('5 empty');
                                }else{
                                    $('#form-slider-email').next().css('display','none');

                                    $('.carousel').carousel('next');
                                    slideNo++;
                                    console.log(slideNo);

                                    var name = $('#form-slider-name').val();
                                    var idea = $('#form-slider-idea').val();
                                    var campaign = $('#form-slider-link').val();
                                    var email = $('#form-slider-email').val();


                                    $.ajax({
                                        type: "POST",
                                        url: "{{ route('got.idea') }}",
                                        data: {
                                            _token: '{{csrf_token()}}',
                                            name: name,
                                            idea: idea,
                                            campaign: campaign,
                                            email: email,
                                            capcha: grecaptcha.getResponse()
                                        },
                                        success: function (result) {
                                            console.log('submited')
                                        },
                                        error: function (result) {
                                            $('.carousel').carousel('prev');
                                            $('#form-slider-next').show();
                                            slideNo--;
                                            alert("Please check I'm not a robot box.");
                                        }
                                    });
                                }
                                break;
                        }

                        if(slideNo === 6){
                            $('#form-slider-next').hide();
                        }
                    });

                    $('#form-slider-prev').on('click',function(e){
                        e.preventDefault();

                        console.log(slideNo);


                        switch(slideNo) {
                            case 1:
                                break;
                            case 2:
                                $('.carousel').carousel('prev');
                                slideNo--;
                                console.log(slideNo);

                                break;
                            case 3:
                                $('.carousel').carousel('prev');
                                slideNo--;
                                console.log(slideNo);

                                break;
                            case 4:
                                $('.carousel').carousel('prev');
                                slideNo--;
                                console.log(slideNo);

                                break;
                            case 5:
                                if($('input[name="have_campaign"]:checked').val() === "1"){
                                    $('.carousel').carousel('prev');
                                    slideNo--;
                                    console.log(slideNo);
                                }else if($('input[name="have_campaign"]:checked').val() === "2"){
                                    $('.carousel').carousel(2);
                                    slideNo--;
                                    slideNo--;
                                    console.log(slideNo);
                                }
                                break;
                            case 6:
                                $('.carousel').carousel('prev');
                                slideNo--;
                                console.log(slideNo);

                                $('#form-slider-next').show();

                                break;
                        }

                        if(slideNo === 1){
                            $('#form-slider-prev').hide();
                        }
                    });


                });

                function isValidEmailAddress(emailAddress) {
                    var pattern = new RegExp(/^(("[\w-+\s]+")|([\w-+]+(?:\.[\w-+]+)*)|("[\w-+\s]+")([\w-+]+(?:\.[\w-+]+)*))(@((?:[\w-+]+\.)*\w[\w-+]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][\d]\.|1[\d]{2}\.|[\d]{1,2}\.))((25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\.){2}(25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\]?$)/i);
                    return pattern.test(emailAddress);
                };
            </script>
        </div>
    </div>
@endsection
