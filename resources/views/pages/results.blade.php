@extends('layouts.app')

@section('content')
    <div class="container">
        <h1> Search results for: <b>{{ $query }}</b></h1>
        <div class="row">
            @if(count($resources))
                <div class="col-md-12">
                    <h3>Resources:</h3>
                </div>
                @foreach($resources as $resource)
                    <div class="col-md-4">
                        <div class="basic-card">
                            <a href="{{ route('resource.show',['id' => $resource->id,'slug'=>$resource->slug]) }}">
                                <img src="{{ asset($resource->image) }}">
                            </a>
                            <div class="basic-card-details">
                                <h3 class="basic-card-title">
                                    {{ $resource->title }}
                                </h3>
                                <p class="basic-card-content">
                                    {{ strip_tags($resource->content) }}
                                </p>
                                <a href="{{ $resource->link }}" class="basic-card-button" target="_blank">View</a>
                                <a href="{{ route('resource.show',['id' => $resource->id,'slug'=>$resource->slug]) }}" class="basic-card-button">Read More</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
            @if(count($projects))
                <div class="col-md-12">
                    <h3>Projects:</h3>
                </div>
                @foreach($projects as $project)
                    <div class="col-md-4">
                        <div class="card mb-4 text-white bg-dark">
                            <img class="card-img-top" src="{{ asset($project->image) }}" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title">{{ $project->title }}</h5>
                                <p class="card-text">{{ $project->subtitle }}</p>
                                <a href="{{ route('project.show',[ 'id' => $project->id , 'slug' => $project->slug]) }}" class="btn btn-outline-light btn-sm">View Project</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
            @if(count($posts))
                <div class="col-md-12">
                    <h3>Blog posts:</h3>
                </div>
                @foreach($posts as $post)
                    <div class="col-md-4">
                        <div class="card mb-4 text-white bg-dark">
                            <img class="card-img-top" src="{{ asset($post->image) }}" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title">{{ $post->title }}</h5>
                                <a href="{{ route('post.show',[ 'id' => $post->id , 'slug' => $post->slug]) }}" class="btn btn-outline-light btn-sm">Read Post</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
            @if(count($users))
                <div class="col-md-12">
                    <h3>Members:</h3>
                </div>
                @foreach($users as $user)
                        <div class="col-sm-12 col-md-6 col-lg-3 home-user-element">
                            <div class="user-hover-container-home">
                                <div class="hover-profile">
                                    <div class="row">
                                        <div class="col-md-3 no-margin">
                                            <img class="hover-image" src="{{ $user->profile->avatar }}" width="50" height="50">
                                        </div>
                                        <div class="col-md-9 no-margin">
                                            <p><span class="hover-user-name"">{{ $user->name }}</span></p>
                                            <a href="{{ route('profile.show',['id' => $user->profile->id, 'slug'=>$user->profile->slug]) }}">View profile</a>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="simple-line"></div>
                                            <div class="hover-social">
                                                <p><b>Social</b></p>
                                                <p>
                                                    @if(!in_array($user->id,$friends) && !in_array($user->id,$sentFriendRequests) && !in_array($user->id,$receivedFriendRequests))
                                                        <a href="{{ route('add.friend',['id' => $user->id ]) }}" data-user-id="{{$user->id}}">Add Friend </a>
                                                    @endif
                                                    @if(in_array($user->id,$friends))
                                                        <span><i class="fas fa-check"></i> Friends</span>
                                                    @endif
                                                    @if(in_array($user->id,$sentFriendRequests))
                                                        <span>Pending</span>
                                                    @endif
                                                    @if(in_array($user->id,$receivedFriendRequests))
                                                        <a href="{{ route('accept.friend',['id' => $user->id ]) }}" data-user-id="{{$user->id}}">Accept Friend Request</a>
                                                        <a href="{{ route('remove.friend',['id' => $user->id ]) }}" data-user-id="{{$user->id}}">Decline Friend Request</a>
                                                    @endif
                                                </p>
                                                <p><a href="{{ route('start.conversation',['id' => $user->id] ) }}"><i class="far fa-comment"></i> Start Conversation</a></p>

                                            </div>
                                            <div class="simple-line"></div>
                                            <div class="hover-campaigns">
                                                <p><b>Campaigns</b></p>
                                                <p><a href="{{ route('profile.show',['id' => $user->profile->id, 'slug' => $user->profile->slug]) }}?created"><i class="far fa-lightbulb"></i> Created Campaigns({{ count($user->projects)  }})</a></p>
                                                <p><a href="{{ route('profile.show',['id' => $user->profile->id, 'slug' => $user->profile->slug]) }}?backed"><i class="fab fa-bitcoin"></i> Backed Campaigns({{ count($user->backers) }})</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                @endforeach
            @endif
        </div>
    </div>
@endsection