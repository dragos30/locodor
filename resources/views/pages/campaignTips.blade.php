@extends('layouts.simple')

@section('content')
    <div class="container-fluid">
        <h1>Campaign Tips</h1>
        <form method="POST" action="{{ route('store.campaign.tasks') }}">
            @csrf
            <input type="hidden" name="project_id" value="{{ $campaign_tasks[0]->project_id }}">
            <table class="campaign-tips-table-1">
                <tr>
                    <td>Status</td>
                    <td>Task</td>
                    <td>Assigned to</td>
                    <td>Deadline</td>
                    <td>Notes</td>
                </tr>
                <tr>
                    <td colspan="5" style="text-align: center">Pre Campaign</td>
                </tr>
                @foreach($campaign_tasks as $task)
                    <tr>
                        <input type="hidden" name="row_index[]" value="{{ $task->row_index }}">
                        <td>
                            <select name="task_status[]">
                                <option value="ongoing" @if(isset($task->status) && $task->status == "ongoing") selected @endif>Ongoing</option>
                                <option value="completed" @if(isset($task->status) && $task->status == "completed") selected @endif>Completed</option>
                            </select>
                        </td>
                        <td>{{ $task->task }}</td>
                        <td><input type="text" name="task_assigned[]" @if(isset($task->assigned_to)) value="{{ $task->assigned_to }}" @endif></td>
                        <td><input type="date" name="task_deadline[]"@if(isset($task->deadline)) value="{{ $task->deadline }}" @endif></td>
                        <td><textarea rows="1" name="task_note[]">@if($task->notes) {{ $task->notes }} @endif</textarea></td>
                    </tr>
                @endforeach



            </table>
            <button type="submit" class="btn btn-dark">Save</button>
        </form>
        <div class="fancy-hr"></div>
        <form method="POST" action="{{ route('store.campaign.team') }}">
            @csrf
            <input type="hidden" name="project_id" value="{{ $campaign_tasks[0]->project_id }}">
            <table class="campaign-tips-table-2">
                <tr>
                    <td colspan="7">Team Contact List</td>
                </tr>
                <tr>
                    <td>Name</td>
                    <td>Role</td>
                    <td>Availability</td>
                    <td>Social Media Link</td>
                    <td>Email</td>
                    <td>Phone</td>
                    <td>Notes</td>
                </tr>
                @if($campaign_team_members->count() !== 0)
                    @foreach($campaign_team_members as $member)
                        <tr>
                            <td><input type="text" name="member_name[]" value="{{ $member->name }}"></td>
                            <td><input type="text" name="member_role[]" value="{{ $member->role }}"></td>
                            <td><input type="text" name="member_availability[]" value="{{ $member->availability }}"></td>
                            <td><input type="text" name="member_link[]" value="{{ $member->social_media_link }}"></td>
                            <td><input type="text" name="member_email[]" value="{{ $member->email }}"></td>
                            <td><input type="text" name="member_phone[]" value="{{ $member->phone }}"></td>
                            <td><input type="text" name="member_note[]" value="{{ $member->notes }}"></td>
                            <td><button class="remove-row"><i class="fas fa-times"></i> Remove</button></td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td><input type="text" name="member_name[]"></td>
                        <td><input type="text" name="member_role[]"></td>
                        <td><input type="text" name="member_availability[]"></td>
                        <td><input type="text" name="member_link[]"></td>
                        <td><input type="text" name="member_email[]"></td>
                        <td><input type="text" name="member_phone[]"></td>
                        <td><input type="text" name="member_note[]"></td>
                        <td><button class="remove-row"><i class="fas fa-times"></i> Remove</button></td>
                    </tr>
                @endif
                <tr>
                    <td colspan="7" class="add-row"><i class="far fa-plus-square"></i> Add Row</td>
                </tr>
            </table>
            <button type="submit" class="btn btn-dark">Save</button>
        </form>

        <div class="fancy-hr"></div>


        <form method="POST" action="{{ route('store.campaign.perks') }}">
            @csrf
            <input type="hidden" name="project_id" value="{{ $campaign_tasks[0]->project_id }}">
            <table class="campaign-tips-table-3">
                <tr>
                    <td colspan="3">Publishing Plan</td>
                    <td colspan="5">Perks Total (cost 1750 deducted)</td>
                </tr>
                <tr>
                    <td>Perk Price(USD)</td>
                    <td>Perk Cost(USD)</td>
                    <td>Sales Goal</td>
                    <td>Perk Profit</td>
                    <td>Perk Name</td>
                    <td>Short Description</td>
                    <td>Tested Yes / No</td>
                    <td>Notes</td>
                </tr>
                @if($campaign_perks->count() !== 0)
                    @foreach($campaign_perks as $perk)
                        <tr>
                            <td><input type="number" name="perk_price[]" value="{{ $perk->price }}"></td>
                            <td><input type="number" name="perk_cost[]" value="{{ $perk->cost }}"></td>
                            <td><input type="number" name="sales_goal[]" value="{{ $perk->goal }}"></td>
                            <td>{{ ($perk->price - $perk->cost) * $perk->goal }}</td>
                            <td><input type="text" name="perk_name[]" value="{{ $perk->name }}"></td>
                            <td><textarea rows="1" name="perk_description[]">{{ $perk->description }}</textarea></td>
                            <td>
                                <select name="perk_tested[]">
                                    <option value="1" @if($perk->tested == 1) selected @endif>Yes</option>
                                    <option value="0" @if($perk->tested == 0) selected @endif>No</option>
                                </select>
                            </td>
                            <td><textarea rows="1" name="perk_notes[]">{{ $perk->notes }}</textarea></td>
                            <td><button class="remove-row"><i class="fas fa-times"></i> Remove</button></td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td><input type="number" name="perk_price[]"></td>
                        <td><input type="number" name="perk_cost[]"></td>
                        <td><input type="number" name="sales_goal[]"></td>
                        <td>1750</td>
                        <td><input type="text" name="perk_name[]"></td>
                        <td><textarea rows="1" name="perk_description[]"></textarea></td>
                        <td>
                            <select name="perk_tested[]">
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select>
                        </td>
                        <td><textarea rows="1" name="perk_notes[]"></textarea></td>
                        <td><button class="remove-row"><i class="fas fa-times"></i> Remove</button></td>
                    </tr>
                @endif
                <tr>
                    <td colspan="8" class="add-row"><i class="far fa-plus-square"></i> Add Row</td>
                </tr>
            </table>
            <button type="submit" class="btn btn-dark">Save</button>
        </form>

        <div class="fancy-hr"></div>

        <form method="POST" action="{{ route('store.week.plan') }}">
            @csrf
            <input type="hidden" name="project_id" value="{{ $campaign_tasks[0]->project_id }}">
            <table class="campaign-tips-table-4">
                <tr>
                    <td colspan="23">Publishing Plan</td>
                </tr>
                <tr>
                    <td>WEEK</td>
                    <td></td>
                    <td colspan="3">Monday</td>
                    <td colspan="3">Tuesday</td>
                    <td colspan="3">Wednesday</td>
                    <td colspan="3">Thursday</td>
                    <td colspan="3">Friday</td>
                    <td colspan="3">Saturday</td>
                    <td colspan="3">Sunday</td>
                </tr>
                <tr>
                    <td></td>
                    <td>Focus</td>
                    <td>Social Channels</td>
                    <td>Social Channels</td>
                    <td>Email</td>
                    <td>Social Channels</td>
                    <td>Social Channels</td>
                    <td>Email</td>
                    <td>Social Channels</td>
                    <td>Social Channels</td>
                    <td>Email</td>
                    <td>Social Channels</td>
                    <td>Social Channels</td>
                    <td>Email</td>
                    <td>Social Channels</td>
                    <td>Social Channels</td>
                    <td>Email</td>
                    <td>Social Channels</td>
                    <td>Social Channels</td>
                    <td>Email</td>
                    <td>Social Channels</td>
                    <td>Social Channels</td>
                    <td>Email</td>
                </tr>
                @if($campaign_weeks->count() !== 0)
                    @foreach($campaign_weeks as $key => $week)
                        <tr>
                            <td>Week {{ $key + 1 }}</td>
                            <td><input type="text" name="focus[]" value="{{ $week->focus }}"></td>
                            <td><input type="text" name="social_channel_monday_1[]" value="{{ $week->monday_channel1 }}"></td>
                            <td><input type="text" name="social_channel_monday_2[]" value="{{ $week->monday_channel2 }}"></td>
                            <td><input type="text" name="email_monday[]" value="{{ $week->monday_email }}"></td>
                            <td><input type="text" name="social_channel_tuesday_1[]" value="{{ $week->tuesday_channel1 }}"></td>
                            <td><input type="text" name="social_channel_tuesday_2[]" value="{{ $week->tuesday_channel2 }}"></td>
                            <td><input type="text" name="email_tuesday[]" value="{{ $week->tuesday_email }}"></td>
                            <td><input type="text" name="social_channel_wednesday_1[]" value="{{ $week->wednesday_channel1 }}"></td>
                            <td><input type="text" name="social_channel_wednesday_2[]" value="{{ $week->wednesday_channel2 }}"></td>
                            <td><input type="text" name="email_wednesday[]" value="{{ $week->wednesday_email }}"></td>
                            <td><input type="text" name="social_channel_thursday_1[]" value="{{ $week->thursday_channel1 }}"></td>
                            <td><input type="text" name="social_channel_thursday_2[]" value="{{ $week->thursday_channel2 }}"></td>
                            <td><input type="text" name="email_thursday[]" value="{{ $week->thursday_email }}"></td>
                            <td><input type="text" name="social_channel_friday_1[]" value="{{ $week->friday_channel1 }}"></td>
                            <td><input type="text" name="social_channel_friday_2[]" value="{{ $week->friday_channel2 }}"></td>
                            <td><input type="text" name="email_friday[]" value="{{ $week->friday_email }}"></td>
                            <td><input type="text" name="social_channel_saturday_1[]" value="{{ $week->saturday_channel1 }}"></td>
                            <td><input type="text" name="social_channel_saturday_2[]" value="{{ $week->saturday_channel2 }}"></td>
                            <td><input type="text" name="email_saturday[]" value="{{ $week->saturday_email }}"></td>
                            <td><input type="text" name="social_channel_sunday_1[]" value="{{ $week->sunday_channel1 }}"></td>
                            <td><input type="text" name="social_channel_sunday_2[]" value="{{ $week->sunday_channel1 }}"></td>
                            <td><input type="text" name="email_sunday[]" value="{{ $week->sunday_email }}"></td>
                            <td><button class="remove-row"><i class="fas fa-times"></i> Remove</button></td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td>Week 1</td>
                        <td><input type="text" name="focus[]"></td>
                        <td><input type="text" name="social_channel_monday_1[]"></td>
                        <td><input type="text" name="social_channel_monday_2[]"></td>
                        <td><input type="text" name="email_monday[]"></td>
                        <td><input type="text" name="social_channel_tuesday_1[]"></td>
                        <td><input type="text" name="social_channel_tuesday_2[]"></td>
                        <td><input type="text" name="email_tuesday[]"></td>
                        <td><input type="text" name="social_channel_wednesday_1[]"></td>
                        <td><input type="text" name="social_channel_wednesday_2[]"></td>
                        <td><input type="text" name="email_wednesday[]"></td>
                        <td><input type="text" name="social_channel_thursday_1[]"></td>
                        <td><input type="text" name="social_channel_thursday_2[]"></td>
                        <td><input type="text" name="email_thursday[]"></td>
                        <td><input type="text" name="social_channel_friday_1[]"></td>
                        <td><input type="text" name="social_channel_friday_2[]"></td>
                        <td><input type="text" name="email_friday[]"></td>
                        <td><input type="text" name="social_channel_saturday_1[]"></td>
                        <td><input type="text" name="social_channel_saturday_2[]"></td>
                        <td><input type="text" name="email_saturday[]"></td>
                        <td><input type="text" name="social_channel_sunday_1[]"></td>
                        <td><input type="text" name="social_channel_sunday_2[]"></td>
                        <td><input type="text" name="email_sunday[]"></td>
                        <td><button class="remove-row"><i class="fas fa-times"></i> Remove</button></td>
                    </tr>
                @endif

                <tr>
                    <td colspan="23" class="add-row add-row-week-plan text-left"><i class="far fa-plus-square"></i> Add Row</td>
                </tr>
            </table>
            <button type="submit" class="btn btn-dark">Save</button>
        </form>

        <script>

            $( document ).ready(function() {
                $('.add-row').on('click',function () {
                    var html= $(this).parent().prev().clone();
                    console.log(html);
                    $(this).parent().before(html);
                    $('.remove-row').on('click',function () {
                        $(this).parent().parent().remove();
                    });
                });

                $('.remove-row').on('click',function () {
                    $(this).parent().parent().remove();
                });

                var weekNumber = {{ $campaign_weeks->count() }};
                $('.add-row-week-plan').on('click',function(){
                    weekNumber++;
                    var week = $(this).parent().prev().children().first().text('Week '+ weekNumber);
                });
            });
        </script>

        <div class="fancy-hr"></div>

        <form method="POST" action="{{ route('store.general.plan') }}">
            @csrf
            <input type="hidden" name="project_id" value="{{ $campaign_tasks[0]->project_id }}">
            <table class="campaign-tips-table-5">
                <tr>
                    <td colspan="9">
                        Publishing Plan
                    </td>
                </tr>
                <tr>
                    <td>Checklist Sheet</td>
                    <td>Team Sheet</td>
                    <td></td>
                    <td>Perks Sheet</td>
                    <td></td>
                    <td>General</td>
                    <td>Publishing Plan</td>
                    <td>Social Media</td>
                    <td>Email</td>
                </tr>
                <tr>
                    <td>Task Status</td>
                    <td>Roles</td>
                    <td>Availability</td>
                    <td>Pricing</td>
                    <td>Comission(%)</td>
                    <td></td>
                    <td>Focus</td>
                    <td></td>
                    <td></td>
                </tr>
                @if($campaign_plans->count() !== 0)
                    @foreach($campaign_plans as  $plan)
                        <tr>
                            <td>
                                <select name="task_status[]">
                                    <option value="open" @if($plan->status == 'open') selected @endif>Open</option>
                                    <option value="assigned" @if($plan->status == 'assigned') selected @endif>Assigned</option>
                                    <option value="in-progress" @if($plan->status == 'in-progress') selected @endif>In Progress</option>
                                    <option value="done" @if($plan->status == 'done') selected @endif>Done</option>
                                </select>
                            </td>
                            <td><input type="text" name="role[]" value="{{ $plan->roles }}"></td>
                            <td><input type="text" name="availability[]" value="{{ $plan->availability }}"></td>
                            <td><input type="number" name="pricing[]" value="{{ $plan->pricing }}"></td>
                            <td><input type="number" name="commission[]" value="{{ $plan->commission }}"></td>
                            <td>
                                <select name="general[]">
                                    <option value="1" @if($plan->general == 1) selected @endif>Yes</option>
                                    <option value="0" @if($plan->general == 0) selected @endif>No</option>
                                </select>
                            </td>
                            <td><input type="text" name="focus[]" value="{{ $plan->focus }}"></td>
                            <td><input type="text" name="social_media[]" value="{{ $plan->social_media }}"></td>
                            <td><input type="text" name="email[]" value="{{ $plan->email }}"></td>
                            <td><button class="remove-row"><i class="fas fa-times"></i> Remove</button></td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td>
                            <select name="task_status[]">
                                <option value="open">Open</option>
                                <option value="assigned">Assigned</option>
                                <option value="in-progress">In Progress</option>
                                <option value="done">Done</option>
                            </select>
                        </td>
                        <td><input type="text" name="role[]"></td>
                        <td><input type="number" name="availability[]"></td>
                        <td><input type="number" name="pricing[]"></td>
                        <td><input type="number" name="commission[]"></td>
                        <td>
                            <select name="general[]">
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select>
                        </td>
                        <td><input type="text" name="focus[]"></td>
                        <td><input type="text" name="social_media[]"></td>
                        <td><input type="text" name="email[]"></td>
                        <td><button class="remove-row"><i class="fas fa-times"></i> Remove</button></td>
                    </tr>
                @endif

                <tr>
                    <td colspan="9" class="add-row text-left"><i class="far fa-plus-square"></i> Add Row</td>
                </tr>
            </table>
            <button type="submit" class="btn btn-dark">Save</button>
        </form>
    </div>
@endsection