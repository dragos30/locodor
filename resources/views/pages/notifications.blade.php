@extends('layouts.app')

@section('content')
    <div class="container">
        <h3>Notifications</h3>
        @foreach($all_notifications as $notification)
            @switch($notification->action_name)
                @case("conversation_new_message")
                    @if($notification->conversation->user1_id == Auth::id())
                            <a class="notification-item" href="{{ route('conversations') }}?c={{$notification->action_id}}&r={{$notification->conversation->user2_id}}">
                                You got a new message from {{ $notification->conversation->user2->name }}
                            </a>
                    @else
                            <a class="notification-item" href="{{ route('conversations') }}?c={{$notification->action_id}}&r={{$notification->conversation->user1_id}}">
                                You got a new message from {{ $notification->conversation->user1->name }}
                            </a>
                    @endif
                @break
                @case("project_new_comment")
                <a class="notification-item" href="{{ route('project.show',['id' => $notification->comment->project->id,'slug' => $notification->comment->project->slug]) }}">
                    You have a new comment on your project.
                </a>
                @break
                @case("new_friend_request")
                <a class="notification-item" href="{{ route('profile.show',['id' => Auth::user()->profile->id,'slug' => Auth::user()->profile->slug]) }}?friends=1&request=1&notification={{$notification->id}}">
                    You have a new friend request.
                </a>
                @break
                @case("mention")
                <a class="notification-item" href="{{ route('activity.show',['id' => $notification->action_id]) }}" @if($notification->status == 'new') style="border-left:5px solid #ff6600" @endif>
                    You have been mentioned in a post.
                </a>
                @break
                @case("new_profile_post")
                <a class="notification-item" href="{{ route('profile.show',['id' => Auth::user()->profile->id,'slug' => Auth::user()->profile->slug]) }}">
                    Somebody wrote on your timeline.
                </a>
                @break
                @case('new_profile_reply')
                <a class="notification-item" href="{{ route('profile.show',['id' => $notification->profile_reply->post->user_2->profile->id,'slug' => $notification->profile_reply->post->user_2->profile->slug]) }}">
                    There's a new reply on a profile message.
                </a>
                @break
                @case('watcher_new_backer')
                <a class="notification-item" href="{{ route('project.show',['id' => $notification->action_id,'slug' => $notification->project->slug]) }}">
                    There is a new backer for "{{ str_limit($notification->project->title,30) }}"
                </a>
                @break
                @case('watcher_new_comment')
                <a class="notification-item" href="{{ route('project.show',['id' => $notification->comment->project->id,'slug' => $notification->comment->project->slug]) }}">
                    There is a new comment for "{{ str_limit($notification->comment->project->title,30) }}"
                </a>
                @break
            @endswitch
        @endforeach
    </div>
@endsection