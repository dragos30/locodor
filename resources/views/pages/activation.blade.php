@extends('layouts.app')

@section('content')
    <div class="container">
        @if($activated)
            <h1>Account activated successfully</h1>
        @else
            <h1>Failed to activate account</h1>
        @endif
    </div>
@endsection