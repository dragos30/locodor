@extends('layouts.app')

@section('content')
    <div class="back">
        <a href="{{ route('home') }}"><i class="fas fa-arrow-left"></i> Back to Locodor</a>
    </div>
    <div class="container mt-5">
        <div class="row">
            <div class="col-md-8 how-it-works-container">
                <h1 class="how-it-works-title">How it <span>works</span></h1>
                <div class="simple-line"></div>
                <div class="step-element">
                    <p><i class="fas fa-check-circle"></i> <span><a href="{{ route('register') }}">Create a Profile</a> - It's super easy and free.</span></p>
                </div>
                <div class="step-element">
                    <p class="tom-step-text"><i class="fas fa-check-circle"></i> <span>Get Support & Services from <a href="{{ route('tom') }}">Tom</a>.</span></p>
                </div>
                <div class="step-element">
                    <p><i class="fas fa-check-circle"></i> <span>Showcase your <a href="{{ route('tom') }}">Crowdfunding portfolio.</a></span></p>
                </div>
                <div class="step-element">
                    <p><i class="fas fa-check-circle"></i> <span><a href="/members">Add users</a> to your profile, make connections to grow.</span></p>
                </div>
                <div class="step-element">
                    <p><i class="fas fa-check-circle"></i> <span>Join conversations on our <a href="{{ route('activity') }}">Social Community.</a></span></p>
                </div>
                <div class="step-element">
                    <p><i class="fas fa-check-circle"></i> <span><a href="{{ route('projects') }}">Discover</a> the real status of a crowdfunding campaign from other backers.</span></p>
                </div>
                <div class="step-element">
                    <p><i class="fas fa-check-circle"></i> <span><a href="{{ route('project.create-start') }}">For Campaigners</a> - Get noticed by top pros and skyrocket your project.</span></p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="how-it-works-contact">
                    <form action="{{ route('contact') }}" method="POST">
                        @csrf
                        <h2>
                            Get a FREE consultation about your Crowdfunding Project!
                        </h2>
                        <div class="form-group">
                            <label for="name">Your Name*</label>
                            <input type="text" name="name" id="name" class="form-control">
                            @error('name')
                                    <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="email">Your Email</label>
                            <input type="text" name="email" id="email" class="form-control">
                            @error('email')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="message">Your Message:</label>
                            <textarea name="message" id="" cols="30" rows="3" class="form-control"></textarea>
                            @error('message')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <button type="submit">@if(Session::has('success')) Message sent! Thank you! @else Get Started @endif</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
