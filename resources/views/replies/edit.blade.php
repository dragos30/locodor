@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="{{ route('reply.update',['id' => $reply->id]) }}" method="post">
            {{ csrf_field() }}
            <div class="form-group">
                <textarea type="text" class="form-control" placeholder="Write your comment..." name="content">{{ $reply->content }}</textarea>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-dark">Post</button>
            </div>
        </form>
    </div>
@endsection