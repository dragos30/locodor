@extends('layouts.app')

@section('content')
    <div class="container">
        <h1 class="text-center">
            <b>{{ $discussion->title }}</b>
            @if(Auth::check() && $watching)
                <a href="{{ route('discussion.unwatch',['id' => $discussion->id]) }}" class="btn btn-outline-primary float-left">Unwatch</a>
            @else
                <a href="{{ route('discussion.watch',['id' => $discussion->id]) }}" class="btn btn-outline-primary float-left">Watch</a>
            @endif

            <span class="discussion-likes">
                @if(Auth::check() && !$liked)
                    <a href="{{ route('discussion.like',['id' => $discussion->id]) }}" class="btn btn-success float-right"><i class="fas fa-arrow-up"></i></a>
                @else
                    <a href="{{ route('discussion.unlike',['id' => $discussion->id]) }}" class="btn btn-danger float-right"><i class="fas fa-arrow-down"></i></a>
                @endif
                <span class="float-right">{{$likes->count()}} Upvotes</span>
            </span>
        </h1>

        <h4> {{ $discussion->description }} </h4>
        <p> {{ $discussion->content }} </p>

        {{--Replies section--}}

        @foreach($replies as $reply)
            <div class="alert alert-light" role="alert">
                <div class="row">
                    <div class="col-md-2 text-center">
                        <img src="{{ asset($reply->user->profile->avatar) }}" width="50" height="50"><br>
                        {{ $reply->user->name }}:
                    </div>
                    <div class="col-md-10">
                        {{ $reply->content }}
                        @if(Auth::check() && Auth::id()==$reply->user_id)
                            <a href="{{ route('reply.edit',['id' => $reply->id]) }}" class="float-right text-success reply-icons"><i class="far fa-edit"></i></a>
                            <a href="{{ route('reply.delete',['id' => $reply->id]) }}" class="float-right text-danger reply-icons"><i class="far fa-times-circle"></i></a>
                        @endif
                        @if(Auth::check())
                            <span class="reply-like">
                                @if(Auth::check() && !in_array($reply->id,$liked_replies))
                                    <a href="{{ route("reply.like",['id' => $reply->id]) }}" class="btn btn-sm btn-success float-right reply-like-btn">Like</a>
                                @else
                                    <a href="{{ route("reply.unlike",['id' => $reply->id]) }}" class="btn btn-sm btn-danger float-right reply-like-btn">Unlike</a>
                                @endif
                                <span class="reply-likes float-right">{{$reply->reply_likes->count()}}</span>
                            </span>
                        @endif
                    </div>
                </div>
            </div>
        @endforeach

        <form action="{{ route('reply.store') }}" method="post">
            {{ csrf_field() }}
            <input type="hidden" value="{{ $discussion->id }}" name="discussion_id">
            <div class="form-group">
                <textarea type="text" class="form-control" placeholder="Write your comment..." name="content"></textarea>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-dark">Post</button>
            </div>
        </form>
    </div>
@endsection