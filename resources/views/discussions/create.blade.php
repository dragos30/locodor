@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="{{ route('discussion.store',['id' => $topic_id]) }}" method="post">
            {{ csrf_field() }}

            <div class="form-group">
                <label for="title">Discussion title</label>
                <input class="form-control" type="text" name="title" placeholder="Title of the discussion">
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                <textarea class="form-control" name="description" placeholder="Short description.."></textarea>
            </div>
            <div class="form-group">
                <label for="content">Content</label>
                <textarea class="form-control" name="content" placeholder="Content of the discusssion" rows="10"></textarea>
            </div>

            <button type="submit" class="btn btn-dark">Create Topic</button>
        </form>
    </div>
@endsection