
@include('includes.simpleheader')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-2 admin-menu">
            <a href="/"><img width="200px" src='{{ asset('/uploads/logo-locodor-crowdfunding.png') }}'></a>
            <a href="{{ route('admin.dashboard') }}"><p>Main Dashboard</p></a>
            <a href="{{ route('admin.dashboard.projects') }}"><p>Projects</p></a>
            <a href="{{ route('admin.backers') }}"><p>Backers</p></a>
            <a href="{{ route('admin.syncs') }}"><p>Sync Projects</p></a>
            <a href="{{ route('projects.slider') }}"><p>Projects Slider</p></a>
            <a href="{{ route('manage.categories') }}"><p>Manage Categories</p></a>
            <a href="{{ route('admin.dashboard.posts') }}"><p>Posts</p></a>
            <a href="{{ route('admin.dashboard.users.table') }}"><p>Users table</p></a>
            <a href="{{ route('admin.dashboard.user.search') }}"><p>User Search</p></a>
            <a href="{{ route('export.emails') }}"><p>Export Emails</p></a>
            {{--<a href="{{ route('help.requests') }}"><p>Help Requests</p></a>--}}
            <a href="{{ route('emails.unread.notifications') }}"><p>Unread Notifications Email</p></a>
            <a href="{{ route('ranks') }}"><p>Ranks</p></a>
            <a href="{{ route('meta.tags') }}"><p>Meta Tags</p></a>
            <a href="{{ route('locodor.url') }}"><p>Locodor Import</p></a>
            <a href="{{ route('project.url') }}"><p>Crowdfunding Import</p></a>
            <a href="{{ route('announcement') }}"><p>Announcement</p></a>
            <a href="{{ route('email') }}"><p>Send Mails</p></a>
            <a href="{{ route('set.bots.avatars') }}"><p>Set Bots Avatars</p></a>
            <a href="{{ route('bots.auto.pledge.form') }}"><p>Bots Auto Pledge</p></a>
            <a href="{{ route('run.once') }}"><p style="color:red"> Run Script ( DANGER ) </p></a>
        </div>
        <div class="col-md-10 admin-content">
            @yield('content')
        </div>
    </div>
</div>
</main>
@include('includes.simplefooter')

