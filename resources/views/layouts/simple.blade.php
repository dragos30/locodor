@include('includes.simpleheader')
<div class="container">
    {{--@foreach($errors->all() as $error)--}}
    {{--<div class="notice notice-danger">--}}
    {{--<strong>{{ $error }}</strong>--}}
    {{--</div>--}}
    {{--@endforeach--}}
</div>

<main class="py-4">
    @yield('content')
</main>
@include('includes.simplefooter')

